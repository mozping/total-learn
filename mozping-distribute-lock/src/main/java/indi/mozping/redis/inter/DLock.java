package indi.mozping.mozping.redis.inter;

import indi.mozping.mozping.redis.enums.LockConstant;
import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @author by mozping
 * @Classname DLock
 * @Description TODO
 * @Date 2019/9/6 11:26
 */
public interface DLock {

    RLock lock(String lockKey);

    RLock lock(String lockKey, int timeout);

    RLock lock(String lockKey, TimeUnit unit, int timeout);

    boolean tryLock(String lockKey, TimeUnit unit, LockConstant lockTime);

    boolean fairLock(String lockKey, TimeUnit unit, LockConstant lockTime);

    void unlock(String lockKey);

    void unlock(RLock lock);

}
