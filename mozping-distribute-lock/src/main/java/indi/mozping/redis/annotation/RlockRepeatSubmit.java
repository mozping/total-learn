package indi.mozping.mozping.redis.annotation;


import indi.mozping.mozping.redis.enums.LockConstant;

import java.lang.annotation.*;

/**
 * @author by mozping
 * @Classname RlockRepeatSubmit
 * @Description 防止重复提交的注解
 * @Date 2019/9/6 11:28
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface RlockRepeatSubmit {

    /**
     * 分布式锁枚举类
     *
     * @return
     */
    LockConstant lockConstant();

}
