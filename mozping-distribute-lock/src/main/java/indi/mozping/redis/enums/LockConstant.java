package indi.mozping.mozping.redis.enums;


/**
 * @author by mozping
 * @Classname LockConstant
 * @Description 分布式锁枚举类
 * @Date 2019/9/6 11:29
 */
public enum LockConstant {

    CASHIER("cashierLock:", 3, 300, "请勿重复点击收银操作！！"), // 收银锁
    SUBMIT_ORDER("submitOrderLock:", 3, 30, "请勿重复点击下单！！"), // 下单锁
    COMMON_LOCK("commonLock:", 3, 120, "请勿重复点击");// 通用锁常量

    private String keyPrefix; // 分布式锁前缀
    private int waitTime;// 等到最大时间，强制获取锁
    private int leaseTime;// 锁失效时间
    private String message;// 加锁提示

    // 构造方法
    private LockConstant(String keyPrefix, int waitTime, int leaseTime, String message) {
        this.keyPrefix = keyPrefix;
        this.waitTime = waitTime;
        this.leaseTime = leaseTime;
        this.message = message;
    }

    // 省略getter,setter


    public String getKeyPrefix() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    public int getLeaseTime() {
        return leaseTime;
    }

    public void setLeaseTime(int leaseTime) {
        this.leaseTime = leaseTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
