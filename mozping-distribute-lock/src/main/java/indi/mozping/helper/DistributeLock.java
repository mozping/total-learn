package indi.mozping.helper;

/**
 * @author by mozping
 * @Classname DistributeLock
 * @Description 分布式锁接口
 * @Date 2019/11/13 16:52
 */
public interface DistributeLock {

    /**
     * @param key   加锁 key
     * @param value 加锁 value
     * @Description: 加锁方法，一直阻塞尝试加锁直到成功
     * @date 2019/11/13 16:10
     * @author by mozping
     */
    boolean lock(String key, String value);

    /**
     * @param key        加锁 key
     * @param value      加锁 value
     * @param exireptime 锁的过期时间，单位毫秒
     * @Description: 加锁方法，一直阻塞尝试加锁直到成功
     * @date 2019/11/13 16:10
     * @author by mozping
     */
    boolean lock(String key, String value, long exireptime);

    /**
     * @param key       加锁 key
     * @param value     加锁 value
     * @param blockTime 加锁的阻塞时间,如果blockTime时间内加锁未成功，就放弃加锁尝试,单位毫秒
     * @Description: 加锁方法，加锁阻塞一定时间
     * @date 2019/11/13 15:10
     * @author by mozping
     */
    boolean lockTimeout(String key, String value, long blockTime);

    /**
     * @param key        加锁 key
     * @param value      加锁 value
     * @param expiretime 锁的过期时间，单位毫秒
     * @param blockTime  加锁的阻塞时间,如果blockTime时间内加锁未成功，就放弃加锁尝试,单位毫秒
     * @Description: 加锁方法，加锁阻塞一定时间
     * @date 2019/11/13 15:10
     * @author by mozping
     */
    boolean lockTimeout(String key, String value, long expiretime, long blockTime);

    /**
     * @param key   加锁 key
     * @param value 加锁 value
     * @Description: 尝试加锁方法
     * @date 2019/11/13 15:10
     * @author by mozping
     */
    boolean tryLock(String key, String value);

    /**
     * @param key        加锁 key
     * @param value      加锁 value
     * @param expiretime 锁的过期时间，单位毫秒
     * @Description: 尝试加锁方法
     * @date 2019/11/13 15:10
     * @author by mozping
     */
    boolean tryLock(String key, String value, long expiretime);

    /**
     * @param key   解锁 key
     * @param value 解锁 value
     * @Description: 解锁方法，只有当key和value匹配时，才会解锁
     * @date 2019/11/13 15:10
     * @author by mozping
     */
    boolean unLock(String key, String value);

}
