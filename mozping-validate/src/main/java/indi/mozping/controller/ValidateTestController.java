package indi.mozping.controller;

import indi.mozping.bean.UserDTO;
import indi.mozping.bean.ValidateBean;
import indi.mozping.exception.BizException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

/**
 * @author by mozping
 * @Classname ValidateTestController
 * @Description TODO
 * @Date 2020/9/17 15:30
 */
@RestController
public class ValidateTestController {

    private static final Logger log = LoggerFactory.getLogger(ValidateTestController.class);

    @GetMapping("/hello")
    @ResponseBody
    public String test(@RequestBody @Validated ValidateBean validateBean) {
        long begin = System.currentTimeMillis();
        log.info("Process query TodayHistoryBean end... {},consume time is:{}", 1, 1);
        return "123";
    }


    @PostMapping("/user")
    @ResponseBody
    public String test(@RequestBody @Validated UserDTO userDTO) throws BizException {
        long begin = System.currentTimeMillis();
        Random random = new Random();
        log.info("入参：" + userDTO);
        int code = random.nextInt();
        if (random.nextBoolean()) {
            throw new BizException(code, code + " error");
        }
        String ss = "1";
        ss.isEmpty();
        log.info("Process test end... " + code);
        return userDTO.toString();
    }
}