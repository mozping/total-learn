package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 参考：https://mp.weixin.qq.com/s/1FFkwZo_-yHNvjsRJSQkZA
 */
@SpringBootApplication
public class MozpingValidateApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingValidateApplication.class, args);
    }

}
