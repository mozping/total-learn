package indi.mozping.bean;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author by mozping
 * @Classname ValidateBean
 * @Description TODO
 * @Date 2020/9/17 15:35
 */
@Data
public class ValidateBean {

    @NotNull(message = "notnull不能为空")
    private Long notnull;


}