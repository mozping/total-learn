package indi.mozping.bean;

import lombok.Data;

/**
 * @author by mozping
 * @Classname RspDTO
 * @Description TODO
 * @Date 2020/9/17 18:33
 */
@Data
public class RspDTO {

    int code;
    String message;

    public RspDTO(int code, String message) {
        this.code = code;
        this.message = message;
    }
}