package indi.mozping.bean;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author by mozping
 * @Classname UserDTO
 * @Description TODO
 * @Date 2020/9/17 16:22
 */
@Data
public class UserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /*** 用户ID*/
    @NotNull(message = "用户id不能为空")
    private Long userId;

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    @Length(max = 20, message = "用户名不能超过20个字符")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9\\*]*$", message = "用户昵称限制：最多20字符，包含文字、字母和数字")
    private String username;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String mobile;

    /**
     * 性别
     */
    private String sex;

    /**
     * 邮箱
     */
    @NotBlank(message = "联系邮箱不能为空")
    @Email(message = "邮箱格式不对")
    private String email;

    /**
     * 密码
     */
    private String password;

    /*** 其他字段 */

    /*** 限制时间范围 */
    @Future(message = "时间必须是将来时间")
    private Date futureTime;
    @Past(message = "时间必须是过去时间")
    private Date pastTime;


    /*** 不为null */
    @NotNull(message = "notNull不能为空")
    private String notNull;

    /*** 限制最小值，最小值不能小于value(可以等于)，可以限制int，long，short，BigDecimal，BigInteger等类型 */
    @Min(value = 10, message = "min最小值为10")
    private int min;

    /*** 限制最大值，最小值不能大于value(可以等于)，可以限制int，long，short，BigDecimal，BigInteger等类型 */
    @Max(value = 100, message = "max最大值为100")
    private int max;

    /*** 限制最整数和小数的位数上限 */
    @Digits(integer = 1, fraction = 1, message = "数字超过允许范围")
    private double floatVal;

    /*** 限制字符串和集合的大小上下限*/
    @Size(min = 2, max = 5)
    private String str;


    /*** 必须为true */
    @AssertTrue(message = "mustTrue必须为true")
    private boolean mustTrue;

    /*** 必须为false,注意不传此参数默认就是false */
    @AssertFalse(message = "mustFalse必须为false")
    private boolean mustFalse;


}