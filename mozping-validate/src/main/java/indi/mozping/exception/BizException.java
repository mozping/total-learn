package indi.mozping.exception;

import lombok.Data;

/**
 * @author by mozping
 * @Classname BizException
 * @Description TODO
 * @Date 2020/9/17 18:35
 */
@Data
public class BizException extends Exception {

    int code;
    String message;

    public BizException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}