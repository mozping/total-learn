+ validation-api是一套标准，hibernate-validator实现了此标准，pom依赖引入了这两个jar
+ 参考：https://m.imooc.com/article/292766

@NotNull：不能为null，适用于所有类型；比如字符串可以是："" 
@NotEmpty：长度不能为0，适用于字符串和集合类型，比如：""不行，但是" "是可以的，内部通过isEmpty()方法来判断
@NotBlank: 不能为空，空格会被忽略，使用与字符串；比如：""和" "是不行的，也就是说会去除首位的空格(trim)