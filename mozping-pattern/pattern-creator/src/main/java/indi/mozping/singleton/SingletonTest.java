package indi.mozping.singleton;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * @author by mozping
 * @Classname SingletonTest
 * @Description TODO
 * @Date 2020/8/24 15:56
 */
public class SingletonTest {

    public static Set<Object> set = new HashSet<Object>();

    static int threadNum = 30;
    static CountDownLatch countDownLatch = new CountDownLatch(threadNum);

    public static void main(String[] args) throws InterruptedException {

        final Thread[] mts = new Thread[threadNum];
        for (int i = 0; i < mts.length; i++) {
            mts[i] = new Thread(new Runnable() {
                public void run() {
                    for (int i = 0; i < 100000000; i++) {
                        SingletonDcl instance = SingletonDcl.getInstance();
                        set.add(instance);
                        int hashCode = instance.hashCode();
//                        System.out.println(hashCode);
                    }
                    countDownLatch.countDown();
                }
            });
        }

        for (int j = 0; j < mts.length; j++) {
            mts[j].start();
        }

        countDownLatch.await();

        for (Object o : set) {
            System.out.println(o.hashCode());
        }

    }
}