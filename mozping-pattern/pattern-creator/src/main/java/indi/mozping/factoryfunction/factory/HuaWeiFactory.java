package indi.mozping.factoryfunction.factory;

import indi.mozping.factoryfunction.producte.HuaWeiPhone;
import indi.mozping.factoryfunction.producte.Phone;

public class HuaWeiFactory implements Factory {
    @Override
    public Phone createPhone() {
        return new HuaWeiPhone();
    }
}
