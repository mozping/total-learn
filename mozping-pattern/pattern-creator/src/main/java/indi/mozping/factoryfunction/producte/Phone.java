package indi.mozping.factoryfunction.producte;

/**
 * 代表产品抽象接口
 * 手机抽象产品，具备电话和短信功能
 */
public abstract class Phone {

    public abstract void call();

    public abstract void sendMsg();
}
