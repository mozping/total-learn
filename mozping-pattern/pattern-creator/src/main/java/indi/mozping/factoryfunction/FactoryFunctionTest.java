package indi.mozping.factoryfunction;

import indi.mozping.factoryfunction.factory.HuaWeiFactory;
import indi.mozping.factoryfunction.factory.VivoFactory;
import indi.mozping.factoryfunction.factory.Factory;
import indi.mozping.factoryfunction.producte.Phone;

import java.util.Random;

/**
 * 测试类
 */
public class FactoryFunctionTest {
    public static void main(String[] args) {
        Factory factory;
        if (new Random().nextBoolean()) {
            factory = new HuaWeiFactory();
        } else {
            factory = new VivoFactory();
        }
        Phone phone = factory.createPhone();
        phone.call();
        phone.sendMsg();
    }
}
