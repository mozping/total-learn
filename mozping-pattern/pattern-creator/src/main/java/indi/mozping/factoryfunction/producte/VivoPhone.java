package indi.mozping.factoryfunction.producte;

public class VivoPhone extends Phone {
    @Override
    public void call() {
        System.out.println("Vivo phone call...");
    }

    @Override
    public void sendMsg() {

        System.out.println("Vivo phone send message...");
    }
}
