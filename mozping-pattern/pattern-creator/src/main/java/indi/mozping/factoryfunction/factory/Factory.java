package indi.mozping.factoryfunction.factory;

import indi.mozping.factoryfunction.producte.Phone;

/**
 * 抽象工厂角色
 */
public interface Factory {

    Phone createPhone();
}
