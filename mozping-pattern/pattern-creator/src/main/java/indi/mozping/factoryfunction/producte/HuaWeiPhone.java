package indi.mozping.factoryfunction.producte;

public class HuaWeiPhone extends Phone {
    @Override
    public void call() {
        System.out.println("HuaWei phone call...");
    }

    @Override
    public void sendMsg() {
        System.out.println("HuaWei phone send message...");
    }
}
