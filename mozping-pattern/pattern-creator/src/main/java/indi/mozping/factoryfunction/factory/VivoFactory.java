package indi.mozping.factoryfunction.factory;

import indi.mozping.factoryfunction.producte.VivoPhone;
import indi.mozping.factoryfunction.producte.Phone;

public class VivoFactory implements Factory {
    @Override
    public Phone createPhone() {
        return new VivoPhone();
    }
}
