package indi.mozping.abstractfactory.producte;

/**
 * */
public abstract class VivoPhone {

    public abstract void call();

    public abstract void sendMsg();
}
