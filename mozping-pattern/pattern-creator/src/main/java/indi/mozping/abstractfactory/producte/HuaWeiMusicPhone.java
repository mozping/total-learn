package indi.mozping.abstractfactory.producte;

public class HuaWeiMusicPhone extends HuaWeiPhone {
    @Override
    public void call() {
        System.out.println("HuaWeiMusic Phone  call...");
    }

    @Override
    public void sendMsg() {
        System.out.println("HuaWeiMusic Phone send message...");
    }
}
