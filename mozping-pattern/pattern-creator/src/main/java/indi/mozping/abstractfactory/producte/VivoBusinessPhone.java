package indi.mozping.abstractfactory.producte;

public class VivoBusinessPhone extends VivoPhone {
    @Override
    public void call() {
        System.out.println("Vivo Business Phone call...");
    }

    @Override
    public void sendMsg() {

        System.out.println("Vivo Business Phone send message...");
    }
}
