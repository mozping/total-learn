package indi.mozping.abstractfactory;

import indi.mozping.abstractfactory.factory.BusinessPhoneFactory;
import indi.mozping.abstractfactory.factory.Factory;
import indi.mozping.abstractfactory.factory.MusicPhoneFactory;
import indi.mozping.abstractfactory.producte.HuaWeiPhone;
import indi.mozping.abstractfactory.producte.VivoPhone;

import java.util.Random;

/**
 * 测试类
 */
public class AbstructFactoryTest {
    public static void main(String[] args) {
        Factory factory;
        if (new Random().nextBoolean()) {
            factory = new MusicPhoneFactory();
        } else {
            factory = new BusinessPhoneFactory();
        }

        VivoPhone vivoPhone = factory.createVivoPhone();
        HuaWeiPhone huaWeiPhone = factory.creatHuaWeiPhone();
        vivoPhone.call();
        vivoPhone.sendMsg();
        huaWeiPhone.call();
        huaWeiPhone.sendMsg();

    }
}
