package indi.mozping.abstractfactory.producte;

public class VivoMusicPhone extends VivoPhone {
    @Override
    public void call() {
        System.out.println("VivoMusic phone call...");
    }

    @Override
    public void sendMsg() {

        System.out.println("VivoMusic phone send message...");
    }
}
