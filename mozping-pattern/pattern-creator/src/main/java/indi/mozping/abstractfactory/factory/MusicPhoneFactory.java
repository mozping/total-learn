package indi.mozping.abstractfactory.factory;

import indi.mozping.abstractfactory.producte.HuaWeiMusicPhone;
import indi.mozping.abstractfactory.producte.VivoMusicPhone;
import indi.mozping.abstractfactory.producte.HuaWeiPhone;
import indi.mozping.abstractfactory.producte.VivoPhone;

public class MusicPhoneFactory implements Factory {

    @Override
    public HuaWeiPhone creatHuaWeiPhone() {
        return new HuaWeiMusicPhone();
    }

    @Override
    public VivoPhone createVivoPhone() {
        return new VivoMusicPhone();
    }
}
