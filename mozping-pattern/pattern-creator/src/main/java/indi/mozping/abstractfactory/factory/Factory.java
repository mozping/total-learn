package indi.mozping.abstractfactory.factory;

import indi.mozping.abstractfactory.producte.HuaWeiPhone;
import indi.mozping.abstractfactory.producte.VivoPhone;

/**
 * 抽象工厂角色
 */
public interface Factory {

    HuaWeiPhone creatHuaWeiPhone();

    VivoPhone createVivoPhone();
}
