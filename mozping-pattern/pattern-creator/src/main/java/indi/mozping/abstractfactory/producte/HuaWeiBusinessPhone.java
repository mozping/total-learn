package indi.mozping.abstractfactory.producte;

public class HuaWeiBusinessPhone extends HuaWeiPhone {
    @Override
    public void call() {
        System.out.println("HuaWei Business  phone call...");
    }

    @Override
    public void sendMsg() {
        System.out.println("HuaWei business phone send message...");
    }
}
