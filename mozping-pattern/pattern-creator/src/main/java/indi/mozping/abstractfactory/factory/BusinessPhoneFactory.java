package indi.mozping.abstractfactory.factory;

import indi.mozping.abstractfactory.producte.VivoBusinessPhone;
import indi.mozping.abstractfactory.producte.HuaWeiBusinessPhone;
import indi.mozping.abstractfactory.producte.HuaWeiPhone;
import indi.mozping.abstractfactory.producte.VivoPhone;

public class BusinessPhoneFactory implements Factory {

    @Override
    public HuaWeiPhone creatHuaWeiPhone() {
        return new HuaWeiBusinessPhone();
    }

    @Override
    public VivoPhone createVivoPhone() {
        return new VivoBusinessPhone();
    }
}
