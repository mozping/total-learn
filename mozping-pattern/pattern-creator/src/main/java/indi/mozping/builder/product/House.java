package indi.mozping.builder.product;

/**
 * @author by mozping
 * @Classname House
 * @Description 需要创建的具体产品
 * @Date 2019/5/20 11:05
 */
public class House {

    private String type; //类型
    private String bedroom; //卧室
    private String diningRoom; //餐厅
    private String livingRoom; //客厅
    private String balcony; //阳台
    private String washRoom; //洗漱间

    @Override
    public String toString() {
        return "House{" +
                "carType='" + type + '\'' +
                ", bedroom='" + bedroom + '\'' +
                ", diningRoom='" + diningRoom + '\'' +
                ", livingRoom='" + livingRoom + '\'' +
                ", balcony='" + balcony + '\'' +
                ", washRoom='" + washRoom + '\'' +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBedroom() {
        return bedroom;
    }

    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }

    public String getDiningRoom() {
        return diningRoom;
    }

    public void setDiningRoom(String diningRoom) {
        this.diningRoom = diningRoom;
    }

    public String getLivingRoom() {
        return livingRoom;
    }

    public void setLivingRoom(String livingRoom) {
        this.livingRoom = livingRoom;
    }

    public String getBalcony() {
        return balcony;
    }

    public void setBalcony(String balcony) {
        this.balcony = balcony;
    }

    public String getWashRoom() {
        return washRoom;
    }

    public void setWashRoom(String washRoom) {
        this.washRoom = washRoom;
    }
}