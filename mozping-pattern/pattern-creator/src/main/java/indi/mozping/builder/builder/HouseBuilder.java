package indi.mozping.builder.builder;

import indi.mozping.builder.product.House;

/**
 * @author by mozping
 * @Classname GoodProductBuilder
 * @Description 建造器类
 * @Date 2019/5/20 10:52
 */
public class HouseBuilder {

    private House house;

    public HouseBuilder() {
        this.house = new House();
    }

    public HouseBuilder type(String type) {
        house.setType(type);
        return this;
    }

    public HouseBuilder bedroom(String bedroom) {
        house.setBedroom(bedroom);
        return this;
    }

    public HouseBuilder diningRoom(String diningRoom) {
        house.setDiningRoom(diningRoom);
        return this;
    }


    public HouseBuilder livingRoom(String livingRoom) {
        house.setLivingRoom(livingRoom);
        return this;
    }

    public HouseBuilder balcony(String balcony) {
        house.setBalcony(balcony);
        return this;
    }

    public HouseBuilder washRoom(String washRoom) {
        house.setWashRoom(washRoom);
        return this;
    }

    public House build() {
        return house;
    }
}