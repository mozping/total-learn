package indi.mozping.builder;

import indi.mozping.builder.builder.HouseBuilder;
import indi.mozping.builder.product.House;

/**
 * @author by mozping
 * @Classname BuilderTest
 * @Description 建造者模式测试
 * @Date 2019/5/20 11:25
 */
public class BuilderTest {

    public static void main(String[] args) {
        HouseBuilder builder = new HouseBuilder();
        House bigHouse = builder.type("商品房").bedroom("3个卧室")
                .livingRoom("大客厅").diningRoom("大餐厅")
                .balcony("大阳台").washRoom("双卫").build();
        System.out.println(bigHouse);

        House smallHouse = builder.type("公寓").bedroom("2个卧室")
                .livingRoom("小客厅").diningRoom("小餐厅")
                .balcony("小阳台").washRoom("单卫").build();
        System.out.println(smallHouse);
    }
}