package indi.mozping.proxy.itf;

/**
 * @author mozping
 */
public interface Buy {
    /**
     * 定义接口
     */
    void buySomeThing(String string);
}
