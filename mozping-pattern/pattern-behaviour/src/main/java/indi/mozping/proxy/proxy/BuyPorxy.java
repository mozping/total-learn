package indi.mozping.proxy.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author intellif
 */
public class BuyPorxy implements InvocationHandler {

    private Object target;

    public BuyPorxy(Object obj) {
        target = obj;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.toString().contains("buy")) {
            System.out.println("出国购物...");
            method.invoke(target, args);
            System.out.println("代购完成...");
        } else if (method.toString().contains("visit")) {
            System.out.println("代理访问" + args[0]);
            System.out.println("访问" + args[0] + "完成...");
        }
        return null;
    }
}
