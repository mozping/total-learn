package indi.mozping.proxy;

import indi.mozping.proxy.impl.Person;
import indi.mozping.proxy.itf.Buy;
import indi.mozping.proxy.itf.Visit;
import indi.mozping.proxy.proxy.BuyPorxy;
import indi.mozping.proxy.utils.ProxyGeneratorUtils;

import java.lang.reflect.Proxy;

/**
 * @author intellif
 */
public class BuyProxyTest {

    public static void main(String[] args) {
        Person xiaoming = new Person();
        BuyPorxy buyPorxy = new BuyPorxy(xiaoming);
        Buy p = (Buy) Proxy.newProxyInstance(xiaoming.getClass().getClassLoader(), xiaoming.getClass().getInterfaces(), buyPorxy);
        p.buySomeThing("毛大衣");
        Visit v = (Visit) Proxy.newProxyInstance(xiaoming.getClass().getClassLoader(), xiaoming.getClass().getInterfaces(), buyPorxy);
        v.visitSomeWhere("百度");
        ProxyGeneratorUtils.writeProxyClassToHardDisk("F:/$Proxy11.class");
    }
}
