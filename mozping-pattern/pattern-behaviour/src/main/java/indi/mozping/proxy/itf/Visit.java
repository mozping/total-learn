package indi.mozping.proxy.itf;

public interface Visit {

    /**
     * 定义接口
     */
    void visitSomeWhere(String string);
}
