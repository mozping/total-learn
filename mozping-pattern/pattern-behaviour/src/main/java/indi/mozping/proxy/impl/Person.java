package indi.mozping.proxy.impl;


import indi.mozping.proxy.itf.Buy;
import indi.mozping.proxy.itf.Visit;

/**
 * @author intellif
 */
public class Person implements Buy, Visit {
    public void buySomeThing(String str) {
        System.out.println("小明买衣服...:" + str);
    }

    public void visitSomeWhere(String string) {
        System.out.println("小明访问...:" + string);
    }
}
