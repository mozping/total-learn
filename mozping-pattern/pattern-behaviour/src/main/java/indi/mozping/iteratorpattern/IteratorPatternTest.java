package indi.mozping.iteratorpattern;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * @author by mozping
 * @Classname IteratorPatternTest
 * @Description TODO
 * @Date 2019/6/26 14:55
 */
public class IteratorPatternTest {
    static Random random = new Random();

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            list.add(random.nextInt(100));
        }

        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            Integer result = iterator.next();
            System.out.print(result + " ");
            //list.add(100);  --CommentA
            //list.remove(5); --CommentB
            if (result > 50 && result < 70) {
                iterator.remove();
            }
        }
        System.out.println();
        Iterator<Integer> iterator1 = list.iterator();
        while (iterator1.hasNext()) {
            System.out.print(iterator1.next() + " ");
        }
    }
}