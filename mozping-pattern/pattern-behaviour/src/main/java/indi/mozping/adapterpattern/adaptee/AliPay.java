package indi.mozping.adapterpattern.adaptee;

/**
 * @author by mozping
 * @Classname AliPay
 * @Description TODO
 * @Date 2019/6/24 16:09
 */
public class AliPay {

    public void aliPay(int money) {
        System.out.println("aliPay pay...  " + money);
    }
}