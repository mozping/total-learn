package indi.mozping.adapterpattern.adaptee;

/**
 * @author by mozping
 * @Classname WeChatPay
 * @Description TODO
 * @Date 2019/6/24 16:09
 */
public class WeChatPay {

    public void weChatPay(int money) {
        System.out.println("weChatPay pay...  " + money);
    }

}