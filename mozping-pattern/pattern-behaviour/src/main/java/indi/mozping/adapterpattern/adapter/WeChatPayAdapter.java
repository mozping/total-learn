package indi.mozping.adapterpattern.adapter;

import indi.mozping.adapterpattern.adaptee.WeChatPay;
import indi.mozping.adapterpattern.target.MyPay;

/**
 * @author by mozping
 * @Classname AliPayAdapter
 * @Description TODO
 * @Date 2019/6/24 16:08
 */
public class WeChatPayAdapter extends WeChatPay implements MyPay {

    private WeChatPay weChatPay;

    public WeChatPayAdapter() {
        this.weChatPay = new WeChatPay();
    }

    @Override
    public void pay(int money) {
        System.out.println("调用第三方支付接口...");
        weChatPay.weChatPay(money);
    }
}