package indi.mozping.adapterpattern.adapter;

import indi.mozping.adapterpattern.adaptee.AliPay;
import indi.mozping.adapterpattern.target.MyPay;

/**
 * @author by mozping
 * @Classname AliPayAdapter
 * @Description TODO
 * @Date 2019/6/24 16:08
 */
public class AliPayAdapter extends AliPay implements MyPay {

    private AliPay aliPay;

    public AliPayAdapter() {
        this.aliPay = new AliPay();
    }

    @Override
    public void pay(int money) {
        System.out.println("调用第三方支付接口...");
        aliPay.aliPay(money);
    }
}