package indi.mozping.adapterpattern;

import indi.mozping.adapterpattern.adapter.AliPayAdapter;
import indi.mozping.adapterpattern.adapter.WeChatPayAdapter;
import indi.mozping.adapterpattern.target.MyPay;

/**
 * @author by mozping
 * @Classname AdapterTest
 * @Description TODO
 * @Date 2019/6/24 16:21
 */
public class AdapterTest {

    public static void main(String[] args) {
        MyPay myPay = new AliPayAdapter();
        myPay.pay(100);
        MyPay myPay1 = new WeChatPayAdapter();
        myPay1.pay(100);
    }
}