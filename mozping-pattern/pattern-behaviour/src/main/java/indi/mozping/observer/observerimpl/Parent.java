package indi.mozping.observer.observerimpl;

/**
 * @author by mozping
 * @Classname Parent
 * @Description TODO
 * @Date 2019/6/26 10:55
 */
public class Parent implements Observer {

    @Override
    public void getNotic() {

        System.out.println("家长收到通知，安排假期活动...");
    }
}