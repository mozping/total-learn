package indi.mozping.observer.subject;

import indi.mozping.observer.observerimpl.Observer;

/**
 * @author by mozping
 * @Classname Notice
 * @Description TODO
 * @Date 2019/6/26 10:54
 */
public interface Notice {

    void notice();

    void addObserver(Observer observer);
}
