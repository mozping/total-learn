package indi.mozping.observer.observerimpl;

/**
 * @author by mozping
 * @Classname Student
 * @Description TODO
 * @Date 2019/6/26 10:55
 */
public class Student implements Observer {

    @Override
    public void getNotic() {
        System.out.println("学生收到通知，很高兴...");
    }
}