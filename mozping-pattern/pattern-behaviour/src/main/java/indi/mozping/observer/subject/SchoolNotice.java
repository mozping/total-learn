package indi.mozping.observer.subject;

import indi.mozping.observer.observerimpl.Observer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author by mozping
 * @Classname SchoolNotice
 * @Description TODO
 * @Date 2019/6/26 10:57
 */
public class SchoolNotice implements Notice {

    ArrayList<Observer> observers = new ArrayList();

    @Override
    public void notice() {
        for (Observer observer : observers) {
            observer.getNotic();
        }
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public List<Observer> getObservers() {
        if (observers != null) {
            return observers;
        }
        return Collections.EMPTY_LIST;
    }

    public boolean removeObserver(Observer observer) {
        if (observers != null && observers.contains(observer)) {
            return observers.remove(observer);
        }
        return false;
    }

}