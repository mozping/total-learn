package indi.mozping.observer.observerimpl;

/**
 * @author by mozping
 * @Classname Student
 * @Description TODO
 * @Date 2019/6/26 10:55
 */
public class Teacher implements Observer {

    @Override
    public void getNotic() {
        System.out.println("老师收到通知，可以休息一阵子...");
    }
}