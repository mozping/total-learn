package indi.mozping.observer;

import indi.mozping.observer.observerimpl.Observer;
import indi.mozping.observer.observerimpl.Parent;
import indi.mozping.observer.observerimpl.Student;
import indi.mozping.observer.observerimpl.Teacher;
import indi.mozping.observer.subject.Notice;
import indi.mozping.observer.subject.SchoolNotice;

/**
 * @author by mozping
 * @Classname ObserverTest
 * @Description TODO
 * @Date 2019/6/26 10:59
 */
public class ObserverTest {
    public static void main(String[] args) {
        Observer student = new Student();
        Observer teacher = new Teacher();
        Observer parent = new Parent();

        Notice schoolNotice = new SchoolNotice();
        schoolNotice.addObserver(student);
        schoolNotice.addObserver(teacher);
        schoolNotice.addObserver(parent);

        schoolNotice.notice();
    }
}