package indi.mozping.decorator.decorators;

import indi.mozping.decorator.Player;

/**
 * @author by mozping
 * @Classname VipPlayer
 * @Description TODO
 * @Date 2019/6/24 17:01
 */
public class VipPlayer implements Player {

    private final Player delegate;

    public VipPlayer(Player delegate) {
        this.delegate = delegate;
    }

    private long loginTime;

    @Override
    public void login() {
        delegate.login();
        loginTime = System.currentTimeMillis();
        System.out.println("VipPlayer 登陆后增加5个积分...");
    }

    @Override
    public void play(String gameName) {
        delegate.play(gameName);
    }

    @Override
    public void loginOut() {
        if (System.currentTimeMillis() - loginTime > 200) {
            System.out.println("VipPlayer 在线时长达标，赠送5个积分...");
        }
        delegate.loginOut();
    }
}