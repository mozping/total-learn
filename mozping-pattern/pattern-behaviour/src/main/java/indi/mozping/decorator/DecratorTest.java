package indi.mozping.decorator;

import indi.mozping.decorator.decorators.BlackDiamondPlayer;
import indi.mozping.decorator.decorators.BlueDiamondPlayer;
import indi.mozping.decorator.decorators.VipPlayer;
import indi.mozping.decorator.impl.NormalPlayer;

/**
 * @author by mozping
 * @Classname DecratorTest
 * @Description TODO
 * @Date 2019/6/24 17:41
 */
public class DecratorTest {

    public static void main(String[] args) throws InterruptedException {

        System.out.println("普通玩家:");
        Player player = new NormalPlayer();
        player.login();
        player.play("英雄联盟");
        player.loginOut();

        System.out.println("\n" + "Vip玩家:");
        Player vipPlayer = new VipPlayer(player);
        vipPlayer.login();
        vipPlayer.play("DNF");
        Thread.sleep(300);
        vipPlayer.loginOut();

        System.out.println("\n" + "蓝钻玩家:");
        Player blueDiamondPlayer = new BlueDiamondPlayer(player);
        blueDiamondPlayer.login();
        blueDiamondPlayer.play("QQMusic");
        blueDiamondPlayer.loginOut();

        System.out.println("\n" + "黑钻玩家:");
        Player blackDiamondPlayer = new BlackDiamondPlayer(player);
        blackDiamondPlayer.login();
        blackDiamondPlayer.play("DNF");
        blackDiamondPlayer.loginOut();

        System.out.println("\n" + "黑钻Vip玩家:");
        Player blackVip = new BlackDiamondPlayer(new VipPlayer(new NormalPlayer()));
        blackVip.login();
        blackVip.play("DNF");
        Thread.sleep(300);
        blackVip.loginOut();


        System.out.println("\n" + "超级玩家:");
        Player superPlayer = new BlueDiamondPlayer(new BlackDiamondPlayer(new VipPlayer(new NormalPlayer())));
        superPlayer.login();
        superPlayer.play("DNF");
        superPlayer.play("QQMusic");
        Thread.sleep(300);
        superPlayer.loginOut();
    }
}