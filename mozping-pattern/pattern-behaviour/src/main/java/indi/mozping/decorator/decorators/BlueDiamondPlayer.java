package indi.mozping.decorator.decorators;

import indi.mozping.decorator.Player;

/**
 * @author by mozping
 * @Classname BlackDiamondPlayer
 * @Description TODO
 * @Date 2019/6/24 17:01
 */
public class BlueDiamondPlayer implements Player {

    private final Player delegate;

    public BlueDiamondPlayer(Player delegate) {
        this.delegate = delegate;
    }

    private long loginTime;

    @Override
    public void login() {
        delegate.login();
    }

    @Override
    public void play(String gameName) {
        delegate.play(gameName);
        if ("QQMusic".equals(gameName)) {
            System.out.println("玩QQ音乐免费听歌....");
        }
    }

    @Override
    public void loginOut() {
        delegate.loginOut();
    }
}