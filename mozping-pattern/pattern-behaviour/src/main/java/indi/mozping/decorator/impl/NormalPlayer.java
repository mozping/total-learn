package indi.mozping.decorator.impl;

import indi.mozping.decorator.Player;

/**
 * @author by mozping
 * @Classname NormalPlayer
 * @Description TODO
 * @Date 2019/6/24 16:59
 */
public class NormalPlayer implements Player {
    @Override
    public void login() {
        System.out.println("登陆...");
    }

    @Override
    public void play(String gameName) {
        System.out.println("玩: " + gameName);
    }


    @Override
    public void loginOut() {
        System.out.println("退出...");
    }
}