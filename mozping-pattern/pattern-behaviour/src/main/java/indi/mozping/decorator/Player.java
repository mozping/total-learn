package indi.mozping.decorator;

/**
 * @author by mozping
 * @Classname Player
 * @Description TODO
 * @Date 2019/6/24 16:55
 */
public interface Player {

    void login();

    void play(String gameName);


    void loginOut();
}