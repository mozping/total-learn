package indi.mozping.strategypattern.strategy;

import java.util.Collection;

/**
 * @author by mozping
 * @Classname Sort
 * @Description TODO
 * @Date 2019/6/25 14:53
 */
public interface Sort {

    void sort(Collection collection);
}
