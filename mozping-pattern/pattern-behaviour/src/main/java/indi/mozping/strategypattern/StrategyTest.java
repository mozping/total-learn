package indi.mozping.strategypattern;

import indi.mozping.strategypattern.context.Context;
import indi.mozping.strategypattern.strategyImpl.HeapSort;
import indi.mozping.strategypattern.strategyImpl.QuickSort;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author by mozping
 * @Classname StrategyTest
 * @Description TODO
 * @Date 2019/6/25 14:57
 */
public class StrategyTest {

    public static void main(String[] args) {
        Context context;
        if (new Random().nextBoolean()) {
            context = new Context(new QuickSort());
        } else {
            context = new Context(new HeapSort());
        }
        context.contextSort(new ArrayList());
    }
}