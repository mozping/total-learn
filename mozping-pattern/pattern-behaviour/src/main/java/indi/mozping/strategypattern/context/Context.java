package indi.mozping.strategypattern.context;

import indi.mozping.strategypattern.strategy.Sort;

import java.util.Collection;

/**
 * @author by mozping
 * @Classname Context
 * @Description TODO
 * @Date 2019/6/25 14:55
 */
public class Context {

    Sort sort;

    public Context(Sort sort) {
        this.sort = sort;
    }

    public void contextSort(Collection collection) {
        sort.sort(collection);
    }
}