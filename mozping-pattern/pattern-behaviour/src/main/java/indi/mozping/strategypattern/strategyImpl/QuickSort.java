package indi.mozping.strategypattern.strategyImpl;

import indi.mozping.strategypattern.strategy.Sort;

import java.util.Collection;

/**
 * @author by mozping
 * @Classname QuickSort
 * @Description TODO
 * @Date 2019/6/25 14:53
 */
public class QuickSort implements Sort {
    @Override
    public void sort(Collection collection) {
        System.out.println("使用快速排序...");
    }
}