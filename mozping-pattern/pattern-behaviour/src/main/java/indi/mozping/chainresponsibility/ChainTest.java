package indi.mozping.chainresponsibility;

import indi.mozping.chainresponsibility.handlerimpl.TypeOneHandler;
import indi.mozping.chainresponsibility.handlerimpl.TypeThreeHandler;
import indi.mozping.chainresponsibility.handlerimpl.TypeTwoHandler;

/**
 * @author by mozping
 * @Classname ChainTest
 * @Description TODO
 * @Date 2019/6/25 15:47
 */
public class ChainTest {

    public static void main(String[] args) {
        TypeOneHandler typeOneHandler = new TypeOneHandler();
        TypeTwoHandler typeTwoHandler = new TypeTwoHandler();
        TypeThreeHandler typeThreeHandler = new TypeThreeHandler();

        typeOneHandler.setNextHandler(typeTwoHandler);
        typeTwoHandler.setNextHandler(typeThreeHandler);

        typeOneHandler.handler("One");
        typeOneHandler.handler("Two");
        typeOneHandler.handler("Three");
        typeOneHandler.handler("Four");
    }
}