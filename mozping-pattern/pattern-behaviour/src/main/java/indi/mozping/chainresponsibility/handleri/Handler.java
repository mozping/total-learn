package indi.mozping.chainresponsibility.handleri;

/**
 * @author by mozping
 * @Classname Handler
 * @Description TODO
 * @Date 2019/6/25 15:34
 */
public interface Handler {

    void handler(String taskType);
}
