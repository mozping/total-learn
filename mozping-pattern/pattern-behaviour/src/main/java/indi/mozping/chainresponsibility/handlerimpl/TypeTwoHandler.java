package indi.mozping.chainresponsibility.handlerimpl;

import indi.mozping.chainresponsibility.handleri.BaseHandler;

/**
 * @author by mozping
 * @Classname TypeOneHandler
 * @Description TODO
 * @Date 2019/6/25 15:42
 */
public class TypeTwoHandler extends BaseHandler {

    @Override
    public void handler(String taskType) {
        if ("Two".equals(taskType)) {
            System.out.println("TypeTwoHandler handler type two task... ");
        } else if (nextHandler != null) {
            nextHandler.handler(taskType);
        } else {
            System.out.println("TypeTwoHandler can not handler task because it is not type two... ");
        }
    }
}