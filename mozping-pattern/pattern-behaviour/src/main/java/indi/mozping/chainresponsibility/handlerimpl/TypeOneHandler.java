package indi.mozping.chainresponsibility.handlerimpl;

import indi.mozping.chainresponsibility.handleri.BaseHandler;

/**
 * @author by mozping
 * @Classname TypeOneHandler
 * @Description TODO
 * @Date 2019/6/25 15:42
 */
public class TypeOneHandler extends BaseHandler {

    @Override
    public void handler(String taskType) {
        if ("One".equals(taskType)) {
            System.out.println("TypeOneHandler handler type one task... ");
        } else if (nextHandler != null) {
            nextHandler.handler(taskType);
        } else {
            System.out.println("TypeOneHandler can not handler task because it is not type one... ");
        }
    }
}