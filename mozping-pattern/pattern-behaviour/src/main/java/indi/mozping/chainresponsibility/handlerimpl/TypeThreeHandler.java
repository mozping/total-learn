package indi.mozping.chainresponsibility.handlerimpl;

import indi.mozping.chainresponsibility.handleri.BaseHandler;

/**
 * @author by mozping
 * @Classname TypeOneHandler
 * @Description TODO
 * @Date 2019/6/25 15:42
 */
public class TypeThreeHandler extends BaseHandler {

    @Override
    public void handler(String taskType) {
        if ("Three".equals(taskType)) {
            System.out.println("TypeThreeHandler handler type three task... ");
        } else {
            System.out.println("TypeThreeHandler can not handler task because it is not type one/two/three , abandon it ... ");
        }
    }
}