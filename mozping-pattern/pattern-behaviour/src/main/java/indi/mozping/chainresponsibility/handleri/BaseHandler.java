package indi.mozping.chainresponsibility.handleri;

/**
 * @author by mozping
 * @Classname BaseHandler
 * @Description TODO
 * @Date 2019/6/25 15:35
 */
public abstract class BaseHandler implements Handler {

    protected Handler nextHandler;

    public Handler getNextHandler() {
        return nextHandler;
    }

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }
}