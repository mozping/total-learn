# 设计模式 - 代理模式(行为型)
## 一. 代理模式
#### 1.1 引入
```aidl
代理模式是最常见的设计模式，阅读各种框架源码必备，动不动就是来一个动态代理，所以这一关先要弄个明白，下面就把自己的学习过程记录以供参考复习；
```
#### 1.2 示例
###### 1.2.1 场景
```aidl
小明要购买一件国外的衣服，因为不熟悉套路，找代理给他买。
购买行为 -- Buy 接口
小明 -- Person 类
代理 -- BuyPorxy 类  
测试 -- BuyProxyTest 类
```
###### 1.2.2 代码
```aidl
public interface Buy {
    /**
     * 定义接口
     */
    void bugSomeThing();
}

public class Person implements Buy {
    @Override
    public void bugSomeThing() {
        System.out.println("小明买衣服...");
    }
}

public class BuyPorxy implements InvocationHandler {

    private Object target;

    public BuyPorxy(Object obj) {
        target = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("出国购物...");
        method.invoke(target, args);
        System.out.println("代购完成...");
        return null;
    }
}

public class BuyProxyTest {

    public static void main(String[] args) {
        Person xiaoming = new Person();
        BuyPorxy buyPorxy = new BuyPorxy(xiaoming);
        Buy p = (Buy) Proxy.newProxyInstance(xiaoming.getClass().getClassLoader(), xiaoming.getClass().getInterfaces(), buyPorxy);
        p.bugSomeThing();
    }
}

输出：
    出国购物...
    小明买衣服...
    代购完成...
```

#### 1.3 原理初探
```aidl
1.这一步我们抽象一些几个概念，小明对应的Person类，是真实的目标类，BuyPorxy对应的是handler类(实现InvocationHandler接口)，这里handler类内部包含一个目标
类，从new BuyPorxy(xiaoming)这里可以看出来；
2.我们通过Proxy的newProxyInstance这个静态方法获取了一个对象，通过这个对象调用我们的目标方法，表面上看起来调用了BuyPorxy类的invoke方法，这是如何做到的呢？
这个问题需要从Proxy.newProxyInstance方法入手：
```
###### 1.3.1 Proxy.newProxyInstance
>* 
```aidl
第一个参数ClassLoader是类加载器,作用是用来生成类的
第二个参数interfaces是获取真实对象的所有接口，获取所有接口的目的是用来生成代理的,因为代理要实现所有的接口
第三个参数InvocationHandler是调用处理器,这里传入调用处理器，是因为生成代理实例需要调用处理器,在代理对象调用目标接口方法时，
内部就是调用处理器的invoke方法，在invoke里面再调用真实目标对象的方法
public static Object newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler h) throws IllegalArgumentException{
        //校验代码不关注
        Objects.requireNonNull(h);

        //获取目标接口
        final Class<?>[] intfs = interfaces.clone();
        final SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkProxyAccess(Reflection.getCallerClass(), loader, intfs);
        }

        //1.代理类获取
        Class<?> cl = getProxyClass0(loader, intfs);

        /*
         * Invoke its constructor with the designated invocation handler.
         */
        try {
            if (sm != null) {
                checkNewProxyPermission(Reflection.getCallerClass(), cl);
            }
            //2.通过代理类获取其构造方法
            final Constructor<?> cons = cl.getConstructor(constructorParams);
            final InvocationHandler ih = h;
            if (!Modifier.isPublic(cl.getModifiers())) {
                AccessController.doPrivileged(new PrivilegedAction<Void>() {
                    public Void run() {
                        cons.setAccessible(true);
                        return null;
                    }
                });
            }
            //3.通过反射new可一个实例，cons是第一步的cl类，因此第一步的getProxyClass0方法获取到的就是代理类
            return cons.newInstance(new Object[]{h});
            
         //下面的异常先不关注
        } catch (IllegalAccessException|InstantiationException e) {
            throw new InternalError(e.toString(), e);
        } catch (InvocationTargetException e) {
            Throwable t = e.getCause();
            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new InternalError(t.toString(), t);
            }
        } catch (NoSuchMethodException e) {
            throw new InternalError(e.toString(), e);
        }
    }
```

###### 1.3.2 Proxy.getProxyClass0
>* Java中必须要有类，才会有该类的实例。而是JDK在运行期间帮我们生成了一个代理类的字节码，通过类加载器加载这个字节码，然后执行
引擎进行一系列处理后生成代理类，再进行实例化。因为这个类只在内存中保存，最后面的1.3.4将该类持久化并且反编译出来，直接可以看到结果。
```aidl
    private static Class<?> getProxyClass0(ClassLoader loader, Class<?>... interfaces) {
        //异常处理先不关注
        if (interfaces.length > 65535) {
            throw new IllegalArgumentException("interface limit exceeded");
        }

        // If the proxy class defined by the given loader implementing
        // the given interfaces exists, this will simply return the cached copy;
        // otherwise, it will create the proxy class via the ProxyClassFactory
        //1.从英文注释大致知道，有些情况会从缓存获取，有些情况会通过ProxyClassFactory创建proxy，继续跟进该方法(proxyClassCache是WeakCache类)
        return proxyClassCache.get(loader, interfaces);
    }
    
```

###### 1.3.3 WeakCache.get


###### 1.3.4 庐山真面目
>* 将代理类实例化并反编译
```aidl
public final class $Proxy1111 extends Proxy  implements Say{
	
	private static Method m1;
	private static Method m3;
	private static Method m2;
	private static Method m0;

	public $Proxy1111(InvocationHandler invocationhandler){
		super(invocationhandler);
	}

	public final boolean equals(Object obj)
	{
		try{
			return ((Boolean)super.h.invoke(this, m1, new Object[] {obj})).booleanValue();
		}
		catch (Error ) { }
		catch (Throwable throwable){
			throw new UndeclaredThrowableException(throwable);
		}
	}

	public final void saySomeThing(){
		try{
			super.h.invoke(this, m3, null);
			return;
		}
		catch (Error ) { }
		catch (Throwable throwable){
			throw new UndeclaredThrowableException(throwable);
		}
	}

	public final String toString()
	{
		try{
			return (String)super.h.invoke(this, m2, null);
		}
		catch (Error ) { }
		catch (Throwable throwable){
			throw new UndeclaredThrowableException(throwable);
		}
	}

	public final int hashCode(){
		try{
			return ((Integer)super.h.invoke(this, m0, null)).intValue();
		}
		catch (Error ) { }
		catch (Throwable throwable){
			throw new UndeclaredThrowableException(throwable);
		}
	}

	static {
		try{
			m1 = Class.forName("java.lang.Object").getMethod("equals", new Class[] {Class.forName("java.lang.Object")});
			m3 = Class.forName("dynamicproxy.itf.Say").getMethod("saySomeThing", new Class[0]);
			m2 = Class.forName("java.lang.Object").getMethod("toString", new Class[0]);
			m0 = Class.forName("java.lang.Object").getMethod("hashCode", new Class[0]);
		}
		catch (NoSuchMethodException nosuchmethodexception){
			throw new NoSuchMethodError(nosuchmethodexception.getMessage());
		}
		catch (ClassNotFoundException classnotfoundexception){
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}
}

```
>1. 看了代码我们明白了，原来生成的代理类是Proxy的一个子类，并且实现类目标接口，因此他也重写了目标接口的方法，因此代理对象调用目标方法也就顺理成章了。
>2. 除了接口的方法之外，还默认包含三个方法，hashCode，toString，equals三个方法。
>3. 代理类调用目标接口的方法时，代码中执行saySomeThing方法，实际上调用的是handler对象的invoke方法，这也就解释了为什么我们在文章最开始的问题1。
>4. 为什么代理对象调用saySomeThing方法时，会调用handler的invoke方法呢，代理对象和handler的对象关系是什么？很简单，代理对象持有handler对象，回头去看
    Proxy.newProxyInstance方法的第三个参数不就是handler对象么？
>5. Java是单继承，代理类已经继承了Proxy，因此不能再继承其他的类了，因此jdk代理只能代理接口，不能代理类。
#### 1.4 温故知新
```aidl
前提：
    1.目标接口(定义抽象的目标方法)
    2.目标类(实现目标接口，重写目标方法)
    3.handler类(实现InvocationHandler接口，实现自定义代理逻辑)
    4.代理类(Proxy.newProxyInstance获取，持有handler对象)
粗看:
    代理类调用目标方法 --> (代理内内部实现了目标接口，并且持有handler对象) --> 代理类内部调用handler对象的invoke方法，而
    handler对象持有目标对象，在invoke方法里面就可以调用目标对象的方法，并且做点其他的事情，从而实现了代理
    (表面看起来就是调用了我们在handler里面写的逻辑) 
细看:
    代理类的生成过程，这是难点，里面的缓存机制等等。        

```

 
 
 
#### 1.5 思考
###### 1.5.1 invoke参数
invoke方法的三个参数，第二个是方法名称，第三个是参数列表，第一个proxy我们好像并没有用到，作用是啥？
查了一下有下面2个的作用，一个是在方法内可以反射获取proxy的信息，另一个是可以在invoke方法中返回proxy，返回的这个proxy
实际上就是我们的代理对象，这里不能返回this，因为这里的this只是一个handler对象而已，什么情况下返回proxy对象？返回有没有用呢？
返回之后，我们可以用这个proxy再继续调用方法，前面我们不是看到proxy实现了接口的方法嘛？加入接口有多个方法，我调用了方法1，还要继续调用方法2
那么我就可以在方法1处理完之后返回proxy，在由proxy调用方法2，类似于链式编程，可以查看下面的链接和代码示例
https://stackoverflow.com/questions/22930195/understanding-proxy-arguments-of-the-invoke-method-of-java-lang-reflect-invoca
```aidl
接口:
private interface Account {
    public Account deposit (double value);
    public double getBalance ();
}
自定义handler:
private class ExampleInvocationHandler implements InvocationHandler {

    private double balance;

    @Override
    public Object invoke (Object proxy, Method method, Object[] args) throws Throwable {

        // simplified method checks, would need to check the parameter count and types too
        if ("deposit".equals(method.getName())) {
            Double value = (Double) args[0];
            System.out.println("deposit: " + value);
            balance += value;
            return proxy; // here we use the proxy to return 'this'
        }
        if ("getBalance".equals(method.getName())) {
            return balance;
        }
        return null;
    }
}

使用:
Account account = (Account) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] {Account.class, Serializable.class},
    new ExampleInvocationHandler());

//链式调用，因为每一步调用都把proxy对象给返回了
account.deposit(5000).deposit(4000).deposit(-2500);

```
 
###### 1.5.2 动态代理里面的缓存
>* getProxyClass0方法调用了proxyClassCache.get(loader, interfaces);这里代码
```aidl
public V get(K key, P parameter) {
        //校验代码不关注
        Objects.requireNonNull(parameter);

        expungeStaleEntries();

        Object cacheKey = CacheKey.valueOf(key, refQueue);

        // lazily install the 2nd level valuesMap for the particular cacheKey
        ConcurrentMap<Object, Supplier<V>> valuesMap = map.get(cacheKey);
        if (valuesMap == null) {
            ConcurrentMap<Object, Supplier<V>> oldValuesMap
                = map.putIfAbsent(cacheKey,
                                  valuesMap = new ConcurrentHashMap<>());
            if (oldValuesMap != null) {
                valuesMap = oldValuesMap;
            }
        }

        // create subKey and retrieve the possible Supplier<V> stored by that
        // subKey from valuesMap
        Object subKey = Objects.requireNonNull(subKeyFactory.apply(key, parameter));
        Supplier<V> supplier = valuesMap.get(subKey);
        Factory factory = null;

        while (true) {
            if (supplier != null) {
                // supplier might be a Factory or a CacheValue<V> instance
                V value = supplier.get();
                if (value != null) {
                    return value;
                }
            }
            // else no supplier in cache
            // or a supplier that returned null (could be a cleared CacheValue
            // or a Factory that wasn't successful in installing the CacheValue)

            // lazily construct a Factory
            if (factory == null) {
                factory = new Factory(key, parameter, subKey, valuesMap);
            }

            if (supplier == null) {
                supplier = valuesMap.putIfAbsent(subKey, factory);
                if (supplier == null) {
                    // successfully installed Factory
                    supplier = factory;
                }
                // else retry with winning supplier
            } else {
                if (valuesMap.replace(subKey, supplier, factory)) {
                    // successfully replaced
                    // cleared CacheEntry / unsuccessful Factory
                    // with our Factory
                    supplier = factory;
                } else {
                    // retry with current supplier
                    supplier = valuesMap.get(subKey);
                }
            }
        }
    }
```
>* 原理待补充