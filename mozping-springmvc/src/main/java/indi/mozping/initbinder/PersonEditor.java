package indi.mozping.initbinder;

/**
 * @author by mozping
 * @Classname PersonEditor
 * @Description 自定义参数绑定器
 * @Date 2019/11/15 15:03
 */

import indi.mozping.bean.Person;

import java.beans.PropertyEditorSupport;

public class PersonEditor extends PropertyEditorSupport {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Person p = new Person();
        if (text != null) {
            String[] items = text.split(":");
            p.setUsername(items[0]);
            p.setAddress(items[1]);
        }
        setValue(p);
    }

    @Override
    public String getAsText() {
        return getValue().toString();
    }
}
