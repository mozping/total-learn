package indi.mozping.bean;

import lombok.Data;

/**
 * @author by mozping
 * @Classname Person
 * @Description TODO
 * @Date 2019/11/15 15:02
 */
@Data
public class Person {
    private String username;
    private String address;
}