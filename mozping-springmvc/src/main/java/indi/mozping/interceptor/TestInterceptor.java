package indi.mozping.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author by mozping
 * @Classname com.intellif.mozping.interceptor.TestInterceptor
 * @Description 自定义拦截器
 * @Date 2019/11/5 12:23
 */
@Component
public class TestInterceptor implements HandlerInterceptor {

    public TestInterceptor() {
        System.out.println("---TestInterceptor构造----");
    }


    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        System.out.println("自定义拦截器 TestInterceptor preHandle...");
        return true;
    }

    /**
     * 在请求不报异常，顺利完成后执行,不重写该方法
     */
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        System.out.println("自定义拦截器 TestInterceptor postHandle ...");
    }


    /**
     * handle方法执行完毕后执行，无论请求是否异常，最后都会执行。用于清理资源，关闭连接等，不重写该方法
     */
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        System.out.println("自定义拦截器 TestInterceptor afterCompletion...");
    }
}