package indi.mozping.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author by mozping
 * @Classname com.intellif.mozping.interceptor.RequestInterceptorRegister
 * @Description 拦截器注册
 * @Date 2019/11/5 12:33
 */
@Component
public class RequestInterceptorRegister extends WebMvcConfigurerAdapter {

    @Autowired
    TestInterceptor testInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(testInterceptor).addPathPatterns("/**");
    }
}