package indi.mozping.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mozping
 * @Classname com.intellif.mozping.controller.TestController
 * @Description TODO
 * @Date 2019/11/4 9:51
 */
@RestController
public class TestController {

    @PostMapping(value = "/hello")
    public String hello(@RequestParam("a") String a, @RequestParam("b") String b) {
        System.out.println("--------1----------" + a + " --- " + b);
        return "hello!";
    }


}