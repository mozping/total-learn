package indi.mozping.controller;

import indi.mozping.bean.Person;
import indi.mozping.initbinder.PersonEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author by mozping
 * @Classname BinderTestController
 * @Description 测试参数绑定
 * @Date 2019/11/15 15:15
 */
@RestController
public class BinderTestController {

    @RequestMapping("/binderTest")
    public String binderTest(@RequestParam("person") Person person) {
        String result = person.toString() + " " + new Date();
        System.out.println(result);
        return result;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Person.class, new PersonEditor());
    }
}