package indi.mozping.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mozping
 * @Classname ModelAttributeController
 * @Description 自定义ModelAttribute方法
 * @Date 2019/11/15 15:39
 */
@RestController
public class ModelAttributeController {

    @ModelAttribute
    public void myModel(Model model) {
        System.out.println("ModelAttribute method ------------ ");
    }

    @RequestMapping("/modelAttr")
    public String modelAttr(@RequestParam("name") String name) {
        System.out.println("modelAttr execute ... " + name);
        return "modelAttr";
    }

    @RequestMapping("/modelAttr1")
    public String modelAttr1(@RequestParam("name") String name) {
        System.out.println("modelAttr execute ... " + name);
        return "modelAttr";
    }
}