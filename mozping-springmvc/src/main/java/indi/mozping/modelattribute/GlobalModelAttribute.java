package indi.mozping.modelattribute;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * @author by mozping
 * @Classname GlobalModelAttribute
 * @Description 全局 ModelAttribute 方法，需要使用 @ControllerAdvice声明
 * @Date 2019/11/15 15:59
 */
@ControllerAdvice
public class GlobalModelAttribute {

    @ModelAttribute
    public void myModel(Model model) {
        System.out.println("Global ModelAttribute method ------------ ");
    }
}