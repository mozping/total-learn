package indi.mozping.bean;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author by mozping
 * @Classname UserRegisterDto
 * @Description TODO
 * @Date 2019/10/16 15:18
 */
@Data
@ApiModel(value = "UserRegisterDto", description = "用户")
public class UserRegisterDto {

    @ApiModelProperty(value = "用户ID", required = true)
    private Long id;
    @ApiModelProperty(value = "用户姓名", required = true)
    private String username;
    @ApiModelProperty(value = "用户描述", required = true)
    private String description;
    @ApiModelProperty(value = "用户密码", required = true)
    private String password;
    @ApiModelProperty(value = "用户邮箱", required = true)
    private String mail;
}