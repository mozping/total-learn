package indi.mozping.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author intellif
 */
@Data
@ApiModel(description = "用户信息")
public class User {

    @ApiModelProperty(value = "用户ID")
    private Long id;
    @ApiModelProperty(value = "用户姓名")
    private String username;
    @ApiModelProperty(value = "用户描述")
    private String description;

    public User(Long id, String username, String description) {
        this.id = id;
        this.username = username;
        this.description = description;
    }
}
