package indi.mozping.controller;


import com.battcn.swagger.properties.ApiDataType;
import com.battcn.swagger.properties.ApiParamType;
import indi.mozping.bean.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author intellif
 */
@Api(tags = "1.0", value = "用户查询")
@RestController
@RequestMapping("/users")
public class UserQueryController {

    private static final Logger LOG = LoggerFactory.getLogger(UserQueryController.class);

    @GetMapping("/{id}")
    @ApiOperation(value = "用户查询接口", notes = "根据主键获取用户详情信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户编号", dataType = ApiDataType.LONG, paramType = ApiParamType.PATH),
    })
    public User get(@PathVariable long id) {
        return new User(id, "user1", "description1");
    }

}
