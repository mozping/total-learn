package indi.mozping.controller;

import indi.mozping.bean.UserRegisterDto;
import indi.mozping.bean.User;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author intellif
 */
@Api(tags = {"1.1"}, value = "用户注册")
@RestController
@RequestMapping("/users")
public class UserRegisterController {

    private static final Logger LOG = LoggerFactory.getLogger(UserRegisterController.class);

    @PostMapping("/register")
    @ApiOperation(value = "用户注册接口", notes = "注册用户信息接口")
    public User register(@RequestBody UserRegisterDto userRegisterDto) {
        Long id = userRegisterDto.getId();
        String username = userRegisterDto.getUsername();
        String description = userRegisterDto.getDescription();
        LOG.info("参数信息: " + userRegisterDto.toString());
        return new User(id, username, description);
    }
}
