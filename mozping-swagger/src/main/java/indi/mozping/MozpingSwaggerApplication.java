package indi.mozping;

import com.battcn.swagger.annotation.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author intellif
 */
@SpringBootApplication
@EnableSwagger2Doc
@EnableSwagger2
public class MozpingSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingSwaggerApplication.class, args);
    }

}
