import indi.mozping.config.MainConfig1;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Test1 {

    @Test
    public void test01() {
        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig1.class);

        System.out.println("The beans in the IOC container are listed as follows: ");
        //1.打印IOC容器中所有的 实例对象名称
        String[] names = app.getBeanDefinitionNames();

        for (String name : names) {
            System.out.println(name + " --- " + app.getBean(name));
        }

        ((AnnotationConfigApplicationContext) app).close();
    }
}
