package indi.mozping.controller;

import indi.mozping.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname TestController
 * @Description TODO
 * @Date 2019/5/5 20:06
 */
@Component
public class TestController {

    @Value("${testName}")
    String testName;

    @Autowired
    TestService testService;

    @Override
    public String toString() {
        return "TestController{" +
                "testName='" + testName + '\'' +
                ", testService=" + testService +
                '}';
    }
}
