import indi.mozping.ch7.config.Cap7MainConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch7Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(Cap7MainConfig.class);
        ApplicationContext app1 = new ClassPathXmlApplicationContext("zzz");

        System.out.println("------cut-off---------");

        ((AnnotationConfigApplicationContext) app).close();
        //2.获取Person类型的bean的名字
        String[] names = app.getBeanDefinitionNames();
        //2.遍历bean的名称
//        for (String name : names) {
//            System.out.println(name);
//        }
    }


}
