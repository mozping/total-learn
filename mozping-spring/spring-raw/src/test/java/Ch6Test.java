import indi.mozping.ch6.config.Cap6MainConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch6Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(Cap6MainConfig.class);
        System.out.println("------cut-off---------");

        //2.获取Person类型的bean的名字
        String[] names = app.getBeanDefinitionNames();
        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }
    }


}
