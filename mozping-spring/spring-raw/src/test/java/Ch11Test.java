import indi.mozping.ch11.bean.Person;
import indi.mozping.ch11.config.Cap11MainConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch11Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(Cap11MainConfig.class);
        System.out.println("------cut-off---------");


        //2.从容器中获取bean
        Person person = (Person) app.getBean(Person.class);

        System.out.println(person.toString());
        ((AnnotationConfigApplicationContext) app).close();

    }


}
