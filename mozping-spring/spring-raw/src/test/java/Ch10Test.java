import indi.mozping.ch10.MyTarget;
import indi.mozping.ch10.config.Cap10MainConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch10Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(Cap10MainConfig.class);
        System.out.println("------cut-off---------");


        //2.从容器中获取bean
        MyTarget myTarget = (MyTarget) app.getBean(MyTarget.class);
        myTarget.target(100, 1);


        ((AnnotationConfigApplicationContext) app).close();

    }


}
