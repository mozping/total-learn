import indi.mozping.ch3.config.MainConfig31;
import indi.mozping.ch3.config.MainConfig32;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch3Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig31.class);

        String[] names = app.getBeanDefinitionNames();
        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }

        Object bean1 = app.getBean("person");
        Object bean2 = app.getBean("person");
        System.out.println(bean1 == bean2);
        //结论:bean1就是bean2,不是一个对象，prototype是多例
    }

    @Test
    public void test02() {

        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig32.class);

        String[] names = app.getBeanDefinitionNames();
        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }

        Object bean1 = app.getBean("person");
        Object bean2 = app.getBean("person");
        System.out.println(bean1 == bean2);
        //结论:bean1就是bean2,同一个对象,singleton是单例的
    }


}
