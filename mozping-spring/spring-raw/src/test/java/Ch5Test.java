import indi.mozping.bean.Person;
import indi.mozping.ch5.config.MainConfig51;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch5Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig51.class);
        System.out.println("------cut-off---------");

        //2.获取Person类型的bean的名字
        String[] namesForBean = app.getBeanNamesForType(Person.class);

        //3.根据名字拿到bean实例
        for (String name : namesForBean) {
            Person p1 = (Person) app.getBean(name);
            System.out.println(p1);
        }
    }


}
