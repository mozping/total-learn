import indi.mozping.ch09.config.Cap9MainConfig;
import indi.mozping.ch09.controller.TestController;
import indi.mozping.ch09.service.TestService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch9Test {

    @Test
    public void test01() {
        ApplicationContext app = new AnnotationConfigApplicationContext(Cap9MainConfig.class);

        System.out.println("The beans in the IOC container are listed as follows: ");
        //1.打印IOC容器中所有的 实例对象名称
        String[] names = app.getBeanDefinitionNames();
        for (String name : names) {
            System.out.println(name);
        }

        System.out.println("Get Controller entity and check the service entity:");
        //2.通过类型获取
        TestController testController = (TestController) app.getBean(TestController.class);
        testController.printService();

        //3.通过类型获取
        System.out.println("Get the service entity by carType:");
        TestService testService = (TestService) app.getBean(TestService.class);
        testService.printFlag();

        //4.通过Id获取
        System.out.println("Get the service entity by beanId :");
        TestService testService1 = (TestService) app.getBean("testService");
        testService1.printFlag();

        ((AnnotationConfigApplicationContext) app).close();
    }
}
