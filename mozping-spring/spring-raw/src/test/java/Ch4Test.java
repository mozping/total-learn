import indi.mozping.bean.Person;
import indi.mozping.ch4.config.MainConfig41;
import indi.mozping.ch4.config.MainConfig42;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch4Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig41.class);
        System.out.println("------cut-off---------");

        String[] names = app.getBeanDefinitionNames();
        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }

        Person p1 = (Person) app.getBean("person");
        System.out.println(p1);
        //默认是懒加载的，Person的构造方法在分割线之后才执行(在使用这个bean的使用还会执行，即调用getBean的时候才会构造对象)

    }

    @Test
    public void test02() {

        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig42.class);
        System.out.println("------cut-off---------");

        String[] names = app.getBeanDefinitionNames();
        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }

        Person p1 = (Person) app.getBean("person");
        System.out.println(p1);
        //MainConfig42使用立即加载，Person的构造方法在分割线之前执行
    }


}
