package indi.mozping.mozping;

import indi.mozping.ch12.Cap12MainConfig;
import indi.mozping.ch12.A;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch12Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(Cap12MainConfig.class);
        System.out.println("------cut-off---------");


        //2.从容器中获取bean
        A person = (A) app.getBean(A.class);

        System.out.println(person.toString());
        ((AnnotationConfigApplicationContext) app).close();

    }


}
