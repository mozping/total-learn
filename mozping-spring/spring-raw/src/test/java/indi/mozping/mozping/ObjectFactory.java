package indi.mozping.mozping;

import org.springframework.beans.BeansException;

/**
 * @author by mozping
 * @Classname ObjectFactory
 * @Description TODO
 * @Date 2019/9/20 14:18
 */
public interface ObjectFactory {

    Object getObject();
}