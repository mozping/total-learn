package indi.mozping.mozping;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/9/20 14:19
 */
public class Test {

    public static void main(String[] args) {
        GetObject getObject = new GetObject();
        Object object = getObject.getObject(1, () -> {
                    return "234";
                }
        );

        System.out.println(object);
    }
}

