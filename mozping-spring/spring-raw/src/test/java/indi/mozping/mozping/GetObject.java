package indi.mozping.mozping;

/**
 * @author by mozping
 * @Classname GetObject
 * @Description TODO
 * @Date 2019/9/20 14:19
 */
public class GetObject {

    public Object getObject(int i, ObjectFactory objectFactory) {
        return i + objectFactory.getObject().toString();
    }
}