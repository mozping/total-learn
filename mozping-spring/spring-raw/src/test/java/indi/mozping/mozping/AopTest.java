package indi.mozping.mozping;

import indi.mozping.aop.AopConfig;
import indi.mozping.aop.Calculator;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname AopTest
 * @Description TODO
 * @Date 2019/7/4 14:55
 */
public class AopTest {

    @Test
    public void test01() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(AopConfig.class);
        //Calculator c = new Calculator();
        Calculator c = app.getBean(Calculator.class);
        int result = c.div(4, 2);
        System.out.println(result);
        app.close();
    }
}