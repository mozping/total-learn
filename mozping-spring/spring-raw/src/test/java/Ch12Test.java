import indi.mozping.ch12.Cap12MainConfig;
import indi.mozping.ch12.B;
import indi.mozping.ch12.A;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch12Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(Cap12MainConfig.class);
        //禁止循环依赖
        //((AnnotationConfigApplicationContext) app).setAllowCircularReferences(false);
//        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
//        System.out.println(systemClassLoader);
        System.out.println("------cut-off---------");
//        String[] beanNames =  app.getBeanDefinitionNames();
//
//        System.out.println("beanNames个数："+beanNames.length);
//
//        for(String bn:beanNames){
//
//            System.out.println(bn);
//
//        }

        A person = (A) app.getBean("person");
        B man = (B) app.getBean("man");
        //2.从容器中获取bean
        //Person person = (Person) app.getBean(Person.class);

        System.out.println(person.toString());
        System.out.println(man.toString());
        ((AnnotationConfigApplicationContext) app).close();

    }


}
