import indi.mozping.ch8.bean.ValueFromDefault;
import indi.mozping.ch8.bean.ValueInject;
import indi.mozping.ch8.bean.ValueInjectWithMyFile;
import indi.mozping.ch8.config.Cap8MainConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch8Test {

    @Test
    public void test01() {

        ApplicationContext app = new AnnotationConfigApplicationContext(Cap8MainConfig.class);
        System.out.println("------cut-off---------");


        //2.从容器中获取bean
        ValueFromDefault valueFromDefault = (ValueFromDefault) app.getBean("valueFromDefault");
        System.out.println(valueFromDefault);

        ValueInject valueInject = (ValueInject) app.getBean("valueInject");
        System.out.println(valueInject);

        ValueInjectWithMyFile valueInjectWithMyFile = (ValueInjectWithMyFile) app.getBean("valueInjectWithMyFile");
        System.out.println(valueInjectWithMyFile);


        ((AnnotationConfigApplicationContext) app).close();

    }


}
