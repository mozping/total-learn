import indi.mozping.ch2.config1.PersonBeanConfig;
import indi.mozping.ch2.config2.ScanAllBeanConfig;
import indi.mozping.ch2.config3.PersonBeanConfigWithFilter;
import indi.mozping.ch2.config4.PersonBeanConfigWithUserFilter;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Ch2Test {

    @Test
    public void test01() {

        //1.加载配置文件，PersonBeanConfig指定了扫描路径，默认会把com.intellif.ch2.config1下面的bean全部加载到容器，还会有系统的bean
        ApplicationContext app = new AnnotationConfigApplicationContext(PersonBeanConfig.class);

        String[] names = app.getBeanDefinitionNames();

        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }
    }

    @Test
    public void test02() {

        //1.加载配置文件，PersonBeanConfig指定了扫描路径，默认会把com.intellif.ch2下面的bean全部加载到容器，还会有系统的bean
        ApplicationContext app = new AnnotationConfigApplicationContext(ScanAllBeanConfig.class);

        String[] names = app.getBeanDefinitionNames();

        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }
    }

    @Test
    public void test03() {

        //1.加载配置文件，PersonBeanConfig指定了扫描路径，默认会把com.intellif.ch2下面的bean全部加载到容器,但是
        // 通过过滤器定义只扫描Scan目录下面的Controller类型的bean，还会有系统的bean，@Bean指定的bean不受影响
        ApplicationContext app = new AnnotationConfigApplicationContext(PersonBeanConfigWithFilter.class);

        String[] names = app.getBeanDefinitionNames();

        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }
    }

    @Test
    public void test04() {

        //1.加载配置文件，PersonBeanConfigWithUserFilter指定了扫描路径，默认会把com.intellif.ch2下面的controller,service,dao全部加载到容器,但是
        // 通过自定义过滤器定义只扫描指定的bean，还会有系统的bean,@Bean指定的bean不受影响
        ApplicationContext app = new AnnotationConfigApplicationContext(PersonBeanConfigWithUserFilter.class);

        String[] names = app.getBeanDefinitionNames();

        //2.遍历bean的名称
        for (String name : names) {
            System.out.println(name);
        }
    }


}
