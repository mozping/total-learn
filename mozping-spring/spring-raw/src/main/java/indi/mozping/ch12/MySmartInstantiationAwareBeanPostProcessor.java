package indi.mozping.ch12;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname MySmartInstantiationAwareBeanPostProcessor
 * @Description TODO
 * @Date 2019/9/6 16:18
 */
@Component
public class MySmartInstantiationAwareBeanPostProcessor implements SmartInstantiationAwareBeanPostProcessor {

    @Override
    public Object getEarlyBeanReference(Object bean, String beanName) throws BeansException {
        if (bean instanceof A || bean instanceof B) {
            System.out.println("添加Bean到三级缓存：" + beanName + ": " + bean);
        }
        return bean;
    }
}