package indi.mozping.ch12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname Person
 * @Description TODO
 * @Date 2019/6/12 10:39
 */
//@Scope("prototype")
@Component
public class A {

    @Autowired
    B b;

//    public A(B b) {
//        this.b = b;
//    }
}