package indi.mozping.ch09.config;


import indi.mozping.ch09.service.TestService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.intellif.ch09")
public class Cap9MainConfig {


    @Bean("testService2")
    public TestService testService() {
        TestService testService = new TestService();
        testService.setFlag(2);
        return testService;
    }

}
