package indi.mozping.ch09.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname TestServiceImplOne
 * @Description TODO
 * @Date 2019/5/5 11:27
 */
@Component
@Primary
public class TestService {

    int flag = 1;

    public void printFlag() {
        System.out.println("flag:" + flag);
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }


}
