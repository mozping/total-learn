package indi.mozping.ch09.controller;

import indi.mozping.ch09.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname TestController
 * @Description TODO
 * @Date 2019/5/5 11:27
 */
@Component
public class TestController {

    @Qualifier("testService2")
    @Autowired
    TestService testService;

    public void printService() {
        testService.printFlag();
    }

}
