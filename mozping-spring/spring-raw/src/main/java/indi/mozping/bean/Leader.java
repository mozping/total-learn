package indi.mozping.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author by mozping
 * @Classname Leader
 * @Description TODO
 * @Date 2019/3/8 10:55
 */
public class Leader implements DisposableBean, InitializingBean {

    private String name;
    private int age;
    private int num;

    public Leader(String name, int age) {
        System.out.println("Leader ���ι��췽��ִ��...");
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Leader{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", num=" + num +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("Leader destroy ��������");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Leader afterPropertiesSet ��������");
    }

    public void init() {
        this.num = 100;
        System.out.println("Leader ��� init ����ִ��...");
    }

    public void destroySelf() {
        System.out.println("Leader ��� destroySelf ����ִ��...");
    }

}
