package indi.mozping.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author by mozping
 * @Classname Man
 * @Description TODO
 * @Date 2019/3/8 12:19
 */
public class Man implements DisposableBean, InitializingBean {

    private String name;
    private int age;


    public Man(String name, int age) {
        System.out.println("Man 带参构造方法执行...");
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Man{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("Man destroy 方法调用");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Man afterPropertiesSet 方法调用");
    }

    public void init() {
        System.out.println("Man 类的 init 方法执行...");
    }

    public void destroySelf() {
        System.out.println("Man 类的 destroySelf 方法执行...");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Man.....PostConstruct方法执行........");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Man.....PreDestroy方法执行......");
    }

}
