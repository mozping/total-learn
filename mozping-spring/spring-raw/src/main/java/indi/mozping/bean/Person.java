package indi.mozping.bean;

public class Person {
    private String name;
    private Integer age;

    public Person() {
        super();
        System.out.println("Person ����޲ι��췽��ִ��...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person(String name, Integer age) {
        super();
        System.out.println("Person ��Ĵ��ι��췽��ִ��...");
        this.name = name;
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + "]";
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
