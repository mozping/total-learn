package indi.mozping.bean;

/**
 * @author by mozping
 * @Classname Coder
 * @Description TODO
 * @Date 2019/3/8 10:32
 */
public class Coder {

    private String name;
    private Integer age;

    public Coder() {
        super();
        System.out.println("Coder ����޲ι��췽��ִ��...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coder(String name, Integer age) {
        super();
        System.out.println("Coder ��Ĵ��ι��췽��ִ��...");
        this.name = name;
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + "]";
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void init() {
        System.out.println("Coder ��� init ����ִ��...");
    }

    public void destroy() {
        System.out.println("Coder ��� destory ����ִ��...");
    }
}
