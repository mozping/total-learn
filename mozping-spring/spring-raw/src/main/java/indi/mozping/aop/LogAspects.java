package indi.mozping.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.Arrays;

//日志切面类
@Aspect
public class LogAspects {
    @Pointcut("execution(public int indi.mozping.aop.Calculator.*(..))")
    public void myPointCut() {
    }

    ;

    @Before("myPointCut()")
    public void logStart(JoinPoint joinPoint) {
        System.out.println("@Before execute , Args:{" + Arrays.asList(joinPoint.getArgs()) + "}");
    }

    @After("myPointCut()")
    public void logEnd(JoinPoint joinPoint) {
        System.out.println("@After execute finished ......");
    }

    @AfterReturning(value = "myPointCut()", returning = "result")
    public void logReturn(Object result) {
        System.out.println("@AfterReturning execute... Return successfully:{" + result + "}");
    }

    @AfterThrowing(value = "myPointCut()", throwing = "exception")
    public void logException(Exception exception) {
        System.out.println(" @AfterThrowing execute .... Exception:{" + exception + "}");
    }

    @Around("myPointCut()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("@Around:Before...");
        //相当于开始调目标方法
        Object obj = proceedingJoinPoint.proceed();
        System.out.println("@Around:After...");
        return obj;
    }
}
