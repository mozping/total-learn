package indi.mozping.aop;

/**
 * 业务逻辑类
 */
public class Calculator {
    /**
     * 业务逻辑方法
     */
    public int div(int i, int j) {
        System.out.println("div execute...");
        return i / j;
    }
}
