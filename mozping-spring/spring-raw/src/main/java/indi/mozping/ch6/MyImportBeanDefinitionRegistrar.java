package indi.mozping.ch6;

import indi.mozping.ch6.bean.Pig;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    /**
     * AnnotationMetadata:��ǰ���ע����Ϣ
     * BeanDefinitionRegistry:BeanDefinitionע����
     * ��������Ҫ��ӵ������е�entity����;
     *
     * @Scope
     */
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        boolean bean1 = registry.containsBeanDefinition("com.intellif.ch6.entity.Dog");
        boolean bean2 = registry.containsBeanDefinition("com.intellif.ch6.entity.Cat");
        //���Dog��Catͬʱ����������IOC������,��ô����Pig��, ���뵽����
        //��������Ҫע���entity, ��entity���з�װ,
        if (bean1 && bean2) {
            RootBeanDefinition beanDefinition = new RootBeanDefinition(Pig.class);
            registry.registerBeanDefinition("pig", beanDefinition);
        }
    }

}
