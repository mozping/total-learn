package indi.mozping.ch6;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        //����ȫ������entity
        return new String[]{"com.intellif.ch6.entity.Fish", "com.intellif.ch6.entity.Tiger"};
    }
}
