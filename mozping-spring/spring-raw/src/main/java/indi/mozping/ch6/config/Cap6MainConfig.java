package indi.mozping.ch6.config;


import indi.mozping.bean.Person;
import indi.mozping.ch6.MyFactoryBean;
import indi.mozping.ch6.MyImportSelector;
import indi.mozping.ch6.MyImportBeanDefinitionRegistrar;
import indi.mozping.ch6.bean.Cat;
import indi.mozping.ch6.bean.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {Dog.class, Cat.class, MyImportSelector.class, MyImportBeanDefinitionRegistrar.class})
public class Cap6MainConfig {
    /*
     * ��������ע������ķ�ʽ
     * 1,@Bean: [��������������������],����PersonΪ����������, ��Ҫ�����ǵ�IOC������ʹ��
     * 2,��ɨ��+����ı�עע��(@ComponentScan:  @Controller, @Service  @Reponsitory  @ Componet),һ������� �����Լ�д����,ʹ�����
     * 3,@Import:[���ٸ���������һ�����] ע��:@Bean�е��
     *      a,@Import(Ҫ���뵽�����е����):�������Զ�ע��������,entity �� idΪȫ����
     *      b,ImportSelector:��һ���ӿ�,������Ҫ���뵽�����������ȫ��������
     *      c,ImportBeanDefinitionRegistrar:�����ֶ���������IOC����, ����Bean��ע�����ʹ��BeanDifinitionRegistry
     *          дJamesImportBeanDefinitionRegistrarʵ��ImportBeanDefinitionRegistrar�ӿڼ���
     *  4,ʹ��Spring�ṩ��FactoryBean(����entity)����ע��
     *
     *
     */
    //��������ʱ��ʼ��person��beanʵ��
    @Bean("person")
    public Person person() {
        return new Person("jams", 20);
    }

    @Bean
    public MyFactoryBean jamesFactoryBean() {
        return new MyFactoryBean();
    }


}
