package indi.mozping.Util;

import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * @author by mozping
 * @Classname FileUtil
 * @Description TODO
 * @Date 2019/3/7 20:20
 */
public class FileUtil {


    public static String getPropertiesFromClassPath(String fileName, String key) {

        Properties properties = new Properties();
        //1. 读取classpath下的配置文件
        File cfgFileClassPath;
        BufferedReader bufferedReader;
        try {
            cfgFileClassPath = ResourceUtils.getFile("classpath:" + fileName);
            bufferedReader = new BufferedReader(new FileReader(cfgFileClassPath));
            properties.load(bufferedReader);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String value = properties.getProperty(key, "null");
        return value;
    }


}
