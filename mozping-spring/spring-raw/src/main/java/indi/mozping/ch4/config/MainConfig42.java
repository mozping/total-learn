package indi.mozping.ch4.config;

import indi.mozping.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @author by mozping
 * @Classname MainConfig3
 * @Description TODO
 * @Date 2019/3/7 18:36
 */
@Configuration
public class MainConfig42 {

    /**
     * 测试bean是不是懒加载，默认是懒加载的
     * 修改为立即加载
     *
     * @Lazy(value = false)是立即加载
     * @Lazy(value = true)是懒加载
     * 不写@Lazy注解等价于@Lazy(value = false),立即加载
     */
    @Bean
    @Lazy(value = false)
    public Person person() {
        return new Person("Duncan", 40);
    }

}
