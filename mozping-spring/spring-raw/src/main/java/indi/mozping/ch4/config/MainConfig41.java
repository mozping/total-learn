package indi.mozping.ch4.config;

import indi.mozping.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @author by mozping
 * @Classname MainConfig3
 * @Description TODO
 * @Date 2019/3/7 18:36
 */
@Configuration
public class MainConfig41 {

    /**
     * 测试bean是不是懒加载，默认是懒加载的
     */
    @Bean
    @Lazy
    public Person person() {
        return new Person("Duncan", 40);
    }

}
