package indi.mozping.ch11.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.context.weaving.LoadTimeWeaverAware;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.jmx.export.notification.NotificationPublisher;
import org.springframework.jmx.export.notification.NotificationPublisherAware;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname Person
 * @Description TODO
 * @Date 2019/6/12 10:39
 */
//@Component
@Configuration
public class Person implements BeanNameAware, BeanClassLoaderAware, BeanFactoryAware, ApplicationContextAware, ImportAware, LoadTimeWeaverAware {

    @Value("${name}")
    String name;

    @Override
    public void setBeanName(String name) {
        System.out.println("Person setBeanName function execute:  " + name);

    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        System.out.println("Person setBeanClassLoader function execute:  " + classLoader);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("Person setBeanFactory function execute:  " + beanFactory);
        System.out.println("Person setBeanFactory function execute:  " + beanFactory.getBean("person"));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("Person setApplicationContext function execute:  " + applicationContext.toString());
        System.out.println("Class : " + applicationContext.getClass());
        System.out.println("Id : " + applicationContext.getId());
        System.out.println("BeanNames : " + applicationContext.getBeanDefinitionNames());

    }

//    @Override
//    public void setEnvironment(Environment environment) {
//        System.out.println("Person setEnvironment function execute:  " + environment.getProperty("application-01.properties"));
//        System.out.println("Person setEnvironment function execute:  " + environment.getActiveProfiles());
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }


    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        System.out.println("Person setImportMetadata function execute:  " + importMetadata.toString());
    }

    @Override
    public void setLoadTimeWeaver(LoadTimeWeaver loadTimeWeaver) {
        System.out.println("Person setLoadTimeWeaver function execute:  " + loadTimeWeaver);
    }

//    @Override
//    public void setNotificationPublisher(NotificationPublisher notificationPublisher) {
//        System.out.println("Person setNotificationPublisher function execute:  " + notificationPublisher);
//    }
}