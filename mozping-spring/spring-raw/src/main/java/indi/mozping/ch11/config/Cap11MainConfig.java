package indi.mozping.ch11.config;


import org.springframework.context.annotation.*;

@Import(Cap11Config.class)
@Configuration
@ComponentScan("com.intellif.ch11")
@PropertySource(value = "classpath:/application-01.properties")
public class Cap11MainConfig {

//    @Bean
//    public Person person() {
//        return new Person();
//    }

}
