package indi.mozping.ch11.config;


import indi.mozping.ch11.bean.Man;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.context.weaving.LoadTimeWeaverAware;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.instrument.classloading.LoadTimeWeaver;

import java.util.Set;

@Configuration
public class Cap11Config implements ImportAware, LoadTimeWeaverAware {

    @Bean
    public Man man() {
        return new Man();
    }

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        System.out.println("Cap11Config setImportMetadata function execute:  " + importMetadata.toString());
        Set<String> annotationTypes = importMetadata.getAnnotationTypes();
        for (String annotation : annotationTypes) {
            System.out.println(annotation);
        }
    }

    @Override
    public void setLoadTimeWeaver(LoadTimeWeaver loadTimeWeaver) {
        System.out.println("Cap11Config setLoadTimeWeaver function execute:  " + loadTimeWeaver);
    }
}
