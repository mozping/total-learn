package indi.mozping.ch10.config;


import indi.mozping.ch10.LogAspect;
import indi.mozping.ch10.MyTarget;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class Cap10MainConfig {

    //��������ʱ��ʼ��beanʵ��
    @Bean
    public MyTarget myTarget() {
        return new MyTarget();
    }

    @Bean
    public LogAspect logAspect() {
        return new LogAspect();
    }


}
