package indi.mozping.ch10;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

@Aspect
public class LogAspect {

    //�����صķ�ʽ��ȡ�������ķ�������ȥ
    @Pointcut("execution(public int indi.mozping.ch10.MyTarget.*(..))")
    public void pointCut() {
    }

    ;

    @Before("pointCut()")
    public void logBefore() {
        System.out.println("log @Before...");
    }

    @After("pointCut()")
    public void logAfter() {
        System.out.println("log @After...");
    }

    @AfterReturning(value = "pointCut()", returning = "result")
    public void logReturn(Object result) {
        System.out.println("log @AfterReturning...:" + result);
    }

    @AfterThrowing(value = "pointCut()", throwing = "exception")
    public void logThrowing(Exception exception) {
        System.out.println("log @AfterThrowing..." + exception + " .");
    }

    @Around("pointCut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        System.out.println("log @Around before...");
        Object object = joinPoint.proceed();
        System.out.println("log @Around after...");
        return object;
    }

    public LogAspect() {
        System.out.println("LogAspect construct ... ");
    }
}
