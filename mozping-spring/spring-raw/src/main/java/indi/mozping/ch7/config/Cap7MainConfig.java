package indi.mozping.ch7.config;


import indi.mozping.bean.Man;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.intellif.ch7.processor")
public class Cap7MainConfig {

    //��������ʱ��ʼ��person��beanʵ��
//    @Bean("person")
//    public Person person() {
//        return new Person("Duncan", 40);
//    }
//
//    @Bean(initMethod = "init", destroyMethod = "destroy")
//    public Coder coder() {
//        return new Coder();
//    }
//
//    @Bean(value = "leader", initMethod = "init", destroyMethod = "destroySelf")
//    public Leader leader() {
//        return new Leader("Iverson", 41);
//    }


    @Bean(value = "man", initMethod = "init", destroyMethod = "destroySelf")
    public Man man() {
        return new Man("Parker", 33);
    }
}
