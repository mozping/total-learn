package indi.mozping.ch1.config;

import indi.mozping.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author by mozping
 * @Classname PersonBeanConfig
 * @Description TODO
 * @Date 2019/3/7 16:28
 */
//该注解表示这个配置类等价于配置文件
@Configuration
public class PersonBeanConfig {

    //给容器中注册一个bean, 类型为返回值的类型,默认名称是方法名称, 可以使用@Bean("testBean")指定名称
    @Bean
    public Person person() {
        return new Person("kobe", 20);
    }
}
