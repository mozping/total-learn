package indi.mozping.ch1;

import indi.mozping.bean.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author by mozping
 * @Classname MainTest1
 * @Description 测试通过xml配置文件来获取bean，比较原始的方式
 * @Date 2019/3/7 15:54
 */
public class MainTest1 {

    public static void main(String[] args) {
        //1.把beans.xml的类加载到容器
        ApplicationContext app = new ClassPathXmlApplicationContext("beans.xml");

        //2.从容器中获取bean
        Person person = (Person) app.getBean("person");

        System.out.println(person);
    }

}
