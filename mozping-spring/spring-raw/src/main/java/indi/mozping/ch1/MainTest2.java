package indi.mozping.ch1;

import indi.mozping.bean.Person;
import indi.mozping.ch1.config.PersonBeanConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname MainTest1
 * @Description 测试通过配置Bean文件来获取bean
 * @Date 2019/3/7 16:34
 */
public class MainTest2 {

    public static void main(String[] args) {
        //1.把beans.xml的类加载到容器

        ApplicationContext app = new AnnotationConfigApplicationContext(PersonBeanConfig.class);

        //2.从容器中获取bean,这里的名称默认是配置bean的方法，名称，可以自定义
        Person person = (Person) app.getBean("person");
        System.out.println(person);

        //3.获取指定类型的bean的名称，名称默认是配置bean的方法名称,可以在@Bean注解上指定名称
        String[] namesForBean = app.getBeanNamesForType(Person.class);
        for (String name : namesForBean) {
            System.out.println(name);
        }
    }

}
