package indi.mozping.ch2.config4;

import indi.mozping.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

/**
 * @author by mozping
 * @Classname PersonBeanConfig
 * @Description 使用ComponentScan自定义加载bean的路径，并过滤类型类型
 * @Date 2019/3/7 16:28
 */
//该注解表示这个配置类等价于配置文件
@Configuration
@ComponentScan(value = {"com.intellif.ch2.controller", "com.intellif.ch2.service", "com.intellif.ch2.com.intellif.mozping.dao"}, includeFilters = {
        @ComponentScan.Filter(type = FilterType.CUSTOM, value = {MyTypeFilter.class})
}, useDefaultFilters = false)
public class PersonBeanConfigWithUserFilter {

    //给容器中注册一个bean, 类型为返回值的类型,默认名称是方法名称, 可以使用@Bean("testBean")指定名称
    @Bean
    public Person person() {
        return new Person("kobe", 20);
    }
}
