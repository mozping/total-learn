package indi.mozping.ch8.bean;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author by mozping
 * @Classname ValueFromDefault
 * @Description TODO
 * @Date 2019/3/8 15:10
 */
public class ValueFromDefault {

    @Value("${name}")
    private String name;

    @Value("${age}")
    private int age;

    @Value("${addr}")
    private String addr;

    public ValueFromDefault() {
    }

    public ValueFromDefault(String name, String address, int age) {
        super();
        this.name = name;
        this.addr = address;
        this.age = age;
    }

    @Override
    public String toString() {
        return "ValueFromDefault{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", addr='" + addr + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
}
