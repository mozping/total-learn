package indi.mozping.ch8.config;


import indi.mozping.ch8.bean.ValueFromDefault;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.intellif.ch8.entity")
@PropertySource(value = "classpath:/application.properties")
public class Cap8MainConfig {

    //��������ʱ��ʼ��beanʵ��
    @Bean("valueFromDefault")
    public ValueFromDefault valueFromDefault() {
        return new ValueFromDefault("Duncan", "USA", 40);
    }


}
