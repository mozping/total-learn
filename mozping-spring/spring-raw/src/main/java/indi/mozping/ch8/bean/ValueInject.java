package indi.mozping.ch8.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname ValueInject
 * @Description TODO
 * @Date 2019/3/8 15:29
 */
@Component
public class ValueInject {

    @Value("${valueInject}")
    String valueInject;

    @Override
    public String toString() {
        return "ValueInject{" +
                "valueInject='" + valueInject + '\'' +
                '}';
    }

    public ValueInject() {
    }
}
