package indi.mozping.ch8.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname ValueInject
 * @Description TODO
 * @Date 2019/3/8 15:29
 */
@Component
@PropertySource(value = "classpath:/test.properties")
public class ValueInjectWithMyFile {

    @Value("${valueInjectWithMyFile}")
    String valueInjectWithMyFile;

    @Override
    public String toString() {
        return "ValueInject{" +
                "valueInject='" + valueInjectWithMyFile + '\'' +
                '}';
    }

    public ValueInjectWithMyFile() {
    }
}
