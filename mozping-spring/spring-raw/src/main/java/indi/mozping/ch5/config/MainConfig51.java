package indi.mozping.ch5.config;

import indi.mozping.bean.Person;
import indi.mozping.ch5.condition.JamesCondition;
import indi.mozping.ch5.condition.KobeCondition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @author by mozping
 * @Classname MainConfig3
 * @Description TODO
 * @Date 2019/3/7 18:36
 */
@Configuration
public class MainConfig51 {

    /**
     * 测试bean是不是懒加载，默认是懒加载的
     */
    @Bean
    @Conditional(KobeCondition.class)
    public Person person1() {
        return new Person("Kobe", 38);
    }

    @Bean
    @Conditional(JamesCondition.class)
    public Person person2() {
        return new Person("James", 34);
    }

}
