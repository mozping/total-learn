package indi.mozping.ch5.condition;

import indi.mozping.Util.FileUtil;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author by mozping
 * @Classname JamesCondition
 * @Description TODO
 * @Date 2019/3/7 19:13
 */
public class JamesCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String player = FileUtil.getPropertiesFromClassPath("application.properties", "player");
        if ("James".equalsIgnoreCase(player)) {
            return true;
        }
        return false;
    }

}
