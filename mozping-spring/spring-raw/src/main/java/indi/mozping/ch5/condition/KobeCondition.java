package indi.mozping.ch5.condition;

import indi.mozping.Util.FileUtil;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author by mozping
 * @Classname KobeCondition
 * @Description TODO
 * @Date 2019/3/7 19:08
 */
public class KobeCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String player = FileUtil.getPropertiesFromClassPath("application.properties", "player");
        System.out.println("Player:" + player);
        if ("kobe".equalsIgnoreCase(player)) {
            return true;
        }
        return false;
    }
}
