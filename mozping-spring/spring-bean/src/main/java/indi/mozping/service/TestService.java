package indi.mozping.service;

import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname TestService
 * @Description TODO
 * @Date 2019/5/5 20:06
 */
//@Component
public class TestService {

    String id = "100";

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "TestService{" +
                "id='" + id + '\'' +
                '}';
    }
}
