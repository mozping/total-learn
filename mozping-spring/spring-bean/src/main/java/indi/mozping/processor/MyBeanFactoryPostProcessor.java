package indi.mozping.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname MyBeanFactoryPostProcessor
 * @Description TODO
 * @Date 2019/5/7 20:19
 */
@Component
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("MyBeanFactoryPostProcessor��postProcessBeanFactory����ִ�У�the beanFactory   is:" + beanFactory);

        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("bear");
        beanDefinition.setLazyInit(true);
    }
}