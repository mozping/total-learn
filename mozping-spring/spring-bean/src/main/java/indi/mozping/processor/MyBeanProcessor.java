package indi.mozping.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname MyBeanProcessor
 * @Description TODO
 * @Date 2019/5/5 16:23
 */
//@Component
public class MyBeanProcessor implements BeanPostProcessor {


    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("MyBeanPostProcessor���ô�������postProcessBeforeInitialization����ִ��,the entity name is :  " + beanName);
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("MyBeanPostProcessor���ô�������postProcessAfterInitialization����ִ��,the entity name is : " + beanName);
        return bean;
    }

}
