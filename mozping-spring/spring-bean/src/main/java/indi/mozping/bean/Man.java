package indi.mozping.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.*;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author by mozping
 * @Classname Man
 * @Description TODO
 * @Date 2019/3/8 12:19
 */
public class Man implements DisposableBean, InitializingBean, ApplicationContextAware, BeanNameAware, BeanFactoryAware, EnvironmentAware, ApplicationEventPublisherAware {

    private String name;
    private int age;
    private String beanName;


    public Man(String name, int age) {
        System.out.println("Man ���ι��췽��ִ��...");
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Man{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public void destroy() throws Exception {
        System.out.println("Man �̳���DisposableBean�ӿڵ�destroy ����ִ��...");
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Man �̳���InitializingBean�ӿڵ�afterPropertiesSet ����ִ��...");
    }

    public void init() {
        System.out.println("Man ��ͨ��@Bean��init-methodָ����init����ִ��...");
    }

    public void init2() {
        System.out.println("Man init2����ִ��...");
    }

    public void destroySelf() {
        System.out.println("Man ��ͨ��@Bean��destroyMethodָ����destroySelf����ִ��...");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Man ��ͨ��@PostConstructע��ָ����PostConstruct����ִ��........");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Man ��ͨ��@PreDestroyע��ָ����PreDestroy����ִ��......");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("Man �̳���ApplicationContextAware�ӿڵ�setApplicationContext����ִ��......" + applicationContext.getId());
    }

    @Override
    public void setBeanName(String name) {
        this.beanName = name;
        System.out.println("Man �̳���BeanNameAware�ӿڵ�setBeanName����ִ��......" + name);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("Man �̳���BeanFactoryAware�ӿڵ�setBeanFactory����ִ��......" + beanFactory.containsBean(beanName));
    }

    @Override
    public void setEnvironment(Environment environment) {
        System.out.println("Man �̳���EnvironmentAware�ӿڵ�setEnvironment����ִ��......" + environment.toString());
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        System.out.println("Man �̳���ApplicationEventPublisherAware�ӿڵ�setApplicationEventPublisher����ִ��......"
                + applicationEventPublisher.toString());
    }
}
