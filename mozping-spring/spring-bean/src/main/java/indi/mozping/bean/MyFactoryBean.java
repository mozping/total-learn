package indi.mozping.bean;

import org.springframework.beans.factory.FactoryBean;

import java.util.Random;

/**
 * @author by mozping
 * @Classname MyFactoryBean
 * @Description TODO
 * @Date 2019/5/7 20:08
 */

public class MyFactoryBean implements FactoryBean<Man> {
    @Override
    public Man getObject() throws Exception {
        if (new Random().nextBoolean()) {
            return new Man("Kobe", 24);
        }
        return new Man("James", 23);
    }

    @Override
    public Class<?> getObjectType() {
        return Man.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}