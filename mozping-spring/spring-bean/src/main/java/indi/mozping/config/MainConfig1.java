package indi.mozping.config;

import indi.mozping.bean.Bear;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.intellif.ch1")
@PropertySource(value = "classpath:/application.properties")
public class MainConfig1 {


//    @Bean(value = "man", initMethod = "init", destroyMethod = "destroySelf")
//    public Man man() {
//        return new Man("Parker", 33);
//    }


    @Bean
    public Bear bear() {
        return new Bear("myBear");
    }

}
