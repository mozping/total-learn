import indi.mozping.config.MainConfig1;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author by mozping
 * @Classname Ch2Test
 * @Description TODO
 * @Date 2019/3/7 15:56
 */
public class Test1 {

    @Test
    public void test01() {
        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig1.class);

        //1.��ӡIOC���������е� ʵ����������
        String[] names = app.getBeanDefinitionNames();
        System.out.println("�����ӡIOC�������Ѿ����ڵ�Bean.. ");
        for (String name : names) {
            System.out.println(name);
        }
        // System.out.println(app.getBean("myFactoryBean").toString());
        System.out.println("�����ӡIOC�������Ѿ����ڵ�Bean���.. ");
        System.out.println(app.getBean("bear").toString());
        ((AnnotationConfigApplicationContext) app).close();
    }
}
