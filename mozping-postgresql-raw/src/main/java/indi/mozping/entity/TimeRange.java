package indi.mozping.entity;

import java.util.Date;

/**
 * @author by mozping
 * @Classname TimeRange
 * @Description TODO
 * @Date 2019/7/4 16:39
 */
public class TimeRange {

    Date begin;
    Date end;

    public TimeRange(Date begin, Date end) {
        this.begin = begin;
        this.end = end;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }


}