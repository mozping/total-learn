package indi.mozping.entity;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Entity
 * @Description TODO
 * @Date 2019/7/4 19:51
 */
public class Entity {

    String tid;
    String sourceId;
    Date time;
    Date vtime;
    Date pdate;

    public Entity(String tid, Date time, Date vtime, Date pdate, String sourceId) {
        this.tid = tid;
        this.time = time;
        this.vtime = vtime;
        this.pdate = pdate;
        this.sourceId = sourceId;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "tid='" + tid + '\'' +
                ", time=" + time +
                ", vtime=" + vtime +
                ", pdate=" + pdate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return this.tid.equals(((Entity) o).tid);
    }


    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getVtime() {
        return vtime;
    }

    public void setVtime(Date vtime) {
        this.vtime = vtime;
    }

    public Date getPdate() {
        return pdate;
    }

    public void setPdate(Date pdate) {
        this.pdate = pdate;
    }
}