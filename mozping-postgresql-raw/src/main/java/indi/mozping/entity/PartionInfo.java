package indi.mozping.entity;

import java.util.Date;

/**
 * @author by mozping
 * @Classname PartionInfo
 * @Description TODO
 * @Date 2019/7/4 20:03
 */
public class PartionInfo {

    String name;
    String tableName;
    Date begin;
    Date end;
}