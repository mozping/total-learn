package indi.mozping.util;

import indi.mozping.entity.Entity;
import indi.mozping.entity.TimeRange;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author by mozping
 * @Classname Util
 * @Description TODO
 * @Date 2019/7/4 16:36
 */
public class Util {

    public static Connection getConn(String url) throws ClassNotFoundException, SQLException {
        Connection conn = null;

        if (url.contains("pivotal:greenplum")) {
            Class.forName("com.pivotal.jdbc.GreenplumDriver");
        } else if (url.contains("mysql")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } else {
            throw new RuntimeException("unknown url:" + url);
        }
        conn = DriverManager.getConnection(url);
        return conn;
    }


    public static TimeRange getTimeRangeFromTwo(TimeRange range1, TimeRange range2) {
        Date range1Begin = range1.getBegin();
        Date range1End = range1.getEnd();
        Date range2Begin = range2.getBegin();
        Date range2End = range2.getEnd();

        if (range1Begin.getTime() <= range2Begin.getTime() &&
                range1End.getTime() <= range2End.getTime() &&
                range1End.getTime() >= range2Begin.getTime()) {
            return new TimeRange(range2Begin, range1End);
        }

        if (range2Begin.getTime() <= range1Begin.getTime() &&
                range2End.getTime() <= range1End.getTime() &&
                range2End.getTime() >= range1Begin.getTime()) {
            return new TimeRange(range1Begin, range2End);
        }

        if (range1Begin.getTime() <= range2Begin.getTime() && range1End.getTime() >= range2End.getTime()) {
            return range2;
        }

        if (range2Begin.getTime() <= range1Begin.getTime() && range2End.getTime() >= range1End.getTime()) {
            return range1;
        }
        return new TimeRange(range1Begin, range2End);

    }

    public static ArrayList<Object> dealQueryResult(ResultSet resultSet) throws SQLException {
        ArrayList<Object> arrayList = new ArrayList<>();
        while (resultSet.next()) {
            String tid = resultSet.getString("tid");
            Date time = resultSet.getTimestamp("time");
            //Date vtime = resultSet.getTimestamp("vtime");
            Date pdate = resultSet.getDate("pdate");
            String sourceId = resultSet.getString("sourceId");
            arrayList.add(new Entity(tid, time, time, pdate, sourceId));
        }
        return arrayList;
    }

    public static boolean rowsBigThanZero(String line) {
        return line.contains("Avg") && line.contains("Max");

    }

    public static String getPartionName(String line) {
        String[] strArr = line.trim().split(" ");
        for (int i = 0; i < strArr.length; i++) {
            if ("on".equals(strArr[i])) {
                return strArr[i + 1];
            }
        }
        return "";
    }


    /**
     * 初始化分区信息
     */
    public static Map<String, TimeRange> initPartionsInfo(Date begin, Date end, int interval, boolean printDetail) {
        System.out.println("初始化分区信息...");
        Map<String, TimeRange> partionTimeRanges = new HashMap<>();
        int pNum = 1 + Math.abs(DateUtil.getDayGap(begin, end)) / interval;
        for (int i = 1; i <= pNum; i++) {
            String pName = Configuration.TABLE_NAME_SUFFIX + "_1_prt_pn_" + i;
            Date pBegin = begin;
            Date pEnd = DateUtil.getDateIncSomeDay(pBegin, interval);
            partionTimeRanges.put(pName, new TimeRange(pBegin, pEnd));
            begin = pEnd;
            if (printDetail) {
                System.out.println("分区名称：" + pName + " , 时间范围"
                        + DateUtil.getDateStr(pBegin) + " -> " + DateUtil.getDateStr(pEnd));
            }
        }
        return partionTimeRanges;
    }
}

