package indi.mozping.util;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Constant
 * @Description TODO
 * @Date 2019/7/4 17:23
 */
public class Constant {

    //数据库配置
    public static final String GP_URL = "jdbc:pivotal:greenplum://192.168.13.51:5432;DatabaseName=db1;user=gpadmin;password=gpadmin";
    public static final String TABLE_NAME = "public.car_vtime";
    public static final String TABLE_NAME_SUFFIX = "car_vtime";

    //查询条件
    public static String QUERY_COLOR = "blue";
    public static Date QUERY_BEGIN = DateUtil.convertStringToDate("2019-01-01 00:00:00");
    public static Date QUERY_END = DateUtil.convertStringToDate("2019-01-03 00:00:00");


}