package indi.mozping.util;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Configuration
 * @Description TODO
 * @Date 2019/7/9 12:30
 */
public class Configuration {

    //查询表名称
    public static final String TABLE_NAME = "public.tcar";
    public static final String TABLE_NAME_SUFFIX = "tcar";

    //查询条件
    public static String MODE = "dataConsistency";
    public static final int LIMIT_NUM = 1000;
    //white, yellow ,brown ,green ,blue , black , golden ， red ， silvery ， gray
    public static String QUERY_COLOR = "blue";
    public static String QUERY_PLATE_COLOR = "golden";
    // suv, craneTruck , minivan，escortVehicle , slagCar, midiBus, escortVehicle,  slagCar , mpv , largeTruck ,tanker
    public static String[] QUERY_CAR_TYPE = new String[]{"minivan", "suv"};
    public static String BRAND_CODE = "12171112";

    /**
     * 模糊匹配
     * 前缀: 津AM%
     * 后缀: %12
     * 全匹配: %qw1%
     */
    public static String PLATE_NUMBER = "%A319%";


    public static Date QUERY_BEGIN = DateUtil.convertStringToDate("2019-07-01 00:00:00");
    public static Date QUERY_END = DateUtil.convertStringToDate("2020-06-01 00:00:00");


    private static Date QUERY_BEGIN_REVERSE = DateUtil.convertStringToDate("2019-01-02 00:00:00");
    private static Date QUERY_END_REVERSE = DateUtil.convertStringToDate("2019-01-06 00:00:00");


}