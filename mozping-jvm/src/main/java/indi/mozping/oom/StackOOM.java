package indi.mozping.oom;

import java.lang.String;

/**
 * @author by mozping
 * @Classname StackOOM
 * @Description TODO
 * @Date 2019/8/13 13:50
 */
public class StackOOM {

    /**
     * 使用虚拟机参数-Xss1m设置栈大小为1m，默认是几百kb，设置不能小于108kb，递归1000次左右，1m可以递归1万次左右，10m可以递归10万次
     */
    private static int count = 0;

    public static void main(String[] args) {
        new StackOOM().test();
    }

    void test() {
        System.out.println(count++);
        test();
    }
}