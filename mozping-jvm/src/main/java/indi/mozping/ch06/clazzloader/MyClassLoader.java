package indi.mozping.ch06.clazzloader;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.String;

/**
 * @author by mozping
 * @Classname MyClassLoader
 * @Description TODO
 * @Date 2019/9/16 20:02
 */
public class MyClassLoader extends ClassLoader {
    public Class findClass(String name) {
        byte[] b = null;
        try {
            b = getByte(name);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return defineClass(null, b, 0, b.length);
    }

    private byte[] getByte(String name) throws IOException {
        FileInputStream in = new FileInputStream(name);
        byte[] b = new byte[1024];
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
        out.close();
        b = out.toByteArray();
        return b;
    }
}