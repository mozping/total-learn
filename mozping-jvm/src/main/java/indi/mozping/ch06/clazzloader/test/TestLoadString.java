package indi.mozping.ch06.clazzloader.test;

import indi.mozping.ch06.clazzloader.FileClassLoader;

import java.util.Arrays;

/**
 * @author by mozping
 * @Classname TestLoadString
 * @Description 测试加载自定义的Object类
 * @Date 2019/9/16 19:36
 */
public class TestLoadString {

    public static void main(String[] args) throws ClassNotFoundException {
        //1.创建自定义文件类加载器
        String path = "E:/";
        FileClassLoader loader = new FileClassLoader(path);

        try {
            //2.加载指定的class文件
            Class<?> aClass = loader.loadClass("java.lang.String");
            //Class<?> aClass = loader.findClass("java.lang.String");
            System.out.println(aClass.getName());
            System.out.println(Arrays.toString(aClass.getDeclaredConstructors()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}