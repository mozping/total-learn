package indi.mozping.ch06.clazzloader.test;

import indi.mozping.ch06.clazzloader.FileUrlClassLoader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * @author by mozping
 * @Classname HotDeployTest
 * @Description TODO
 * @Date 2019/9/16 19:48
 */
public class HotDeployTest {

    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException {
        /**
         * 通过不同的类加载器实例加载的类对象，不是一个类对象，其hashcode值不同
         */
        String rootDir = "E:/";
        //1.创建自定义文件类加载器
        File file = new File(rootDir);
        //2.File转换为URI
        URI uri = file.toURI();
        URL[] urls = {uri.toURL()};
        //3.创建类加载器
        FileUrlClassLoader loader1 = new FileUrlClassLoader(urls);
        FileUrlClassLoader loader2 = new FileUrlClassLoader(urls);

        try {
            //4.加载指定的class文件
            Class<?> aClass1 = loader1.loadClass("TestObj");
            Class<?> aClass2 = loader2.loadClass("TestObj");
//            Object o1 = aClass1.newInstance();
//            Method method1 = aClass1.getMethod("print");
//
//            Object o2 = aClass2.newInstance();
//            Method method2 = aClass2.getMethod("print");
//            method1.invoke(o1);
//            method2.invoke(o2);
            System.out.println(aClass1.hashCode() + " -- " + aClass2.hashCode());
            System.out.println(aClass1.equals(aClass2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}