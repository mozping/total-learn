package indi.mozping.ch06.clazzloader.test;

import indi.mozping.ch06.clazzloader.FileUrlClassLoader;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.Object;
import java.lang.String;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * @author by mozping
 * @Classname FileUrlClassLoaderTest
 * @Description TODO
 * @Date 2019/9/16 12:40
 */
public class FileUrlClassLoaderTest {

    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException {
        String rootDir = "E:/";
        //1.创建自定义文件类加载器
        File file = new File(rootDir);
        //2.File转换为URI
        URI uri = file.toURI();
        URL[] urls = {uri.toURL()};
        //3.创建类加载器
        FileUrlClassLoader loader = new FileUrlClassLoader(urls);

        try {
            //4.加载指定的class文件
            Class<?> aClass = loader.loadClass("TestObj");
            Object o = aClass.newInstance();
            Method method = aClass.getMethod("print");
            method.invoke(o);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}