package indi.mozping.ch06.clazzloader;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author by mozping
 * @Classname FileUrlClassLoader
 * @Description 自定义文件类加载器，从文件中读取字节码文件，转化为Class对象
 * 继承自URLClassLoader来自定义类加载器，这样不需要重写findClass方法,非常简单
 * @Date 2019/9/16 12:24
 */
public class FileUrlClassLoader extends URLClassLoader {

    public FileUrlClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    /**
     * 注意自定义的类加载器的parent父类加载器是AppClassLoader
     * 在ClassLoader的构造方法中可以看到，
     * protected ClassLoader() {
     * this(checkCreateClassLoader(), getSystemClassLoader());
     * }
     */
    public FileUrlClassLoader(URL[] urls) {
        super(urls);
    }
}