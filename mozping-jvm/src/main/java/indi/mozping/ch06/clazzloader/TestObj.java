package indi.mozping.ch06.clazzloader;

/**
 * @author by mozping
 * @Classname TestObj
 * @Description TODO
 * @Date 2019/9/16 11:31
 */
public class TestObj {

    Object ins;
    Integer flag = 0;

    public void setIns(Object ins) {
        this.ins = ins;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public void print() {
        System.out.println("TestObj ... 自己：" + flag);
    }

    @Override
    public String toString() {
        return "TestObj{" +
                "ins=" + ins +
                ", flag=" + flag +
                '}';
    }
}