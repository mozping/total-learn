package indi.mozping.ch06.clazzloader.test;

import indi.mozping.ch06.clazzloader.MyClassLoader1;
import indi.mozping.ch06.clazzloader.MyClassLoader2;

import java.lang.reflect.Method;

/**
 * @author by mozping
 * @Classname MyClassLoaderTest
 * @Description TODO
 * @Date 2019/9/16 20:02
 */
public class MyClassLoaderTest {
    public static void main(String[] args) throws ClassNotFoundException {
        //1.创建自定义文件类加载器
        String path = "E:/";
        MyClassLoader1 loader1 = new MyClassLoader1(path);
        MyClassLoader2 loader2 = new MyClassLoader2(path);

        try {
            //2.加载指定的class文件
            Class<?> aClass1 = loader1.loadClass("TestObj");
            Object obj1 = aClass1.newInstance();
            Method method1 = aClass1.getMethod("print");
            method1.invoke(obj1);

            System.out.println("---------------" + obj1.getClass());

            Class<?> aClass2 = loader1.loadClass("TestObj");
            Object obj2 = aClass2.newInstance();
            Method method2 = aClass2.getMethod("print");
            method2.invoke(obj2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}