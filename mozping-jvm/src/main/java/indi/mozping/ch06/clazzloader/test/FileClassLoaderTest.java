package indi.mozping.ch06.clazzloader.test;

import indi.mozping.ch06.clazzloader.FileClassLoader;

import java.lang.reflect.Method;

/**
 * @author by mozping
 * @Classname FileUrlClassLoaderTest
 * @Description TODO
 * @Date 2019/9/16 12:40
 */
public class FileClassLoaderTest {

    public static void main(String[] args) throws ClassNotFoundException {
        //1.创建自定义文件类加载器
        String path = "E:/";
        FileClassLoader loader = new FileClassLoader(path);
        FileClassLoader loader2 = new FileClassLoader(path);

        try {
            //2.加载指定的class文件
            // Class<?> aClass = loader.loadClass("TestObj");
            System.out.println("-----先打印加载得到得两个类的基本信息------------");
            Class<?> aClass = loader.loadClass("TestObj");
            Object obj = aClass.newInstance();
            Method method = aClass.getMethod("print");
            method.invoke(obj);

            Class<?> aClass2 = loader2.loadClass("TestObj");
            Object obj2 = aClass2.newInstance();
            Method method2 = aClass2.getMethod("print");
            method2.invoke(obj2);

            Method[] methods = aClass.getMethods();
            Method[] methods2 = aClass2.getMethods();
            System.out.println("------打印1的方法-----------");
            for (Method m : methods) {
                System.out.println(m);
            }
            System.out.println("------打印2的方法-----------");
            for (Method mm : methods2) {
                System.out.println(mm);
            }
            System.out.println("------打印结束-----------\n\n\n");


            System.out.println("-----反射给两个实例赋不同的值------------");
            Method setFlag1 = aClass.getMethod("setFlag", Integer.class);
            setFlag1.invoke(obj, 100);
            method.invoke(obj);

            Method setFlag2 = aClass2.getMethod("setFlag", Integer.class);
            setFlag2.invoke(obj2, 200);
            method2.invoke(obj2);
            System.out.println("-----反射赋值结束------------\n\n\n");

            System.out.println("-----通过反射让两个实例相互引用------------");
            Method setIns1 = aClass.getMethod("setIns", Object.class);

            setIns1.invoke(obj, obj2);
            method.invoke(obj);
            //System.out.println(obj.toString());;

            Method setIns2 = aClass2.getMethod("setIns", Object.class);
            setIns2.invoke(obj2, obj);
            method2.invoke(obj2);
            //method2.invoke(obj2);
            System.out.println("-----两个实例相互引用结束------------");


            System.out.println(aClass.hashCode() + " -- " + aClass2.hashCode());
            System.out.println(aClass.equals(aClass2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}