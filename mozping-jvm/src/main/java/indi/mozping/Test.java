package indi.mozping;

import java.lang.String;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/8/30 9:54
 */
public class Test {

    private static AtomicInteger integer = new AtomicInteger(0);
    private static int[] numArr = new int[]{1, 2, 3};
    private static char[] charArr = new char[]{'a', 'b', 'c'};

    public static void main(String[] args) {
        new Thread(new Run1()).start();
        new Thread(new Run2()).start();
    }

    static class Run1 implements Runnable {

        public void run() {
            int index = 0;
            while (true) {
                if (index >= numArr.length) {
                    break;
                }
                if (integer.get() % 2 == 0) {
                    System.out.print(numArr[index++]);
                    integer.getAndIncrement();
                }
            }
        }
    }

    static class Run2 implements Runnable {

        public void run() {
            int index = 0;
            while (true) {
                if (index >= charArr.length) {
                    break;
                }
                if (integer.get() % 2 == 1) {
                    System.out.print(charArr[index++]);
                    integer.getAndIncrement();
                }
            }
        }
    }
}
