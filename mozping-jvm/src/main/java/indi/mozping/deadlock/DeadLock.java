package indi.mozping.deadlock;

import java.lang.Object;
import java.lang.String;

/**
 * @author by mozping
 * @Classname deadLock
 * @Description 死锁
 * @Date 2019/8/27 15:42
 */
public class DeadLock {

    private static Object lock1 = new Object();
    private static Object lock2 = new Object();

    public static void main(String[] args) {
        new Thread(new MyThread(lock1, lock2), "thread-1").start();
        new Thread(new MyThread(lock2, lock1), "thread-2").start();
    }

    static class MyThread implements Runnable {

        Object lockBefore;
        Object lockAfter;

        public MyThread(Object lockBefore, Object lockAfter) {
            this.lockBefore = lockBefore;
            this.lockAfter = lockAfter;
        }

        public void run() {
            while (true) {
                synchronized (lockBefore) {
                    System.out.println("Thread [ " + Thread.currentThread().getName() + " ] 获取lockBefore锁成功...");
                    synchronized (lockAfter) {
                        System.out.println("Thread [ " + Thread.currentThread().getName() + " ] 获取全部锁成功...");
                    }
                }
            }
        }
    }
}