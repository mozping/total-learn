package indi.mozping.mem;

import java.lang.String;

/**
 * @author by mozping
 * @Classname GetMem
 * @Description TODO
 * @Date 2019/8/13 16:39
 */
public class GetMem {

    public static void main(String[] args) {

        new GetMem().getMem();
    }

    void getMem() {
        long maxMemory = Runtime.getRuntime().maxMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        int availableProcessors = Runtime.getRuntime().availableProcessors();

        System.out.println("maxMemory :" + maxMemory / 1024 / 1024);
        System.out.println("freeMemory :" + freeMemory / 1024 / 1024);
        System.out.println("totalMemory :" + totalMemory / 1024 / 1024);
        System.out.println("availableProcessors :" + availableProcessors);

    }


}