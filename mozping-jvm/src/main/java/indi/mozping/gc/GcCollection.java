package indi.mozping.gc;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by mozping
 * @Classname GcCollection
 * @Description TODO
 * @Date 2019/8/13 17:32
 */
public class GcCollection {

    private static final int SIZE = 1024 * 1024;

    /**
     * -Xmx2g -Xms2g  -XX:+PrintGCDetails
     * -XX:+PrintGCDetails  -XX:+PrintCommandLineFlags 打印内存信息，打印JVM参数
     * 依次指定不同的垃圾收集器，查看日志中打印的垃圾收集器信息
     * -XX:+UseSerialGC
     * -XX:+UseParallelGC
     * -XX:+UseParNewGC
     * -XX:+UseConcMarkSweepGC
     * -XX:+UseParallelOldGC
     * -XX:+UseG1GC
     */
    public static void main(String[] args) throws InterruptedException {

        List<GarbageCollectorMXBean> beans = ManagementFactory.getGarbageCollectorMXBeans();
        for (GarbageCollectorMXBean bean : beans) {
            System.out.println(bean.getName());
        }
        ArrayList<Object> list = new ArrayList();
        for (int i = 0; ; i++) {
            if (i % 100 == 0) {
                list.add(new int[1][1024]);
            }
            int[][] ints = new int[1024][1024];
            Thread.sleep(1);
        }
    }
}