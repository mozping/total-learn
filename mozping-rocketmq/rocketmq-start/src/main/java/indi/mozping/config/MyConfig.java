package indi.mozping.config;

/**
 * @author by mozping
 * @Classname Config
 * @Description TODO
 * @Date 2020/9/30 11:00
 */
public class MyConfig {

    /**
     * 端口
     */
//    public static final String NAME_SERVER = "192.168.11.191:9876";
    public static final String NAME_SERVER = "127.0.0.1:9876";

    /**
     * topic,消息依赖于topic
     */
    public static final String TOPIC = "pay_test_topic";

}