package indi.mozping.controller;

import indi.mozping.config.MyConfig;
import indi.mozping.kafka.producer.MyProducer;
import indi.mozping.util.Motto;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mozping
 * @Classname TestController
 * @Description TODO
 * @Date 2020/9/30 11:05
 */
@Slf4j
@RestController
public class TestController {


    @Autowired
    private MyProducer producer;


    @RequestMapping("/start")
    public Object start() throws Exception {
        Thread.sleep(2000);
        //总共发送五次消息
        for (int i = 0; i < 10; i++) {
            //创建消息
            Message message = new Message(MyConfig.TOPIC, "tag" + i, Motto.randomMotto().getBytes());
            //生产发送
            SendResult sendResult = producer.getProducer().send(message);
            log.info("输出生产者信息={}", sendResult);
            Thread.sleep(1000);
        }
        return "成功";
    }
}