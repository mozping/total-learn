package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsgQueueConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsgQueueConsumerApplication.class, args);
    }

}
