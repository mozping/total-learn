package indi.mozping.utils;

/**
 * @author by mozping
 * @Classname StrUtils
 * @Description TODO
 * @Date 2019/1/8 19:01
 */
public class StrUtils {

    public static int getOffset(String message) {
        int begin = message.indexOf("=") + 1;
        int end = message.indexOf(",");
        int offset;
        try {
            offset = Integer.parseInt(message.substring(begin, end));
        } catch (Exception e) {
            offset = 1;
        }
        return offset;
    }

}
