/*
 *文件名： Sink.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： Description
 *创建人：mozping
 *创建时间： 2018/8/13 9:21
 *修改理由：
 *修改内容：
 */
package indi.mozping.kafka.pc;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * MQ入口，监听mq消息接口
 *
 * @author mozping
 * @version 1.0
 * @date 2018/8/13 9:21
 * @see MqSinkI
 * @since JDK1.8
 */
public interface MqSinkI {

    String INPUT_CHANNEL = "myInputChannel";

    /**
     * 消费者接口
     *
     * @return org.springframework.messaging.SubscribableChannel 接口对象
     */
    @Input(MqSinkI.INPUT_CHANNEL)
    SubscribableChannel inputChannel();

}