package indi.mozping.kafka.utils;

import org.springframework.messaging.Message;

/**
 * @author by mozping
 * @Classname MsgFilter
 * @Description TODO
 * @Date 2019/1/8 18:51
 */
public interface MsgFilter<T> {

    boolean filter(Message<T> message) throws Exception;
}
