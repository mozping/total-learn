/*
 *文件名： MySource.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： Description
 *创建人：mozping
 *创建时间： 2018/8/29 12:23
 *修改理由：
 *修改内容：
 */
package indi.mozping.kafka.producer;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author mozping
 * @version 1.0
 * @date 2018/8/29 12:23
 * @see MySource
 * @since JDK1.8
 */
public interface MySource {

    String OUTPUT = "myOutputChannel";


    /**
     * kafka发送消息方法
     *
     * @param
     * @return org.springframework.messaging.MessageChannel
     * @throws
     * @see
     */
    @Output(MySource.OUTPUT)
    MessageChannel outPutChannel();
}