package indi.mozping.kafka.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname MyMsgFilter
 * @Description TODO
 * @Date 2019/1/8 18:52
 */
@Component
public class MyMsgFilter implements MsgFilter<JSONObject> {

    @Override
    public boolean filter(Message<JSONObject> message) throws Exception {

//        if (message.toString().length() > 10) {
//            throw new Exception("break.................");
//        }

        String msgData = message.getPayload().toString();
        String msgHead = message.getHeaders().toString();

        if (StrUtils.getOffset(msgHead) % 3 == 0) {
            return true;
        }
        return false;
    }
}
