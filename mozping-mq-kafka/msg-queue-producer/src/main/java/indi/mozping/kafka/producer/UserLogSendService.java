package indi.mozping.kafka.producer;//package com.intellif.mozping.kafka.producer;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.stream.annotation.EnableBinding;
//import org.springframework.messaging.support.MessageBuilder;
//
///**
// * @author Liangzhifeng
// * date: 2018/11/19
// */
//@Slf4j
//@EnableBinding(UserLogSource.class)
//public class UserLogSendService {
//
//    @Autowired
//    private UserLogSource mySource;
//
//    public void sendMessage(String msg) {
//        try {
//            boolean send = mySource.output().send(MessageBuilder.withPayload(msg).build());
//            log.info("发送Kafka消息,msg={},send={}", msg, send);
//        } catch (Exception e) {
//            log.error("发送kafka消息失败msg={}", msg, e);
//        }
//    }
//}
