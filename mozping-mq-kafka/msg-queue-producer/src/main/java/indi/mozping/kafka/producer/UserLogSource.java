package indi.mozping.kafka.producer;//package com.intellif.mozping.kafka.producer;
//
//
//import org.springframework.cloud.stream.annotation.Output;
//import org.springframework.messaging.MessageChannel;
//
///**
// * @author Liangzhifeng
// * date: 2018/11/19
// */
//public interface UserLogSource {
//
//    /**
//     * 发送kafka消息
//     * @return
//     */
//    @Output(value = "oauthUserLog")
//    MessageChannel output();
//}
