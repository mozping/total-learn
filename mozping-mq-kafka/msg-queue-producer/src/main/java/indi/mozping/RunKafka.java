package indi.mozping;

import indi.mozping.kafka.producer.ProducerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author intellif
 */
@Component
public class RunKafka implements CommandLineRunner {

    @Autowired
    ProducerController producerController;

    @Override
    public void run(String... args) throws Exception {
        producerController.send();
    }
}
