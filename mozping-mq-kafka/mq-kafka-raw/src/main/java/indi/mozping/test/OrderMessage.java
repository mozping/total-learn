package indi.mozping.test;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 订单数据信息
 *
 * @author chase.yang
 * @date 2020/10/29
 */
@Data
public class OrderMessage implements Serializable {
    /**
     * 订单id
     */
    @JSONField(name = "order_id")
    private Long orderId;

    /**
     * 用户id
     */
    @JSONField(name = "user_id")
    private Long userId;

    /**
     * 距离
     */
    private Integer distance;

    /**
     * 订单日期
     */
    @JSONField(name = "order_date_time")
    private String orderDateTime;

    /**
     * 企业id
     */
    @JSONField(name = "ep_id")
    private Long epId;

    /**
     * 总金额
     */
    @JSONField(name = "total_price_fen")
    private Integer totalPriceFen;

    /**
     * 支付金额
     */
    @JSONField(name = "pay_price_fen")
    private Integer payPriceFen;

    /**
     * 基础金额
     */
    @JSONField(name = "base_price_fen")
    private Integer basePriceFen;

    /**
     * 订单状态
     */
    @JSONField(name = "order_status")
    private Integer orderStatus;

    /**
     * 支付类型
     */
    @JSONField(name = "pay_type")
    private Integer payType;

    /**
     * 订单UUID
     */
    @JSONField(name = "order_uuid")
    private String orderUuid;

    /**
     * 订单创建时间
     */
    @JSONField(name = "order_create_time")
    private String orderCreateTime;

    /**
     * 订单创建时间
     */
    @JSONField(name = "create_time")
    private String createTime;

    /**
     * 订单类型
     */
    @JSONField(name = "order_type")
    private String orderType;

    /**
     * 客户端类型
     */
    @JSONField(name = "client_type")
    private String clientType;

    /**
     * 城市id
     */
    @JSONField(name = "city_id")
    private String cityId;

    /**
     * 车辆特征，0：小车，1：大车
     */
    @JSONField(name = "vehicle_attr")
    private Integer vehicleAttr;

    /**
     * 订单车型id
     */
    @JSONField(name = "order_vehicle_id")
    private String orderVehicleId;

    /**
     * 坐标经纬度
     */
    @JSONField(name = "latlong")
    private String latLong;

    /**
     * 总距离
     */
    @JSONField(name = "total_distance")
    private Integer totalDistance;

    /**
     * 零担标识
     */
    @JSONField(name = "business_type")
    private Integer businessType;

    /**
     * 一口价
     */
    @JSONField(name = "hit_one_price")
    private Integer hitOnePrice;

    /**
     * 订单事件创建时间戳
     */
    private Long actionCreateTime;

    /**
     * 订单转送平台方式：0-所有司机/手动转平台，1-所有收藏司机，2-自动收藏转平台，3-已废弃，4-指定收藏司机
     */
    @JSONField(name = "order_fleet_status")
    private Integer orderFleetStatus;

    /**
     * 是否清单小分队标记，true: 是
     */
    @JSONField(name = "is_clean_assign")
    private Boolean isCleanAssign;
}
