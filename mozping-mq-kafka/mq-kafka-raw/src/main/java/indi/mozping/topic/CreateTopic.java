package indi.mozping.topic;

import kafka.admin.TopicCommand;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * @author by mozping
 * @Classname CreateTopic
 * @Description TODO
 * @Date 2019/7/22 11:19
 */
public class CreateTopic {

    public static void main(String[] args) {
        //create();
        createWithAdminApi();

    }

    private static void createWithAdminApi() {
        Properties properties = new Properties();
        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.11.27:9092");
        properties.put(AdminClientConfig.REQUEST_TIMEOUT_MS_CONFIG, 10000);
        AdminClient adminClient = AdminClient.create(properties);
        NewTopic newTopic = new NewTopic("topic-test-1", 1, (short) 1);
        CreateTopicsResult createTopicsResult = adminClient.createTopics(Collections.singleton(newTopic));
        try {
            Void aVoid = createTopicsResult.all().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static void create() {
        String[] options = new String[]{
                "--zookeeper", "192.168.11.27:2181",
                "--create",
                "--replication-factor", "1",
                "--partitions", "1",
                "--topic", "topic-1"
        };

        TopicCommand.main(options);
    }
}