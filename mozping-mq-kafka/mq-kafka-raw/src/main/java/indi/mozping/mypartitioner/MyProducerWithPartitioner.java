package indi.mozping.mypartitioner;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

/**
 * @author by mozping
 * @Classname MyKafkaProducer
 * @Description TODO
 * @Date 2019/7/20 9:38
 */
public class MyProducerWithPartitioner {

    public static final String BROKER_LIST = "192.168.13.53:9092";
    public static final String TOPIC = "testTopic-1";
    public static final String CLIENT_ID = "producer-mzp-1";


    public static Properties initConfig() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BROKER_LIST);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, CLIENT_ID);
        properties.put(ProducerConfig.RETRIES_CONFIG, 10);
        properties.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, MyPartitioner.class.getName());
        properties.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 3000);
        return properties;
    }

    public static void main(String[] args) {
        sendAndForget();
    }

    private static void sendAndForget() {
        Properties prop = initConfig();
        KafkaProducer<String, String> producer = new KafkaProducer<>(prop);
        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, "Hello world , MyProducerWithPartitioner !");
        try {
            producer.send(record);
            System.out.println("发送完毕...");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        producer.flush();
        System.out.println("End ... ");
    }
}