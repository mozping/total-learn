package indi.mozping.protobufkafka;

/**
 * @author by mozping
 * @Classname Config
 * @Description TODO
 * @Date 2020/1/2 15:02
 */
public class Config {

    public static final String BROKER_LIST = "192.168.13.99:9092";
    public static final String TOPIC_NAME = "mzpTestTopic-1";
    public static final String GROUP_ID = "mzpTestTopic-1";

}