package indi.mozping.protobufkafka.inter;

public interface Protobufable {

    //将对象转为字节数组
    public byte[] encode();

}
