package indi.mozping.MySerilizable;

import lombok.Data;

/**
 * @author by mozping
 * @Classname Item
 * @Description TODO
 * @Date 2019/7/20 12:29
 */
@Data
public class Item {

    String name;
    String detail;

    public Item(String name, String detail) {
        this.name = name;
        this.detail = detail;
    }
}