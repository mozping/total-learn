package indi.mozping.MySerilizable;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

/**
 * @author by mozping
 * @Classname MyKafkaProducer
 * @Description TODO
 * @Date 2019/7/20 9:38
 */
public class KafkaProducerWithSerializer {

    public static final String BROKER_LIST = "192.168.13.53:9092";
    public static final String TOPIC = "testTopic-1";
    public static final String CLIENT_ID = "producer-mzp-1";


    public static Properties initConfig() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BROKER_LIST);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ItemSerilializer.class.getName());
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, CLIENT_ID);
        properties.put(ProducerConfig.RETRIES_CONFIG, 10);
        return properties;
    }

    public static void main(String[] args) {
        sendAndForget();
    }

    private static void sendAndForget() {
        Properties prop = initConfig();
        KafkaProducer<String, Item> producer = new KafkaProducer<>(prop);
        ProducerRecord<String, Item> record = new ProducerRecord<>(TOPIC, new Item("mozping", "hello!"));
        try {
            producer.send(record);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        producer.flush();
        System.out.println("End ... ");
    }

}