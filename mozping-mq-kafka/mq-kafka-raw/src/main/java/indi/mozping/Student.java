package indi.mozping;

import lombok.Data;

/**
 * @author moore.mo
 * @description indi.mozping Student
 * @date 2021/1/7 2:04 下午
 **/
@Data
public class Student {

    private int id;
    private String name;

    public static void main(String[] args) {
        long timeMillis = System.currentTimeMillis();
        System.out.println(timeMillis);
    }
}

