package indi.mozping;

import java.sql.*;

public class TestBatchGet {

    private static String url = "jdbc:pivotal:greenplum://192.168.13.51:5432;DatabaseName=db1;user=gpadmin;password=gpadmin";
    public static final int SIZE = 5000000;
    private static final int READ_SIZE = 4000;
    public static final int THREAD_NUM = 20;
    private static String table = "public.tface";

    public static void main(String[] args) throws InterruptedException {
        long start;
        long end;
        Thread.sleep(2000);
        Connection conn = null;
        try {
            conn = getConn(url);
            start = System.currentTimeMillis();
            System.out.println("read:" + start);
            for (int i = 0; i < 3; i++) {
                readFromDb(conn, 0, READ_SIZE);
                System.out.println(System.currentTimeMillis());
            }
            end = System.currentTimeMillis();
            System.out.println("read end:" + ((end - start) / 3));
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void readFromDb(Connection conn, long from, int len) throws SQLException {
        String sql = "select * from " + table + "WHERE time between ?" +
                "AND  ?   limit ? offset ? ";

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = (PreparedStatement) conn.prepareStatement(sql);
            pstmt.setDate(1, new Date(1561910400000L));
            pstmt.setDate(2, new Date(1562083200000L));
            pstmt.setInt(3, len);
            pstmt.setLong(4, from);
            rs = pstmt.executeQuery();
            int n = 0;
            while (rs.next()) {
                n++;
            }
            System.out.println(n);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static Connection getConn(String url) throws ClassNotFoundException, SQLException {
        Connection conn = null;

        if (url.contains("pivotal:greenplum")) {
            Class.forName("com.pivotal.jdbc.GreenplumDriver");
        } else if (url.contains("mysql")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } else {
            throw new RuntimeException("unknown url:" + url);
        }
        conn = DriverManager.getConnection(url);
        return conn;
    }

}
