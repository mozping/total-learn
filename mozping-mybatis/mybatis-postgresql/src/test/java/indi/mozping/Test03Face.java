package indi.mozping;

import indi.mozping.dao.FaceMapper;
import indi.mozping.entity.Face;
import indi.mozping.entityfactory.RandomObjFactory;
import indi.mozping.entityfactory.Util;
import indi.mozping.querybean.BaseQueryBean;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ��postgreSql�������
 * @Date 2019/5/9 19:00
 */
public class Test03Face {

    private static final String CONFIG_FILE_PATH = "mybatis/postgresql-config.xml";

    @Test
    public void addFace() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        Face face = RandomObjFactory.getRandomFace();

        FaceMapper faceMapper = sqlSession.getMapper(FaceMapper.class);

        int rowAffected = faceMapper.addFace(face);
        System.out.println("The rows be affected :" + rowAffected);

        //��ʾ�ύ����
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

    @Test
    public void addFaceBatch() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        ArrayList<Face> list = new ArrayList<>(800);

        for (int i = 0; i < 800; i++) {
            list.add(RandomObjFactory.getRandomFace());
        }

        FaceMapper faceMapper = sqlSession.getMapper(FaceMapper.class);

        int rowAffected = faceMapper.addFaceBatch(list);
        System.out.println("The rows be affected :" + rowAffected);

        //��ʾ�ύ����
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

    @Test
    public void addSomeBatch() {
        long begin = System.currentTimeMillis();
        for (int i = 1; i <= 10; i++) {
            addFaceBatch();
        }
        System.out.println(System.currentTimeMillis() - begin);
    }

    @Test
    public void queryBatch() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        Face face = RandomObjFactory.getRandomFace();

        FaceMapper faceMapper = sqlSession.getMapper(FaceMapper.class);
        BaseQueryBean baseQueryBean = new BaseQueryBean();
        baseQueryBean.setLimitNum(5000);
        baseQueryBean.setBeginTime(Util.convertStringToDate("2019-10-01 00:00:00"));
        baseQueryBean.setEndTime(Util.convertStringToDate("2019-10-02 00:00:00"));
        long begin = System.currentTimeMillis();
        List<Face> list = faceMapper.queryBatch(baseQueryBean);
        System.out.println("The rows be affected :" + list.size() + " Time:" + (System.currentTimeMillis() - begin));

        //��ʾ�ύ����
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

}