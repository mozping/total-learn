package indi.mozping;

import indi.mozping.entityfactory.Util;
import indi.mozping.util.DateUtil;
import org.junit.Test;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ��postgreSql�������
 * @Date 2019/5/9 19:00
 */
public class DateTest {

    private static final Date DATE_REFER = Util.convertStringToDate("2019-01-04 01:00:00");

    @Test
    public void test1() {
        Date d1 = Util.convertStringToDate("2019-01-01 05:01:02");
        Date d2 = Util.convertStringToDate("2019-01-02 03:01:02");
        System.out.println(DateUtil.getDayGap(d1, d2));
    }

    @Test
    public void test2() {
        Date d1 = Util.convertStringToDate("2019-01-01 05:01:02");
        System.out.println(DateUtil.getDateIncSomeDay(d1, 5));
    }

    @Test
    public void test3() {
        Date d1 = Util.convertStringToDate("2019-01-01 05:01:02");
        System.out.println(DateUtil.getVertialDate(d1, DATE_REFER));
    }


}