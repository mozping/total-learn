package indi.mozping;

import indi.mozping.dao.CarMapper;
import indi.mozping.entity.Car;
import indi.mozping.entityfactory.RandomObjFactory;
import indi.mozping.entityfactory.RandomUtil;
import indi.mozping.entityfactory.Util;
import indi.mozping.querybean.CarNumBean;
import indi.mozping.querybean.ColorQueryBean;
import indi.mozping.querybean.FieldQueryBean;
import indi.mozping.querybean.MultiFieldQueryBean;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ��postgreSql�������
 * @Date 2019/5/9 19:00
 */
public class Test02Car {

    private static final String CONFIG_FILE_PATH = "mybatis/postgresql-config.xml";

    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        Car car = RandomObjFactory.getRandomCar();

        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);

        int rowAffected = carMapper.addCar(car);
        System.out.println("The rows be affected :" + rowAffected);

        //��ʾ�ύ����
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

    @Test
    public void addBatch() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        ArrayList<Car> list = new ArrayList<>(800);

        for (int i = 0; i < 800; i++) {
            list.add(RandomObjFactory.getRandomCar());
        }

        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);

        int rowAffected = carMapper.addCarBatch(list);
        System.out.println("The rows be affected :" + rowAffected);

        //��ʾ�ύ����
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

    @Test
    public void addSomeBatch() {
        long begin = System.currentTimeMillis();
        for (int i = 1; i < 3500; i++) {
            addBatch();
        }
        System.out.println(System.currentTimeMillis() - begin);
    }

    public static void queryColor() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();

        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        ColorQueryBean colorQueryBean = new ColorQueryBean();
        colorQueryBean.setColor("blue");
        colorQueryBean.setLimitNum(1);
        //"yyyy-MM-dd HH:mm:ss"
        colorQueryBean.setBeginTime(Util.convertStringToDate("2019-07-01 0:0:0"));
        colorQueryBean.setEndTime(Util.convertStringToDate("2020-06-30 0:0:0"));

        long begin = System.currentTimeMillis();

        ArrayList<Car> cars = carMapper.queryCarByColor(colorQueryBean);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

    private static ArrayList<Long> times = new ArrayList<>(100);
    private static int concurrence = 10;
    private static final CountDownLatch latch = new CountDownLatch(concurrence);

    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < concurrence; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    queryColor();
                    //queryField();
                    //queryFieldMulti();
                    //queryCarNum();
                    //queryCarIds1000();
                    //queryCarIds2000();
                    queryCarIds10000();
                }
            }).start();
        }
        latch.await();

        long total = 0;
        for (long time : times) {
            total += time;
        }
        System.out.println("Size is :" + times.size() + " ,Average Time is : " + (total / concurrence) + " ms");

    }

    @Test
    public static void queryCarIds2000() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        ArrayList<String> ids = Util.ids_2000;
        long begin = System.currentTimeMillis();
        List<Car> cars = carMapper.selectCarById(ids);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    @Test
    public static void queryCarIds1000() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        ArrayList<String> ids = Util.ids_1000;
        long begin = System.currentTimeMillis();
        List<Car> cars = carMapper.selectCarById(ids);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    @Test
    public static void queryCarIds10000() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        ArrayList<String> ids = Util.ids_10000;
        long begin = System.currentTimeMillis();
        List<Car> cars = carMapper.selectCarById(ids);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    public void queryCarNum() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        long begin = System.currentTimeMillis();
        CarNumBean carNumBean = new CarNumBean();
        carNumBean.setCarNum("��A1");
        carNumBean.setLimitNum(1000);
        carNumBean.setBeginTime(Util.convertStringToDate("2019-07-01 00:00:00"));
        carNumBean.setEndTime(Util.convertStringToDate("2019-10-01 00:00:00"));
        //List<Car> cars = carMapper.selectCarNumPrefix(carNumBean);
        List<Car> cars = carMapper.selectCarNum(carNumBean);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    public void queryFieldMulti() {

        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        MultiFieldQueryBean multiFieldQueryBean = new MultiFieldQueryBean();
        multiFieldQueryBean.setColor("blue");
        multiFieldQueryBean.setLimitNum(1000);
        multiFieldQueryBean.setPlateColor("blue");
        multiFieldQueryBean.setCarType("minivan");
        multiFieldQueryBean.setBrandCode("12171112");
        multiFieldQueryBean.setPlateNumber("��AMYOT9");
        multiFieldQueryBean.setBeginTime(Util.convertStringToDate("2019-07-01 00:00:00"));
        multiFieldQueryBean.setEndTime(Util.convertStringToDate("2020-06-01 00:00:00"));
        long begin = System.currentTimeMillis();

        List<Car> cars = carMapper.selectCarByMultiField(multiFieldQueryBean);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }


    public void queryField() {

        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        FieldQueryBean fieldQueryBean = new FieldQueryBean();
        fieldQueryBean.setColor("blue");
        fieldQueryBean.setLimitNum(1000);
        fieldQueryBean.setPlateColor("blue");
        fieldQueryBean.setCarType("minivan");
        fieldQueryBean.setBeginTime(Util.convertStringToDate("2019-07-01 00:00:00"));
        fieldQueryBean.setEndTime(Util.convertStringToDate("2020-06-01 00:00:00"));
        long begin = System.currentTimeMillis();

        List<Car> cars = carMapper.selectCarByField(fieldQueryBean);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }


    @Test
    public void queryIds() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();

        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ids.add(RandomUtil.numId(15));
        }
        long begin = System.currentTimeMillis();
        System.out.println("Begin:" + begin);
        List<Car> cars = carMapper.selectCarById(ids);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
//        for (Car car : cars) {
//            System.out.println(car);
//        }
        latch.countDown();
        System.out.println(cars.size() + "\n");
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

}