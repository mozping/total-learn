package indi.mozping;

import indi.mozping.dao.CarMapper;
import indi.mozping.entityfactory.Util;
import indi.mozping.querybean.ColorQueryBean;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ��postgreSql�������
 * @Date 2019/5/9 19:00
 */
public class Test03Car {

    private static final String CONFIG_FILE_PATH = "mybatis/postgresql-config.xml";


    public static void queryColorExplain() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();

        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        ColorQueryBean colorQueryBean = new ColorQueryBean();
        colorQueryBean.setColor("blue");
        colorQueryBean.setLimitNum(50);
        colorQueryBean.setBeginTime(Util.convertStringToDate("2019-01-01 0:0:0"));
        colorQueryBean.setEndTime(Util.convertStringToDate("2019-01-07 0:0:0"));

        long begin = System.currentTimeMillis();

        carMapper.queryCarByColorExplain(colorQueryBean);
        long time = System.currentTimeMillis() - begin;
        System.out.println("��ʱ��" + time + " ms");
        times.add(time);
        latch.countDown();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }

    private static ArrayList<Long> times = new ArrayList<>(100);
    private static int concurrence = 1;
    private static final CountDownLatch latch = new CountDownLatch(concurrence);

    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < concurrence; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    queryColorExplain();

                }
            }).start();
        }
        latch.await();

        long total = 0;
        for (long time : times) {
            total += time;
        }
        System.out.println("Size is :" + times.size() + " ,Average Time is : " + (total / concurrence) + " ms");

    }


}