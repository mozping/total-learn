package indi.mozping;

import indi.mozping.dao.UserMapper;
import indi.mozping.entity.User;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ��postgreSql�������
 * @Date 2019/5/9 19:00
 */
public class Test01 {

    private static final String CONFIG_FILE_PATH = "mybatis/postgresql-config.xml";

    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        User user = new User();
        user.setUname("testName");

        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        int rowAffected = userMapper.addUser(user);
        //int rowAffected = userMapper.addUserGetId(user);
        System.out.println("The rows be affected :" + rowAffected);
        System.out.println("The primary key is:" + user.getId());
        //��ʾ�ύ����
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    @Test
    public void test1() {
        System.out.println(System.currentTimeMillis());
        System.out.println(System.currentTimeMillis() / 1000);

    }


}