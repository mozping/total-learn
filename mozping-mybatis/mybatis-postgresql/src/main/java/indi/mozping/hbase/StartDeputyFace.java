package indi.mozping.hbase;


import indi.mozping.dao.DeputyFaceMapper;
import indi.mozping.entity.DeputyFace;
import indi.mozping.hbase.tophoenix.ReadWriteDeputyFace;
import indi.mozping.util.GlobalConst;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

/**
 * @author by mozping
 * @Classname StartDeputyFace
 * @Description TODO
 * @Date 2020/04/09 14:24
 */
public class StartDeputyFace {


    private static final Logger LOG = LoggerFactory.getLogger(StartDeputyFace.class);

    public static void main(String[] args) throws InterruptedException, SQLException {

        //read1();
        //read100();
        List<DeputyFace> deputyFaces = select100();
        long begin = System.currentTimeMillis();
        insert2HbaseBatch(10000, deputyFaces);
        System.out.println("consume time: " + (System.currentTimeMillis() - begin));

    }

    static void read1() throws InterruptedException {
        Thread.sleep(500);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_FILE_PATH_GP).openSession();
        DeputyFaceMapper deputyFaceMapper = sqlSession.getMapper(DeputyFaceMapper.class);

        long begin = System.currentTimeMillis();
        DeputyFace deputyFace = deputyFaceMapper.selectDeputyFaceById(658952313741906316L);
        LOG.info("deputyFace:" + deputyFace);
        long end = System.currentTimeMillis();
        LOG.info("��ʱ: " + (end - begin) + " ms");

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    static void insert2HbaseBatch(int num, List<DeputyFace> deputyFaces) throws SQLException {
        long begin = System.currentTimeMillis();
        for (int i = 0; i < num; i++) {
            deal(deputyFaces);
            System.out.println("BatchSize:" + deputyFaces.size() + " -> Num: " + i);
            if (num % 10 == 0) {
                System.out.println("To [ " + i + " ] consume time: " + (System.currentTimeMillis() - begin));
            }
        }
    }

    private static List<DeputyFace> select100() throws InterruptedException, SQLException {
        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_DEPUTYFACE_PATH_GP).openSession();
        DeputyFaceMapper deputyFaceMapper = sqlSession.getMapper(DeputyFaceMapper.class);

        long beginIndex = 657564975308607630L;
        List<DeputyFace> deputyFaceList = deputyFaceMapper.selectDeputyFaceByIdBatch100(beginIndex);
        return deputyFaceList;
    }

    private static void read100() throws InterruptedException, SQLException {
        //Thread.sleep(500);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_DEPUTYFACE_PATH_GP).openSession();
        DeputyFaceMapper deputyFaceMapper = sqlSession.getMapper(DeputyFaceMapper.class);

//        long beginIndex = 657950055252234240L;
        long beginIndex = 657564975308607630L;


        int count = 0;
        long begin1 = System.currentTimeMillis();
        while (true) {
            //Thread.sleep(100);
            long begin = System.currentTimeMillis();
            List<DeputyFace> deputyFaceList = deputyFaceMapper.selectDeputyFaceByIdBatch100(beginIndex);
            LOG.info("pull size:" + deputyFaceList.size());
            long end = System.currentTimeMillis();
            LOG.info("��ʱ: " + (end - begin) + " ms");

            deputyFaceList.sort(Comparator.comparingLong(DeputyFace::getId));

            for (DeputyFace deputyFace : deputyFaceList) {
                LOG.info("id: " + deputyFace.getId());
            }
            beginIndex = deputyFaceList.get(deputyFaceList.size() - 1).getId();
            LOG.info("id: " + beginIndex);
            deal(deputyFaceList);
            count += deputyFaceList.size();
            if (deputyFaceList.size() < 101) {
                break;
            }
        }
        System.out.println("Time : " + (System.currentTimeMillis() - begin1));
        System.out.println("Num : " + count);

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    private static void deal(DeputyFace deputyFace) throws SQLException {
        ReadWriteDeputyFace.writeDeputyFace(deputyFace);
    }

    private static void deal(List<DeputyFace> deputyFaceList) throws SQLException {
        ReadWriteDeputyFace.writeDeputyFace(deputyFaceList);
    }
}
