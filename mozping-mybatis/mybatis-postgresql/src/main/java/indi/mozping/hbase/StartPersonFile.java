package indi.mozping.hbase;

import indi.mozping.dao.PersonfileMapper;
import indi.mozping.entity.PersonFile;
import indi.mozping.hbase.tophoenix.ReadWritePersonFile;
import indi.mozping.util.GlobalConst;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * @author by mozping
 * @Classname StartPersonFile
 * @Description TODO
 * @Date 2020/4/10 9:45
 */
public class StartPersonFile {

    private static final Logger LOG = LoggerFactory.getLogger(StartDeputyFace.class);

    public static void main(String[] args) throws InterruptedException, SQLException {

        //read1();
        read100();

    }


    private static void read100() throws InterruptedException, SQLException {
        //Thread.sleep(200);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_PERSONFILE_PATH_GP).openSession();
        PersonfileMapper personfileMapper = sqlSession.getMapper(PersonfileMapper.class);

        String beginIndex = "4622003119088205824";

        while (true) {
            long begin = System.currentTimeMillis();
            List<PersonFile> personFileList = personfileMapper.selectPersonfileByIdBatch100(beginIndex);
            LOG.info("pull size:" + personFileList.size());
            long end = System.currentTimeMillis();
            LOG.info("��ʱ: " + (end - begin) + " ms");

            personFileList.sort((o1, o2) -> {
                long l1 = new Long(o1.getArchive_id());
                long l2 = new Long(o2.getArchive_id());
                return Long.compare(l1, l2);
            });

            beginIndex = personFileList.get(personFileList.size() - 1).getArchive_id();
            String name = personFileList.get(personFileList.size() - 1).getPerson_name();
            LOG.info("beginIndex: " + beginIndex + " -> " + name);
            deal(personFileList);
//            for (PersonFile personFile : personFileList) {
//                LOG.info(personFile.getArchive_id() +" -> "+ personFile.getPerson_name());
//            }
            if (personFileList.size() < 100) {
                LOG.info("�˳�...");
                break;
            }
        }

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    private static void deal(PersonFile personFile) throws SQLException {
        ReadWritePersonFile.writePersonFile(personFile);
    }

    private static void deal(List<PersonFile> personFileList) throws SQLException {
        ReadWritePersonFile.writePersonFile(personFileList);
    }

    static void read1() throws InterruptedException {
        Thread.sleep(500);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_PERSONFILE_PATH_GP).openSession();
        PersonfileMapper personfileMapper = sqlSession.getMapper(PersonfileMapper.class);

        long begin = System.currentTimeMillis();
        PersonFile personFile = personfileMapper.selectPersonfileById("4622003119088205824");
        LOG.info("Personfile:" + personFile);
        long end = System.currentTimeMillis();
        LOG.info("��ʱ: " + (end - begin) + " ms");

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }
}