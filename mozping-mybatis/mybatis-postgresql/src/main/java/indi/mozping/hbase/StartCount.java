package indi.mozping.hbase;

import indi.mozping.dao.EventMapper;
import indi.mozping.entity.Event;
import indi.mozping.hbase.tophoenix.ReadWriteEvent;
import indi.mozping.util.GlobalConst;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * @author by mozping
 * @Classname StartEvent
 * @Description TODO
 * @Date 2020/4/10 9:44
 */
public class StartCount {

    private static final Logger LOG = LoggerFactory.getLogger(StartDeputyFace.class);

    public static void main(String[] args) throws InterruptedException, SQLException {

        //read1();
        read100();

    }


    private static void read100() throws InterruptedException, SQLException {
        //Thread.sleep(200);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_COUNT_PATH_GP).openSession();
        EventMapper eventMapper = sqlSession.getMapper(EventMapper.class);

//        String beginIndex = "1126115838328838";
//        String beginIndex = "2252005975355120";
        String beginIndex = "2533480950313943";

        while (true) {
            long begin = System.currentTimeMillis();
            List<Event> eventList = eventMapper.selectEventByIdBatch100(beginIndex);
            LOG.info("pull size:" + eventList.size());
            long end = System.currentTimeMillis();
            LOG.info("��ʱ: " + (end - begin) + " ms");

            eventList.sort((o1, o2) -> {
                long l1 = new Long(o1.getThumbnail_id());
                long l2 = new Long(o2.getThumbnail_id());
                return Long.compare(l1, l2);
            });

            beginIndex = eventList.get(eventList.size() - 1).getThumbnail_id();
            LOG.info("beginIndex: " + beginIndex);
            deal(eventList);
//            for (Event event : eventList) {
//                LOG.info(event.getThumbnail_id());
//            }
            if (eventList.size() < 100) {
                break;
            }
        }

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    private static void deal(Event event) throws SQLException {
        ReadWriteEvent.writeEvent(event);
    }

    private static void deal(List<Event> eventList) throws SQLException {
        ReadWriteEvent.writeEvent(eventList);
    }

    static void read1() throws InterruptedException {
        Thread.sleep(500);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_FILE_PATH_GP).openSession();
        EventMapper eventMapper = sqlSession.getMapper(EventMapper.class);

        long begin = System.currentTimeMillis();
        Event event = eventMapper.selectEventById("1126115838328838");
        LOG.info("event:" + event);
        long end = System.currentTimeMillis();
        LOG.info("��ʱ: " + (end - begin) + " ms");

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

}