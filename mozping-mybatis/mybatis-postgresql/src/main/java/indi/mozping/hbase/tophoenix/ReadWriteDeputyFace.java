package indi.mozping.hbase.tophoenix;


import indi.mozping.entity.DeputyFace;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.List;


/**
 * @author by mozping
 * @Classname ReadWriteDeputyFace
 * @Description TODO
 * @Date 2020/4/7 17:27
 */
public class ReadWriteDeputyFace {

    private static final String str = "yyyy-MM-dd HH:mm:ss";

    private static Connection connection = null;
    private static PreparedStatement prepareStatement = null;
//    private static final String sql = "upsert into deputyface_bak1 (id,aid,feature_val,image_id,image_url,thumbnail_id,\n" +
//            "thumbnail_url,present_flag,create_time,modify_time,status,source,algo_version,column1,column2,column3,pq_probe,pq_code) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String sql = "upsert into bigdata_mid.mid_bigdata_deputy_face_5030  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    static {
        try {
            Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
            connection = DriverManager.getConnection(Config.CONN_URL);
            prepareStatement = connection.prepareStatement(sql);
            System.out.println("-------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {

        //readDeputyFace();
        writeDeputyFace();
    }


    private static void writeDeputyFace() {

        try {
            prepareStatement.setInt(1, 12);
            prepareStatement.setString(2, "1");
            prepareStatement.setBytes(3, "1".getBytes());
            prepareStatement.setString(4, "1");
            prepareStatement.setString(5, "1");
            prepareStatement.setString(6, "1");
            prepareStatement.setString(7, "1");
            prepareStatement.setInt(8, 1);

            SimpleDateFormat sdf = new SimpleDateFormat(str);
            String dateStr = sdf.format(new java.util.Date());
            prepareStatement.setString(9, dateStr.split(" ")[0]);

            prepareStatement.setDate(10, new Date(System.currentTimeMillis()));
            prepareStatement.setDate(11, new Date(System.currentTimeMillis()));
            prepareStatement.setInt(12, 1);
            prepareStatement.setString(13, "1");
            prepareStatement.setString(14, "1");
            prepareStatement.setString(15, "1");
            prepareStatement.setString(16, "1");
            prepareStatement.setString(17, "1");
            prepareStatement.setInt(18, 1);
            prepareStatement.setBytes(19, "2".getBytes());

            int i = prepareStatement.executeUpdate();
            System.out.println(i + " --> " + i);
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeDeputyFace(DeputyFace deputyFace) throws SQLException {

        prepareStatement.setLong(1, deputyFace.getId());
        prepareStatement.setString(2, deputyFace.getAid());
        prepareStatement.setBytes(3, deputyFace.getFeature_val());
        prepareStatement.setString(4, deputyFace.getImage_id());
        prepareStatement.setString(5, deputyFace.getImage_url());
        prepareStatement.setString(6, deputyFace.getThumbnail_id());
        prepareStatement.setString(7, deputyFace.getThumbnail_url());
        prepareStatement.setInt(8, deputyFace.getPresent_flag());

        SimpleDateFormat sdf = new SimpleDateFormat(str);
        String dateStr = sdf.format(deputyFace.getCreate_time());
        prepareStatement.setString(9, dateStr.split(" ")[0]);

        prepareStatement.setDate(10, new Date(deputyFace.getCreate_time().getTime()));
        prepareStatement.setDate(11, new Date(deputyFace.getModify_time().getTime()));
        prepareStatement.setInt(12, deputyFace.getStatus());
        prepareStatement.setString(13, deputyFace.getSource());
        prepareStatement.setString(14, deputyFace.getAlgo_version());
        prepareStatement.setString(15, deputyFace.getColumn1());
        prepareStatement.setString(16, deputyFace.getColumn2());
        prepareStatement.setString(17, deputyFace.getColumn3());
        prepareStatement.setInt(18, deputyFace.getPq_probe());
        prepareStatement.setBytes(19, deputyFace.getPq_code());
        prepareStatement.executeUpdate();
        prepareStatement.addBatch();
        connection.commit();
    }

    static void init() {
        if (prepareStatement == null) {
            try {
                Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
                connection = DriverManager.getConnection(Config.CONN_URL);
                prepareStatement = connection.prepareStatement(sql);
                System.out.println("-------------------------");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeDeputyFace(List<DeputyFace> deputyFaceList) throws SQLException {
        init();
        for (DeputyFace deputyFace : deputyFaceList) {
            prepareStatement.setLong(1, deputyFace.getId());
            prepareStatement.setString(2, deputyFace.getAid());
            prepareStatement.setBytes(3, deputyFace.getFeature_val());
            prepareStatement.setString(4, deputyFace.getImage_id());
            prepareStatement.setString(5, deputyFace.getImage_url());
            prepareStatement.setString(6, deputyFace.getThumbnail_id());
            prepareStatement.setString(7, deputyFace.getThumbnail_url());
            prepareStatement.setInt(8, deputyFace.getPresent_flag());

            SimpleDateFormat sdf = new SimpleDateFormat(str);
            String dateStr = sdf.format(deputyFace.getCreate_time());
            prepareStatement.setString(9, dateStr.split(" ")[0]);

            prepareStatement.setDate(10, new Date(deputyFace.getCreate_time().getTime()));
            prepareStatement.setDate(11, new Date(deputyFace.getModify_time().getTime()));
            prepareStatement.setInt(12, deputyFace.getStatus());
            prepareStatement.setString(13, deputyFace.getSource());
            prepareStatement.setString(14, deputyFace.getAlgo_version());
            prepareStatement.setString(15, deputyFace.getColumn1());
            prepareStatement.setString(16, deputyFace.getColumn2());
            prepareStatement.setString(17, deputyFace.getColumn3());
            prepareStatement.setInt(18, deputyFace.getPq_probe());
            prepareStatement.setBytes(19, deputyFace.getPq_code());
            //prepareStatement.executeUpdate();
            prepareStatement.addBatch();
        }
        prepareStatement.executeBatch();
        connection.commit();
    }
}
