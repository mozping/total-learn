package indi.mozping.hbase;

import indi.mozping.dao.EventMapper;
import indi.mozping.entity.Event;
import indi.mozping.hbase.tophoenix.ReadWriteEvent;
import indi.mozping.util.GlobalConst;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * @author by mozping
 * @Classname StartEvent
 * @Description TODO
 * @Date 2020/4/10 9:44
 */
public class StartEvent {

    private static final Logger LOG = LoggerFactory.getLogger(StartEvent.class);

    public static void main(String[] args) throws InterruptedException, SQLException {

        //readFromHbaseRandom();


        //write1();
        //write100();
        writeRandom();

    }

    private static void readFromHbaseRandom() throws InterruptedException, SQLException {

        int begin = 1;
        int end = 1000000;
        int batch = 100;
        boolean ret;
        Thread.sleep(500);
        int loop = 0;
        long beginTime = System.currentTimeMillis();

        for (int i = begin; i < end; i += batch) {
            ret = ReadWriteEvent.readEvent("2020-04-16", Integer.toString(i), batch);
            if (!ret) {
                System.out.println("��ѯ" + i + "-> batch" + batch + "ʧ�ܻ��߽���!");
                break;
            }
            loop++;
            if ((System.currentTimeMillis() - beginTime) > 200000) {
                break;
            }
        }
        System.out.println("Num : " + loop * batch);
        System.out.println("End success with time :" + (System.currentTimeMillis() - beginTime));
    }

    private static void writeRandom() throws InterruptedException, SQLException {

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_FILE_PATH_GP).openSession();
        EventMapper eventMapper = sqlSession.getMapper(EventMapper.class);


        long begin1 = System.currentTimeMillis();
        int count = 0;
        int loop = 0;
        while (true) {
            long begin = System.currentTimeMillis();
            List<Event> eventList = eventMapper.selectEventRandom100();
            LOG.info("pull size:" + eventList.size());
            long end = System.currentTimeMillis();
            LOG.info("��ʱ: " + (end - begin) + " ms");

//            eventList.sort((o1, o2) -> {
//                long l1 = new Long(o1.getThumbnail_id());
//                long l2 = new Long(o2.getThumbnail_id());
//                return Long.compare(l1, l2);
//            });

            dealRandom(eventList);
//            for (Event event : eventList) {
//                LOG.info(event.getThumbnail_id());
//            }
            count += eventList.size();
            loop++;
            if (eventList.size() < 100 || loop >= 10000 || (end - begin1) > 420 * 1000) {
                break;
            }
        }
        long end = System.currentTimeMillis();
        LOG.info("��ʱ: " + (end - begin1) + " ms");
        LOG.info("count: " + count);

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    private static void write100() throws InterruptedException, SQLException {
        //Thread.sleep(200);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_FILE_PATH_GP).openSession();
        EventMapper eventMapper = sqlSession.getMapper(EventMapper.class);

//        String beginIndex = "1126115838328838";
        String beginIndex = "2533486134137574";
//        String beginIndex = "2814958312949396";

        while (true) {
            long begin = System.currentTimeMillis();
            List<Event> eventList = eventMapper.selectEventByIdBatch100(beginIndex);
            LOG.info("pull size:" + eventList.size());
            long end = System.currentTimeMillis();
            LOG.info("��ʱ: " + (end - begin) + " ms");

            eventList.sort((o1, o2) -> {
                long l1 = new Long(o1.getThumbnail_id());
                long l2 = new Long(o2.getThumbnail_id());
                return Long.compare(l1, l2);
            });

            beginIndex = eventList.get(eventList.size() - 1).getThumbnail_id();
            LOG.info("beginIndex: " + beginIndex);
            deal(eventList);
//            for (Event event : eventList) {
//                LOG.info(event.getThumbnail_id());
//            }
            if (eventList.size() < 100) {
                break;
            }
        }

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

    private static void deal(Event event) throws SQLException {
        ReadWriteEvent.writeEvent(event);
    }

    private static void deal(List<Event> eventList) throws SQLException {
        ReadWriteEvent.writeEvent(eventList);
    }

    private static void dealRandom(List<Event> eventList) throws SQLException {
        ReadWriteEvent.writeEventRandom(eventList);
    }


    static void write1() throws InterruptedException {
        Thread.sleep(500);

        SqlSession sqlSession =
                SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(GlobalConst.CONFIG_FILE_PATH_GP).openSession();
        EventMapper eventMapper = sqlSession.getMapper(EventMapper.class);

        long begin = System.currentTimeMillis();
        Event event = eventMapper.selectEventById("1126115838328838");
        LOG.info("event:" + event);
        long end = System.currentTimeMillis();
        LOG.info("��ʱ: " + (end - begin) + " ms");

        SqlSessionFactoryUtil.closeSession(sqlSession);
    }

}