package indi.mozping.entity;

import java.util.Date;

public class PartitionInfo {
    private Integer id;

    private Integer nodeIpId;

    private Integer initSchemaInfoId;

    private String prefixTableIndex;

    private String partitionNum;

    private Date startTime;

    private Date endTime;

    private Date createTime;

    private Byte isDelete;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeIpId() {
        return nodeIpId;
    }

    public void setNodeIpId(Integer nodeIpId) {
        this.nodeIpId = nodeIpId;
    }

    public Integer getInitSchemaInfoId() {
        return initSchemaInfoId;
    }

    public void setInitSchemaInfoId(Integer initSchemaInfoId) {
        this.initSchemaInfoId = initSchemaInfoId;
    }

    public String getPrefixTableIndex() {
        return prefixTableIndex;
    }

    public void setPrefixTableIndex(String prefixTableIndex) {
        this.prefixTableIndex = prefixTableIndex == null ? null : prefixTableIndex.trim();
    }

    public String getPartitionNum() {
        return partitionNum;
    }

    public void setPartitionNum(String partitionNum) {
        this.partitionNum = partitionNum == null ? null : partitionNum.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }
}