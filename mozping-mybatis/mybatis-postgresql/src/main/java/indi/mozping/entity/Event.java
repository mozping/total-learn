package indi.mozping.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Face
 * @Description TODO
 * @Date 2019/4/12 15:49
 */
@Data
public class Event {


    private String aid;
    private String sys_code;
    private String thumbnail_id;
    private String thumbnail_url;
    private String image_id;
    private String image_url;


    public byte[] feature_info;


    private String algo_version;
    private String gender_info;
    private String age_info;
    private String hairstyle_info;

    private String hat_info;
    private String glasses_info;
    private String race_info;
    private String mask_info;
    private String skin_info;
    private String pose_info;
    private float quality_info;
    private String target_rect;
    private String target_rect_float;

    private String land_mark_info;
    private String source_id;
    private String source_type;
    private String site;
    private float feature_quality;


    private Date time;
    private Date create_time;
    private Date save_time;
    private String score;

    private String column1;
    private String column2;
    private String column3;

    private String target_thumbnail_rect;
    private String field1;

}
