package indi.mozping.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @author by mozping
 * @Classname Car
 * @Description TODO
 * @Date 2019/4/12 15:57
 */
@Data
@ToString(callSuper = true)
public class Car extends BaseObject {


    private String carType;
    private float carTypeConfidence;

    private String color;
    private float colorConfidence;

    private String brand;
    private String brandCode;
    private float brandConfidence;

    private String plateColor;
    private float plateColorConfidence;

    private String plateNumber;
    private float plateNumberConfidence;

}
