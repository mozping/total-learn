package indi.mozping.entity;

import java.util.Date;

public class ClusterNodesIpInfo {
    private Integer id;

    private String ip;

    private String pgPort;

    private String nodePort;

    private Date createTime;

    private Byte isDelete;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getPgPort() {
        return pgPort;
    }

    public void setPgPort(String pgPort) {
        this.pgPort = pgPort == null ? null : pgPort.trim();
    }

    public String getNodePort() {
        return nodePort;
    }

    public void setNodePort(String nodePort) {
        this.nodePort = nodePort == null ? null : nodePort.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }
}