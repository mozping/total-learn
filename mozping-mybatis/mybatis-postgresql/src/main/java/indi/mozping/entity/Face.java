package indi.mozping.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @author by mozping
 * @Classname Face
 * @Description TODO
 * @Date 2019/4/12 15:49
 */
@ToString(callSuper = true)
@Data
public class Face extends BaseObject {

    private int age;
    private float ageConfidence;

    private String gender;
    private float genderConfidence;

    private String hat;
    private float hatConfidence;

    private String glasses;
    private float glassesConfidence;

    private String race;
    private float raceConfidence;

    private String mask;
    private float maskConfidence;

    public byte[] feature;
}
