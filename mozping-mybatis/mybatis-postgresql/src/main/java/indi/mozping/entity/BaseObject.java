package indi.mozping.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author by mozping
 * @Classname BaseObject
 * @Description TODO
 * @Date 2019/4/12 15:40
 */
@Data
public class BaseObject {
    public String operator;
    public String targetType;
    public String targetImage;
    public String backgroundImage;

    public String fromImageId;
    public String sourceId;
    public String areaId;

    public String sourceType;
    public String taskType;
    public String fingerprint;

    public String guid;
    public String uuid;
    public String tid;
    public int algVersion;


    public int bgImgHigh;
    public int bgImgWidth;

    public String targetRect;
    public String targetRectFloat;
    public String imgRect;
    public String imgRectFloat;

    public String debug;
    public String imgQuality;

    public Date recvLocalTime;
    public Date recvUtcTime;
    public Date time;
    private Date vtime;
    public Date pdate;
    public String padding = "abcdefghijklmnopqrstuvwxyz";


}
