package indi.mozping.util;

/**
 * @author by mozping
 * @Classname GlobalConst
 * @Description TODO
 * @Date 2019/6/27 11:14
 */
public class GlobalConst {

    public static final String CONFIG_FILE_PATH = "mybatis/postgresql-config.xml";
    public static final String CONFIG_DEPUTYFACE_PATH_GP = "mybatis/gp-config-deputyface.xml";
    public static final String CONFIG_FILE_PATH_GP = "mybatis/gp-config.xml";

    public static final String CONFIG_COUNT_PATH_GP = "mybatis/gp-config-count.xml";

    public static final String CONFIG_PERSONFILE_PATH_GP = "mybatis/gp-config-personfile.xml";


}