package indi.mozping.util;

import indi.mozping.entityfactory.Util;

import java.util.Calendar;
import java.util.Date;

/**
 * @author by mozping
 * @Classname DateUtil
 * @Description TODO
 * @Date 2019/7/4 11:28
 */
public class DateUtil {

    public static final Date DATE_REFER = Util.convertStringToDate("2020-01-01 00:00:00");
    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final int SECONDS_ONE_DAY = 24 * 60 * 60 * 1000;
    private static int TIME_BEGIN_2019 = 1546272000;
    //对应北京时间2020/01/01 00:00:00
    private static int TIME_END = 1577808000;

    /**
     * 返回2个Date日期对象之间的天数差距
     * 1.如果date1 < date2 ，天数差为n，的
     */
    public static int getDayGap(Date date1, Date date2) {

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar1.set(Calendar.HOUR_OF_DAY, 0);
        calendar1.set(Calendar.MINUTE, 0);
        calendar1.set(Calendar.SECOND, 0);
        long day1 = calendar1.getTimeInMillis();

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        calendar2.set(Calendar.HOUR_OF_DAY, 0);
        calendar2.set(Calendar.MINUTE, 0);
        calendar2.set(Calendar.SECOND, 0);
        long day2 = calendar2.getTimeInMillis();

        long l = day1 - day2;

        return (int) (l / (SECONDS_ONE_DAY));
    }

    public static Date getDateIncSomeDay(Date date1, int num) {
        Calendar c = Calendar.getInstance();
        c.setTime(date1);
        c.add(Calendar.DATE, num);
        return c.getTime();
    }

    public static Date getTimeBetweenYear() {
        int unix = getRandomNum(TIME_BEGIN_2019, TIME_END);
        long unixTime = (long) unix * 1000;
        return new Date(unixTime);
    }

    public static int getRandomNum(int min, int max) {
        return min + (int) (Math.random() * (max - min));
    }


    public static Date getVertialDate(Date date1, Date dateRefer) {
        int gapRaw = getDayGap(date1, dateRefer);
        return getDateIncSomeDay(date1, -2 * gapRaw);

    }
}