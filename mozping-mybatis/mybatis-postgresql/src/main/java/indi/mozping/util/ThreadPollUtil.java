package indi.mozping.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author by mozping
 * @Classname ThreadPollUtil
 * @Description 获取线程池的线程统计mongdb记录数量
 * @Date 2018/12/27 18:34
 */
public class ThreadPollUtil {

    public static final ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
            10,
            20,
            60,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<Runnable>(20)
            , Executors.defaultThreadFactory()
            , new ThreadPoolExecutor.AbortPolicy()
    );


    private ThreadPollUtil() {
    }
}
