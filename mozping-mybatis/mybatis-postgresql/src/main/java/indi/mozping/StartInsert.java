package indi.mozping;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.boot.AddCarThread;
import indi.mozping.boot.AddFaceThread;
import indi.mozping.dao.CarMapper;
import indi.mozping.entity.Car;
import indi.mozping.entityfactory.RandomObjFactory;
import indi.mozping.util.DbUtil;
import indi.mozping.util.SqlSessionFactoryUtil;
import indi.mozping.util.ThreadPollUtil;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.FutureTask;

/**
 * @author by mozping
 * @Classname StartInsert
 * @Description TODO
 * @Date 2019/6/22 14:24
 */
public class StartInsert {

    private static String type;
    private static int total;
    private static int batch;
    private static int threadNum;

    private static final String CONFIG_FILE_PATH = "mybatis/postgresql-config.xml";
    public static final CountDownLatch latch = new CountDownLatch(10);
    private static final Logger LOG = LoggerFactory.getLogger(StartInsert.class);

    public static void main(String[] args) throws Exception {
        for (int i = 0; i < 20; i++) {
            new Thread(new MyRunnable()).start();
        }
    }

    static class MyRunnable implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 20; i++) {
                addSomeBatch();
            }
        }
    }

    public static void addSomeBatch() {
        long begin = System.currentTimeMillis();
        for (int i = 1; i < 200; i++) {
            addBatch();
        }
        System.out.println(System.currentTimeMillis() - begin);
    }

    public static void addBatch() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        ArrayList<Car> list = new ArrayList<>(800);

        for (int i = 0; i < 500; i++) {
            list.add(RandomObjFactory.getRandomCar());
        }

        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);

        int rowAffected = carMapper.addCarBatch(list);
        System.out.println("The rows be affected :" + rowAffected);

        //显示提交事务
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);

    }


    public static void main1(String[] args) throws Exception {
        Properties properties = initConfig();
        long begin = System.currentTimeMillis();
        type = properties.getProperty("type");
        total = Integer.valueOf(properties.getProperty("total"));
        batch = Integer.valueOf(properties.getProperty("batch"));
        threadNum = Integer.valueOf(properties.getProperty("threadNum"));

        if ("car".equals(type)) {
            addCar(total, batch);
        } else if ("face".equals(type)) {
            addFace(total, batch);
        } else {
            System.out.println("Type error!!!!!!!!!!!!!");
        }

        latch.await();
        long time = System.currentTimeMillis() - begin;
        LOG.info("Total spend time: " + time);
        LOG.info("Speed is: " + (total * 1000) / time + " record /S");

        System.exit(0);

    }

    public static void addCar(int total_, int batch_) {
        for (int i = 1; i <= threadNum; i++) {
            AddCarThread addCarThread = new AddCarThread(total_ / threadNum, batch_);
            FutureTask<JSONObject> jsonObjectFutureTask = new FutureTask<>(addCarThread);
            new Thread(jsonObjectFutureTask).start();
            //            ThreadPollUtil.poolExecutor.execute(jsonObjectFutureTask);
        }
    }

    public static void addFace(int total_, int batch_) {
        for (int i = 1; i <= 10; i++) {
            AddFaceThread addFaceThread = new AddFaceThread(total_ / 10, batch_);
            FutureTask<JSONObject> jsonObjectFutureTask = new FutureTask<>(addFaceThread);
            ThreadPollUtil.poolExecutor.execute(jsonObjectFutureTask);
        }
    }

    public static Properties initConfig() throws Exception {
        return DbUtil.loadPropertyFromFile("application.properties");

    }
}