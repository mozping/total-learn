package indi.mozping.dao;

import indi.mozping.entity.PersonFile;

import java.util.List;

/**
 * @author by mozping
 * @Classname DeputyFaceMapper
 * @Description TODO
 * @Date 2020/4/9 14:33
 */
public interface PersonfileMapper {

    PersonFile selectPersonfileById(String id);

    List<PersonFile> selectPersonfileByIdBatch100(String id);


}