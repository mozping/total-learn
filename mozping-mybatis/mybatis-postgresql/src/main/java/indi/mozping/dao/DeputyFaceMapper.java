package indi.mozping.dao;

import indi.mozping.entity.DeputyFace;

import java.util.List;

/**
 * @author by mozping
 * @Classname DeputyFaceMapper
 * @Description TODO
 * @Date 2020/4/9 14:33
 */
public interface DeputyFaceMapper {

    DeputyFace selectDeputyFaceById(long id);

    List<DeputyFace> selectDeputyFaceByIdBatch100(long id);


}