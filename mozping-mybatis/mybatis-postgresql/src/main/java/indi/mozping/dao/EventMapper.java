package indi.mozping.dao;

import indi.mozping.entity.Event;

import java.util.List;

/**
 * @author by mozping
 * @Classname DeputyFaceMapper
 * @Description TODO
 * @Date 2020/4/9 14:33
 */
public interface EventMapper {

    Event selectEventById(String id);

    List<Event> selectEventByIdBatch100(String id);
    List<Event> selectEventRandom100();


}