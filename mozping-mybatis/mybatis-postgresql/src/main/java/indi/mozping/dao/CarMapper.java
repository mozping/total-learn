package indi.mozping.dao;

import indi.mozping.entity.Car;
import indi.mozping.querybean.CarNumBean;
import indi.mozping.querybean.ColorQueryBean;
import indi.mozping.querybean.FieldQueryBean;
import indi.mozping.querybean.MultiFieldQueryBean;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by mozping
 * @Classname PeopleMapper
 * @Description TODO
 * @Date 2019/5/9 19:25
 */
public interface CarMapper {


    List<Car> selectCarById(@Param("ids") List<String> ids);

    List<Car> selectCarByField(FieldQueryBean fieldQueryBean);

    List<Car> selectCarByMultiField(MultiFieldQueryBean multiFieldQueryBean);

    List<Car> selectCarNum(CarNumBean carNumBean);

    List<Car> selectCarNumPrefix(CarNumBean carNumBean);


    int addCar(Car carObject);

    int addCarBatch(ArrayList<Car> carArrayList);


    ArrayList<Car> queryCarByColor(ColorQueryBean colorQueryBean);

    ArrayList<Car> queryCarByColorExplain(ColorQueryBean colorQueryBean);


}