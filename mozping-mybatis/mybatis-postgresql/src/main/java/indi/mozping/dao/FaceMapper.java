package indi.mozping.dao;

import indi.mozping.entity.Face;
import indi.mozping.entity.User;
import indi.mozping.querybean.BaseQueryBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by mozping
 * @Classname PeopleMapper
 * @Description TODO
 * @Date 2019/5/9 19:25
 */
public interface FaceMapper {

    User selelctFace(int id);

    int addFace(Face faceObject);

    int addFaceBatch(ArrayList<Face> faceArrayList);


    List<Face> queryBatch(BaseQueryBean baseQueryBean);

}