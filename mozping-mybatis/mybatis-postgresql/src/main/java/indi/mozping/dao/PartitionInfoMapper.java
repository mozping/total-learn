package indi.mozping.dao;


import indi.mozping.entity.PartitionInfo;

public interface PartitionInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PartitionInfo record);

    int insertSelective(PartitionInfo record);

    PartitionInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PartitionInfo record);

    int updateByPrimaryKey(PartitionInfo record);
}