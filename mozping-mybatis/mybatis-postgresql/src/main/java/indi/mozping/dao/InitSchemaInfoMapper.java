package indi.mozping.dao;


import indi.mozping.entity.InitSchemaInfo;

public interface InitSchemaInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(InitSchemaInfo record);

    int insertSelective(InitSchemaInfo record);

    InitSchemaInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InitSchemaInfo record);

    int updateByPrimaryKeyWithBLOBs(InitSchemaInfo record);

    int updateByPrimaryKey(InitSchemaInfo record);
}