package indi.mozping;

import lombok.Data;

/**
 * @author by mozping
 * @Classname GpConfig
 * @Description TODO
 * @Date 2019/7/23 10:21
 */
@Data
public class GpConfig {

    /**
     * 插入数据类型，car或者face
     */
    String type;

    /**
     * 插入的线程数量
     */
    int threadNum;

    /**
     * 批量大小,每一次创建对象的个数
     */
    int batchNum;

    /**
     * 事物提交间隔,每保存commitInterval个batchNum的数据之后，提交一次事物
     */
    int commitInterval;

}