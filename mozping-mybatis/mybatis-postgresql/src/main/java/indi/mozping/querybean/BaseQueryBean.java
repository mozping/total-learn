package indi.mozping.querybean;

import lombok.Data;

import java.util.Date;

/**
 * @author by mozping
 * @Classname BaseQueryBean
 * @Description TODO
 * @Date 2019/6/27 10:33
 */
@Data
public class BaseQueryBean {

    Date beginTime;
    Date endTime;
    int limitNum;
}