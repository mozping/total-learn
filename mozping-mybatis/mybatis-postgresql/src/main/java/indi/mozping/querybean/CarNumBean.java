package indi.mozping.querybean;

import lombok.Data;

/**
 * @author by mozping
 * @Classname CarNumBean
 * @Description TODO
 * @Date 2019/6/29 21:40
 */
@Data
public class CarNumBean extends BaseQueryBean {

    String carNum;

}