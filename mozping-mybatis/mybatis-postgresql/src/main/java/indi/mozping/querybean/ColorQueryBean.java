package indi.mozping.querybean;

import lombok.Data;

/**
 * @author by mozping
 * @Classname ColorQueryBean
 * @Description TODO
 * @Date 2019/6/27 10:34
 */
@Data
public class ColorQueryBean extends BaseQueryBean {

    String color;

}