package indi.mozping.boot;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.StartInsert;
import indi.mozping.dao.FaceMapper;
import indi.mozping.entity.Face;
import indi.mozping.entityfactory.RandomObjFactory;
import indi.mozping.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author by mozping
 * @Classname AddCarThread
 * @Description TODO
 * @Date 2019/6/22 11:18
 */
public class AddFaceThread implements Callable<JSONObject> {

    private static final Logger LOG = LoggerFactory.getLogger(AddFaceThread.class);
    private static final String CONFIG_FILE_PATH = "mybatis/postgresql-config.xml";


    private int total;
    private int batchNum;

    public AddFaceThread(int total, int batchNum) {
        this.total = total;
        this.batchNum = batchNum;
    }

    @Override
    public JSONObject call() throws Exception {
        JSONObject jsonObject = new JSONObject();
        int loop = total / batchNum;
        ArrayList<Face> list = new ArrayList<>(batchNum);
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        FaceMapper faceMapper = sqlSession.getMapper(FaceMapper.class);

        //ѭ������
        long begin = System.currentTimeMillis();
        int numOfRecord = 0;
        for (int j = 1; j <= loop; j++) {
            //1.������batchNum��car���󣬲����뵽list
            for (int i = 1; i <= batchNum / 10; i++) {
                //Face face = RandomObjFactory.getRandomFace();
                //list.add(face);
                List<Face> faceList = RandomObjFactory.getRandomFaceWithNum(10);
                list.addAll(faceList);
            }
            //2.�������ݿ�
            int rowAffected = faceMapper.addFaceBatch(list);
            list.clear();
            numOfRecord += rowAffected;
            //��ʾ�ύ����
            if (j % 10 == 0) {
                sqlSession.commit();
            }
        }
        sqlSession.close();
        long time = System.currentTimeMillis() - begin;
        long speed = (numOfRecord * 1000) / time;

        jsonObject.put("Added", numOfRecord);
        jsonObject.put("Total", total);
        jsonObject.put("Error", total - numOfRecord);
        jsonObject.put("Time", time);
        jsonObject.put("Speed", speed + " /S;");

        StartInsert.latch.countDown();
        LOG.info("�߳�" + Thread.currentThread().getName() + "ִ�н����" + jsonObject);
        return jsonObject;
    }

}