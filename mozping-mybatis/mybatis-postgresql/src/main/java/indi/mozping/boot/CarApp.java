package indi.mozping.boot;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.util.ThreadPollUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.FutureTask;

public class CarApp {

    private static final int THREAD_NUM = 5;
    private static final int TOTAL = 1 * 10000;
    private static final int BATCH_NUM = 100;
    private static final int NUM_PER_THREAD = TOTAL / THREAD_NUM;

    private static final Logger LOG = LoggerFactory.getLogger(CarApp.class);
    private static ArrayList<FutureTask<JSONObject>> resultList = new ArrayList<>();

    public static final CountDownLatch latch = new CountDownLatch(THREAD_NUM);


    public static void main(String[] args) throws InterruptedException {

        for (int i = 1; i <= THREAD_NUM; i++) {
            AddCarThread addCarThread = new AddCarThread(NUM_PER_THREAD, BATCH_NUM);
            FutureTask<JSONObject> jsonObjectFutureTask = new FutureTask<>(addCarThread);
            resultList.add(jsonObjectFutureTask);
            ThreadPollUtil.poolExecutor.execute(jsonObjectFutureTask);
        }
//        LOG.info("await- : .........");
//        latch.await();
//        LOG.info("over- : .........");
        //打印结果
//        for (FutureTask<JSONObject> futureTask : resultList) {
//            JSONObject jsonObject = null;
//            if (futureTask != null) {
//                try {
//                    jsonObject = futureTask.get();
//                    LOG.info(jsonObject.toJSONString());
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                }
//            }
//
//        }

    }

}
