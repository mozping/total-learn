package indi.mozping;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class JdbcDemo {

    static final String JDBC_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://192.168.11.31:5432/bigdata_odl";
    static final String USER = "gpadmin";
    static final String PASS = "gpadmin";

    static Statement stmt = null;
    static PreparedStatement preparedStatement = null;
    static Connection conn = null;

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName(JDBC_DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        String sql_1 = "select table_name from  information_schema.TABLES  where table_catalog = 'bigdata_odl' and TABLE_schema = 'public' and table_name like 'odl_bigdata_event_face_pending_1_prt_p%';\n";
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(sql_1);
        List<String> tableList = new ArrayList<>();
        if (rs != null) {
            while (rs.next()) {
                String tablename = rs.getString(1);
                if (!tablename.contains("copy") && !tablename.contains("bak")) {
                    tableList.add(tablename);
                }
            }
        }

        for (String tableName : tableList) {
            System.out.println(tableName + " ->: " + tableName);
        }

        System.out.println(" ------------------------------- ");
        String sql_2 = "select count(*) as num from ";
        int total = 0;
        for (String tableName : tableList) {
            ResultSet resultSet = statement.executeQuery(sql_2 + tableName);
            while (resultSet.next()) {
                int count = resultSet.getInt("num");
                System.out.println(tableName + " ->: " + count);
                total += count;
            }
        }
        System.out.println("total : " + total);
    }


    public static int executeSqlWithPreparedStatement(String sql) {
        int count = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                count++;
            }
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return 0;
    }


}
