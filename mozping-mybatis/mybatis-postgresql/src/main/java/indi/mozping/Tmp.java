package indi.mozping;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * @author by mozping
 * @Classname Tmp
 * @Description TODO
 * @Date 2020/4/9 19:46
 */
public class Tmp {

    private static final Random random = new Random();
    private static final String randomStr = "1234567890";

    public static void main(String[] args) throws ParseException {
        System.out.println(getRandom(16));
        System.out.println(getRandom(15));
        System.out.println(getRandom(14));
        System.out.println(getRandom(11));
        System.out.println(getRandom(1));
//        String str="2020-01-01 01:01:01";
//        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = sdf.parse(str);
//        getDayStr(date);
//
//        SimpleDateFormat sdf1= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String dateStr=sdf1.format(date);
//        System.out.println(dateStr);
//        System.out.println(dateStr.split(" ")[0]);
    }

    static String getRandom(int num) {
        StringBuilder sb = new StringBuilder();
        int index = Math.abs(random.nextInt()) % 9;
        for (int i = 0; i < num; i++) {
            sb.append(randomStr.charAt(index));
            index = Math.abs(random.nextInt()) % 10;
        }
        return sb.toString();
    }

    static void getDayStr(Date date) {
        System.out.println("before: " + date);
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        System.out.println("YEAR: " + instance.get(Calendar.YEAR));
        System.out.println("MONTH: " + instance.get(Calendar.MONTH));
        System.out.println("DAY: " + instance.get(Calendar.DAY_OF_MONTH));
    }

}