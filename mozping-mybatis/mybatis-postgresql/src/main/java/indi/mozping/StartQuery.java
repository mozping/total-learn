package indi.mozping;

import indi.mozping.entityfactory.RandomUtil;
import indi.mozping.entityfactory.Util;
import indi.mozping.query.QueryCar;
import indi.mozping.querybean.ColorQueryBean;
import indi.mozping.querybean.FieldQueryBean;
import indi.mozping.querybean.MultiFieldQueryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static indi.mozping.entityfactory.RandomUtil.color;
import static indi.mozping.entityfactory.RandomUtil.numId;

/**
 * @author by mozping
 * @Classname StartInsert
 * @Description TODO
 * @Date 2019/6/22 14:24
 */
public class StartQuery {


    private static final Logger LOG = LoggerFactory.getLogger(StartQuery.class);
    private static int concurrent = 3;
    private static int numPerThread = 1;
    private static boolean printDetail = false;

    private static int limitNum = 100;


    private static ArrayList<ColorQueryBean> colorQueryBeanList = new ArrayList<>();
    private static List<List<String>> idsList = new ArrayList<>();
    private static List<FieldQueryBean> fieldQueryBeanList = new ArrayList<>();
    private static List<MultiFieldQueryBean> multiFieldQueryBeanList = new ArrayList<>();


    public static void main(String[] args) throws InterruptedException {


        Thread.sleep(3 * 1000);

        QueryCar.queryCarColor(colorQueryBeanList, concurrent, numPerThread, printDetail);

        //QueryCar.queryCarIds(idsList, concurrent, numPerThread, printDetail);

        //QueryCar.queryCarField(fieldQueryBeanList, concurrent, numPerThread, printDetail);

        //QueryCar.queryCarFieldMulti(multiFieldQueryBeanList, concurrent, numPerThread, printDetail);

        System.exit(0);

    }


    static {
        for (int i = 0; i < 10; i++) {
            ColorQueryBean colorQueryBean = new ColorQueryBean();
            colorQueryBean.setColor("blue");
            colorQueryBean.setLimitNum(limitNum);
            //"yyyy-MM-dd HH:mm:ss"
            colorQueryBean.setBeginTime(Util.convertStringToDate("2019-10-01 00:00:00"));
            colorQueryBean.setEndTime(Util.convertStringToDate("2019-10-30 00:00:00"));
            colorQueryBeanList.add(colorQueryBean);
        }
        for (int i = 0; i < 10; i++) {
            ArrayList<String> ids = new ArrayList<String>();
            for (int j = 0; j < 1000; j++) {
                ids.add(numId(15));
            }
            idsList.add(ids);
        }

        for (int i = 0; i < 10; i++) {
            FieldQueryBean fieldQueryBean = new FieldQueryBean();
            fieldQueryBean.setColor(color());
            fieldQueryBean.setLimitNum(limitNum);
            fieldQueryBean.setPlateColor(color());
            fieldQueryBean.setCarType(RandomUtil.type());
            fieldQueryBean.setBeginTime(Util.convertStringToDate("2019-10-01 00:00:00"));
            fieldQueryBean.setEndTime(Util.convertStringToDate("2019-10-30 00:00:00"));
            fieldQueryBeanList.add(fieldQueryBean);
        }

        for (int i = 0; i < 10; i++) {
            MultiFieldQueryBean multiFieldQueryBean = new MultiFieldQueryBean();
            multiFieldQueryBean.setColor(color());
            multiFieldQueryBean.setLimitNum(limitNum);
            multiFieldQueryBean.setPlateColor(color());
            multiFieldQueryBean.setCarType(RandomUtil.type());
            multiFieldQueryBean.setBrandCode(RandomUtil.brandCode());
            multiFieldQueryBean.setPlateNumber(generateCarID());
            multiFieldQueryBean.setBeginTime(Util.convertStringToDate("2019-10-01 00:00:00"));
            multiFieldQueryBean.setEndTime(Util.convertStringToDate("2019-10-30 00:00:00"));
            multiFieldQueryBeanList.add(multiFieldQueryBean);
        }

    }

    private static String generateCarID() {
        Random random = new Random();
        int i = Math.abs(random.nextInt());
        String[] carIds = new String[]{"��AOSYVV",
                "��AJSZTM", "��H6U9M8", "��BZENPA", "��A22RII", "��DBYHAV",
                "��DGNJ4O", "��BM41VO", "��HVAY08", "��ASJ7U2", "��AVRHXO",
                "��HGYYPA", "��A6NTVK", "��HFTNC8", "��FJC0W9", "��AGYXR3",
                "��FBJEQV", "��AN7P1N", "��EVL2K6", "��BA3L7C", "��GX4GUU"};
        return carIds[(i % carIds.length)];
    }
}
