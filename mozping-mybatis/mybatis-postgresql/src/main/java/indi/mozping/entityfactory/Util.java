package indi.mozping.entityfactory;

import indi.mozping.util.MyArrayUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 * @author by mozping
 * @Classname Util
 * @Description TODO
 * @Date 2019/2/22 14:13
 */
public class Util {

    //��Ӧ����ʱ��2018/01/01 00:00:00
    private static int TIME_BEGIN = 1514736000;
    //��Ӧ����ʱ��2019/01/01 00:00:00
    private static int TIME_BEGIN_2019 = 1546272000;
    //��Ӧ����ʱ��2020/01/01 00:00:00
    private static int TIME_END = 1577808000;
    //��Ӧ����ʱ��2019/03/31 23:59:59
    private static int TIME_END_2019_03_31 = 1554047999;
    //��Ӧ����ʱ��2019/02/28 23:59:59
    private static int TIME_END_2019_02_28 = 1551369599;

    private static Random random = new Random();

    private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    public static int getRandomNum() {
        return getRandomNum(0, Integer.MAX_VALUE);
    }

    public static int getRandomNum(int max) {
        return getRandomNum(0, max);
    }

    //��ȡ�����������min��������max
    public static int getRandomNum(int min, int max) {
        return min + (int) (Math.random() * (max - min));
    }

    public static Date getTimeBetweenTwoYear() {
        int unix = getRandomNum(TIME_BEGIN, TIME_END);
        long unixTime = (long) unix * 1000;
        return new Date(unixTime);
    }

    public static Date getTimeBetweenYear() {
        int unix = getRandomNum(TIME_BEGIN_2019, TIME_END);
        long unixTime = (long) unix * 1000;
        return new Date(unixTime);
    }

    public static Date getTimeBetweenYearIncrement(long unixTime) {
        return new Date(unixTime);
    }

    public static Date getTimeBetween3Month() {
        int unix = getRandomNum(TIME_BEGIN_2019, TIME_END_2019_03_31);
        long unixTime = (long) unix * 1000;
        return new Date(unixTime);
    }

    public static Date getTimeBetween3MonthLast() {
        int unix = getRandomNum(TIME_BEGIN_2019 - 90 * 24 * 3600, TIME_BEGIN_2019 - 24 * 3600);
        long unixTime = (long) unix * 1000;
        return new Date(unixTime);
    }

    public static Date getTimeBetween2Month() {
        int unix = getRandomNum(TIME_BEGIN_2019, TIME_END_2019_02_28);
        long unixTime = (long) unix * 1000;
        return new Date(unixTime);
    }

    public static Date getTimeBetweenSomeDay(int days) {
        int unix = getRandomNum(TIME_BEGIN_2019, TIME_BEGIN_2019 + days * 24 * 3600);
        long unixTime = (long) unix * 1000;
        return new Date(unixTime);
    }


    public static Date convertStringToDate(String dateStr) {
        if (dateStr == null) {
            return null;
        }

        Date date;
        SimpleDateFormat formatSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            date = formatSecond.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatMin = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            date = formatMin.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatHour = new SimpleDateFormat("yyyy-MM-dd HH");
        try {
            date = formatHour.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatDay = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = formatDay.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatMonth = new SimpleDateFormat("yyyy-MM");
        try {
            date = formatMonth.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
        try {
            date = formatMonth.parse(dateStr);
            return date;
        } catch (ParseException pe) {
            return null;
        }

    }


    public static boolean isBlank(Object o) {
        if (o instanceof CharSequence) {
            CharSequence str = (CharSequence) o;
            int strLen;
            if ((strLen = str.length()) == 0) {
                return true;
            }
            for (int i = 0; i < strLen; i++) {
                if ((Character.isWhitespace(str.charAt(i)) == false)) {
                    return false;
                }
            }
            return true;
        } else {
            return (o == null) ? true : isBlank(o.toString());
        }
    }


    public static ArrayList<String> getRetKeys(String original, String split) {
        ArrayList<String> arr = new ArrayList<>();
        if (original == null || isBlank(original)) {
            return arr;
        }
        String[] str = original.split(split);
        for (String s : str) {
            if (!isBlank(s)) {
                arr.add(s.trim());
            }
        }
        return arr;
    }

    static ArrayList<String> listCarNum = new ArrayList<>();

    static {
        listCarNum.add("A");
        listCarNum.add("A");
        listCarNum.add("A");
        listCarNum.add("A");
        listCarNum.add("B");
        listCarNum.add("B");
        listCarNum.add("B");
        listCarNum.add("C");
        listCarNum.add("D");
        listCarNum.add("E");
        listCarNum.add("F");
        listCarNum.add("G");
        listCarNum.add("H");
        listCarNum.add("I");
        listCarNum.add("J");
    }

    public static String getRandomCatNum() {
        StringBuilder sb = new StringBuilder();
        sb.append("��");
        sb.append(listCarNum.get((new Random().nextInt()) % (listCarNum.size())));
        sb.append((new Random().nextInt()) % 10);
        sb.append((new Random().nextInt()) % 10);
        sb.append((new Random().nextInt()) % 10);
        sb.append((new Random().nextInt()) % 10);
        return sb.toString();
    }


    // ���ƺŵ����һ��Ϊ��ʡ��+��������+5λ����/��ĸ
    public static String generateCarID() {

        char[] provinceAbbr = { // ʡ�ݼ�� 4+22+5+3
                '��', '��', '��'
        };
        String alphas = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890"; // 26����ĸ + 10������
        String alphas1 = "AAAABBBCDEFGH"; // 26����ĸ + 10������
        Random random = new Random(); // �����������
        String carID = "";

        // ʡ��+��������+��  �� ��A�� �������ʵ�Ǹ���������������������һЩ
        carID += provinceAbbr[random.nextInt(provinceAbbr.length)]; // ע�⣺�ֿ��ӣ���Ϊ�ӵ���2��char
        carID += alphas1.charAt(random.nextInt(13));

        // 5λ����/��ĸ
        for (int i = 0; i < 5; i++) {
            carID += alphas.charAt(random.nextInt(36));
        }
        return carID;
    }


    public static byte[] FEATURE = new byte[2064];
    public static byte[] FEATURE_1 = new byte[64];

    static {
        //random.nextBytes(FEATURE);
        for (int i = 0; i < 2064; i++) {
            FEATURE[i] = (byte) (1 % 127);
        }
    }


    public static String[] idArr = new String[]{
            "283038636068116",
            "211197683341231",
            "899604659961693",
            "269128944711686",
            "559339004902031",
            "340313333201630",
            "809139636590171",
            "324328473287297",
            "466767982630980",
            "037291852510144",
            "904165622910007",
            "538680888513553",
            "286971159175125",
            "803743807105643",
            "191249066612707",
            "038011879382934",
            "314417342016652",
            "434890272928910",
            "732967140437083",
            "991861262418929",
            "795432243030413",
            "627062114894800",
            "222177039765937",
            "380355900822245",
            "589523529010099",
            "482378183917784",
            "790962452122268",
            "024478390498932",
            "592186769197113",
            "997930255796863",
            "529271287419401",
            "356076591654601",
            "785568090073856",
            "211142646181065",
            "897491262637358",
            "404086256910275",
            "886179260429438",
            "889536140151672",
            "493184500914485",
            "639525576482464",
            "740788315622097",
            "630166575817473",
            "770895999907633",
            "076856074023885",
            "466992898867332",
            "130276517452091",
            "956549849317875",
            "245132037387543",
            "697515135382997",
            "234480064047476",
            "245635290147938",
            "900342562097250",
            "064657481136685",
            "486626015285785",
            "512203340866307",
            "281601309739847",
            "511412111243697",
            "092006858015113",
            "706478005757736",
            "741653134266275",
            "122646952936175",
            "609496903129827",
            "414396417343224",
            "567140834739532",
            "093520984694492",
            "281147531244092",
            "153189776376505",
            "469087421397044",
            "022466287867901",
            "894410784060089",
            "904999076232804",
            "442419497561103",
            "033875527728044",
            "540022205004656",
            "633819737470132",
            "348590316673753",
            "343751702436404",
            "489554938746604",
            "175220331192158",
            "865385214864792",
            "941285906062476",
            "864542005126474",
            "882033420415955",
            "054542549931402",
            "942526009775295",
            "547606621946548",
            "254053383026827",
            "669547901602542",
            "159526151232974",
            "331075212929919",
            "619984596384804",
            "961713437569667",
            "081652694783143",
            "069107804847798",
            "348749173291166",
            "439719890686277",
            "782518119928774",
            "626295604335578",
            "794965711977571",
            "423569636566007",
            "523068094673009",
            "894100784990544",
            "036970077375993",
            "162180305716018",
            "020411641769326",
            "218024746244055",
            "909299419533596",
            "572333140498104",
            "673972306013070",
            "731565112731392",
            "030590020574961",
            "759742952883413",
            "385028284360086",
            "013454820922467",
            "991359252190598",
            "376771255262679",
            "468690919381898",
            "262275218375807",
            "952169478515487",
            "717509709524351",
            "693861514474047",
            "540419502369240",
            "781435975191336",
            "088405931778007",
            "260865314873742",
            "713162078256379",
            "054866940340311",
            "375271756791840",
            "882821139456341",
            "321634677485078",
            "482455842234208",
            "417089429956525",
            "884208805087873",
            "903386253536684",
            "873473865210126",
            "646569196866743",
            "074558006523972",
            "898441735790531",
            "708977719835564",
            "856849103327222",
            "503138803828419",
            "692109616171889",
            "980880736885965",
            "695169761305566",
            "994384866129131",
            "343140912290857",
            "274916640752586",
            "456182574282995",
            "170916102791775",
            "668145351840972",
            "503574902280285",
            "549572105524415",
            "431484101585639",
            "741762953846863",
            "337369685549380",
            "430147268021726",
            "486374631518032",
            "807037273169514",
            "578069620912415",
            "019751002467963",
            "560432889874256",
            "624088344043343",
            "294484613978634",
            "477724339945581",
            "061395475000644",
            "735737465569341",
            "800023809628798",
            "842414973858331",
            "953105233061496",
            "838367133886907",
            "169909533041956",
            "193487534182727",
            "367109052274422",
            "689677142105081",
            "413149444264472",
            "906911603262836",
            "435150503194016",
            "263110302519605",
            "070309172280094",
            "734305836878251",
            "760167481578042",
            "873265315039065",
            "435332851645987",
            "597292950848190",
            "178017184186844",
            "226772129086192",
            "832450769341906",
            "246352075279741",
            "751178553066313",
            "524593167548762",
            "726071011064879",
            "640799276169403",
            "361222194600492",
            "741488528938227",
            "949288964580818",
            "152704253779571",
            "718680461181342",
            "685403224649313",
            "050207716939950",
            "559181955685593",
            "793113022064531",
            "759232827286524",
            "850139506215064",
            "976462911479491",
            "444121405026675",
            "429219430514519",
            "454108657437062",
            "174396225001798",
            "285757956546014",
            "136968760889833",
            "092654422943762",
            "736855385835617",
            "444206606035649",
            "943878821233812",
            "525016655296989",
            "866943510662319",
            "942357706248764",
            "551295602398324",
            "316111771461377",
            "613578865480347",
            "815625242724122",
            "658352227705794",
            "510986738785990",
            "653264842412525",
            "805665447030781",
            "923639557811436",
            "169650395589304",
            "343618941457081",
            "570756836946616",
            "766180817953454",
            "277119442500073",
            "835096075093360",
            "120935433436628",
            "640263536057468",
            "221823792937975",
            "223434145560082",
            "963023519982538",
            "967329178653510",
            "542512469532043",
            "849986041768193",
            "238455301153820",
            "054325445548178",
            "571691449971307",
            "026664830730285",
            "946173806569949",
            "146312584362542",
            "917071787642327",
            "076192168292784",
            "591343038723796",
            "395769095253688",
            "202575724613422",
            "157010474554063",
            "487409528803518",
            "836730098452694",
            "785188795497588",
            "605576503255702",
            "064245734811271",
            "288698166628085",
            "513621850487513",
            "762677162649572",
            "968868750975435",
            "695869888801720",
            "030026164211655",
            "746042311206482",
            "436702444498566",
            "835572350307185",
            "343396098600881",
            "688644830988845",
            "843014577789442",
            "835171775399090",
            "328213331486262",
            "377239907823214",
            "055552626826703",
            "297673259093333",
            "110816108514696",
            "010095530095061",
            "539538979378545",
            "535952397793126",
            "102629507911949",
            "909440495188272",
            "326694805035205",
            "087536115437797",
            "807818326268925",
            "099437817579017",
            "241691319544961",
            "039611791528118",
            "501895608588874",
            "036005867066831",
            "349443165923240",
            "490901771824693",
            "577565666921667",
            "147868455942581",
            "860793674551248",
            "795303277234328",
            "677565721010634",
            "124213172094185",
            "465384234881980",
            "878502818765906",
            "756807071614602",
            "105520430849829",
            "510465747410027",
            "399521653093907",
            "458200703281672",
            "365320535290039",
            "811973989822967",
            "943772002516283",
            "341055707665342",
            "704205881382848",
            "383283663738894",
            "430707689283423",
            "484230789059087",
            "953732444433647",
            "993464031789912",
            "673240222041600",
            "207983318376529",
            "674932618805160",
            "528955668199726",
            "428400594936070",
            "613842878552861",
            "939557743062649",
            "022066124335463",
            "892666134679228",
            "104372037799069",
            "550414326130665",
            "663013964031565",
            "432552875976390",
            "391929847751527",
            "108798798160154",
            "590849414924067",
            "433819458979857",
            "208447763407609",
            "418763987433466",
            "825964303607697",
            "401869094430268",
            "544159331125402",
            "990690769136081",
            "687464774375658",
            "936489335638095",
            "770603543459508",
            "896533137471532",
            "821084437998670",
            "993743711404039",
            "265984157122763",
            "174386443421905",
            "527580958619449",
            "964066012817302",
            "736098969221933",
            "316432295841942",
            "960919351453590",
            "064129490758333",
            "460971282503488",
            "975687969224787",
            "113467672101950",
            "937345424764485",
            "191108460996512",
            "306182259069637",
            "231297323724846",
            "327224241964815",
            "080837032040657",
            "372373414686922",
            "079855811904551",
            "301405784977425",
            "746228269828267",
            "963879294343873",
            "308394730506736",
            "627767675043385",
            "087652826921404",
            "688244713723223",
            "715542801261083",
            "405272074129500",
            "255899678513498",
            "953660783329558",
            "698721275345817",
            "178284226474938",
            "027889827981440",
            "436052202473751",
            "198492243792921",
            "291423802155287",
            "976423582544558",
            "797401928414813",
            "786877124273509",
            "567118733121078",
            "279122037460405",
            "587758365435446",
            "773204150919652",
            "424609617650077",
            "464625175709813",
            "118247209455532",
            "231042294256043",
            "803943115862832",
            "406276443702100",
            "780234395924484",
            "179239130296456",
            "216005790625630",
            "803491366219945",
            "424942869172045",
            "262734470809862",
            "411343178192831",
            "060007489182699",
            "587215166996483",
            "891226100156531",
            "632904931802720",
            "107995481854346",
            "517911919508653",
            "009549936276010",
            "755226304962565",
            "959575366270176",
            "394401903846485",
            "665916960725056",
            "243023461935688",
            "728387888655306",
            "732212704466861",
            "795767684397031",
            "414709437147690",
            "197925992588513",
            "316688355678025",
            "903382620204966",
            "324227936596245",
            "592568693126392",
            "891141919566707",
            "993016619570180",
            "773130753233025",
            "041542667356342",
            "717844892133357",
            "938431628428854",
            "683147280310692",
            "990471656656886",
            "424500164421394",
            "216975154052854",
            "806795880967774",
            "485717238916522",
            "404508232335720",
            "170346202533076",
            "242083179352652",
            "699751903159758",
            "764001392588075",
            "670734870245457",
            "898517696060985",
            "651234209552950",
            "908481061782689",
            "382630648692703",
            "044580598488396",
            "495398966945520",
            "958451794005870",
            "976091798278902",
            "397713091994785",
            "340421328563205",
            "877765553016399",
            "460699291777247",
            "878209710252291",
            "414263164626405",
            "991805179062300",
            "805746842880317",
            "668583944405864",
            "276819794156767",
            "386900522619806",
            "825802189601950",
            "530949957459238",
            "740206324006953",
            "617703623643115",
            "599298129607878",
            "299274535495021",
            "066251071627602",
            "729764866781724",
            "052112392074979",
            "730417120319110",
            "961512125712717",
            "694689785538454",
            "932849095210143",
            "326432419406020",
            "785017180448582",
            "891555752104875",
            "539502218306546",
            "176762619280056",
            "189424021412583",
            "947202260136140",
            "843090531547409",
            "199737839097446",
            "930245680252047",
            "679757101040372",
            "287282280946473",
            "660687359498358",
            "329755416278566",
            "837410528415425",
            "146522230014125",
            "470776623530775",
            "374173386396430",
            "127790828675958",
            "634066495915358",
            "879598607434628",
            "380816464999102",
            "432256132797611",
            "078414720527306",
            "447065872477292",
            "223948417531166",
            "686726275494302",
            "804424743596232",
            "346209939270999",
            "228276271567854",
            "962216959251539",
            "485205368724200",
            "927543269995662",
            "956237180070818",
            "921634908017511",
            "771776960834706",
            "926330322534340",
            "001143947055232",
            "758913555431328",
            "743682204229663",
            "830246619543607",
            "353666422135306",
            "790088634051909",
            "771797038967999",
            "649510916819763",
            "647451902700181",
            "422165742999112",
            "999196909046186",
            "844694164618135",
            "901633747423771",
            "049897902765926",
            "110526835433475",
            "845207867221239",
            "016650705654474",
            "277222241282758",
            "712881189134877",
            "309006169429957",
            "744114918848572",
            "668309815575495",
            "012748985059774",
            "267556362466566",
            "485356915613265",
            "706750360457001",
            "188278206792458",
            "207200436301781",
            "512499429724221",
            "934410536789405",
            "614913637717560",
            "242038362471196",
            "185520359546637",
            "652019702429570",
            "198709680845405",
            "597341875074757",
            "999984660162167",
            "416459316454411",
            "984944576164311",
            "449304598241615",
            "607060669564409",
            "330185735226455",
            "843999525817880",
            "236370715865026",
            "241329517569135",
            "341532202483812",
            "493531910209047",
            "924450528711712",
            "655015758139559",
            "035126903980730",
            "185912344436161",
            "386508040990581",
            "762394620812019",
            "930724684938673",
            "360412621602535",
            "337449831564682",
            "276391862778888",
            "961155528569733",
            "374725139539815",
            "984534404024094",
            "460596095022647",
            "338143513301406",
            "223797628665481",
            "026428593212353",
            "301932315217094",
            "711996339067384",
            "219360556914659",
            "685401308645760",
            "153850238489208",
            "463975460661623",
            "034147235845983",
            "417068640037784",
            "582098567697875",
            "207747396374381",
            "655828123826704",
            "641365910900191",
            "763246748607787",
            "250655995519339",
            "822066849667529",
            "838880393669209",
            "612975339825826",
            "899736592757143",
            "457307541032073",
            "531230438261887",
            "536523921983830",
            "359312002811560",
            "474445226254702",
            "594594429159278",
            "506024984338681",
            "432869368993938",
            "900294807893540",
            "877676553938753",
            "820976593180236",
            "089059550952302",
            "172322578953320",
            "853668976444501",
            "955214072031949",
            "490273198438455",
            "036917925732483",
            "088502601699885",
            "277156565723931",
            "261625944828350",
            "649173633777726",
            "651270851451809",
            "270665329485968",
            "326132340596809",
            "272620906211828",
            "388118991760230",
            "180630317230854",
            "762507679953289",
            "571789951735992",
            "293126592854894",
            "428417114662353",
            "549642949026355",
            "006279968198291",
            "569036594005336",
            "290212111460693",
            "758470750790600",
            "144688537893065",
            "848987829987360",
            "254124718201730",
            "288158112336632",
            "652086104301023",
            "703205781384933",
            "159181317388027",
            "330480901382909",
            "866928959659873",
            "442440981465649",
            "067405641624324",
            "397087859358600",
            "366492646219452",
            "883136709657170",
            "482254939711965",
            "592557258382472",
            "420996137393878",
            "886232218602463",
            "944744061172489",
            "785363717261425",
            "645551007794479",
            "503130279284688",
            "405413598998253",
            "773262629168506",
            "136225586032216",
            "449619955266065",
            "035878901563168",
            "164682651434411",
            "279889026280175",
            "080787663836812",
            "521329380151021",
            "728307646302323",
            "851178450402739",
            "931896490720678",
            "499085542477796",
            "726433264923647",
            "727737535198159",
            "667728518805648",
            "466231434919423",
            "344998674290815",
            "722651169486967",
            "235704010750775",
            "172586248295986",
            "558157645308127",
            "423628005450074",
            "415984916339563",
            "630948736391962",
            "257646278153240",
            "017292656631467",
            "408124756434344",
            "241500726073065",
            "786948839776422",
            "211365215753273",
            "807961120572025",
            "879511316746214",
            "604026168376471",
            "652572310394890",
            "987063210957814",
            "894049609468587",
            "396285805128216",
            "564859831872113",
            "272403761513425",
            "218561706945583",
            "846282906372241",
            "918562754853100",
            "928330509321458",
            "981674796811916",
            "951673900615788",
            "757091747883354",
            "416695646240592",
            "765055237195200",
            "460964999369236",
            "035382555694079",
            "588130333756826",
            "424152136411797",
            "627501729101517",
            "550882899568577",
            "772003335064080",
            "993713184461588",
            "824244936584874",
            "889657199161760",
            "339699099790049",
            "043635937970038",
            "099950795452107",
            "489151083996029",
            "219632188408372",
            "841137559747721",
            "761493911415658",
            "434535527035249",
            "571850504408051",
            "318236239987342",
            "500461263896106",
            "457018777729440",
            "927403546784033",
            "307064163830064",
            "918697581370330",
            "425153373275835",
            "347722561888360",
            "901146774010990",
            "887007572299609",
            "234177159462453",
            "842217402887532",
            "129784844113933",
            "498889641983245",
            "649659366115596",
            "897685270178862",
            "778307679599537",
            "441732697353899",
            "478207967196138",
            "430509271166810",
            "076756878744255",
            "093684644168231",
            "344566442017823",
            "876578378422314",
            "756382631432236",
            "846420986867230",
            "829377768031964",
            "031626237922035",
            "759696527495105",
            "202175629792783",
            "679761790727376",
            "451055287582224",
            "647098865342191",
            "197467194505689",
            "893041317619087",
            "511223770441693",
            "197847894489891",
            "622751543867153",
            "352144972994494",
            "278403669597920",
            "699479187236725",
            "490824314018763",
            "880478478253577",
            "459157466375807",
            "793175960937320",
            "132050703011560",
            "607670763230318",
            "645815213316399",
            "893848903719000",
            "719561064238405",
            "894851294386519",
            "303186940923298",
            "966579776047887",
            "264012478744024",
            "000869135631562",
            "632015137733265",
            "642924230337810",
            "891859193157417",
            "534638386523174",
            "528539610372398",
            "690517781222163",
            "491775196466932",
            "248274096477583",
            "509597398687579",
            "828755845230486",
            "274998735912234",
            "356163563009896",
            "596123035224351",
            "233005659546550",
            "077057314305930",
            "993305575746408",
            "890394611739986",
            "371239339031570",
            "963639555530088",
            "402498242079511",
            "944879347988399",
            "785411955243173",
            "249291946917247",
            "628230321811499",
            "588565499895424",
            "202559278232630",
            "411376646183738",
            "126084761744491",
            "250002803493054",
            "027140932101817",
            "460524586282809",
            "098774721366569",
            "241075224635302",
            "660092494779654",
            "451984258189866",
            "618104069690257",
            "370033954262434",
            "450520838218925",
            "660815680621847",
            "705458091280267",
            "855186611950168",
            "794125040647676",
            "469082194442225",
            "222780822406173",
            "501699440131274",
            "542398673980317",
            "012210847117664",
            "660290584991921",
            "847764792372446",
            "507784030239255",
            "498476157714349",
            "816277470626396",
            "238717740078475",
            "283048849076722",
            "253465995561886",
            "118419416885964",
            "391705711140444",
            "979656065092917",
            "968244463349994",
            "014351432852138",
            "729578632618772",
            "662970257903739",
            "200438305530701",
            "218352675933469",
            "835477714951735",
            "027267171839614",
            "419030179204928",
            "687698690311589",
            "035334917280261",
            "154775882867339",
            "011055541003475",
            "494175185826233",
            "947124805840941",
            "083526624138997",
            "894803249977785",
            "834423526561019",
            "906260845262732",
            "292204348903422",
            "245345454305668",
            "069928239619466",
            "285570869596391",
            "748253527046649",
            "222025696296826",
            "476258191425053",
            "803446408141414",
            "462355335244986",
            "436824060741951",
            "131982284940476",
            "311949930679788",
            "772806653584567",
            "280717187892586",
            "215065029517769",
            "257481494242484",
            "353742917805843",
            "980029365659713",
            "719839193632987",
            "186656478245607",
            "414981489510590",
            "115994497187684",
            "654409707943501",
            "894202851809907",
            "977661530725986",
            "760765943618559",
            "260628001243792",
            "662065774092387",
            "540568094843038",
            "402208860852415",
            "750389077430208",
            "053907172707438",
            "307800893747737",
            "801778194862110",
            "891997307255027",
            "369644802285624",
            "487861337965040",
            "150766421147753",
            "120175661336148",
            "539329360960945",
            "657766440450862",
            "555064571782669",
            "066829037160107",
            "523522993361889",
            "243661187257924",
            "226752121743569",
            "680212045762736",
            "634031953629482",
            "161061386782439",
            "022865149211886",
            "708781432628914",
            "930960868815434",
            "768910098542332",
            "999098164118061",
            "805756597186784",
            "945308499957080",
            "346243180405549",
            "142751552384507",
            "878276874224096",
            "832225802374579",
            "543676877398639",
            "512912993975136",
            "676878680730719",
            "291608410940735",
            "742049522850709",
            "951646984332963",
            "555261187307591",
            "068833202925009",
            "217504387561028",
            "840432878939184",
            "238199383866445",
            "024305137260137",
            "702554387380966",
            "655852105105009",
            "376431781371749",
            "144741869191380",
            "807581117551762",
            "772807258631476",
            "922420571432693",
            "763881585241916",
            "507613496585187",
            "145709196945680",
            "460453450987381",
            "044530049222537",
            "813982023088683",
            "374859907756690",
            "116036584734416",
            "897871258486208",
            "185947932461028",
            "576886082944691",
            "625372583933634",
            "600593412666275",
            "982125970950232",
            "774613406040823",
            "995446889041148",
            "199402882622571",
            "723321521006366",
            "244877988277021",
            "600089638337314",
            "337532931295883",
            "276676433907070",
            "229195169118446",
            "655234264377783",
            "824544106903995",
            "325119182732143",
            "194989641742414",
            "265604980875945",
            "797103506224325",
            "715134154903814",
            "931335094210838",
            "131242419219594",
            "480975428824965",
            "791592538980974",
            "042068653536263",
            "582065612679960",
            "134018274364830",
            "610606657740488",
            "097842789193141",
            "878798378097343",
            "505637902312482",
            "495230465894452",
            "365882687990596",
            "234201657419527",
            "499167047612755",
            "554251288285479",
            "350056163772244",
            "505196797610003",
            "822766131280026",
            "518965904792841",
            "300427847876356",
            "288158455279334",
            "099259037545683",
            "705759835076108",
            "039979189013755",
            "822178905778830",
            "703102825468733",
            "766342900777050",
            "389801624496069",
            "896062584763487",
            "394623317842422",
            "900637316651995",
            "516067850261821",
            "486827830554233",
            "606235248664639",
            "737026060339653",
            "308549084868150",
            "792394836282651",
            "866082460816958",
            "150653399305829",
            "032707041091261",
            "550494657451832",
            "362976855490238",
            "974805142636193",
            "689479603737113"
    };


    public static ArrayList<String> ids_1000 = new ArrayList<>(1000);
    public static ArrayList<String> ids_2000 = new ArrayList<>(2000);
    public static ArrayList<String> ids_10000 = new ArrayList<>(10000);

    static {
        ids_1000.addAll(Arrays.asList(idArr).subList(0, 1000));
    }

    static {
        ids_2000.addAll(Arrays.asList(MyArrayUtil.idArr_10000).subList(0, 2000));
    }

    static {
        ids_10000.addAll(Arrays.asList(MyArrayUtil.idArr_10000));
    }

}
