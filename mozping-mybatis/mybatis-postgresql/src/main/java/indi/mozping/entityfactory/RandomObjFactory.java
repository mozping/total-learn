package indi.mozping.entityfactory;

import indi.mozping.entity.BaseObject;
import indi.mozping.entity.BodyObject;
import indi.mozping.entity.Car;
import indi.mozping.entity.Face;
import indi.mozping.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import static indi.mozping.entityfactory.RandomUtil.*;

/**
 * @author by mozping
 * @Classname RandomObjFactory
 * @Description ��������
 * @Date 2019/4/12 16:24
 */
public class RandomObjFactory {

    private static Random random = new Random();
    private static AtomicLong atomicLong = new AtomicLong(1546272000000L);


    public static List<Face> getRandomFaceWithNum(int num) {
        ArrayList<Face> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(getRandomFace());
        }
        atomicLong.addAndGet(1000);
        return list;
    }

    public static Car getRandomCar() {
        return getRandomCar(Math.abs(random.nextInt()));
    }

    public static Car getRandomCar(int i) {
        Car car = new Car();
        setBaseFeild(car, i);
        float confidence = confidence();
        car.setTargetType("car");
        car.setCarType(type(i));
        car.setCarTypeConfidence(confidence);

        car.setColor(color(i));
        car.setColorConfidence(confidence);

        car.setBrand(brand(i));
        car.setBrandCode(brandCode(i));
        car.setBrandConfidence(confidence);

        car.setPlateColor(color(i));
        car.setPlateColorConfidence(confidence);

        car.setPlateNumber(Util.generateCarID());
        car.setPlateNumberConfidence(confidence);


        return car;
    }

    public static Face getRandomFace() {
        return getRandomFace(Math.abs(random.nextInt()));
    }

    public static Face getRandomFace(int i) {
        Face face = new Face();
        setBaseFeild(face, i);
        float confidence = confidence();
        face.setTargetType("face");

        face.setAge(age(i));
        face.setAgeConfidence(confidence);

        face.setGender(gender(i));
        face.setGenderConfidence(confidence);

        face.setHat(yesNoUnKnow(i));
        face.setHatConfidence(confidence);

        face.setGlasses(glasses(i));
        face.setGlassesConfidence(confidence);

        face.setRace(race());
        face.setRaceConfidence(confidence);

        face.setMask(yesNoUnKnow(i));
        face.setMaskConfidence(confidence);

        //face.setFeature(Util.FEATURE);
        face.setFeature(Util.FEATURE_1);

        return face;
    }

    public static BodyObject getRandomBody() {
        return getRandomBody(Math.abs(random.nextInt()));
    }

    public static BodyObject getRandomBody(int i) {
        BodyObject body = new BodyObject();
        setBaseFeild(body, i);
        float confidence = confidence();
        body.setTargetType("body");
        body.setCoatColor(color(i));
        body.setCoatColorConfidence(confidence);

        body.setPantsColor(color(i));
        body.setPantsColorConfidence(confidence);

        body.setAngle(angle(i));
        body.setAngleConfidence(confidence);

        body.setAgeStage(ageStage(i));
        body.setAngleConfidence(confidence);

        body.setCoatStyle(coatStyle(i));
        body.setCoatStyleConfidence(confidence);

        body.setCoatPattern(coatPattern(i));
        body.setCoatPatternConfidence(confidence);

        body.setPantsStyle(pantsStyle(i));
        body.setPantsStyleConfidence(confidence);

        body.setPantsPattern(coatPattern(i));
        body.setPantsPatternConfidence(confidence);

        body.setHasCoat(yesNoUnKnow(i));
        body.setHasCoatConfidence(confidence);

        body.setGender(gender(i));
        body.setGenderConfidence(confidence);

        body.setHandbag(yesNoUnKnow(i));
        body.setHandbagConfidence(confidence());

        body.setSingleBag(yesNoUnKnow(i));
        body.setSingleBagConfidence(confidence);

        body.setBackBag(yesNoUnKnow(i));
        body.setBackBagConfidence(confidence);

        body.setDrawBox(yesNoUnKnow(i));
        body.setDrawBoxConfidence(confidence);

        body.setCart(yesNoUnKnow(i));
        body.setCartConfidence(confidence);

        //baseObject.setFeature(GlobalConsts.FEATURE);

        return body;
    }


    public static BaseObject setBaseFeild(BaseObject baseObject, int i) {
        baseObject.setTargetImage(picUrl(i));
        baseObject.setBackgroundImage(picUrl(i));
        baseObject.setFromImageId(numId(15));
        baseObject.setSourceId(Integer.toString(random.nextInt(100)));
        baseObject.setAreaId(numId(3));
        baseObject.setSourceType(sourceType(i));
        baseObject.setTaskType(taskType(i));
        baseObject.setFingerprint(fingerprint(i));
        baseObject.setGuid(numId(15));
        baseObject.setUuid("");
        baseObject.setTid(numId(15));
        baseObject.setAlgVersion(5030);

        baseObject.setBgImgHigh(bgImgHigh(i));
        baseObject.setBgImgWidth(bgImgWidth(i));

        baseObject.setTargetRect(getIntArr());
        baseObject.setTargetRectFloat(getFloatArr());
        baseObject.setImgRect(getIntArr());
        baseObject.setImgRectFloat(getFloatArr());

        baseObject.setDebug("Debug info");
        if (i % 100 == 0) {
            baseObject.setImgQuality("ok");
        }
        baseObject.setImgQuality("ok");
        baseObject.setImgQuality("ok");
        if (i % 1000 == 0) {
            baseObject.setOperator("ifaas-collection");
        }
        baseObject.setOperator("ifaas-engine");

        Date time = DateUtil.getTimeBetweenYear();
        baseObject.setTime(time);
        baseObject.setPdate(time);
        baseObject.setVtime(DateUtil.getVertialDate(time, DateUtil.DATE_REFER));
        return baseObject;
    }


}
