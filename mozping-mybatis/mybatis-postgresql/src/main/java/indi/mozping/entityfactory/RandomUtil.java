package indi.mozping.entityfactory;


import java.util.Random;

/**
 * @author by mozping
 * @Classname RandomUtil
 * @Description TODO
 * @Date 2019/4/12 11:26
 */
public class RandomUtil {

    private static Random random = new Random();
    private static String[] groupStr = new String[]{"group1", "group2", "group3", "group4", "group5", "group6"};
    private static String[] mStr = new String[]{"M00", "M01", "M02", "M03", "M04", "M05"};
    private static String[] sourceType = new String[]{"hikCamera", "zipFile", "liveVideo", "videoFile", "box"};
    private static String[] fingerprint = new String[]{"192.168.1.1", "192.168.1.2", "192.168.1.3", "192.168.1.4"};

    private static String alphas = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890";
    private static String nums = "1234567890";
    //private static int[] intArr = new int[]{1, 2, 3, 4};
    //private static float[] floatArr = new float[]{0.1F, 0.2F, 0.3F, 0.4F};
    private static String intArrStr = "1,2,3,4";
    private static String floatArrStr = "0.1,0.2,0.3,0.4";
    private static String[] typeArr = new String[]{
            "sedan",
            "microvan",
            "largeTruck",
            "suv",
            "minivan",
            "largeBus",
            "mpv",
            "bulkLorry",
            "lightBus",
            "craneTruck",
            "pickup",
            "midiBus",
            "engineeringRepairCar",
            "escortVehicle",
            "tanker",
            "slagCar",
            "unknown",
            "tricycle"
    };

    private static String[] colorArr = new String[]{
            "blue",
            "white",
            "gray",
            "black",
            "silvery",
            "red",
            "yellow",
            "green",
            "purple",
            "cyan",
            "brown",
            "golden",
            "pink",
            "unknown"
    };


    public static float age() {
        return Util.getRandomNum(1, 100);
    }

    public static int age(int i) {
        return i % 100;
    }

    public static float confidence() {
        return random.nextFloat();
    }

    public static String yesNo() {
        if (random.nextBoolean()) {
            return "yes";
        }
        return "no";
    }

    public static String yesNo(int i) {
        if (i % 2 == 0) {
            return "yes";
        }
        return "no";
    }

    public static String yesNoUnKnow() {
        int i = random.nextInt();
        if (i % 10 == 0) {
            return "unKnow";
        }
        if (random.nextBoolean()) {
            return "yes";
        }
        return "no";
    }

    public static String yesNoUnKnow(int i) {
        if (i % 10 == 0) {
            return "unKnow";
        }
        if (random.nextBoolean()) {
            return "yes";
        }
        return "no";
    }


    public static String targetType() {
        return getRandomInStrArr("face", "body", "car");
    }

    public static String targetType(int i) {
        if (i % 3 == 0) {
            return "face";
        }
        if (i % 3 == 1) {
            return "body";
        }
        return "car";
    }

    public static String sourceType() {
        return getRandomInStrArr(sourceType);
    }

    public static String sourceType(int i) {
        return sourceType[i % 5];
    }

    public static String taskType() {
        return getRandomInStrArr("analyzeTask", "snaperTask");
    }

    public static String taskType(int i) {
        if (i % 2 == 0) {
            return "analyzeTask";
        }
        return "snaperTask";
    }

    public static String fingerprint() {
        return getRandomInStrArr(fingerprint);
    }

    public static String fingerprint(int i) {
        return fingerprint[i % 4];
    }

    public static int bgImgHigh() {
        return Util.getRandomNum(0, 2000);
    }

    public static int bgImgHigh(int i) {
        return i % 2000;
    }

    public static int bgImgWidth() {
        return Util.getRandomNum(0, 2000);
    }

    public static int bgImgWidth(int i) {
        return i % 2000;
    }

    public static int[] intArr(int length) {
        return intArr(length, 0, 1000);
    }

    public static String getIntArr() {
        return intArrStr;
    }

    public static int[] intArr(int length, int min, int max) {
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = Util.getRandomNum(min, max);
        }
        return arr;
    }

    public static float[] floatArr(int length) {
        return floatArr(length, 0, 1);
    }

    public static String getFloatArr() {
        return floatArrStr;
    }


    public static float[] floatArr(int length, float min, float max) {
        float[] arr = new float[length];
        for (int i = 0; i < length; i++) {
            arr[i] = random.nextFloat() * (max - min);
        }
        return arr;
    }

    public static String type() {
        return getRandomInStrArr(
                "sedan",
                "microvan",
                "largeTruck",
                "suv",
                "minivan",
                "largeBus",
                "mpv",
                "bulkLorry",
                "lightBus",
                "craneTruck",
                "pickup",
                "midiBus",
                "engineeringRepairCar",
                "escortVehicle",
                "tanker",
                "slagCar",
                "unknown",
                "tricycle");

    }


    public static String type(int i) {
        return typeArr[i % 18];
    }

    public static String color() {
        return getRandomInStrArr(
                "blue",
                "white",
                "gray",
                "black",
                "silvery",
                "red",
                "yellow",
                "green",
                "purple",
                "cyan",
                "brown",
                "golden",
                "pink",
                "unknown"
        );
    }

    public static String color(int i) {
        return colorArr[i % 14];
    }

    public static String angle() {
        return getRandomInStrArr("front", "side", "back");
    }

    public static String angle(int i) {
        if (i % 3 == 0) {
            return "front";
        } else if (i % 3 == 1) {
            return "side";
        }
        return "back";
    }

    public static String ageStage() {
        return getRandomInStrArr("adult", "adult", "adult", "senior", "senior", "child");
    }

    public static String ageStage(int i) {
        int ret = i % 10;
        if (ret < 6) {
            return "adult";
        } else if (ret == 1) {
            return "child";
        }
        return "senior";
    }

    private static final String[] coatStyle =
            new String[]{"longSleeve", "shortSleevel", "shortSleevel", "longSleeve", "longSleeve"};

    public static String coatStyle() {
        if (random.nextInt() % 10 == 0) {
            return "unKnow";
        }
        return getRandomInStrArr("longSleeve", "shortSleevel", "shortSleevel", "longSleeve", "longSleeve");
    }

    public static String coatStyle(int i) {
        if (i % 10 == 0) {
            return "unKnow";
        }
        return coatStyle[i % 5];
    }

    private static final String[] coatPattern =
            new String[]{"grid", "joint", "pattern", "pure", "stripe", "uniform", "other"};

    private static final String[] pantsPattern =
            new String[]{"pants", "shorts", "skirt"};


    public static String coatPattern(int i) {
        if (random.nextInt() % 10 == 0) {
            return "unKnow";
        }
        return coatPattern[i % 7];
    }


    private static final String[] pantsStyle =
            new String[]{"pants", "shorts", "skirt"};


    public static String pantsStyle() {
        if (random.nextInt() % 10 == 0) {
            return "unKnow";
        }
        return getRandomInStrArr("pants", "shorts", "skirt");
    }

    public static String pantsStyle(int i) {
        if (random.nextInt() % 10 == 0) {
            return "unKnow";
        }
        return pantsPattern[i % 3];
    }


    public static String gender() {
        return getRandomInStrArr("male", "female");
    }

    public static String gender(int i) {
        if (i % 2 == 0) {
            return "male";
        }
        return "female";
    }

    public static String brand() {
        return getRandomInStrArr(RandomResources.brand);
    }

    public static String brand(int i) {
        return RandomResources.brand[i % (RandomResources.brand.length)];
    }

    public static String brandCode() {
        return getRandomInStrArr(RandomResources.brandCode);
    }


    public static String brandCode(int i) {
        return RandomResources.brandCode[i % (RandomResources.brandCode.length)];
    }

    private static final String[] glasses =
            new String[]{"sunglass", "glasses", "no", "unKnow"};

    public static String glasses() {
        return getRandomInStrArr("sunglass", "glasses", "no", "unKnow");
    }

    public static String glasses(int i) {
        return glasses[i % 4];
    }


    public static String race() {
        int i = random.nextInt();
        if (i % 10 == 0) {
            return "other";
        }
        return "han";
    }

    public static String picUrl() {
        StringBuilder sb = new StringBuilder();
        return sb.append("///")
                .append(getRandomInStrArr(groupStr)).append("/")
                .append("M").append(strWithLength(2)).append("/")
                .append(strWithLength(2)).append("/")
                .append(strWithLength(2)).append("/")
                .append(strWithLength(30))
                .append(".jpg")
                .toString();
    }

    public static String picUrl(int i) {
        return "///group3/MZ6/32/Y2/NZQMWAIGULE21FV5UT0X5M5QWQM0FJ.jpg";
    }


    private static String getRandomInStrArr(String... strArr) {
        int index = Util.getRandomNum(0, strArr.length);
        return strArr[index];
    }

    private static String strWithLength(int length) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(alphas.charAt(random.nextInt(36)));
        }
        return sb.toString();
    }


    public static String numId(int length) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(nums.charAt(random.nextInt(10)));
        }
        return sb.toString();
    }


    private static String featureStr;

    static {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 5000; i++) {
            sb.append(i);
        }
        featureStr = sb.toString();
    }
}
