package indi.mozping;

import java.util.Iterator;
import java.util.Objects;
import java.util.TreeSet;

/**
 * @author by mozping
 * @Classname CollectionTest
 * @Description TODO
 * @Date 2020/4/17 13:52
 */
public class CollectionTest {

    public static void main(String[] args) {
        testTreeSet();
    }

    public static void testTreeSet() {
        //��������
        TreeSet<NodeData> ts = new TreeSet<>();
        //��Ӷ���
        ts.add(new NodeData(1, "��1��"));
        ts.add(new NodeData(2, "��2��"));
        ts.add(new NodeData(3, "��3��"));
        ts.add(new NodeData(4, "��4��"));
        ts.add(new NodeData(5, "��5��"));
        ts.add(new NodeData(6, "��6��"));
        ts.add(new NodeData(7, "��7��"));
        //��foreach����
        for (NodeData cc : ts) {
            System.out.println(cc);
        }

        System.out.println("------------------");
        if(ts.contains(new NodeData(3, "��3��"))){
            ts.remove(new NodeData(3, "��3��"));
        }
        ts.add(new NodeData(15, "��15��"));

        Iterator<NodeData> iterator = ts.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }


    static class NodeData implements Comparable<NodeData> {
        private int index;
        private String data;



        public NodeData(int index, String data) {
            this.index = index;
            this.data = data;
        }

        @Override
        public String toString() {
            return "NodeData{" +
                    "index=" + index +
                    ", data='" + data + '\'' +
                    '}';
        }

        public int getIndex() {
            return index;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            NodeData nodeData = (NodeData) o;
            return index == nodeData.index && data.equals(nodeData.data);
        }

        @Override
        public int hashCode() {
            return Objects.hash(index, data);
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }


        @Override
        public int compareTo(NodeData o) {
            if (this.index > o.index) {
                return 1;
            } else if(this.index < o.index){
                return -1;
            }else {
                return 0;
            }
        }
    }
}