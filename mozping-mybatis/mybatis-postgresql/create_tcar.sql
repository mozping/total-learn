CREATE TABLE tcar  (
tid varchar(20) ,
operator varchar(20) ,
targetType varchar(5) ,
targetImage varchar(70) ,
backgroundImage varchar(70) ,
fromImageId varchar(20) ,

sourceId varchar(20) ,
areaId varchar(10) ,
sourceType varchar(10),
taskType varchar(15) ,
fingerprint varchar(20) ,

guid varchar(20) ,
uuid varchar(20) ,
algVersion int ,
bgImgHigh int ,

bgImgWidth int ,
targetRect varchar(20) ,
targetRectFloat varchar(20) ,
imgRect varchar(20) ,
imgRectFloat varchar(20) ,

debug varchar(15) ,
imgQuality varchar(5) ,
time timestamp,
pdate  date,
vtime timestamp,

padding varchar(30) ,
carType varchar(25) ,
carTypeConfidence FLOAT(10) ,
color varchar(10) ,
colorConfidence FLOAT(10) ,

brand varchar(40) ,
brandCode varchar(20) ,
brandConfidence FLOAT(10) ,
plateColor varchar(10)  ,
plateColorConfidence FLOAT(10) ,

plateNumber varchar(10) ,
plateNumberConfidence FLOAT(10)
)
partition by range (vtime)
(
    PARTITION pn START ('2019-12-31'::date) END ('2021-01-01'::date) EVERY ('7 day'::interval)
);

CREATE INDEX index_tcar_time ON tcar(time);
CREATE INDEX index_tcar_vtime ON tcar(vtime);
CREATE INDEX index_tid ON tcar(tid);
CREATE INDEX index_color ON tcar(color);