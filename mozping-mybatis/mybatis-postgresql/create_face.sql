CREATE TABLE face  (

operator varchar(20) ,
targetType varchar(5) ,
targetImage varchar(70) ,
backgroundImage varchar(70) ,
fromImageId varchar(20) ,

sourceId varchar(20) ,
areaId varchar(10) ,
sourceType varchar(10),
taskType varchar(15) ,
fingerprint varchar(20) ,

guid varchar(20) ,
uuid varchar(20) ,
tid varchar(20) ,
algVersion int ,
bgImgHigh int ,

bgImgWidth int ,
targetRect varchar(20) ,
targetRectFloat varchar(20) ,
imgRect varchar(20) ,
imgRectFloat varchar(20) ,

debug varchar(15) ,
imgQuality varchar(5) ,
time time,
pdate date,

padding varchar(30) ,

age int ,
ageConfidence FLOAT(10) ,
gender varchar(6) ,
genderConfidence FLOAT(10) ,

hat varchar(6) ,
hatConfidence FLOAT(10) ,

glasses varchar(12)  ,
glassesConfidence FLOAT(10) ,

race varchar(8) ,
raceConfidence FLOAT(10) ,

mask varchar(6) ,
maskConfidence FLOAT(10) ,

feature bytea
)
partition by range (pdate)
(
    PARTITION pn START ('2019-01-01'::date) END ('2019-03-01'::date) EVERY ('1 day'::interval),
    DEFAULT PARTITION pdefault
);

CREATE INDEX index_face_time ON face(time);