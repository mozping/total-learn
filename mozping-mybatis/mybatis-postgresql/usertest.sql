/*
Navicat PGSQL Data Transfer

Source Server         : 192.168.13.51_PG
Source Server Version : 80323
Source Host           : 192.168.13.51:5432
Source Database       : db1
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 80323
File Encoding         : 65001

Date: 2019-06-22 18:48:13
*/


-- ----------------------------
-- Table structure for usertest
-- ----------------------------
DROP TABLE IF EXISTS "public"."usertest";
CREATE TABLE "public"."usertest" (
"id" int4 DEFAULT nextval('usertest_id_seq'::regclass) NOT NULL,
"uname" varchar(50)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table usertest
-- ----------------------------
ALTER TABLE "public"."usertest" ADD PRIMARY KEY ("id");
