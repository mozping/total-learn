package jdbcdemo;

import org.junit.Test;

import java.sql.*;

/**
 * @author by mozping
 * @Classname Test01
 * @Description PreparedStatement
 * PreparedStatement只能用来为参数（如参数值）设置动态参数，即用?占位，不可用于表名、字段名等
 * @Date 2019/5/8 15:42
 */
public class Test01 {


    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://192.168.11.27:3306/demo?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true";
    static final String USER = "root";
    static final String PASS = "introcks1234";

    static Statement stmt = null;
    static PreparedStatement preparedStatement = null;
    static Connection conn = null;


    @Test
    public void test11() {
        int count = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql;
            ResultSet rs = null;
            //使用PreparedStatement
            sql = "SELECT * FROM ? where playName= ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "tb_player");
            preparedStatement.setString(2, "leonard");
            System.out.println("打印sql:" + preparedStatement.toString());
            rs = preparedStatement.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count++;
                }
            }
            System.out.println("count:" + count);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

}