package indi.mozping.mybatis;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import indi.mozping.dao.ItemMapper;
import indi.mozping.entity.Item;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @author by mozping
 * @Classname Test08
 * @Description ���Է�ҳ���
 * @Date 2019/5/13 17:10
 */
public class Test11 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-10.xml";

    //���Է�ҳ
    @Test
    public void queryPage() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        ItemMapper itemMapper = sqlSession.getMapper(ItemMapper.class);

        PageHelper.startPage(3, 5);

        List<Item> all = itemMapper.findAll();
        PageInfo<Item> pageInfo = new PageInfo<>(all);
        System.out.println("��ҳ������Ϣ: " + pageInfo);
        for (Item item : all) {
            System.out.println(item);
        }
    }
}