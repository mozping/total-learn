package indi.mozping.mybatis;

import indi.mozping.dao.ItemMapper;
import indi.mozping.entity.Item;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ���Ų��Դ���
 * @Date 2019/5/8 15:42
 */
public class Test01 {

    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstace().openSession();
        Item item = new Item();
        item.setId(1);
        item.setDescription("No 1");
        int rowAffected = sqlSession.insert("com.intellif.mozping.ItemMapper.addItem", item);
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void query() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstace().openSession();
        List<Object> list = sqlSession.selectList("com.intellif.mozping.ItemMapper.findItemById", 1);
        for (Object obj : list) {
            System.out.println(((Item) obj).toString());
        }
    }

    @Test
    public void update() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstace().openSession();
        Item item = new Item();
        item.setId(1);
        item.setDescription("No 1 be updated");
        int rowAffected = sqlSession.update("com.intellif.mozping.ItemMapper.updateItemById", item);
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void delete() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstace().openSession();
        int rowAffected = sqlSession.delete("com.intellif.mozping.ItemMapper.deleteItemById", 1);
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void deleteMany() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstace().openSession();
        int rowAffected = sqlSession.delete("com.intellif.mozping.ItemMapper.deleteManyItem", "No 1");
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void addInterface() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstace().openSession();
        Item item = new Item();
        item.setId(1);
        item.setDescription("No 1");
        ItemMapper mapper = sqlSession.getMapper(ItemMapper.class);
        int rowAffected = mapper.addItem(item);
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void proxyMybatis() {
        ItemMapper mapper = (ItemMapper) Proxy.newProxyInstance(ItemMapper.class.getClassLoader()
                , new Class[]{ItemMapper.class}, new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println(ItemMapper.class.getName() + "." + method.getName());
                        Object obj = null;
                        for (Object object : args) {
                            System.out.println(object);
                            obj = object;
                        }

                        // ʵ���߼�
                        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstace().openSession();
                        int result = sqlSession.insert(ItemMapper.class.getName() + "." + method.getName(), obj);

                        //��ʾ�ύ����
                        sqlSession.commit();
                        sqlSession.close();
                        return result;
                    }
                });

        Item item = new Item();
        item.setId(1);
        item.setDescription("No 1");

        int rowAffected = mapper.addItem(item);
        System.out.println("The rows be affected :" + rowAffected);
    }


}