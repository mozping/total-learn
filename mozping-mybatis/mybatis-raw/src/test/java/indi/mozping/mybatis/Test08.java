package indi.mozping.mybatis;

import indi.mozping.dao.mult.DepartmentMapper;
import indi.mozping.dao.mult.EmployeeMapper;
import indi.mozping.entity.mult.Department;
import indi.mozping.entity.mult.Employee;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @author by mozping
 * @Classname Test08
 * @Description ����һ��һ
 * ����һ�Զ�
 * ���Զ�Զ�
 * @Date 2019/5/10 17:00
 */
public class Test08 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-08.xml";

    //����һ��һ
    @Test
    public void queryEmp() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);

        List<Employee> employeeList = employeeMapper.selectAll();
        for (Employee e : employeeList) {
            System.out.println(e);
        }
    }

    //����һ�Զ�
    @Test
    public void queryDept() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);

        List<Department> departmentList = departmentMapper.queryAllDept();
        for (Department e : departmentList) {
            System.out.println(e);
        }
    }

}