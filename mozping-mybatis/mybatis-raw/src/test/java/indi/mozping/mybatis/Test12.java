package indi.mozping.mybatis;

import indi.mozping.dao.ItemMapper;
import indi.mozping.entity.Item;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

/**
 * @author by mozping
 * @Classname Test08
 * @Description ����Executor����������ݿ�ʱ��ͬ���͵Ĳ�����ʽ��simple(Ĭ��)��reuse��batch
 * @Date 2019/5/13 17:10
 */
public class Test12 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-12.xml";

    //������������
    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession(ExecutorType.BATCH);
        ItemMapper itemMapper = sqlSession.getMapper(ItemMapper.class);
        Item item1 = null;
        for (int i = 0; i < 10; i++) {
            item1 = new Item();
            item1.setDescription("No " + i + i);
            itemMapper.addItem(item1);
        }
        System.out.println("�ύ���...");
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }
}