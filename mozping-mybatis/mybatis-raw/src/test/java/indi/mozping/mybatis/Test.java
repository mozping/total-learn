package indi.mozping.mybatis;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.omg.CORBA.INTERNAL;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/12/11 19:47
 */
public class Test {

    public static void main(String[] args) {

        //yyyy-mm-dd
        LocalDateTime currentDateTime = getCurrentDateTime("Asia/Shanghai");
        LocalDateTime preDateTime = currentDateTime.minusDays(3);

        Date currentDate = Date.from(currentDateTime.atZone(ZoneId.of("Asia/Shanghai")).toInstant());
        Date preDate = Date.from(preDateTime.atZone(ZoneId.of("Asia/Shanghai")).toInstant());

        String currentDateStr = format(currentDate, "yyyy-MM-dd");
        String preDateStr = format(preDate, "yyyy-MM-dd");

        System.out.println(currentDateStr);
        System.out.println(preDateStr);
//        int total = 100;
//        List<Integer> list = new ArrayList<>();
//        for (int i = 1; i < total; i++) {
//            list.add(i);
//        }
//        int pageSize = 6;
//        int maxPage = (total / pageSize) + (total % pageSize > 0 ? 1 : 0);
//        System.out.println(maxPage);
//        System.out.println(list.subList(90, 99));
//        for (int pageNo = 1; pageNo <= maxPage; pageNo++) {
//            int start = (pageNo - 1) * pageSize;
//            int num = pageSize;
//            System.out.println(list.subList(start, start + num));
//        }

        //        int consume = 14;
//        int total = 80;
//        BigDecimal consumeMoney = new BigDecimal(consume);
//        BigDecimal budgetMoney = new BigDecimal(total);
//        int consumeRate = (int) (consumeMoney.divide(budgetMoney, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100 * 100);
//        System.out.println(consumeRate);


//        String str = "{\"name\":\"mozping\",\"age\":1}";
//        System.out.println(str);
//        System.out.println(JSON.toJSONString(str));
        //        Integer[] arr = new Integer[]{1, 2, 5, 100};
//        Integer[] arr = new Integer[]{};
//        System.out.println(Arrays.toString(arr));
//        String strip = StringUtils.strip(Arrays.toString(arr), "[]");
//        System.out.println(strip);
//        System.out.println(strip==null);
//        System.out.println(strip.hashCode());
//        Item item = new Item();
//        item.setName("testName");
//        item.setId(null);
//        System.out.println("---" + item.getId());
//        if(Objects.isNull(item) || 1 !=item.getId()){
//            System.out.println("id不合法");
//        }else {
//            System.out.println("合法");
//        }

//        int answer = new Random().nextInt(101);// ��Χ��[1,100]
//        System.out.println(answer);
//        Scanner sc = new Scanner(System.in);
//        int chance = 5;
//        for (; chance >= 1; chance--) {
//            System.out.println("���һ������");
//            int n = sc.nextInt();
//            if (n > answer) {
//                System.out.println("̫���ˣ����ز�");
//            } else if (n < answer) {
//                System.out.println("��С�ˣ����ز�");
//
//            } else {
//                System.out.println("��ϲ�����¶�����");
//                break;
//            }
//        }
//        System.out.println("�²���������꣬�����¿�ʼ��Ϸ��");
    }

    public static final String DATE_TIME = "yyyy-MM-dd HH:mm:ss";

    public static String format(Date date, String pattern) {
        if (date == null) {
            return "0000-00-00 00:00:00";
        }
        Instant instant = date.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static LocalDateTime getCurrentDateTime(String timeZone) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME);
        String currentDateTime = LocalDateTime.now(ZoneId.of(timeZone)).format(dateTimeFormatter);
        return LocalDateTime.parse(currentDateTime, dateTimeFormatter);
    }

}