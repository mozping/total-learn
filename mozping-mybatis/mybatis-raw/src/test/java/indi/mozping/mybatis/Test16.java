package indi.mozping.mybatis;

import indi.mozping.dao.PartitionInfoMapper;
import indi.mozping.entity.PartitionInfo;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ���Ų��Դ���
 * @Date 2019/5/8 15:42
 */
public class Test16 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-16.xml";

    @Test
    public void addBatch() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        ArrayList<PartitionInfo> list = new ArrayList<>();
        PartitionInfo partitionInfo = new PartitionInfo();
        partitionInfo.setNodeIpId(1);
        partitionInfo.setInitSchemaInfoId(1);
        partitionInfo.setPrefixTableIndex(1);
        partitionInfo.setCreateTime(new Date());

        PartitionInfo partitionInfo2 = new PartitionInfo();
        partitionInfo2.setNodeIpId(2);
        partitionInfo2.setInitSchemaInfoId(2);
        partitionInfo2.setPrefixTableIndex(2);
        partitionInfo2.setCreateTime(new Date());

        PartitionInfo partitionInfo3 = new PartitionInfo();
        partitionInfo3.setNodeIpId(3);
        partitionInfo3.setInitSchemaInfoId(3);
        partitionInfo3.setPrefixTableIndex(3);
        partitionInfo3.setCreateTime(new Date());

        list.add(partitionInfo);
        list.add(partitionInfo2);
        list.add(partitionInfo3);
        System.out.println("before insert ...");
        for (PartitionInfo p : list) {
            System.out.println(p);
        }

        PartitionInfoMapper mapper = sqlSession.getMapper(PartitionInfoMapper.class);
        int i = mapper.insertBatch(list);
        System.out.println("The rows be affected :" + i);

        System.out.println("after insert ...");
        for (PartitionInfo p : list) {
            System.out.println(p);
        }
        sqlSession.commit();
        sqlSession.close();
    }
}