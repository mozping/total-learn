package indi.mozping.mybatis;

import indi.mozping.dao.ItemMapper;
import indi.mozping.dao.PeopleMapper;
import indi.mozping.entity.People;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by mozping
 * @Classname Test07
 * @Description ���Զ�̬sql
 * @Date 2019/5/10 13:00
 */
public class Test07 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-07.xml";

    //����if��̬��ǩ����
    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();

        People people = new People();
//        people.setId(10);
        people.setAge(54);
        people.setAddress("hangzhou");
        people.setEdu("unKnow");

        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        int rowAffected = peopleMapper.addPeopleIf(people);
        System.out.println("The rows be affected :" + rowAffected);
        System.out.println("The primary key is:" + people.getId());
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }


    //����if��̬��ǩ��ѯ
    @Test
    public void query() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        List<People> byNameAndAddressIf = peopleMapper.findByNameAndAddressIf(null, null);
        for (People p : byNameAndAddressIf) {
            System.out.println(p);
        }
    }

    //����findByNameAndAddressChooseWhenOtherwise
    @Test
    public void queryChooseWhenOtherwise() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);

        //List<People> byNameAndAddressIf = peopleMapper.findByNameAndAddressChooseWhenOtherwise("Ma yun", "tianjing");
        //List<People> byNameAndAddressIf = peopleMapper.findByNameAndAddressChooseWhenOtherwise(null, "hangzhou");
        List<People> byNameAndAddressIf = peopleMapper.findByNameAndAddressChooseWhenOtherwise(null, null);
        for (People p : byNameAndAddressIf) {
            System.out.println(p);
        }
    }

    //����set��ǩ
    @Test
    public void updateWithSet() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        People people = new People();
        people.setId(11);
        people.setAge(54);
        people.setName("Ma hua teng");
        people.setAddress("shenzhen");
        people.setEdu("Bachelor");

        int rowAffected = peopleMapper.updateWithSet(people);
        System.out.println("rowAffected: " + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }


    //����trim��̬��ǩ��ѯ
    @Test
    public void queryTrim() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        List<People> byNameAndAddressIf = peopleMapper.findByNameAndAddressTrim("Parker", "tianjing");
        for (People p : byNameAndAddressIf) {
            System.out.println(p);
        }
    }

    //����foreach
    @Test
    public void queryForEach() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        ItemMapper itemMapper = sqlSession.getMapper(ItemMapper.class);
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            ids.add(i);
        }
        List<People> peopleList = peopleMapper.findByIdForEach(ids);
        for (People p : peopleList) {
            System.out.println(p);
        }
    }


    //����bind
    @Test
    public void queryByBind() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);

        List<People> peopleList = peopleMapper.findByBind();
        for (People p : peopleList) {
            System.out.println(p);
        }
    }

    //����sql
    @Test
    public void queryBySql() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);

        List<People> peopleList = peopleMapper.findBySql();
        for (People p : peopleList) {
            System.out.println(p);
        }
    }
}