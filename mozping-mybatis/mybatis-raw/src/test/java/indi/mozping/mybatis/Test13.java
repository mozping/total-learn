package indi.mozping.mybatis;

import indi.mozping.dao.ClusterNodesIpInfoMapper;
import indi.mozping.entity.ClusterNodesIpInfo;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Test13
 * @Description TODO
 * @Date 2019/9/27 12:10
 */
public class Test13 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-13.xml";


    @Test
    public void add1() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();

        ClusterNodesIpInfo clusterNodesIpInfo = new ClusterNodesIpInfo();
        clusterNodesIpInfo.setIp("114.114.114.114");
        clusterNodesIpInfo.setPgPort("1234");
        clusterNodesIpInfo.setNodePort("5678");
        clusterNodesIpInfo.setCreateTime(new Date());
        ClusterNodesIpInfoMapper clusterNodesIpInfoMapper = sqlSession.getMapper(ClusterNodesIpInfoMapper.class);

        int rowAffected = clusterNodesIpInfoMapper.insert(clusterNodesIpInfo);
        //int rowAffected = userMapper.addUserGetId(user);
        System.out.println("The rows be affected :" + rowAffected);
        //显示提交事务
        sqlSession.commit();
        SqlSessionFactoryUtil.closeSession(sqlSession);
    }
}