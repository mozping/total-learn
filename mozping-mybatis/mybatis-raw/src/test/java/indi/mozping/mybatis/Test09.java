package indi.mozping.mybatis;

import indi.mozping.dao.mult.DepartmentMapper;
import indi.mozping.dao.mult.EmployeeMapper;
import indi.mozping.entity.mult.Department;
import indi.mozping.entity.mult.Employee;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @author by mozping
 * @Classname Test08
 * @Description �����ӳټ���
 * @Date 2019/5/10 17:00
 */
public class Test09 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-09.xml";


    //�����ӳټ����µ� һ��һ
    @Test
    public void queryEmp() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);

        List<Employee> employeeList = employeeMapper.queryLazy();
        System.out.println("----------");
        for (Employee e : employeeList) {
            System.out.println(e.getId() + "--" + e.getAge() + "--" + e.getName());
            //System.out.println(e);
        }
    }

    //�����ӳټ����µ� ����һ�Զ�
    @Test
    public void queryDept() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);

        List<Department> departmentList = departmentMapper.queryAllDeptLazy();
        for (Department e : departmentList) {
            System.out.println(e.getDeptid() + "---" + e.getDescription());
            System.out.println(e);
        }
    }


}