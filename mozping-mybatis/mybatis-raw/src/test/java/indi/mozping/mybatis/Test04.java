package indi.mozping.mybatis;

import org.junit.Test;

import java.sql.*;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ���Զ�#��$
 * @Date 2019/5/9 19:00
 */
public class Test04 {

    //���ݿ������Ϣ
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://192.168.11.27:3306/demo?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true";
    static final String USER = "root";
    static final String PASS = "introcks1234";

    static Statement stmt = null;
    static PreparedStatement preparedStatement = null;
    static Connection conn = null;

    @Test
    public void test() {
        String nameRight = "Lebron James"; //ģ���û�������ȷ������ʱ��Ĳ�ѯ
        String fakeName = "Lebron Jamesxxx' or '1 = 1 "; //ģ���û�������������ʱ��Ĳ�ѯ
        int resultOfRightNameStatement = searchByName(nameRight, false); //ʹ��Statement��ѯ��ȷ������
        int resultOfFakeNameStatement = searchByName(fakeName, false); //ʹ��Statement��ѯ���������
        int resultOfRightNamePs = searchByName(nameRight, true); //ʹ��PreparedStatement��ѯ��ȷ������
        int resultOfFakeNamePs = searchByName(fakeName, true); //ʹ��PreparedStatement��ѯ���������
        //��ӡ�����ͨ���۲��ѯ�����ټ�¼�����ж��Ƿ���SQLע��
        System.out.println("ʹ��Statement��ѯ��ȷ��sql, ��ѯ����Ϊ:" + resultOfRightNameStatement);
        System.out.println("ʹ��Statement��ѯ�����sql,��ѯ����Ϊ:" + resultOfFakeNameStatement);
        System.out.println("ʹ��PreparedStatement��ѯ��ȷ��sql, ��ѯ����Ϊ:" + resultOfRightNamePs);
        System.out.println("ʹ��PreparedStatement��ѯ�����sql,��ѯ����Ϊ:" + resultOfFakeNamePs);
    }

    public static int searchByName(String username, boolean safe) {
        int count = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql;
            ResultSet rs = null;
            if (safe) {
                //��ȫ�Ĳ�ѯ����ʹ��PreparedStatement
                sql = "SELECT * FROM tb_player where playName= ?";
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setString(1, username);
                System.out.println("��ӡsql:" + preparedStatement.toString());
                rs = preparedStatement.executeQuery();
            } else {
                //����ȫ�Ĳ�ѯ��ʹ��Statement
                sql = "SELECT * FROM tb_player where playName='" + username + "'";
                Statement statement = conn.createStatement();
                System.out.println("��ӡsql:" + sql);
                rs = statement.executeQuery(sql);
            }
            if (rs != null) {
                while (rs.next()) {
                    //�����ѯ���Ľ������
                    count++;
                }
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // �ر���Դ
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return -1;
    }
}