package indi.mozping.mybatis;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import indi.mozping.dao.TFace3Mapper;
import indi.mozping.entity.TFace3;
import org.apache.commons.codec.binary.Base64;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ���Ų��Դ���
 * @Date 2019/5/8 15:42
 */
public class Test15 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-15.xml";
    private static final String URL = "http://192.168.13.85:9998/intellifDB/add/addBaseDBData";


    @Test
    public void query() throws InterruptedException {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        TFace3Mapper tFace3Mapper = sqlSession.getMapper(TFace3Mapper.class);
        for (long i = 1; i <= 10000; i += 200) {
            JSONArray array = new JSONArray();
            JSONArray data = new JSONArray();
            List<TFace3> list = tFace3Mapper.selectFromIdByCount(i, 200L);
            System.out.println(list.size());
            for (TFace3 tFace3 : list) {
                JSONObject dataItem = toDataItem(tFace3);
                data.add(dataItem);
            }
            JSONObject o = new JSONObject();
            o.put("schema", "t5030face");
            o.put("table", "test_");
            o.put("data", data);
            array.add(o);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<JSONObject> jsonObjectResponseEntity = restTemplate.postForEntity(URL, array, JSONObject.class);
            System.out.println(jsonObjectResponseEntity);
            Thread.sleep(300);
        }
    }


    public static JSONObject toDataItem(TFace3 tFace3) {
        JSONObject dataItem = new JSONObject();
        if (tFace3 != null && tFace3.getId() != null) {
            dataItem.put("id", tFace3.getId());
            dataItem.put("sequence", tFace3.getSequence());
            dataItem.put("type", tFace3.getType());
            dataItem.put("accessories", tFace3.getAccessories());
            dataItem.put("race", tFace3.getRace());
            dataItem.put("age", tFace3.getAge());


            dataItem.put("from_image_id", tFace3.getFromImageId());
            dataItem.put("from_person_id", tFace3.getFromPersonId());
            dataItem.put("from_video_id", tFace3.getFromVideoId());

            dataItem.put("gender", tFace3.getGender());
            dataItem.put("image_data", tFace3.getImageData());
            dataItem.put("indexed", tFace3.getIndexed());
            dataItem.put("source_id", tFace3.getSourceId());

            dataItem.put("source_type", tFace3.getSourceType());

            dataItem.put("time", getFormarMonth(tFace3.getTime()));

            dataItem.put("version", tFace3.getVersion());

            dataItem.put("json", tFace3.getJson());
            dataItem.put("quality", tFace3.getQuality());
            String s = Base64.encodeBase64String(tFace3.getFaceFeature());
            dataItem.put("feature", s);

        }
        return dataItem;
    }

    public static String getFormarMonth(Date date) {
        String format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

//    @Test
//    public void query5018() throws InterruptedException {
//        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
//        T5018Mapper t5018Mapper = sqlSession.getMapper(T5018Mapper.class);
//        List<T5018> list = t5018Mapper.selectFromIdByCount(1L, 100L);
//        System.out.println(list.size());
//        for (T5018 t : list) {
//            System.out.println(t);
//        }
//    }

}