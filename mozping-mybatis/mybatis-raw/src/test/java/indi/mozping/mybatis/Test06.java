package indi.mozping.mybatis;

import indi.mozping.dao.PeopleMapper;
import indi.mozping.entity.People;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ����������д(�������ݵļ�¼����д���� �� ����֮�� �� ���������ص�Java����)
 * @Date 2019/5/9 19:00
 */
public class Test06 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-06.xml";

    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();

        People people = new People();
        people.setAge(54);
        people.setName("Ma yun");
        people.setAddress("hangzhou");
        people.setEdu("unKnow");

        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        //int rowAffected = peopleMapper.addPeopleWithOutPrimaryKey(people);
        int rowAffected = peopleMapper.addPeopleWithOutPrimaryKey1(people);
        System.out.println("The rows be affected :" + rowAffected);
        System.out.println("The primary key is:" + people.getId());
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }


}