package indi.mozping.mybatis;

import indi.mozping.dao.mult.EmployeeMapper;
import indi.mozping.entity.mult.Employee;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @author by mozping
 * @Classname Test08
 * @Description ���Ի���
 * @Date 2019/5/10 17:00
 */
public class Test10 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-10.xml";


    //����һ������
    @Test
    public void queryEmp() throws InterruptedException {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);

        List<Employee> employeeList = employeeMapper.selectAll();
        for (Employee e : employeeList) {
            System.out.println(e);
        }
        Thread.sleep(20 * 1000);
        System.out.println("-------------------");
        List<Employee> employeeList1 = employeeMapper.selectAll();
        for (Employee e : employeeList1) {
            System.out.println(e);
        }

    }

    //���Զ�������
    @Test
    public void queryDept() {

    }


}