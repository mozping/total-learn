package indi.mozping.mybatis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author moore.mo
 * @description indi.mozping.mybatis Item
 * @date 2020/11/3 9:23 上午
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
class Item{
    Integer id;
    String name;
}

