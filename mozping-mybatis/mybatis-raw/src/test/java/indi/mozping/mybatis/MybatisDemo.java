package indi.mozping.mybatis;

import indi.mozping.dao.PlayerDao;
import indi.mozping.entity.Player;
import indi.mozping.querybean.PlayerQueryBean;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MybatisDemo {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws IOException {
        String resource = "mybatis/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 1.��ȡmybatis�����ļ���SqlSessionFactory
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        inputStream.close();
    }

    @Test
    // �����Զ�ӳ��
    public void testAutoMapping() throws IOException {
        // 2.��SqlSessionFactory��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);
        // 4.ִ�в�ѯ��䲢���ؽ��
        Player player = mapper.findPlayerById(1);
        System.out.println(player);
    }

    // �������ѯ
    @Test
    public void testManyParamQuery() {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);

        String team = "Laker";
        Float height = 1.98F;

        // ��һ�ַ�ʽʹ��map
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("team", team);
        params.put("height", height);
        List<Player> list1 = mapper.findPlayer1(params);
        System.out.println(list1);

        // �ڶ��ַ�ʽֱ��ʹ�ò���
        List<Player> list2 = mapper.findPlayer2(team, height);
        System.out.println(list2);

        // �����ַ�ʽ�ö���
        PlayerQueryBean playerQueryBean = new PlayerQueryBean();
        playerQueryBean.setTeam(team);
        playerQueryBean.setHeight(height);
        List<Player> list3 = mapper.findPlayer3(playerQueryBean);
        System.out.println(list3);
    }


    @Test
    // ���Բ��������Զ�����id
    public void testInsertGenerateId1() throws IOException {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);
        // 4.ִ�в�ѯ��䲢���ؽ��
        Player player = new Player();
        player.setPlayName("Allen Iverson");
        player.setPlayNo(3);
        player.setTeam("76ers");
        player.setHeight(1.83F);
        mapper.insertOnePlayer(player);
        sqlSession.commit();
        System.out.println(player.getId());
    }

    @Test
    // ���Բ��������Զ�����id
    public void testInsertGenerateId2() throws IOException {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);
        // 4.ִ�в�ѯ��䲢���ؽ��
        Player player = new Player();
        player.setPlayName("Tony Parker");
        player.setPlayNo(9);
        player.setTeam("spurs");
        player.setHeight(1.88F);
        mapper.insertOnePlayer2(player);
        sqlSession.commit();
        System.out.println(player.getId());
    }

    @Test
    // ����#�Ͳ���$�������
    public void testSqlInject() {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);
        String playName = "Lebron James";
        String fakeName = "Lebron Jamesxxx' or '1 = 1 ";
        //List<Player> list = mapper.findByName(playName);
        List<Player> list = mapper.findByName(fakeName);
        for (Player player : list) {
            System.out.println(player);
        }
        System.out.println(list.size());

    }


    @Test
    // ע�����
    public void testAnno() {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);
        Player player = mapper.selectByUserId(1);
        System.out.println(player.toString());
    }


    //--------------------------------��̬sql---------------------------------------

    /**
     * ��̬sql������Mapperӳ���ļ������֧���ֶΣ�ͨ����ǩ�ж��ֶε����ޣ���֧�ֶ������
     */

    @Test
    // if����select������where���
    public void testSelectDynamic() {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);

        String team = "laker";
        Float height = 1.99F;
        List<Player> list = mapper.selectHeightInTeam(team, height);
        for (Player player : list) {
            System.out.println(player);
        }
        System.out.println(list.size());

        List<Player> list1 = mapper.selectHeightInTeam(team, height);
        for (Player p : list1) {
            System.out.println(p);
        }
    }

    /**
     * ��̬����
     * ��̬�޸�
     */
    @Test
    // if����insert������trim���
    public void testInsertIfOper() {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);
        Player player = new Player();
        player.setPlayName("boshi");
        player.setPlayNo(1);
        player.setTeam("heat");
        player.setHeight(2.09F);
        System.out.println(mapper.insertDynamic(player));
        sqlSession.commit();
    }

    @Test
    // if����update������set���
    public void testUpdateIfOper() {
        // 2.��ȡsqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.��ȡ��Ӧmapper
        PlayerDao mapper = sqlSession.getMapper(PlayerDao.class);

        Player player = new Player();
        player.setPlayName("boshi1");
        player.setPlayNo(11);
        player.setTeam("heat1");
        player.setHeight(2.19F);
        player.setId(117);
        System.out.println(mapper.updateDynamic(player));
        sqlSession.commit();

    }

}
