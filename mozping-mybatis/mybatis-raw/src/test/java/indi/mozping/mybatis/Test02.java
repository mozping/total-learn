package indi.mozping.mybatis;

import indi.mozping.dao.MemberMapper;
import indi.mozping.entity.Member;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ��������ת����
 * @Date 2019/5/8 15:42
 */
public class Test02 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-02.xml";

    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        ArrayList<String> arr = new ArrayList<>();
        arr.add("computer");
        arr.add("Java");
        arr.add("Python");
        Member member = new Member();
        member.setId(1);
        member.setName("No 1");
        member.setSkills(arr);
        MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
        int rowAffected = mapper.addMember(member);
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void query() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
        List<Member> members = mapper.findMemberById(1);
        for (Member m : members) {
            System.out.println(m);
        }
    }


}