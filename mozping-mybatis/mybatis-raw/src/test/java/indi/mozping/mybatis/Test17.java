package indi.mozping.mybatis;

import java.sql.*;

/**
 * @author by mozping
 * @Classname Test17
 * @Description 查询 readOnly
 * @Date 2019/12/30 15:42
 */
public class Test17 {


    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://192.168.11.136:5050/db_test?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true";
    static final String USER = "readUser";
    static final String PASS = "123456";

    static Statement stmt = null;
    static PreparedStatement preparedStatement = null;
    static Connection conn = null;

    public static void main(String[] args) throws Exception {
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        String sql = "show global variables like \"%read_only%\";";
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String key = resultSet.getString("Variable_name");
            String value = resultSet.getNString("Value");
            System.out.println(key + " ---> " + value);
        }

        System.out.println("result:" + resultSet);
    }

}