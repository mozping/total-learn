package indi.mozping.mybatis;

import indi.mozping.dao.PeopleMapper;
import indi.mozping.entity.People;
import indi.mozping.querybean.PeopleQueryBean;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ���Զ�������Ĵ�ֵ��map(������ʹ��)/ע��(С��5��ʱʹ��)/javaBean(����5��ʱʹ��)
 * ���Է��ز����ǻ������ͣ������ѯ���ݼ�¼������
 * ����resultMap����
 * @Date 2019/5/9 19:00
 */
public class Test05 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-05.xml";

    @Test
    public void add() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();

        People people = new People();
        people.setId(3);
        people.setAge(21);
        people.setName("Duncan");
        people.setAddress("tianjing");
        people.setEdu("Bachelor");

        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        int rowAffected = peopleMapper.addPeople(people);
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void query() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);
        HashMap map = new HashMap();
        map.put("name", "Duncan");
        map.put("address", "beijing");
        //��ʽ1��map����
        List<People> peoples1 = peopleMapper.findByNameAndAddress1(map);
        for (People p : peoples1) {
            System.out.println(p);
        }

        PeopleQueryBean peopleQueryBean = new PeopleQueryBean();
        peopleQueryBean.setName("Duncan");
        peopleQueryBean.setAddress("beijing");
        //��ʽ2��javaBean����
        List<People> peoples2 = peopleMapper.findByNameAndAddress2(peopleQueryBean);
        for (People p : peoples2) {
            System.out.println(p);
        }

        //��ʽ3��ע�⴫��
        List<People> peoples3 = peopleMapper.findByNameAndAddress3("Duncan", "tianjing");
        for (People p : peoples3) {
            System.out.println(p);
        }
    }

    @Test
    public void count() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);

        int count = peopleMapper.countAll();
        System.out.println("������: " + count);

    }

    //���Բ�ѯ���ʹ��resultMap
    @Test
    public void queryGetWithResultMap() {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        PeopleMapper peopleMapper = sqlSession.getMapper(PeopleMapper.class);

        List<People> peoples = peopleMapper.findAll();
        for (People p : peoples) {
            System.out.println(p);
        }
    }
}