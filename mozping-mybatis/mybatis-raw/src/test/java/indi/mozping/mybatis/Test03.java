package indi.mozping.mybatis;

import indi.mozping.entity.Item;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ���Զ�environments
 * @Date 2019/5/8 15:42
 */
public class Test03 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-03.xml";

    @Test
    public void add() throws IOException {

        InputStream in = Resources.getResourceAsStream(CONFIG_FILE_PATH);
        //SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in, "other");
        SqlSession sqlSession = factory.openSession();
        Item item = new Item();
        item.setId(1);
        item.setDescription("No 1");
        int rowAffected = sqlSession.insert("com.intellif.mozping.ItemMapper.addItem", item);
        System.out.println("The rows be affected :" + rowAffected);
        //��ʾ�ύ����
        sqlSession.commit();
        sqlSession.close();
    }


}