package indi.mozping.mybatis;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import indi.mozping.dao.T5018Mapper;
import indi.mozping.entity.T5018;
import org.apache.commons.codec.binary.Base64;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author by mozping
 * @Classname Test01
 * @Description ���Ų��Դ���
 * @Date 2019/5/8 15:42
 */
public class Test14 {

    private static final String CONFIG_FILE_PATH = "mybatis/mybatis-config-14.xml";
    private static final String URL = "http://localhost:9998/intellifDB/add/addBaseDBData";

    @Test
    public void getTest() {
        String url = "https://time.geekbang.org/column/article/68319";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<JSONObject> jsonObjectResponseEntity = restTemplate.postForEntity(URL, null, JSONObject.class);
        System.out.println(jsonObjectResponseEntity);
    }

    @Test
    public void query1() throws InterruptedException {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        T5018Mapper t5018Mapper = sqlSession.getMapper(T5018Mapper.class);
        // long begin = 89297;
        // long End = begin + 10000;
        for (long i = 1; i <= 100000; i++) {
            JSONArray array = new JSONArray();
            JSONArray data = new JSONArray();

            T5018 t5018 = t5018Mapper.selectByPrimaryKey(i);
            //List<T5018> list = t5018Mapper.selectByPrimaryKey(i);
            {
                JSONObject dataItem = toDataItem(t5018);
                if (dataItem == null || dataItem.isEmpty()) {
                    continue;
                }

                dataItem.put("create_time",
                        "2019-12-13 06:01:01");
                data.add(dataItem);
            }
            JSONObject o = new JSONObject();
//            o.put("schema", "t5018test");
//            o.put("table", "test_");
            o.put("schema_name", "mzptest19");
            o.put("prefix_table_name", "test_");
            o.put("response_fields", "id,type,version,created");
            o.put("data", data);
            array.add(o);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<JSONObject> jsonObjectResponseEntity = restTemplate.postForEntity(URL, array, JSONObject.class);
            System.out.println(jsonObjectResponseEntity);
//            Thread.sleep(5000);
            break;
        }
    }

    @Test
    public void query() throws InterruptedException {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        T5018Mapper t5018Mapper = sqlSession.getMapper(T5018Mapper.class);
        // long begin = 89297;
        // long End = begin + 10000;
        for (long i = 10000; i <= 1000000; i += 100) {
            JSONArray array = new JSONArray();
            JSONArray data = new JSONArray();

            List<T5018> list = t5018Mapper.selectFromIdByCount(i, 100L);
            //List<T5018> list = t5018Mapper.selectByPrimaryKey(i);
            for (T5018 t : list) {
                JSONObject dataItem = toDataItem(t);
                if (dataItem == null || dataItem.isEmpty()) {
                    continue;
                }
//                dataItem.put("create_time",
//                        "2019-12-13 06:01:01");
                data.add(dataItem);
            }
            JSONObject o = new JSONObject();
//            o.put("schema", "t5018test");
//            o.put("table", "test_");
            o.put("schema_name", "mzptest15");
            o.put("prefix_table_name", "test_");
            o.put("response_fields", "id,type,version,created,updated");
            //o.put("response_fields", "id,type,version,updated");
            o.put("data", data);
            array.add(o);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<JSONObject> jsonObjectResponseEntity = restTemplate.postForEntity(URL, array, JSONObject.class);
            System.out.println(jsonObjectResponseEntity);
            Thread.sleep(100);
            //break;
        }
    }

    public static JSONObject toDataItem(T5018 t5018) {
        JSONObject dataItem = new JSONObject();
        if (t5018.getIndexed() == -2 || t5018.getFaceFeature().length < 2000) {
            return dataItem;
        }
        if (t5018 != null && t5018.getId() != null) {
            dataItem.put("id", t5018.getId());
//            dataItem.put("id", 999999999L);
            dataItem.put("type", "1");
            dataItem.put("created", getFormarMonth(t5018.getCreated()));
            dataItem.put("updated", getFormarMonth(t5018.getUpdated()));
            dataItem.put("accessories", 1);
            dataItem.put("race", 1);
            dataItem.put("age", 1);
            String featrue = "veSObTquY7s9mhJd28AHDAoEBg8JDg0FCwA8MLwwrgW9FagyOzKS7mT3PRM8Td9gPeAlHj0CabC3N7qyPUn7ybxnaOC8qBMjNHo7TbyFpcq9+xh0PGAm88VJPUS8+88HPCOeb71qm353GD0NPG/J9jvEGhc8z0mP4i07zb2LP4m9vs9EPDiAN/BuPSk9F2aPPUcDTLwaNW38yT3kPNI/S70wZuu96ymiouS8WLmeTlq8VNxDvYVovjtlvVC8MkqNvRtOYLset7aBv7znPEumTLxjfVA9q9hyxUu9bL3ke4m7e55jPbbzYSLOuxW94Y6EPVXTWD1aDEqw9ztnvIwzZ7z4EXC7fXPsCGs92r0k8ak8ItttvVoRP1B2PU081WtEPJmuPLsAL4foiL0xvdAEQzwMWEy8d1csGz+8QzxG7kS9Bw94PJIClv37Paa6VE1DvFX0Dby2Ka+/8L2aPdm2IjyVGvM9XM8OitC8tL7SEEq8tnUMPQ47EyBXvSU9437cPTRbnTxyamZ8eT2Pu+SrZT2QtJk8F1T+1VG8sjt+I6G9BAcQPbQDk+VCPT287DxkPHQF0z0bjZz/nb3bvHugQj2GEGS9FoXyvLg9M7xjPD09Yt3PvRe0ijG6uxs9aIBePZs7BbxKFhxRmjwTO7UFEjwurpq9nM/NcbE7Fju26j08AkTdvRLCNAVFvf09cKKDvT+F+rzojGUxDbzovdpjlrxhWq69KnozD3K9Wj3dhdA9l6uhPFsBUA2VPbM9lz50vdwwM72xWFWYYrqpPX2Sy72BPSS9dZYZCus9+j0dCZm9G15evGtGDG1UvQw9JBONvXIacLyMEZQcXTvJvdns2D3AKho5zCJw25895jyy3+S7tFYNPEW8ycHjvdI97h/6vRcXIL1CDCSsMD2jva8KfDwR++I9w4oFbn29Qz0YXmM9PLYpO5ZfsYBHPHe8UIK9vQ+dO7zkDuGEJLx1vbpKLblnZME9pKuDChw8fD0cI1w8MqREvNaIi7W/O7A9sfE3vEBpKzs3NDLWg73XPXIfUDx4KmE8lB1sVSU8OzwECMy8wZL0PQiFmgW+vf08VTgyPIKk77v77Y6167sJvPIGlDxZqpk7pZSn5TC9iD1251Q7zmf1PcyJG+RHPHa7SjdvPIjf0DwghvoZpz3turW9bTzaMK08lIZm+AS7obzQwyg9ivLtvaW6bMcVvQi8jWQfPcCyur2DgoGlhbwDPU7o+LxfOI87okaWVHg7ML2VdMG7HKiAvI1tTi+rPOC9W1soveUnZ72QmwqFKL3uvQuevjzN13y8A+EKGV09kDrzLXu9LzvYPGcF9qSjvaA8WoRzO4ayFzz0gR+eobsbPeSkALykRkw8SLKdwU+97T1217K8Tx5JvABRkxQHvUC9TEkiPSSF+D0/ygK/i7zTO5DuH7pynQO9podbUjk8yrzjD0u9kMo6Pft3wHn1vJo8Lvo3uxK8nDtn2SriiD0gPAUJID1du0u9eCrBq6S8Y7tWWJM9qTzDvOcKRC7cvHw8ewbBPOAc0LxGuYNCM71gPAH28rvHfvq7WTZXE7+9w70qShe9cG93vdwRGaHUPGQ9eRH+vSYJPTwwa2stHbwmvWMNrj2//j47fjXKNEM81brlZva9gasRvGrgD/JXPRw9RqEIPFUSjzx/MRfAgrv0vPWPKzz3+U08XXkmUWw9HjxRePY8b7jIPJ+IeHdfvIE7qVq8Pc869jqCRJExADuNvO6iwr0CMjg9QDMoJD28tT2gxJ48qyyrvYoKGoGcvSk9bu0Du3peOD2nvoQiLDstvdK2CL0OAkc+KYomcji9MjwhBu69DS/ivJt0uEFEvYE9TIVAvWgcAz0cIF9MOzwOvT2jZT0TLBs8eicitj09MTzMaaq86sADvPT790cKvPE8Lzt5vqpsz72cBoFUsjwRPcRoFb1g9PO8d2OJgxW9uL1Wj7q92x3wPAz4Q+/uPf49vQK3PHKaAr1gx89cszuTPCJ4tr7X4g48wgFLKu096bsSx3W9trBQPVEwFwjCvXU9YzDWuwmgILzqg0Xyary2PBCZpL365y28HnnUHlI9xD1Umvq8/rEzO3qOS9phPYS75RgmPMmpXz2Fmvua/TzNvRUndr2FIks9WLkMwdE99j19M247bXhPPSP3oV9HPOK9sMWZvWItQb3SYplCSDyuPW7gm714PGU8vRlOaGI7ub2djrC9KmKnPDphJi9BPTS63EVIPKHp27whoqJisL3FvTk45bwGyuW8n/BVsNM9V7xdjgk9R1jYPLWt6BidvXA9bVMhvZmkx70FPjsaoz3iOk89FDxAsj89LOOn4VY9/ztBUDe9NuPEPAo25cTyvDG8idKzO6LZg7tDxyg3xLzHuhVSurx5auM8kKGFcFG9FLw4WI29YFt4O84VrYvPu5o8J/htPKLB6zyoJ9qRwjwdvSr0FTv80OC9K/OgtTO9871rJQC9KH9cvTdYOCa1PEm9e5eZvahFuj2LcyS4szxXPdorC73bEn29T1Bc+XY8bT2+CYs8yCTVvZTY61hmvTC8bdmdvBOFxzy+fZ4qcLzUvZwy8bxZZQM8j9QwQ289eL0wtr+7HV4+vIf5rL2CvTK8fCUlPLgEarosoZs6AL0tvY4ltTyG7cA863VFO7u7IL2uCFS9ats6PJsBFfLqPYQ8J9aaO6Kdd72/PfsUET3wvax1K7vZaTu9ohwVGzm9rz32GYy9Rg1EPGqVCrFCPQUBAgMI";
            String s = Base64.encodeBase64String(t5018.getFaceFeature());
            dataItem.put("feature", featrue);

            dataItem.put("from_cid_id", t5018.getFromCidId());
            dataItem.put("from_image_id", t5018.getFromImageId());

            dataItem.put("image_data", t5018.getImageData());
            dataItem.put("indexed", t5018.getIndexed());

            String s1 = UUID.randomUUID().toString().replaceAll("-", "");
            dataItem.put("source_id", s1.substring(0, 10));
            dataItem.put("source_type", "");

            dataItem.put("version", "5030");

            dataItem.put("json", t5018.getJson());
            dataItem.put("quality", 1);
        }
        return dataItem;
    }

    public static String getFormarMonth(Date date) {
        String format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    @Test
    public void query5018() throws InterruptedException {
        SqlSession sqlSession = SqlSessionFactoryUtil.getSqlSessionFactoryInstaceByConfig(CONFIG_FILE_PATH).openSession();
        T5018Mapper t5018Mapper = sqlSession.getMapper(T5018Mapper.class);
        List<T5018> list = t5018Mapper.selectFromIdByCount(1L, 100L);
        System.out.println(list.size());
        for (T5018 t : list) {
            System.out.println(t);
        }
    }

}