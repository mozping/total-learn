/*
Navicat MySQL Data Transfer

Source Server         : 192.168.11.31_root
Source Server Version : 50722
Source Host           : 192.168.11.31:3306
Source Database       : mybatis_learn

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2019-03-12 19:48:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_player
-- ----------------------------
DROP TABLE IF EXISTS `tb_player`;
CREATE TABLE `tb_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playName` varchar(18) DEFAULT NULL,
  `playNo` int(11) DEFAULT NULL,
  `team` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_player
-- ----------------------------
INSERT INTO `tb_player` VALUES ('1', 'Kobe Brayent', '24', 'laker');
INSERT INTO `tb_player` VALUES ('2', 'Lebron James', '23', 'laker');
INSERT INTO `tb_player` VALUES ('3', 'Tim Duncan', '21', 'spurs');
INSERT INTO `tb_player` VALUES ('4', 'leonard', '2', 'raptors');
INSERT INTO `tb_player` VALUES ('5', 'Stephen Curry', '30', 'warriors');
INSERT INTO `tb_player` VALUES ('6', 'Klay Thompson', '11', 'warriors');
