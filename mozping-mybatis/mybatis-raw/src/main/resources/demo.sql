/*
Navicat MySQL Data Transfer

Source Server         : 192.168.11.27
Source Server Version : 50722
Source Host           : 192.168.11.27:3306
Source Database       : demo

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2019-05-10 17:41:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_item
-- ----------------------------
DROP TABLE IF EXISTS `tb_item`;
CREATE TABLE `tb_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_item
-- ----------------------------
INSERT INTO `tb_item` VALUES ('1', 'No 1');

-- ----------------------------
-- Table structure for tb_member
-- ----------------------------
DROP TABLE IF EXISTS `tb_member`;
CREATE TABLE `tb_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(18) DEFAULT NULL,
  `skills` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_member
-- ----------------------------
INSERT INTO `tb_member` VALUES ('1', 'No 1', 'computer;Java;Python;');

-- ----------------------------
-- Table structure for tb_people
-- ----------------------------
DROP TABLE IF EXISTS `tb_people`;
CREATE TABLE `tb_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(18) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(18) DEFAULT NULL,
  `edu` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_people
-- ----------------------------
INSERT INTO `tb_people` VALUES ('1', 'Duncan', '12', 'beijing', 'Doctor');
INSERT INTO `tb_people` VALUES ('2', 'Parker', '20', 'tianjing', 'Bachelor');
INSERT INTO `tb_people` VALUES ('3', 'Duncan', '21', 'tianjing', 'Bachelor');
INSERT INTO `tb_people` VALUES ('7', 'Ma yun', '54', 'hangzhou', 'unKnow');
INSERT INTO `tb_people` VALUES ('8', 'Ma yun', '54', 'hangzhou', 'unKnow');
INSERT INTO `tb_people` VALUES ('9', 'Ma yun', '54', 'hangzhou', 'unKnow');
INSERT INTO `tb_people` VALUES ('10', 'Ma hua teng', '54', 'hangzhou', 'unKnow');
INSERT INTO `tb_people` VALUES ('11', 'Ma hua teng', '54', 'shenzhen', 'Bachelor');

-- ----------------------------
-- Table structure for tb_player
-- ----------------------------
DROP TABLE IF EXISTS `tb_player`;
CREATE TABLE `tb_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playName` varchar(18) DEFAULT NULL,
  `playNo` int(11) DEFAULT NULL,
  `team` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_player
-- ----------------------------
INSERT INTO `tb_player` VALUES ('1', 'Kobe Brayent', '24', 'laker');
INSERT INTO `tb_player` VALUES ('2', 'Lebron James', '23', 'laker');
INSERT INTO `tb_player` VALUES ('3', 'Tim Duncan', '21', 'spurs');
INSERT INTO `tb_player` VALUES ('4', 'leonard', '2', 'raptors');
INSERT INTO `tb_player` VALUES ('5', 'Stephen Curry', '30', 'warriors');
INSERT INTO `tb_player` VALUES ('6', 'Klay Thompson', '11', 'warriors');

-- ----------------------------
-- Table structure for tb_team
-- ----------------------------
DROP TABLE IF EXISTS `tb_team`;
CREATE TABLE `tb_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teamName` varchar(20) DEFAULT NULL,
  `description` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_team
-- ----------------------------
