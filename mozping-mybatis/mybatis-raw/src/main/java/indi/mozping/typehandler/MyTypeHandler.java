package indi.mozping.typehandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * @author by mozping
 * @Classname MyTypeHandler
 * @Description 自定义Mybatis中的类型转换器，将List映射为数据库的VARCHAR类型
 * 我们定义一个字段，在java中使用
 * @Date 2019/5/9 13:41
 */
@MappedJdbcTypes(JdbcType.VARCHAR)//映射器对应的数据库类型
@MappedTypes(List.class)//映射器对应的java类型
public class MyTypeHandler extends BaseTypeHandler<List<String>> {


    /**
     * 设置非空参数，执行SQL语句时对占位符进行设置
     * 比如:sql = "insert into t_menber (name,age,skills)values(?,?,?)"
     */
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<String> parameter, JdbcType jdbcType) throws SQLException {
        System.out.println("setNonNullParameter#setNonNullParameter execute..." + jdbcType);
        StringBuilder sb = new StringBuilder();
        for (String str : parameter) {
            sb.append(str).append(";");
        }
        //将list拼装成的String设置进去
        ps.setString(i, sb.toString());
    }

    /**
     * 下面三个方法都是从数据库中获取记录，我们将获取到的记录从VARCHAR转换为List类型
     */
    @Override
    public List<String> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String[] StrArr = rs.getString(columnName).split(";");
        return Arrays.asList(StrArr);
    }

    @Override
    public List<String> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String[] StrArr = rs.getString(columnIndex).split(";");
        return Arrays.asList(StrArr);
    }

    @Override
    public List<String> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String[] StrArr = cs.getString(columnIndex).split(";");
        return Arrays.asList(StrArr);
    }
}