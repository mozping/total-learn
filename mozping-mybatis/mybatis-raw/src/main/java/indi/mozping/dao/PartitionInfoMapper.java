package indi.mozping.dao;

import indi.mozping.entity.PartitionInfo;

import java.util.List;

public interface PartitionInfoMapper {

    void createTabale();

    int deleteByPrimaryKey(Integer id);

    int insert(PartitionInfo record);

    int insertSelective(PartitionInfo record);

    PartitionInfo selectByPrimaryKey(Integer id);

    List<PartitionInfo> selectByNodeIpId(List<Integer> nodeIpId);

    int updateByPrimaryKeySelective(PartitionInfo record);

    int updateByPrimaryKey(PartitionInfo record);

    /**
     * 查询所有分区信息
     *
     * @return
     */
    List<PartitionInfo> selectAll();

    int deleteByInitSchemaInfoIdLogic(List<Integer> list);

    Integer deleteByNodeIpId(Integer nodeIpId);

    List<PartitionInfo> selectBySchemaInfoId(Integer schemaInfoId);


    int insertBatch(List<PartitionInfo> partitionInfoList);
}