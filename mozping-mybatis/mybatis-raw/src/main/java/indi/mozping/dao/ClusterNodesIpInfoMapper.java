package indi.mozping.dao;


import indi.mozping.entity.ClusterNodesIpInfo;

public interface ClusterNodesIpInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClusterNodesIpInfo record);

    int insertSelective(ClusterNodesIpInfo record);

    ClusterNodesIpInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClusterNodesIpInfo record);

    int updateByPrimaryKey(ClusterNodesIpInfo record);
}