package indi.mozping.dao;

import indi.mozping.entity.Player;
import indi.mozping.querybean.PlayerQueryBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author by mozping
 * @Classname PlayerDao
 * @Description TODO
 * @Date 2019/3/13 14:14
 */
public interface PlayerDao {


    int insertOnePlayer(Player player);

    int insertOnePlayer2(Player player);

    int insertDynamic(Player player);

    int updateDynamic(Player player);

    /**
     * @param id
     * @return 查询到的Player信息
     * @Description: 根据id查询运动员信息
     * @date 2019/3/13 14:33
     */
    Player findPlayerById(int id);

    /**
     * 多属性字段条件查询的几种方式：
     * 1.Map的方式
     * 2.bean的方式
     * 3.注解的方式
     */
    List<Player> findPlayer1(Map<String, Object> queryMap);

    List<Player> findPlayer3(PlayerQueryBean playerQueryBean);

    List<Player> findPlayer2(@Param("team") String team, @Param("height") Float height);

    List<Player> findByName(@Param("playName") String playName);

    @Select("select * from tb_player where id = #{id}")
    Player selectByUserId(int id);

    List<Player> selectHeightInTeam(@Param("team") String team, @Param("height") Float height);


}
