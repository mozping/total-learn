package indi.mozping.dao;


import indi.mozping.entity.T5018;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface T5018Mapper {


    T5018 selectByPrimaryKey(Long id);

    List<T5018> selectFromIdByCount(@Param("id") Long id, @Param("count") Long count);


}