package indi.mozping.dao.mult;

import indi.mozping.entity.mult.Employee;

import java.util.List;

/**
 * @author by mozping
 * @Classname EmployeeMapper
 * @Description TODO
 * @Date 2019/5/13 11:08
 */
public interface EmployeeMapper {

    List<Employee> selectAll();

    List<Employee> queryLazy();
}