package indi.mozping.dao;


import indi.mozping.entity.TFace3;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TFace3Mapper {


    TFace3 selectByPrimaryKey(Long sequence);

    List<TFace3> selectFromIdByCount(@Param("id") Long id, @Param("count") Long count);
}