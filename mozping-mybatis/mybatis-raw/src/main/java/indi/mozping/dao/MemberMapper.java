package indi.mozping.dao;

import indi.mozping.entity.Member;

import java.util.List;

/**
 * @author by mozping
 * @Classname ItemMapper
 * @Description TODO
 * @Date 2019/5/8 15:34
 */
public interface MemberMapper {

    int addMember(Member member);

    List<Member> findMemberById(int id);

}