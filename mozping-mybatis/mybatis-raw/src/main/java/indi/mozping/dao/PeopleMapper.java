package indi.mozping.dao;

import indi.mozping.entity.People;
import indi.mozping.querybean.PeopleQueryBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author by mozping
 * @Classname PeopleMapper
 * @Description TODO
 * @Date 2019/5/9 19:25
 */
public interface PeopleMapper {

    int addPeople(People people);

    List<People> findByNameAndAddress1(Map<String, Object> param);


    List<People> findByNameAndAddress2(PeopleQueryBean peopleQueryBean);

    List<People> findByNameAndAddress3(@Param("name") String name, @Param("address") String address);

    int countAll();

    List<People> findAll();

    int addPeopleWithOutPrimaryKey(People people);

    int addPeopleWithOutPrimaryKey1(People people);

    int addPeopleIf(People people);

    List<People> findByNameAndAddressIf(@Param("name") String name, @Param("address") String address);

    List<People> findByNameAndAddressChooseWhenOtherwise(@Param("name") String name, @Param("address") String address);

    int updateWithSet(People people);

    List<People> findByNameAndAddressTrim(@Param("name") String name, @Param("address") String address);

    List<People> findByIdForEach(@Param("ids") List<Integer> ids);

    List<People> findByBind();

    List<People> findBySql();
}