package indi.mozping.dao;

import indi.mozping.entity.Item;

import java.util.List;

/**
 * @author by mozping
 * @Classname ItemMapper
 * @Description TODO
 * @Date 2019/5/8 15:34
 */
public interface ItemMapper {

    int addItem(Item item);

    int deleteItemById(int id);

    int updateItemById(Item item);

    List<Object> findItemById(int id);

    List<Item> findAll();

    List<Integer> deleteManyItem(String description);

}