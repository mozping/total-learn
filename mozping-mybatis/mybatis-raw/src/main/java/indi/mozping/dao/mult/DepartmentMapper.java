package indi.mozping.dao.mult;

import indi.mozping.entity.mult.Department;

import java.util.List;

/**
 * @author by mozping
 * @Classname DepartmentMapper
 * @Description TODO
 * @Date 2019/5/13 11:08
 */
public interface DepartmentMapper {

    List<Department> queryAllDept();

    List<Department> queryAllDeptLazy();
}