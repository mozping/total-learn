package indi.mozping.entity.mult;

import lombok.Data;

/**
 * @author by mozping
 * @Classname Employee
 * @Description TODO
 * @Date 2019/5/10 17:54
 */
@Data
public class Employee {

    private int id;

    private String name;

    private int age;

    // 员工和部门的关联关系是1对1，
    // 体现在面向对象中就是一个对象中包含有另一个对象
    private Department dept;
}