package indi.mozping.entity;

import java.util.Date;

public class InitSchemaInfo {
    private Integer id;

    private String schemaName;

    private String prefixTableName;

    private String partitionType;

    private String partitionNum;

    private String searchPattern;

    private String hotTimeRange;

    private String clusterNodesType;

    private String clusterNodes;

    private Date createTime;

    private Byte isDelete;

    private String createTableInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName == null ? null : schemaName.trim();
    }

    public String getPrefixTableName() {
        return prefixTableName;
    }

    public void setPrefixTableName(String prefixTableName) {
        this.prefixTableName = prefixTableName == null ? null : prefixTableName.trim();
    }

    public String getPartitionType() {
        return partitionType;
    }

    public void setPartitionType(String partitionType) {
        this.partitionType = partitionType == null ? null : partitionType.trim();
    }

    public String getPartitionNum() {
        return partitionNum;
    }

    public void setPartitionNum(String partitionNum) {
        this.partitionNum = partitionNum == null ? null : partitionNum.trim();
    }

    public String getSearchPattern() {
        return searchPattern;
    }

    public void setSearchPattern(String searchPattern) {
        this.searchPattern = searchPattern == null ? null : searchPattern.trim();
    }

    public String getHotTimeRange() {
        return hotTimeRange;
    }

    public void setHotTimeRange(String hotTimeRange) {
        this.hotTimeRange = hotTimeRange == null ? null : hotTimeRange.trim();
    }

    public String getClusterNodesType() {
        return clusterNodesType;
    }

    public void setClusterNodesType(String clusterNodesType) {
        this.clusterNodesType = clusterNodesType == null ? null : clusterNodesType.trim();
    }

    public String getClusterNodes() {
        return clusterNodes;
    }

    public void setClusterNodes(String clusterNodes) {
        this.clusterNodes = clusterNodes == null ? null : clusterNodes.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreateTableInfo() {
        return createTableInfo;
    }

    public void setCreateTableInfo(String createTableInfo) {
        this.createTableInfo = createTableInfo == null ? null : createTableInfo.trim();
    }
}