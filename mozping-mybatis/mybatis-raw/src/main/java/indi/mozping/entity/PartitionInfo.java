package indi.mozping.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author by mozping
 * @Classname PartitionInfo
 * @Description 数据库实体对象
 * @Date 2019/10/9 15:47
 */
@Data
public class PartitionInfo implements Serializable {
    @JsonIgnore
    private Integer id;
    @JsonIgnore
    private Integer nodeIpId;
    @JsonIgnore
    private Integer initSchemaInfoId;

    @JsonProperty(value = "prefix_table_index")
    private Integer prefixTableIndex;
    @JsonIgnore
    private Integer partitionNum;
    @JsonIgnore
    private Date startTime;
    @JsonProperty(value = "end_time")
    private Date endTime;
    @JsonIgnore
    private Date createTime;
    @JsonIgnore
    private Byte isDelete;

}