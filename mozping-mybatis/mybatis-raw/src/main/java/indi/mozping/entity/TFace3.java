package indi.mozping.entity;

import java.util.Arrays;
import java.util.Date;

public class TFace3 {
    private Long sequence;

    private Long id;
    private int type = 1;

    private Integer accessories;

    private Integer race;

    private Integer age;

    private Long fromImageId;

    private Long fromPersonId;

    private Long fromVideoId;

    private Integer gender;

    private String imageData;

    private Integer indexed;

    private Long sourceId;

    private Integer sourceType;

    private Date time;

    private Integer version;

    private String json;

    private Integer quality;

    private byte[] faceFeature;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAccessories() {
        return accessories;
    }

    public void setAccessories(Integer accessories) {
        this.accessories = accessories;
    }

    public Integer getRace() {
        return race;
    }

    public void setRace(Integer race) {
        this.race = race;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Long getFromImageId() {
        return fromImageId;
    }

    public void setFromImageId(Long fromImageId) {
        this.fromImageId = fromImageId;
    }

    public Long getFromPersonId() {
        return fromPersonId;
    }

    public void setFromPersonId(Long fromPersonId) {
        this.fromPersonId = fromPersonId;
    }

    public Long getFromVideoId() {
        return fromVideoId;
    }

    public void setFromVideoId(Long fromVideoId) {
        this.fromVideoId = fromVideoId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData == null ? null : imageData.trim();
    }

    public Integer getIndexed() {
        return indexed;
    }

    public void setIndexed(Integer indexed) {
        this.indexed = indexed;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json == null ? null : json.trim();
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    public byte[] getFaceFeature() {
        return faceFeature;
    }

    public void setFaceFeature(byte[] faceFeature) {
        this.faceFeature = faceFeature;
    }

    @Override
    public String toString() {
        return "TFace3{" +
                "sequence=" + sequence +
                ", id=" + id +
                ", type=" + type +
                ", accessories=" + accessories +
                ", race=" + race +
                ", age=" + age +
                ", fromImageId=" + fromImageId +
                ", fromPersonId=" + fromPersonId +
                ", fromVideoId=" + fromVideoId +
                ", gender=" + gender +
                ", imageData='" + imageData + '\'' +
                ", indexed=" + indexed +
                ", sourceId=" + sourceId +
                ", sourceType=" + sourceType +
                ", time=" + time +
                ", version=" + version +
                ", json='" + json + '\'' +
                ", quality=" + quality +
                ", faceFeature=" + Arrays.toString(faceFeature) +
                '}';
    }
}