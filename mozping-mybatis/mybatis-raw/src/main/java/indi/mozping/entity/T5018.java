package indi.mozping.entity;

import java.util.Arrays;
import java.util.Date;

public class T5018 {
    private Long id;

    private Date created;

    private Date updated;

    private Long fromCidId;

    private Long fromImageId;

    private String imageData;

    private Integer indexed;

    private Integer version;

    private String zplxmc;

    private String json;

    private byte[] faceFeature;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Long getFromCidId() {
        return fromCidId;
    }

    public void setFromCidId(Long fromCidId) {
        this.fromCidId = fromCidId;
    }

    public Long getFromImageId() {
        return fromImageId;
    }

    public void setFromImageId(Long fromImageId) {
        this.fromImageId = fromImageId;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData == null ? null : imageData.trim();
    }

    public Integer getIndexed() {
        return indexed;
    }

    public void setIndexed(Integer indexed) {
        this.indexed = indexed;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getZplxmc() {
        return zplxmc;
    }

    public void setZplxmc(String zplxmc) {
        this.zplxmc = zplxmc == null ? null : zplxmc.trim();
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json == null ? null : json.trim();
    }

    public byte[] getFaceFeature() {
        return faceFeature;
    }

    public void setFaceFeature(byte[] faceFeature) {
        this.faceFeature = faceFeature;
    }

    @Override
    public String toString() {
        return "T5018{" +
                "id=" + id +
                ", created=" + created +
                ", updated=" + updated +
                ", fromCidId=" + fromCidId +
                ", fromImageId=" + fromImageId +
                ", imageData='" + imageData + '\'' +
                ", indexed=" + indexed +
                ", version=" + version +
                ", zplxmc='" + zplxmc + '\'' +
                ", json='" + json + '\'' +
                ", faceFeature=" + Arrays.toString(faceFeature) +
                '}';
    }
}