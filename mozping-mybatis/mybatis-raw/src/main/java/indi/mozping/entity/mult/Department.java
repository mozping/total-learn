package indi.mozping.entity.mult;

import lombok.Data;

import java.util.List;

/**
 * @author by mozping
 * @Classname Department
 * @Description TODO
 * @Date 2019/5/10 17:56
 */
@Data
public class Department {

    // 部门编号
    private int deptid;

    // 部门名称
    private String dname;

    // 部门描述
    private String description;

    //部门包含的员工
    private List<Employee> employees;
}