package indi.mozping.entity;


import lombok.Data;

@Data
public class TUser {
    private Integer id;

    private String userName;

    private String realName;

    private Byte sex;

    private String mobile;

    private String email;

    private String note;

    private Integer positionId;

    @Override
    public String toString() {
        return "TUser [id=" + id + ", userName=" + userName + ", realName="
                + realName + ", sex=" + sex + ", mobile=" + mobile + ", email="
                + email + ", note=" + note + ", positionId=" + positionId + "]";
    }


}