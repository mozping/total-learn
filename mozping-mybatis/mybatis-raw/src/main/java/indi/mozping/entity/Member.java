package indi.mozping.entity;

import lombok.Data;

import java.util.List;

/**
 * @author by mozping
 * @Classname Member
 * @Description TODO
 * @Date 2019/5/9 15:53
 */
@Data
public class Member {

    private Integer id;

    private String name;

    private List<String> skills;
}