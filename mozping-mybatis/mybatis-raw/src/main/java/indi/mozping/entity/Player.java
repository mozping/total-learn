package indi.mozping.entity;

import lombok.Data;


/**
 * @author by mozping
 * @Classname Player
 * @Description TODO
 * @Date 2019/3/11 14:19
 */
@Data
public class Player {

    private Integer id;

    private String playName;

    private Integer playNo;

    private String team;

    private float height;
}
