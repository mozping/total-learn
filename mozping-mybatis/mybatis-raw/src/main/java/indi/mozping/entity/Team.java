package indi.mozping.entity;

import lombok.Data;

/**
 * @author by mozping
 * @Classname Team
 * @Description TODO
 * @Date 2019/3/13 14:09
 */
@Data
public class Team {

    private Integer id;

    private String teamName;

    private String description;

    private String city;

}
