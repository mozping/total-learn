package indi.mozping.entity;

import lombok.Data;

/**
 * @author by mozping
 * @Classname Item
 * @Description TODO
 * @Date 2019/5/8 15:32
 */
@Data
public class Item {

    private int id;
    private String description;

}