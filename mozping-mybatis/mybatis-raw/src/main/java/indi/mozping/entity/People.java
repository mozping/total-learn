package indi.mozping.entity;

import lombok.Data;

/**
 * @author by mozping
 * @Classname People
 * @Description TODO
 * @Date 2019/5/9 19:20
 */
@Data
public class People {

    private int id;
    private String name;
    private int age;
    private String address;
    private String edu;//学士（Bachelor） 硕士(master)  博士(Doctor)

}