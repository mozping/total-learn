package indi.mozping.querybean;

import lombok.Data;

/**
 * @author by mozping
 * @Classname PeopleQueryBean
 * @Description TODO
 * @Date 2019/5/9 19:42
 */
@Data
public class PeopleQueryBean {

    String name;
    String address;
}