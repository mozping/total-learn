package indi.mozping.querybean;

import lombok.Data;

/**
 * @author by mozping
 * @Classname PlayerQueryBean
 * @Description TODO
 * @Date 2019/3/13 15:03
 */
@Data
public class PlayerQueryBean {

    String team;
    Float height;
}
