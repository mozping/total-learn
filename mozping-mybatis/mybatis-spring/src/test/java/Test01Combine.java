import indi.mozping.config.MainConfig;
import indi.mozping.dao.UserDao;
import indi.mozping.entity.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

/**
 * @author by mozping
 * @Classname Test01Combine
 * @Description Spring��MyBatis���ɺ����
 * @Date 2019/3/7 15:56
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("classpath:spring.xml")
public class Test01Combine {

    @Test
    public void test() throws Exception {

//      ApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");
        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig.class);
        System.out.println("------cut-off---------");


        UserDao mapper = app.getBean(UserDao.class);
        List<User> all = mapper.findAll();
        for (User u : all) {
            System.out.println(u);
        }
    }
}
