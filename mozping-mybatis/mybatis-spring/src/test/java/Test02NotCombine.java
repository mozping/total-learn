import indi.mozping.dao.UserDao;
import indi.mozping.entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author by mozping
 * @Classname Test02NotCombine
 * @Description MyBatis��������
 * @Date 2019/11/08 19:56
 */
public class Test02NotCombine {

    private static final String CONFIG_FILE_PATH = "mybatis-config.xml";

    @Test
    public void query() throws IOException {
        InputStream in = Resources.getResourceAsStream(CONFIG_FILE_PATH);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        SqlSession sqlSession = factory.openSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        List<User> all = mapper.findAll();
        for (User u : all) {
            System.out.println(u);
        }
    }
}
