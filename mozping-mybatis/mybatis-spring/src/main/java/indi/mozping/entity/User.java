package indi.mozping.entity;

import lombok.Data;

@Data
public class User {
    Integer id;
    private String name;
    private String job;
}
