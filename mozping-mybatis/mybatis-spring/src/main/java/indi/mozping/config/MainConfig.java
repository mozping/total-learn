package indi.mozping.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

/**
 * @author by mozping
 * @Classname MainConfig
 * @Description Spring����MyBatis���������
 * @Date 2019/11/08 18:36
 */
@Configuration
@ComponentScan("com.intellif.mozping")
@MapperScan(value = "com.intellif.mozping.dao")
public class MainConfig {

    private static final String URL = "jdbc:mysql://192.168.12.168:3306/test";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "introcks1234";
    private static final String DRIVER = "com.mysql.jdbc.Driver";


    /**
     * Druid数据源
     */
    @Bean("dataSource")
    DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(URL);
        dataSource.setDriverClassName(DRIVER);
        dataSource.setUsername(USERNAME);
        dataSource.setPassword(PASSWORD);
        return dataSource;
    }

    /**
     * MyBatis自身数据源
     */
    @Bean("unPooledDataSource")
    DataSource unPooledDataSource() {
        UnpooledDataSource dataSource = new UnpooledDataSource();
        dataSource.setUrl(URL);
        dataSource.setDriver(DRIVER);
        dataSource.setUsername(USERNAME);
        dataSource.setPassword(PASSWORD);
        return dataSource;
    }

    /**
     * 配置sqlSessionFactoryBean，用于创建 sqlSessionFactory
     * 这里通过Qualifier来注入一个数据源，如果注入的是dataSource，就注入DruidDataSource
     * 如果注入 unPooledDataSource，就不使用数据源，使用的是MyBatis内置的数据源
     */
    @Bean("sqlSessionFactoryBean")
    public SqlSessionFactoryBean sqlSessionFactoryBean(@Qualifier("unPooledDataSource") DataSource dataSource) {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        //1.数据源配置
        sqlSessionFactoryBean.setDataSource(dataSource);
        //2.别名扫描
        sqlSessionFactoryBean.setTypeAliasesPackage("com.intellif.mozping.entity");
        //3.Xml映射文件扫描，这里传的是数组，每一个元素只能代表一个xml，不能*.xml代表全部
        sqlSessionFactoryBean.setMapperLocations(new ClassPathResource("mapper/User.xml"));
        //注意前面的别名和Xml扫描都可以在配置文件配，这然后这里加入配置就好了，
        //如果前面配置了，其实配置文件就可以不要了
        //sqlSessionFactoryBean.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
        return sqlSessionFactoryBean;
    }

    @Bean("mapperScannerConfigurer")
    MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("com.intellif.mozping.dao");
        return mapperScannerConfigurer;
    }


}
