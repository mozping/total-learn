package indi.mozping.service;

import indi.mozping.entity.User;

import java.util.List;

/**
 * @author by mozping
 * @Classname UserService
 * @Description TODO
 * @Date 2019/1/21 11:19
 */
public interface UserService {

    List<User> findAll();
}
