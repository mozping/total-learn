package indi.mozping.service;

import indi.mozping.dao.UserDao;
import indi.mozping.entity.User;
import indi.mozping.utils.annotations.EnableCountTimeIntellif;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author by mozping
 * @Classname UserServiceImpl
 * @Description TODO
 * @Date 2019/1/21 11:19
 */
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    @EnableCountTimeIntellif
    public List<User> findAll() {
        return userDao.findAll();
    }

}
