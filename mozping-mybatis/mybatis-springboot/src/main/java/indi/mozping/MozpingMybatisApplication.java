package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author intellif
 */
@SpringBootApplication
public class MozpingMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingMybatisApplication.class, args);
    }

}

