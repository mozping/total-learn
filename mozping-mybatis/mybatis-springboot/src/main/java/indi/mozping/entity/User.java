/*
 *文件名： User.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： 测试实体类
 *创建人：mozping
 *创建时间： 2018/6/23 16:30
 *修改理由：
 *修改内容：
 */
package indi.mozping.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * @author mozping
 */
@Data
public class User implements Serializable {

    private Integer id;
    private String name;
    private String job;

}