package indi.mozping.utils.annotations;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname EnableCountTimeAspect
 * @Description 消耗时间的注解对应的解析器
 * @Date 2018/12/27 14:13
 */
@Component
@Aspect
public class EnableCountTimeAspect {

    private static final Logger LOG = LoggerFactory.getLogger(EnableCountTimeAspect.class.getName());

    @Around("@annotation(ect)")
    public Object doAround(ProceedingJoinPoint pjp, EnableCountTimeIntellif ect) {
        long begin = System.currentTimeMillis();
        Object obj = null;
        try {
            obj = pjp.proceed();
        } catch (Throwable throwable) {
            LOG.warn("AOP异常", throwable);
        }
        String methodName = pjp.getSignature().getName();
        String className = pjp.getSignature().getDeclaringTypeName();
        LOG.info(className + "." + methodName + " method elapsed time is: " + (System.currentTimeMillis() - begin) + " ms");
        return obj;
    }
}
