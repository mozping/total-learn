/*
 *文件名： UserDao.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： 测试DAO
 *创建人：mozping
 *创建时间： 2018/6/23 16:30
 *修改理由：
 *修改内容：
 */
package indi.mozping.dao;

import indi.mozping.entity.User;
import indi.mozping.utils.annotations.EnableCountTimeIntellif;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author mozping
 * @version 1.0
 * @date 2018/6/23 16:30
 * @see UserDao
 * @since JDK1.8
 */
@Mapper
public interface UserDao {

    List<User> findAll();

    @EnableCountTimeIntellif
    User findById(int id);

}