package indi.mozping.controller;

import indi.mozping.entity.User;
import indi.mozping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

/**
 * @author by mozping
 * @Classname UserControllerImpl
 * @Description TODO
 * @Date 2019/1/21 11:15
 */
@RestController
@ResponseBody
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/all")
    public List<User> findAll() {
        System.out.println("findAll ... ");
        return userService.findAll();
    }

    @GetMapping("/random")
    public User random() {
        System.out.println("random ... ");
        List<User> all = userService.findAll();
        return all.get(new Random().nextInt(all.size()));
    }
}
