package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MopzingMongodbApplication {

    public static String str = " {\n" +
            "            \"algVersion\":1099524800514,\n" +
            "            \"areaId\":\"\",\n" +
            "            \"backgroundImage\":\"/group1/M00/02/4A/wKgLk1yUlUOACAYCAA7UyM10bTQ600.jpg\",\n" +
            "            \"bgImgHigh\":2544,\n" +
            "            \"bgImgWidth\":3296,\n" +
            "            \"brand\":\"宝骏-310-20162017(中高配版)\",\n" +
            "            \"brandCode\":\"23B2E8A7617A4318894C83B0A0ED7858\",\n" +
            "            \"brandConfidence\":0.99,\n" +
            "            \"call\":\"no\",\n" +
            "            \"callConfidence\":0.99,\n" +
            "            \"coDriverBelt\":\"unknown\",\n" +
            "            \"coDriverBeltConfidence\":0,\n" +
            "            \"color\":\"white\",\n" +
            "            \"colorConfidence\":0.99,\n" +
            "            \"crash\":\"no\",\n" +
            "            \"crashConfidence\":0.99,\n" +
            "            \"danger\":\"no\",\n" +
            "            \"dangerConfidence\":0.99,\n" +
            "            \"debug\":\"Debug info\",\n" +
            "            \"fingerprint\":\"Engine_v3.0\",\n" +
            "            \"fromImageId\":\"946218067427828\",\n" +
            "            \"guid\":\"946218067427838\",\n" +
            "            \"imgQuality\":\"ok\",\n" +
            "            \"mainDriverBelt\":\"yes\",\n" +
            "            \"mainDriverBeltConfidence\":0.97,\n" +
            "            \"makerPaperBox\":\"no\",\n" +
            "            \"makerPaperBoxConfidence\":0,\n" +
            "            \"makerPendant\":\"yes\",\n" +
            "            \"makerPendantConfidence\":1,\n" +
            "            \"makerSunShield\":\"no\",\n" +
            "            \"makerSunShieldConfidence\":0,\n" +
            "            \"makerTag\":\"yes\",\n" +
            "            \"makerTagConfidence\":1,\n" +
            "            \"plateColor\":\"blue\",\n" +
            "            \"plateColorConfidence\":0.99,\n" +
            "            \"plateDestain\":\"no\",\n" +
            "            \"plateDestainConfidence\":0.99,\n" +
            "            \"plateNumberConfidence\":0.99,\n" +
            "            \"plateShelter\":\"no\",\n" +
            "            \"plateShelterConfidence\":0.99,\n" +
            "            \"recvLocalTime\":\"2019-03-22 15:56:50\",\n" +
            "            \"recvUtcTime\":\"2019-03-22 07:56:50\",\n" +
            "            \"sourceId\":\"568a9239e9704fa0b983d41da54d8ac2\",\n" +
            "            \"sourceType\":\"zipFile\",\n" +
            "            \"targetImage\":\"/group1/M00/02/4A/wKgLk1yUlUOAeT-ZAAKtrrEA6Pw246.jpg\",\n" +
            "            \"targetType\":\"car\",\n" +
            "            \"taskType\":\"analyzeTask\",\n" +
            "            \"tid\":\"946218067427834\",\n" +
            "            \"targetType\":\"sedan\",\n" +
            "            \"typeConfidence\":0.98,\n" +
            "            \"uuid\":\"6\"\n" +
            "        }";


    public static void main(String[] args) {
        SpringApplication.run(MopzingMongodbApplication.class, args);
    }

}
