package indi.mozping.service;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.bean.Entity;
import indi.mozping.dao.MongoDbDaoImpl;
import indi.mozping.utils.Util;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by mozping
 * @Classname AddService
 * @Description TODO
 * @Date 2019/2/22 14:14
 * <p>
 * private int rid;
 * private String createName;
 * private Data createTime;
 * <p>
 * private String peopleNum;
 * private String deviceNum;
 * <p>
 * private String peopleName;
 * private String deviceName;
 */
@Service
public class AddService {


    @Autowired
    MongoDbDaoImpl mongoDbDaoImpl;


    public long count(String tableName) {
        return mongoDbDaoImpl.count(tableName);
    }

    public JSONObject add(List<Entity> list) throws Exception {
        List<Document> listDoc = new ArrayList<>();
        for (Entity entity : list) {
            Document doc = new Document();

            doc.append("rid", entity.getRid());
            doc.append("createName", entity.getCreateName());
            doc.append("createTime", entity.getCreateTime());

            doc.append("peopleNum", entity.getPeopleNum());
            doc.append("deviceNum", entity.getDeviceNum());

            doc.append("peopleName", entity.getPeopleName());
            doc.append("deviceName", entity.getDeviceName());

            listDoc.add(doc);
        }
        mongoDbDaoImpl.insert(listDoc);
        return null;
    }

    public JSONObject addObjList(List<Object> list) throws Exception {
        List<Document> listDoc = new ArrayList<>();
        for (Object t : list) {
            Document parse = Document.parse(t.toString());
            parse.put("time", Util.getTimeBetweenYear());
            listDoc.add(parse);
        }

        mongoDbDaoImpl.insert(listDoc);
        return null;
    }

    public void add(ArrayList<Document> arrayList, String tableName) throws Exception {
        mongoDbDaoImpl.insert(arrayList, tableName);
    }

    public void add(Document doc, String tableName) throws Exception {
        mongoDbDaoImpl.insert(doc, tableName);
    }


}
