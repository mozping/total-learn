/*
 *文件名： GlobalConsts.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： 自定义常量
 *创建人：mozping
 *创建时间： 2018/6/25 20:05
 *修改理由：
 *修改内容：
 */
package indi.mozping.utils;


/**
 * 自定义常量
 *
 * @author mozping
 * @version 1.0
 * @date 2018/6/25 20:05
 * @see GlobalConsts
 * @since JDK1.8
 */

public class GlobalConsts {


    /**
     * 请求参数字段
     */
    public static final String OPERATOR = "operator";
    public static final String TYPE = "targetType";
    public static final String TIME = "time";
    public static final String DATAS = "datas";
    public static final String TID = "tid";


    /**
     * MongoDB查询语句连接符
     */
    public static final String AND = "and";
    public static final String CONDITION_AND = "$and";
    public static final String CONDITION_OR = "$or";

    /**
     * 默认时间格式
     */
    public static final String DEFAULT_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * 分页参数
     */
    public static final String PAGE_NO = "pageNo";
    public static final String PAGE_SIZE = "pageSize";
    public static final String PAGE_NEED_TOTAL = "needTotal";
    public static final int DEFAULT_PAGE_NO = 1;
    public static final int DEFAULT_PAGE_SIZE = 50;

    /**
     * Map集合的大小
     */
    public static final int SIZE_OF_FOUR = 4;


    private GlobalConsts() {
    }

    public static final int CONFIG_TOTAL_NUM_OF_PEOPLE = 10 * 10000;
    public static final int CONFIG_TOTAL_NUM_OF_CREATE = 1 * 10000;
    public static final int CONFIG_TOTAL_NUM_OF_DEVICE = 5000;

    public static final byte[] FEATURE = new byte[32];

    static {
        for (int i = 0; i < 32; i++) {
            FEATURE[i] = (byte) Util.getRandomNum(-127, 128);
        }
    }


    public static final String EXAMPLE_ADD_DATA = "{\n" +
            "\t\"table\":\"test\",\n" +
            "    \"data\":[\n" +
            "        {\n" +
            "            \"rid\":1,\n" +
            "            \"createName\":\"zhangsan\",\n" +
            "            \"createTime\":\"2018-12-05 16:10:00\",\n" +
            "            \"peopleNum\":1,\n" +
            "            \"deviceNum\":1,\n" +
            "            \"peopleName\":\"张三\",\n" +
            "            \"deviceName\":\"device4600\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"rid\":1,\n" +
            "            \"createName\":\"zhangsan\",\n" +
            "            \"createTime\":\"2018-12-05 16:10:00\",\n" +
            "            \"peopleNum\":2,\n" +
            "            \"deviceNum\":2,\n" +
            "            \"peopleName\":\"李四\",\n" +
            "            \"deviceName\":\"device4600\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

}