/*
 * 文件名： DateUtil.java
 * 版权： Copyright by 云天励飞 intellif.com
 * 描述： 日期工具类
 * 创建人：wubo
 * 创建时间： 2018/6/12 20:01
 * 修改理由：
 * 修改内容：
 */
package indi.mozping.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * 日期处理工具类
 *
 * @author mozping
 * @version 1.0
 * @date 2018年4月8日
 * @see SimpleDateFormat,java.util
 * @since JDK1.8
 */
public class DateUtil {

    /**
     * 时间格式化
     *
     * @param dateStr：要格式化的时间
     */
    public static Date convertStringToDate(String dateStr) throws Exception {
        if (dateStr == null) {
            throw new Exception();
        }

        Date date;


        SimpleDateFormat formatSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            date = formatSecond.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatMin = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            date = formatMin.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatHour = new SimpleDateFormat("yyyy-MM-dd HH");
        try {
            date = formatHour.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatDay = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = formatDay.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatMonth = new SimpleDateFormat("yyyy-MM");
        try {
            date = formatMonth.parse(dateStr);
            return date;
        } catch (ParseException pe) {

        }

        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
        try {
            date = formatMonth.parse(dateStr);
            return date;
        } catch (ParseException pe) {
            return new Date();
        }

    }

    /**
     * @param unixTime unix时间的字符串表示
     * @param dateStr  返回的指定格式时间字符串
     * @Description: 根据unix时间毫秒值返回指定格式的时间字符串，
     * * 比如输入是1546490580000，指定格式是：yyyy-MM-dd HH:mm:ss
     * * 则返回2019-01-03 12:43:00
     * @date 2019/1/5 11:33
     */
    public static String getDateStrByUnixTime(String unixTime, String dateStr) {
        long ut = Long.parseLong(unixTime);
        return getDateStrByUnixTime(ut, dateStr);
    }

    /**
     * @param unixTime unix时间的long表示
     * @param dateStr  返回的指定格式时间字符串
     * @Description:
     * @date 2019/1/5 11:34
     */
    public static String getDateStrByUnixTime(long unixTime, String dateStr) {
        Date dateTmp = new Date(unixTime);
        String strDate = DateUtil.getFormarMonth(dateTmp, dateStr);
        return strDate;
    }


    public static boolean isPattern(String dateStr) {
        SimpleDateFormat formatWithMill = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        boolean dateFlag = true;
        try {
            formatWithMill.setLenient(false);
            formatWithMill.parse(dateStr);
        } catch (ParseException e) {
            try {
                format.setLenient(false);
                format.parse(dateStr);
            } catch (ParseException ex) {
                dateFlag = false;
            }
        }
        return dateFlag;
    }


    /**
     * 构造器私有
     */
    private DateUtil() {
    }


    /**
     * 根据给定格式，获取指定日期的字符串表示
     *
     * @param date   日期对象
     * @param format 指定格式
     * @return java.lang.String
     * @see SimpleDateFormat;
     */
    public static String getFormarMonth(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }


    /**
     * 根据指定的格式，获取字符串对应的日期对象
     *
     * @param aMask   指定的日期格式
     * @param strDate 代表日期和时间的字符串
     * @return java.util.Date   Date对象
     * @throws ParseException 字符串不满足格式时抛出异常
     * @see SimpleDateFormat
     */
    public static Date convertStringToDate(String aMask, String strDate)
            throws ParseException {
        SimpleDateFormat df;
        Date date;
        df = new SimpleDateFormat(aMask);

        try {
            date = df.parse(strDate);
        } catch (ParseException pe) {
            throw new ParseException(pe.getMessage(), pe.getErrorOffset());
        }
        return (date);
    }


    /**
     * 2018-11-04T23:52
     */
    public static String utcStrToLocal(String timeStr) throws ParseException {
        StringBuilder sb = new StringBuilder();
        String[] strArr = timeStr.split("T");
        for (String s : strArr) {
            sb.append(s).append(" ");
        }
        Date d;
        String dStr;
        if (timeStr.length() == 13) {
            //时
            d = DateUtil.convertStringToDate("yyyy-MM-dd HH", sb.toString().trim());
            d.setTime(d.getTime() + 8 * 60 * 60 * 1000);
            dStr = DateUtil.getFormarMonth(d, "yyyy-MM-dd HH");
        } else if (timeStr.length() == 10) {
            //天
            d = DateUtil.convertStringToDate("yyyy-MM-dd", sb.toString().trim());
            d.setTime(d.getTime() + 8 * 60 * 60 * 1000);
            dStr = DateUtil.getFormarMonth(d, "yyyy-MM-dd");
        } else if (timeStr.length() == 7) {
            //月
            d = DateUtil.convertStringToDate("yyyy-MM", sb.toString().trim());
            d.setTime(d.getTime() + 8 * 60 * 60 * 1000);
            dStr = DateUtil.getFormarMonth(d, "yyyy-MM");
        } else if (timeStr.length() == 4) {
            //年
            d = DateUtil.convertStringToDate("yyyy", sb.toString().trim());
            d.setTime(d.getTime() + 8 * 60 * 60 * 1000);
            dStr = DateUtil.getFormarMonth(d, "yyyy");
        } else {
            //分
            d = DateUtil.convertStringToDate("yyyy-MM-dd HH:mm", sb.toString().trim());
            d.setTime(d.getTime() + 8 * 60 * 60 * 1000);
            dStr = DateUtil.getFormarMonth(d, "yyyy-MM-dd HH:mm");
        }

        return dStr;
    }

    public static Date getDateIncSomeDay(Date date1, int num) {
        Calendar c = Calendar.getInstance();
        c.setTime(date1);
        c.add(Calendar.DATE, num);
        return c.getTime();
    }

}
