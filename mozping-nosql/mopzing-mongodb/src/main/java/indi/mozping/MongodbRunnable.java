package indi.mozping;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.controller.BigDataTestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname MongodbRunnable
 * @Description TODO
 * @Date 2019/5/24 12:25
 */
@Component
public class MongodbRunnable implements CommandLineRunner {

    @Autowired
    BigDataTestController bigDataTestController;

    @Override
    public void run(String... args) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("threadNum", 10);
        jsonObject.put("recordNum", 3000);
        jsonObject.put("batchNum", 1000);
        jsonObject.put("tableName", "face");
        bigDataTestController.testAdd(jsonObject);
    }
}