package indi.mozping.task;

import indi.mozping.controller.InsertTestController;
import indi.mozping.random.RandomObjFactory;
import indi.mozping.service.AddService;
import indi.mozping.utils.Util;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * @author by mozping
 * @Classname MyInsertMongo
 * @Description TODO
 * @Date 2019/5/16 10:47
 */
public class MyInsertMongo implements Runnable {


    private static final Logger LOG = LoggerFactory.getLogger(MyInsertMongo.class);

    private AddService addService;
    int num;
    ArrayList<Document> arrayList;

    public MyInsertMongo(int num, AddService addService) {
        this.num = num;
        this.addService = addService;
        arrayList = new ArrayList<>(5000);
    }


    @Override
    public void run() {
        int roop = num / 5000;
        Document randomObj = RandomObjFactory.getRandomObj();
        randomObj.put("time", Util.getTimeBetween3Month());
        randomObj.put("sourceId", "1234567890");
        for (int j = 1; j <= roop; j++) {
            for (int i = 1; i <= 5000; i++) {
                arrayList.add(randomObj);
            }
            try {
                addService.add(arrayList, "multobj");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                arrayList.clear();
            }
        }
        InsertTestController.latch.countDown();
        LOG.info("线程" + Thread.currentThread().getName() + "新增 " + num + " 条数据成功...");
    }
}
