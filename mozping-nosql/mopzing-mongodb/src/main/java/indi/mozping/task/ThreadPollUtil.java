package indi.mozping.task;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author by mozping
 * @Classname ThreadPollUtil
 * @Description TODO
 * @Date 2019/5/16 10:42
 */
public class ThreadPollUtil {

    public static final ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
            10,
            100,
            60,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(10)
            , Executors.defaultThreadFactory()
            , new ThreadPoolExecutor.AbortPolicy()
    );

    private ThreadPollUtil() {
    }
}
