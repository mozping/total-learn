/*
 *文件名： MongoDbProperties.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： 初始化MongoDB的配置信息
 *创建人：mozping
 *创建时间： 2018/7/2 14:21
 *修改理由：
 *修改内容：
 */

package indi.mozping.dbpool;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;


/**
 * 初始化MongoDB的配置信息
 *
 * @author mozping
 * @version 1.0
 * @date 2018/7/2 14:21
 * @see MongoDbProperties
 * @since JDK1.8
 */

@Component
@Validated
@Data
@ConfigurationProperties(prefix = "spring.data.mongodb.intellif")
public class MongoDbProperties {

    @NotBlank
    private String database;

    @NotEmpty
    private ArrayList<String> hosts;
    @NotEmpty
    private ArrayList<Integer> ports;

    //账号密码
    private String username = null;
    private String password = null;
    private String authenticationDatabase;
    private Integer minConnectionsPerHost = 5;
    private Integer connectionsPerHost = 20;

}
