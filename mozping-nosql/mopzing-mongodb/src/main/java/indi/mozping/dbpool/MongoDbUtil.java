/*
 *文件名： MongoDbUtil.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： MongoDB的工具类，封装易于连接的对外接口
 *创建人：mozping
 *创建时间： 2018/7/2 17:58
 *修改 理由：
 *修改内容：
 */

package indi.mozping.dbpool;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * MongoDB的工具类，封装mongoClient对象，提供更易用的方法来连接指定名称的集合
 *
 * @author mozping
 * @version 1.0
 * @date 2018/7/2 17:58
 * @see MongoDbUtil
 * @since JDK1.8
 */
@Component
public class MongoDbUtil {

    @Autowired
    MongoClient mongoClient;

    @Autowired
    MongoDbProperties mongoDbProperties;

    public MongoCollection getCollection(String collectionName) {
        String dbName = mongoDbProperties.getDatabase();
        MongoCollection collection;
        try {
            collection = mongoClient.getDatabase(dbName).getCollection(collectionName);
        } catch (Exception e) {
            throw e;
        }
        return collection;
    }
}
