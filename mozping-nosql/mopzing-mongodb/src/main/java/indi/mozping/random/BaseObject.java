package indi.mozping.random;

import lombok.Data;

import java.util.Date;

/**
 * @author by mozping
 * @Classname BaseObject
 * @Description TODO
 * @Date 2019/4/12 15:40
 */
@Data
public class BaseObject {
    public String operator;
    public String targetType;
    public String targetImage;
    public String backgroundImage;


    public String fromImageId;
    public String sourceId;
    public String areaId;

    public String sourceType;
    public String taskType;
    public String fingerprint;

    public String guid;
    public String uuid;
    public String tid;
    public int algVersion;
    public byte[] feature;

    public int bgImgHigh;
    public int bgImgWidth;

    public int[] targetRect;
    public float[] targetRectFloat;
    public int[] imgRect;
    public float[] imgRectFloat;

    public String debug;
    public String imgQuality;


    public Date recvLocalTime;
    public Date recvUtcTime;
    public Date time;
    public String padding = "abcdefghijklmnopqrstuvwxyz";

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getTargetImage() {
        return targetImage;
    }

    public void setTargetImage(String targetImage) {
        this.targetImage = targetImage;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getFromImageId() {
        return fromImageId;
    }

    public void setFromImageId(String fromImageId) {
        this.fromImageId = fromImageId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public int getAlgVersion() {
        return algVersion;
    }

    public void setAlgVersion(int algVersion) {
        this.algVersion = algVersion;
    }

    public byte[] getFeature() {
        return feature;
    }

    public void setFeature(byte[] feature) {
        this.feature = feature;
    }

    public int getBgImgHigh() {
        return bgImgHigh;
    }

    public void setBgImgHigh(int bgImgHigh) {
        this.bgImgHigh = bgImgHigh;
    }

    public int getBgImgWidth() {
        return bgImgWidth;
    }

    public void setBgImgWidth(int bgImgWidth) {
        this.bgImgWidth = bgImgWidth;
    }

    public int[] getTargetRect() {
        return targetRect;
    }

    public void setTargetRect(int[] targetRect) {
        this.targetRect = targetRect;
    }

    public float[] getTargetRectFloat() {
        return targetRectFloat;
    }

    public void setTargetRectFloat(float[] targetRectFloat) {
        this.targetRectFloat = targetRectFloat;
    }

    public int[] getImgRect() {
        return imgRect;
    }

    public void setImgRect(int[] imgRect) {
        this.imgRect = imgRect;
    }

    public float[] getImgRectFloat() {
        return imgRectFloat;
    }

    public void setImgRectFloat(float[] imgRectFloat) {
        this.imgRectFloat = imgRectFloat;
    }

    public String getDebug() {
        return debug;
    }

    public void setDebug(String debug) {
        this.debug = debug;
    }

    public String getImgQuality() {
        return imgQuality;
    }

    public void setImgQuality(String imgQuality) {
        this.imgQuality = imgQuality;
    }

    public Date getRecvLocalTime() {
        return recvLocalTime;
    }

    public void setRecvLocalTime(Date recvLocalTime) {
        this.recvLocalTime = recvLocalTime;
    }

    public Date getRecvUtcTime() {
        return recvUtcTime;
    }

    public void setRecvUtcTime(Date recvUtcTime) {
        this.recvUtcTime = recvUtcTime;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getPadding() {
        return padding;
    }

    public void setPadding(String padding) {
        this.padding = padding;
    }
}
