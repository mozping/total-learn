package indi.mozping.random;

import com.alibaba.fastjson.JSON;
import indi.mozping.utils.Util;
import org.bson.Document;
import java.util.Random;

import static indi.mozping.random.RandomUtil.*;

/**
 * @author by mozping
 * @Classname RandomObjFactory
 * @Description TODO
 * @Date 2019/4/12 16:24
 */
public class RandomObjFactory {

    private static Random random = new Random();

//    public static Document getRandomObj() {
//        int i = Math.abs(random.nextInt()) % 3;
//        if (i == 0) {
//            return getRandomBody();
//        } else if (i == 1) {
//            return getRandomCar();
//        }
//        return getRandomFace();
//    }

    public static Document getRandomObj() {
        return getRandomObj(Math.abs(random.nextInt()));
    }


    public static Document getRandomObj(int r) {
        int i = r % 3;
        if (i == 0) {
            return getRandomBody(r);
        } else if (i == 1) {
            return getRandomCar(r);
        }
        return getRandomFace(r);
    }

    public static Document getRandomCar() {
        return getRandomCar(Math.abs(random.nextInt()));
    }

    public static Document getRandomCar(int i) {
        CarObject car = new CarObject();
        setBaseFeild(car, i);
        float confidence = confidence();
        car.setTargetType("car");
        car.setType(type(i));
        car.setCarTypeConfidence(confidence);

        car.setColor(color(i));
        car.setColorConfidence(confidence);

        car.setBrand(brand(i));
        car.setBrandCode(brandCode(i));
        car.setBrandConfidence(confidence);

        car.setPlateColor(color(i));
        car.setPlateColorConfidence(confidence);

        car.setPlateNumber(Util.generateCarID());
        car.setPlateNumberConfidence(confidence);

        return Document.parse(JSON.toJSONString(car));
    }

    public static Document getRandomFace() {
        return getRandomFace(Math.abs(random.nextInt()));
    }

    public static Document getRandomFace(int i) {
        FaceObject face = new FaceObject();
        setBaseFeild(face, i);
        float confidence = confidence();
        face.setTargetType("face");

        face.setAge(age(i));
        face.setAgeConfidence(confidence);

        face.setGender(gender(i));
        face.setGenderConfidence(confidence);

        face.setHat(yesNoUnKnow(i));
        face.setHatConfidence(confidence);

        face.setGlasses(glasses(i));
        face.setGlassesConfidence(confidence);

        face.setRace(race());
        face.setRaceConfidence(confidence);

        face.setMask(yesNoUnKnow(i));
        face.setMaskConfidence(confidence);

        return Document.parse(JSON.toJSONString(face));

    }

    public static Document getRandomBody() {
        return getRandomBody(Math.abs(random.nextInt()));
    }

    public static Document getRandomBody(int i) {
        BodyObject body = new BodyObject();
        setBaseFeild(body, i);
        float confidence = confidence();
        body.setTargetType("body");
        body.setCoatColor(color(i));
        body.setCoatColorConfidence(confidence);

        body.setPantsColor(color(i));
        body.setPantsColorConfidence(confidence);

        body.setAngle(angle(i));
        body.setAngleConfidence(confidence);

        body.setAgeStage(ageStage(i));
        body.setAngleConfidence(confidence);

        body.setCoatStyle(coatStyle(i));
        body.setCoatStyleConfidence(confidence);

        body.setCoatPattern(coatPattern(i));
        body.setCoatPatternConfidence(confidence);

        body.setPantsStyle(pantsStyle(i));
        body.setPantsStyleConfidence(confidence);

        body.setPantsPattern(coatPattern(i));
        body.setPantsPatternConfidence(confidence);

        body.setHasCoat(yesNoUnKnow(i));
        body.setHasCoatConfidence(confidence);

        body.setGender(gender(i));
        body.setGenderConfidence(confidence);

        body.setHandbag(yesNoUnKnow(i));
        body.setHandbagConfidence(confidence());

        body.setSingleBag(yesNoUnKnow(i));
        body.setSingleBagConfidence(confidence);

        body.setBackBag(yesNoUnKnow(i));
        body.setBackBagConfidence(confidence);

        body.setDrawBox(yesNoUnKnow(i));
        body.setDrawBoxConfidence(confidence);

        body.setCart(yesNoUnKnow(i));
        body.setCartConfidence(confidence);

        return Document.parse(JSON.toJSONString(body));
    }


    public static BaseObject setBaseFeild(BaseObject baseObject, int i) {
        baseObject.setTargetImage(picUrl(i));
        baseObject.setBackgroundImage(picUrl(i));
        baseObject.setFromImageId(numId(15));
        baseObject.setSourceId(Integer.toString(random.nextInt(100)));
        baseObject.setAreaId(numId(3));
        baseObject.setSourceType(sourceType(i));
        baseObject.setTaskType(taskType(i));
        baseObject.setFingerprint(fingerprint(i));
        baseObject.setGuid(numId(15));

        baseObject.setTid(numId(15));
        baseObject.setAlgVersion(5030);
        //baseObject.setFeature(GlobalConsts.FEATURE);
        //baseObject.setFeature(GlobalConsts.FEATURE);
        baseObject.setBgImgHigh(bgImgHigh(i));
        baseObject.setBgImgWidth(bgImgWidth(i));

        baseObject.setTargetRect(getIntArr());
        baseObject.setTargetRectFloat(getFloatArr());
        baseObject.setImgRect(getIntArr());
        baseObject.setImgRectFloat(getFloatArr());

        baseObject.setDebug("Debug info");
        if (i % 100 == 0) {
            baseObject.setImgQuality("ok");
        }
        baseObject.setImgQuality("ok");
        if (i % 1000 == 0) {
            baseObject.setOperator("ifaas-collection");
        }
        baseObject.setOperator("ifaas-engine");
        return baseObject;
    }

}
