package indi.mozping.random;

import java.util.Arrays;

/**
 * @author by mozping
 * @Classname FaceObject
 * @Description TODO
 * @Date 2019/4/12 15:49
 */
//@Data
//@ToString(callSuper = true)
public class FaceObject extends BaseObject {


    private float age;
    private float ageConfidence;

    private String gender;
    private float genderConfidence;

    private String hat;
    private float hatConfidence;

    private String glasses;
    private float glassesConfidence;

    private String race;
    private float raceConfidence;

    private String mask;
    private float maskConfidence;

    public float getAge() {
        return age;
    }

    public void setAge(float age) {
        this.age = age;
    }

    public float getAgeConfidence() {
        return ageConfidence;
    }

    public void setAgeConfidence(float ageConfidence) {
        this.ageConfidence = ageConfidence;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getGenderConfidence() {
        return genderConfidence;
    }

    public void setGenderConfidence(float genderConfidence) {
        this.genderConfidence = genderConfidence;
    }

    public String getHat() {
        return hat;
    }

    public void setHat(String hat) {
        this.hat = hat;
    }

    public float getHatConfidence() {
        return hatConfidence;
    }

    public void setHatConfidence(float hatConfidence) {
        this.hatConfidence = hatConfidence;
    }

    public String getGlasses() {
        return glasses;
    }

    public void setGlasses(String glasses) {
        this.glasses = glasses;
    }

    public float getGlassesConfidence() {
        return glassesConfidence;
    }

    public void setGlassesConfidence(float glassesConfidence) {
        this.glassesConfidence = glassesConfidence;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public float getRaceConfidence() {
        return raceConfidence;
    }

    public void setRaceConfidence(float raceConfidence) {
        this.raceConfidence = raceConfidence;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public float getMaskConfidence() {
        return maskConfidence;
    }

    public void setMaskConfidence(float maskConfidence) {
        this.maskConfidence = maskConfidence;
    }

    @Override
    public String toString() {
        return "FaceObject{" +
                "maskConfidence=" + maskConfidence +
                ", targetType='" + targetType + '\'' +
                ", targetImage='" + targetImage + '\'' +
                ", backgroundImage='" + backgroundImage + '\'' +
                ", fromImageId='" + fromImageId + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", areaId='" + areaId + '\'' +
                ", sourceType='" + sourceType + '\'' +
                ", taskType='" + taskType + '\'' +
                ", fingerprint='" + fingerprint + '\'' +
                ", guid='" + guid + '\'' +
                ", uuid='" + uuid + '\'' +
                ", tid='" + tid + '\'' +
                ", algVersion=" + algVersion +
                ", feature=" + Arrays.toString(feature) +
                ", bgImgHigh=" + bgImgHigh +
                ", bgImgWidth=" + bgImgWidth +
                ", targetRect=" + Arrays.toString(targetRect) +
                ", targetRectFloat=" + Arrays.toString(targetRectFloat) +
                ", imgRect=" + Arrays.toString(imgRect) +
                ", imgRectFloat=" + Arrays.toString(imgRectFloat) +
                ", debug='" + debug + '\'' +
                ", imgQuality='" + imgQuality + '\'' +
                ", recvLocalTime=" + recvLocalTime +
                ", recvUtcTime=" + recvUtcTime +
                ", time=" + time +
                ", padding='" + padding + '\'' +
                '}';
    }
}
