package indi.mozping.random;

import lombok.Data;
import lombok.ToString;

/**
 * @author by mozping
 * @Classname BodyObject
 * @Description TODO
 * @Date 2019/4/12 15:53
 */
@Data
@ToString(callSuper = true)
public class BodyObject extends BaseObject {

    private String coatColor;
    private float coatColorConfidence;

    private String pantsColor;
    private float pantsColorConfidence;

    private String angle;
    private float angleConfidence;

    private String ageStage;
    private float ageStageConfidence;

    private String coatStyle;
    private float coatStyleConfidence;


    private String coatPattern;
    private float coatPatternConfidence;


    private String pantsStyle;
    private float pantsStyleConfidence;


    private String pantsPattern;
    private float pantsPatternConfidence;

    private String hasCoat;
    private float hasCoatConfidence;

    private String gender;
    private float genderConfidence;

    private String handbag;
    private float handbagConfidence;

    private String singleBag;
    private float singleBagConfidence;

    private String backBag;
    private float backBagConfidence;

    private String drawBox;
    private float drawBoxConfidence;

    private String cart;
    private float cartConfidence;

}
