package indi.mozping.random;

import lombok.Data;
import lombok.ToString;

/**
 * @author by mozping
 * @Classname CarObject
 * @Description TODO
 * @Date 2019/4/12 15:57
 */
@Data
@ToString(callSuper = true)
public class CarObject extends BaseObject {

    private String type;
    private float carTypeConfidence;

    private String color;
    private float colorConfidence;

    private String brand;
    private String brandCode;
    private float brandConfidence;

    private String plateColor;
    private float plateColorConfidence;

    private String plateNumber;
    private float plateNumberConfidence;

}
