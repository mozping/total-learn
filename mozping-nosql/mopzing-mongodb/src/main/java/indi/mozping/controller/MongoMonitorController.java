package indi.mozping.controller;

import com.alibaba.fastjson.JSONObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mozping
 * @Classname MongoMonitorController
 * @Description TODO
 * @Date 2019/4/17 14:33
 */
@RestController
public class MongoMonitorController {

    @Autowired
    MongoClient client;

    @GetMapping("/monitor")
    public JSONObject monitor() {
        MongoClientOptions mongoClientOptions = client.getMongoClientOptions();
        JSONObject json = new JSONObject();
        json.put("maxConnectionsPerHost", mongoClientOptions.getConnectionsPerHost());
        json.put("minConnectionsPerHost", mongoClientOptions.getMinConnectionsPerHost());
        json.put("maxWaitTime", mongoClientOptions.getMaxWaitTime());
        return json;
    }
}
