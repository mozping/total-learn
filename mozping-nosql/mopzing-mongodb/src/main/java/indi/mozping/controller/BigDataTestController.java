package indi.mozping.controller;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.random.RandomObjFactory;
import indi.mozping.service.AddService;
import indi.mozping.utils.Util;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

/**
 * @author by mozping
 * @Classname InsertTestController
 * @Description 测试写入性能
 * @Date 2019/5/16 10:02
 */
@RestController
@RequestMapping("/big/test")
public class BigDataTestController {

    private static final int THREAD_NUM = 1;
    private static final Logger LOG = LoggerFactory.getLogger(AddController.class);
    private static CountDownLatch latch;

    @Autowired
    AddService addService;

    @PostMapping("/add")
    @Async
    public JSONObject testAdd(@RequestBody JSONObject param) throws Exception {

        int threadNum = THREAD_NUM;
        if (param.getIntValue("threadNum") > 0) {
            threadNum = param.getIntValue("threadNum");
        }
        latch = new CountDownLatch(threadNum);

        long recordNum = 10000;
        if (param.getIntValue("recordNum") > 0) {
            recordNum = param.getLong("recordNum") * 10000;
        }

        int batchNum = 1000;
        if (param.getIntValue("batchNum") > 0) {
            batchNum = param.getIntValue("batchNum");
        }

        String tableName = "multobj";
        if (!(param.getString("tableName") == null)) {
            tableName = param.getString("tableName");
        }


        long countBefore = addService.count(tableName);
        LOG.info("Begin test Add...,the docs in table " + tableName + " is : " + countBefore);

        int numPerThread = (int) (recordNum / threadNum);

        long begin = System.currentTimeMillis();
        for (int i = 1; i <= threadNum; i++) {
            new BigDataAddThread(numPerThread, batchNum, tableName, addService).start();
            //ThreadPollUtil.poolExecutor.execute(bigDataAddThread);
        }
        latch.await();
        JSONObject json = new JSONObject();
        long time = System.currentTimeMillis() - begin;
        long speed = (recordNum * 1000) / time;
        json.put("time", time + " ms");
        json.put("speed", speed + " /S");
        StringBuilder sb = new StringBuilder();

        JSONObject detail = new JSONObject();
        detail.put("执行插入线程数是: ", threadNum);
        detail.put("插入总数是: ", recordNum / 10000 + "万");
        detail.put("插入耗时是: ", time + "ms;");
        detail.put("插入速度是: ", speed + "/S;");
        detail.put("每次插入的批次记录数是: ", batchNum);
        detail.put("插入前数据库记录数量是: ", countBefore);
        json.put("detail", detail);
        json.put("response", "新增:" + recordNum + "条数据成功!");
        LOG.info("Result: " + json);
        return json;
    }


    class BigDataAddThread extends Thread {

        private int total;
        private int batchNum;
        private String tableName;
        private AddService addService;

        public BigDataAddThread(int total, int batchNum, String tableName, AddService addService) {
            this.total = total;
            this.batchNum = batchNum;
            this.tableName = tableName;
            this.addService = addService;
        }

        ArrayList<Document> arrayList = new ArrayList<>(1000);

        @Override
        public void run() {
            long flag = System.currentTimeMillis();

            int loop = total / batchNum;

            for (int j = 1; j <= loop; j++) {
                //每3min，sleep 30秒
//                if ((System.currentTimeMillis() - flag) > 180 * 1000) {
//                    try {
//                        Thread.sleep(30 * 1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    flag = System.currentTimeMillis();
//                }
//                try {
//                    Thread.sleep(300);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                for (int i = 1; i <= batchNum; i++) {

                    //Document doc = RandomObjFactory.getRandomObj();
                    //Document doc = RandomObjFactory.getRandomCar();
                    Document doc = RandomObjFactory.getRandomFace();
                    //Document doc = RandomObjFactory.getRandomBody();
                    Date date = Util.getTimeBetweenYear();
                    doc.put("time", date);
                    arrayList.add(doc);
                }
                try {
                    addService.add(arrayList, tableName);
                    LOG.info("新增 " + batchNum + " 条数据成功..........");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    arrayList.clear();
                }
            }
            latch.countDown();
            LOG.info("线程" + Thread.currentThread().getName() + "新增 " + total + " 条数据成功...");
        }
    }

    @PostMapping("/add1")
    public String testAdd() throws Exception {
        ArrayList<Document> arrayList = new ArrayList<>(1000);
        for (int j = 1; j <= 1000; j++) {
            for (int i = 1; i <= 100; i++) {
                Document doc = RandomObjFactory.getRandomObj();
                doc.put("time", Util.getTimeBetweenSomeDay(7));

                arrayList.add(doc);
            }
            try {
                addService.add(arrayList, "multobj");
                LOG.info("新增 " + 100 + " 条数据成功..........");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                arrayList.clear();
            }
        }
        return "ok";

    }
}