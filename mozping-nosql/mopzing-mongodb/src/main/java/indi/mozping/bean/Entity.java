package indi.mozping.bean;

import lombok.Data;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Entity
 * @Description TODO
 * <p>
 * 属性一般为
 * id，
 * 创建人，
 * 创建时间，
 * <p>
 * 人员编号，
 * 设备编号，
 * <p>
 * 人员名称，
 * 设备名称
 * <p>
 * 索引个数两个：id一个；时间+两个编号算一个：
 * 查询条件：时间+两个id（一般为in，in里面id数量不超过1000）；
 * 一般操作为并发1000插入时查询性能和插入性能；
 * @Date 2019/2/22 13:55
 */
@Data
public class Entity {

    private int rid;
    private String createName;
    private Date createTime;

    private int peopleNum;
    private int deviceNum;

    private String peopleName;
    private String deviceName;

}
