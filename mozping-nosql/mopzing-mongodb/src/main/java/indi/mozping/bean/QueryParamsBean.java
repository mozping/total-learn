/*
 *文件名： QueryParamsBean.java
 *版权： Copyright by 云天励飞 intellif.com
 *描述： Description
 *创建人：mozping
 *创建时间： 2018/7/16 16:40
 *修改理由：
 *修改内容：
 */
package indi.mozping.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @author mozping
 * @version 1.0
 * @date 2018/7/16 16:40
 * @see QueryParamsBean
 * @since JDK1.8
 */
@Data
public class QueryParamsBean {

    private JSONObject where;
    private JSONObject in;
    private JSONArray eq;
    private JSONArray like;
    private JSONObject pageParam;
    private String table;
    private JSONObject sort;
    /*
    {
        "time":-1,
        "mode":"db"/application,

    }
     */

}