package indi.mozping;

import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author by mozping
 * @Classname MyRedisClientTest
 * @Description TODO
 * @Date 2019/1/31 11:13
 */

public class MyRedisClientTest {

    public static void main(String[] args) {

        System.out.println(resizeStamp(16));
    }


    static final int resizeStamp(int n) {
        return Integer.numberOfLeadingZeros(n) | (1 << (16 - 1));
    }

}
