package indi.mozping.data;

import lombok.Data;

/**
 * @author by mozping
 * @Classname CameraInfo
 * @Description TODO
 * @Date 2019/9/19 12:08
 */
@Data
public class CameraInfo {

    String id;
    String area;
    String type;

    public CameraInfo(String id, String area, String type) {
        this.id = id;
        this.area = area;
        this.type = type;
    }
}