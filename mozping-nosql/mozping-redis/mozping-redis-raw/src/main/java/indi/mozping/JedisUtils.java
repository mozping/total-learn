//package com.intellif.mozping;
//
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//import redis.clients.jedis.Pipeline;
//
//import java.util.List;
//
//
///**
// * Redis通用工具类
// */
////在使用Spring集成的时候，使用该注解，在无参构造方法中初始化JedisPool
////@Component
//public class JedisUtils {
//
//    private JedisPool pool = null;
//    private String ip = "192.168.42.111";
//    private int port = 6379;
//    private String auth = "12345678";
//
//    /**
//     * 无参构造方法初始化redisPool
//     */
//    public JedisUtils() {
//        if (pool == null) {
//            JedisPoolConfig config = new JedisPoolConfig();
//            config.setMaxTotal(500);
//            config.setMaxIdle(5);
//            config.setMaxWaitMillis(100);
//            config.setTestOnBorrow(true);
//            pool = new JedisPool(config, this.ip, this.port, 100000, this.auth);
//        }
//    }
//
//    /**
//     * 获取key对应的value
//     *
//     * @param key
//     * @return 成功返回value 失败返回null
//     */
//    public String get(String key) {
//        Jedis jedis = null;
//        String value = null;
//        try {
//            jedis = pool.getResource();
//            value = jedis.get(key);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            returnResource(jedis);
//        }
//        return value;
//    }
//
//    /**
//     * 存储键值对,并释放连接资，如果key已经存在 则覆盖
//     *
//     * @param key
//     * @param value
//     * @return 成功 返回OK 失败返回 0
//     */
//    public String set(String key, String value) {
//        Jedis jedis = null;
//        try {
//            jedis = pool.getResource();//每次操作时向pool借用个jedis对象，用完即还?
//            return jedis.set(key, value);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//            return "0";
//        } finally {
//            returnResource(jedis);
//        }
//    }
//
//
//    /**
//     * 删除指定的key,也可以传入一个包含key的数组
//     *
//     * @param keys 个key 也可以使 string 数组
//     * @return 返回删除成功的个
//     */
//    public Long del(String... keys) {
//        Jedis jedis = null;
//        try {
//            jedis = pool.getResource();
//            return jedis.del(keys);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//            return 0L;
//        } finally {
//            returnResource(jedis);
//        }
//    }
//
//
//    /**
//     * 判断key是否存在
//     *
//     * @param key
//     * @return true OR false
//     */
//    public Boolean exists(String key) {
//        Jedis jedis = null;
//        try {
//            jedis = pool.getResource();
//            return jedis.exists(key);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//            return false;
//        } finally {
//            returnResource(jedis);
//        }
//    }
//
//
//    /**
//     * 设置key value并制定这个键值的有效间
//     *
//     * @param key
//     * @param value
//     * @param seconds 单位:
//     * @return 成功返回OK 失败和异常返回null
//     */
//    public String setex(String key, String value, int seconds) {
//        Jedis jedis = null;
//        String res = null;
//        try {
//            jedis = pool.getResource();
//            res = jedis.setex(key, seconds, value);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            returnResource(jedis);
//        }
//        return res;
//    }
//
//
//    /**
//     * 通过批量的key获取批量的value
//     *
//     * @param keys string数组 也可以是个key
//     * @return 成功返回value的集, 失败返回null的集 ,异常返回
//     */
//    public List<String> mget(String... keys) {
//        Jedis jedis = null;
//        List<String> values = null;
//        try {
//            jedis = pool.getResource();
//            values = jedis.mget(keys);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            returnResource(jedis);
//        }
//        return values;
//    }
//
//
//    /**
//     * 删除多个字符串key 并释放连
//     *
//     * @param key*
//     * @return 成功返回value 失败返回null
//     */
//    public boolean mdel(List<String> keys) {
//        Jedis jedis = null;
//        boolean flag = false;
//        try {
//            jedis = pool.getResource();//从连接�用Jedis对象
//            Pipeline pipe = jedis.pipelined();//获取jedis对象的pipeline对象
//            for (String key : keys) {
//                pipe.del(key); //将多个key放入pipe删除指令
//            }
//            pipe.sync(); //执行命令，完全此时pipeline对象的远程调
//            flag = true;
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            returnResource(jedis);
//        }
//        return flag;
//    }
//
//    /**
//     * 返还到连接池
//     *
//     * @param redis
//     */
//    public static void returnResource(Jedis jedis) {
//        try {
//            if (jedis != null) {
//                //高版本jedis close 取代池回收
//                jedis.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}