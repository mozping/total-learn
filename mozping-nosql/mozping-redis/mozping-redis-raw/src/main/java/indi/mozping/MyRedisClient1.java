package indi.mozping;

import redis.clients.jedis.Jedis;

/**
 * @author by mozping
 * @Classname MyRedisClient
 * @Description 测试jedis客户端
 * @Date 2019/1/31 11:09
 */
public class MyRedisClient1 {

    public static final String REDIS_HOST = "192.168.11.27";
    public static final String REDIS_PASSWORD = "Intellifusion@20190108";
    public static final int REDIS_PORT = 6379;
    public static final Jedis JEDIS = new Jedis(REDIS_HOST);

    static {
        JEDIS.auth(REDIS_PASSWORD);
    }

    public static void main(String[] args) throws InterruptedException {
        Jedis jedis = new Jedis(REDIS_HOST, 6379, false);
        jedis.auth(REDIS_PASSWORD);
        System.out.println(jedis.set("name", "mozping1"));
        ;
//        String rsp = JEDIS.set("name", "mozping");
//        System.out.println("响应:" + rsp);
//        String ping = JEDIS.ping();
//        System.out.println("ping的响应：" + ping);
    }
}
