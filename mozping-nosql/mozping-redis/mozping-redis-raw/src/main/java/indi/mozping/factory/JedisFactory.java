package indi.mozping.factory;

import redis.clients.jedis.Jedis;

import java.util.LinkedList;

public class JedisFactory {

    private static LinkedList<Jedis> jedisList = new LinkedList<>();

    static {
        for (int i = 0; i < 20; i++) {
            Jedis jedis = new Jedis("192.168.13.53");
            jedis.auth("123456");
            jedisList.add(jedis);
        }

    }

    public static Jedis getJedis() {
        if (jedisList.size() > 0) {
            Jedis jedis = jedisList.removeFirst();
            return jedis;
        }
        return null;
    }

    public static void close(Jedis jedis) {
        if (jedis != null) {
            jedisList.addLast(jedis);
        }
    }
}
