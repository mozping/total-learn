package indi.mozping;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

import java.io.IOException;
import java.util.List;

public class MyPipeline {
    public static final String REDIS_HOST = "127.0.0.1";
    public static final String REDIS_PASSWORD = "Intellifusion@20190108";
    public static final int REDIS_PORT = 6379;
    public static Jedis jedis = new Jedis(REDIS_HOST);

//    static {
//        jedis.auth(REDIS_PASSWORD);
//    }

    public static void main(String[] args) throws InterruptedException, IOException {

        //1.获取一个Pipeline
        Pipeline pipelined = jedis.pipelined();
        //2.设置命令到Pipeline
        pipelined.set("player1", "Duncan");
        pipelined.set("player2", "James");
        pipelined.set("player3", "Kobe");
        pipelined.get("player1");
        pipelined.get("player2");
        pipelined.get("player3");

        //3.执行并获取结果
        List<Object> returnAll = pipelined.syncAndReturnAll();

        for (Object result : returnAll) {
            System.out.println(result);
        }
    }
}
