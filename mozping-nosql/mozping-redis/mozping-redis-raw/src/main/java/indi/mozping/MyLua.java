package indi.mozping;

import redis.clients.jedis.Jedis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyLua {
    public static final String REDIS_HOST = "127.0.0.1";
    public static final String REDIS_PASSWORD = "Intellifusion@20190108";
    public static final int REDIS_PORT = 6379;
    public static Jedis jedis = new Jedis(REDIS_HOST);

//    static {
//        jedis.auth(REDIS_PASSWORD);
//    }


    public static void main(String[] args) throws InterruptedException, IOException {
        byte[] lua = getContent("./02.lua");
        //key的字节数组集合
        List<byte[]> keyList = new ArrayList<>();
        keyList.add("hobby".getBytes());
        //值的字节数组集合
        List<byte[]> valList = new ArrayList<>();
        keyList.add("code java".getBytes());
        jedis.eval(lua, keyList, valList);
    }

    //读取文件并以byte数组形式返回
    public static byte[] getContent(String filePath) throws IOException {
        File file = new File(filePath);
        long fileSize = file.length();
        if (fileSize > Integer.MAX_VALUE) {
            System.out.println("file too big...");
            return null;
        }
        FileInputStream fi = new FileInputStream(file);
        byte[] buffer = new byte[(int) fileSize];
        int offset = 0;
        int numRead = 0;
        while (offset < buffer.length && (numRead = fi.read(buffer, offset, buffer.length - offset)) >= 0) {
            offset += numRead;
        }
        // 确保所有数据均被读取
        if (offset != buffer.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        fi.close();
        return buffer;
    }
}
