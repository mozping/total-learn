//package com.intellif.mozping;
//
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.params.SetParams;
//
///**
// * @author by mozping
// * @Classname MyRedisClient
// * @Description TODO
// * @Date 2019/1/31 11:09
// */
//public class MyRedisClient {
//
//    public static final String REDIS_HOST = "192.168.11.27";
//    public static final String REDIS_PASSWORD = "Intellifusion@20190108";
//    public static final int REDIS_PORT = 6379;
//    public static Jedis jedis = new Jedis(REDIS_HOST);
//
//    static {
//        jedis.auth(REDIS_PASSWORD);
//    }
//
//
//    public static void main(String[] args) throws InterruptedException {
//        setKV(jedis);
//        setKvExpire(jedis, 100);
////        pingJedis(jedis, null);
////        setIfNotExist(jedis, "name", "mozping");
//    }
//
//
//    /**
//     * @param jedis 连接对象
//     * @Description: 设置键值对
//     * @date 2019/3/6 19:26
//     */
//    public static String setKV(Jedis jedis) {
//        String rsp = jedis.set("foo", "foo123");
//        System.out.println("响应:" + rsp);
//        return rsp;
//    }
//
//    /**
//     * @param jedis 连接对象
//     * @Description: 设置键值对
//     * @date 2019/3/6 19:26
//     */
//    public static long setKvExpire(Jedis jedis, int seconds) {
//        jedis.set("foo", "foo123");
//        long rsp = jedis.expire("foo", seconds);
//        System.out.println("响应:" + rsp);
//        return rsp;
//    }
//
//
//    /**
//     * @param jedis 连接对象
//     * @param key   key
//     * @param value value
//     * @Description: 当key不存在时才设置键值对
//     * @date 2019/3/6 19:27
//     */
//    public static String setIfNotExist(Jedis jedis, String key, String value) {
//        SetParams paramNxEx = new SetParams();
//        paramNxEx.nx().ex(3 * 60);
//        String rsp = jedis.set(key, value, paramNxEx);
//        System.out.println("响应:" + rsp);
//        return rsp;
//    }
//
//    /**
//     * @param jedis 连接对象
//     * @param str   str
//     * @Description: ping redis服务端
//     * @date 2019/3/6 19:28
//     */
//    public static String pingJedis(Jedis jedis, String str) {
//        if (str == null) {
//            return jedis.ping();
//        }
//        return jedis.ping(str);
//    }
//}
