package indi.mozping.lock;

import redis.clients.jedis.Jedis;

import java.util.Date;

/**
 * @author by mozping
 * @Classname MyLockTest
 * @Description 我们定义了10个线程，所有线程共用一把锁，预期线程会同步依次执行打印。
 * @Date 2019/4/29 17:04
 */
public class MyRedisLockTest {

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            new MyThread("Thread-" + i).start();
        }
    }

    static class MyThread extends Thread {

        public MyThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            MyRedisLock.lock();
            try {
                System.out.println("Thread " + Thread.currentThread().getName() +
                        " do something thing..." + new Date());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                MyRedisLock.unLock();
            }
        }
    }
}
