package indi.mozping.lock;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.params.SetParams;

public class Test {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("127.0.0.1");
        SetParams setParams = new SetParams();
        setParams.nx().px(3000);
        String ret = jedis.set("KEY", "123", setParams);
        System.out.println(ret);
    }
}
