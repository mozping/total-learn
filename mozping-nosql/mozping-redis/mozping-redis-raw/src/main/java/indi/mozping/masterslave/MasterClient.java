package indi.mozping.masterslave;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

/**
 * @author by mozping
 * @Classname MasterClient
 * @Description TODO
 * @Date 2019/7/16 18:25
 */
public class MasterClient {
    public static void main(String[] args) throws Exception {

        Set<String> hosts = new HashSet<>();
        hosts.add("192.168.13.52:26379");
        //hosts.add("127.0.0.1:36379"); 配置多个哨兵

        JedisSentinelPool pool = new JedisSentinelPool("mymaster", hosts, "123456");
        Jedis jedis = null;

        for (int i = 0; i < 30; i++) {
            Thread.sleep(5000);
            try {
                jedis = pool.getResource();
                String set = jedis.set("key" + i, "val" + i);
                System.out.println(set);

            } catch (Exception e) {
                System.out.println(" [ exception happened]" + e);
            } finally {
                if (jedis != null) {
                    jedis.close();
                }
            }
        }

    }
}