package indi.mozping.data;

import indi.mozping.factory.JedisFactory;
import redis.clients.jedis.Jedis;

import java.util.*;

/**
 * @author by mozping
 * @Classname HashRedis
 * @Description TODO
 * @Date 2019/9/19 12:07
 */
public class HashRedis {

    static Jedis jedis = JedisFactory.getJedis();

    public static void main(String[] args) {
        //putBigHash();
//        List<String> list = new ArrayList<>();
//        for (int i = 0; i < 100000; i++) {
//            list.add("key_" + i);
//        }
//        getBigHash(list);

    }

    public static void getBigHash(List<String> keys) {
        String[] strs = keys.toArray(new String[keys.size()]);
        long begin = System.currentTimeMillis();
        jedis.hmget("camera", strs);
        System.out.println(System.currentTimeMillis() - begin);
    }

    public static void putBigHash() {
        for (int i = 0; i < 100000; i++) {
            Map<String, String> map = new HashMap();
            map.put("key_" + i, "value_" + i);
            jedis.hset("camera", map);
        }
    }

    public static void setValue() {
        jedis.set("test", "123");
        byte[] bytes = new byte[1024 * 1024];
        String str = new String(bytes);
        for (int i = 0; i < 512; i++) {
            jedis.append("test", str);
        }
    }

    public static void getZset() {
        Set<String> rank = jedis.zrevrange("rank", 0, 2);
        for (String s : rank) {
            System.out.println(s + " -- " + jedis.zscore("rank", s));
        }
    }

    public static void putZset() {
        Map<String, Double> map = new HashMap();
        map.put("1", 15D);
        map.put("2", 30D);
        map.put("3", 7D);
        map.put("4", 18D);
        map.put("5", 42D);
        jedis.zadd("rank", map);
    }

    public static void getHash() {
        Map<String, String> camera = jedis.hgetAll("camera");
        for (String s : camera.keySet()) {
            System.out.println(s + " --- " + camera.get(s));
        }
    }

    public static void putHash() {
        Map<String, String> map = new HashMap();
        CameraInfo c1 = new CameraInfo("1", "高新园", "HK");
        CameraInfo c2 = new CameraInfo("2", "深大", "HK");
        CameraInfo c3 = new CameraInfo("3", "生态园", "HW");
        map.put(c1.getId(), c1.toString());
        map.put(c2.getId(), c2.toString());
        map.put(c3.getId(), c3.toString());
        jedis.hset("camera", map);
    }
}