package indi.mozping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author by mozping
 * @Classname RedisOperation
 * @Description TODO
 * @Date 2019/2/1 12:35
 */
@Controller
@ResponseBody
@SpringBootApplication
public class RedisOperation {

    @Resource(name = "redisTemplate")
    private HashOperations<String, String, Object> redisMap;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * intellifAuthorization
     */
    @GetMapping("/{key}")
    public String getKey(@PathVariable String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    @GetMapping("/map/key")
    public void getKey() {
        String personDetailStr = (String) redisMap.get("monitor_blackDetail.map5277", "monitor_blackDetail.map1");
        System.out.println(personDetailStr);
    }

    public static void main(String[] args) {
        SpringApplication.run(RedisOperation.class, args);
    }
}
