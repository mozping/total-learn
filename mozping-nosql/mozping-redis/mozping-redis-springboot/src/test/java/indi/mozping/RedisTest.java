package indi.mozping;

import indi.mozping.redischannel.RedisUtil;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author by mozping
 * @Classname RedisTest
 * @Description TODO
 * @Date 2019/10/16 16:23
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MozpingRedisSpringbootApplication.class)
public class RedisTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    RedisUtil redisUtil;

    public static String SCRIPT1 = "\n" +
            "local datas = {} \n" +
            "\t if redis.call(\"EXISTS\",KEYS[1]) > 0 then\n" +
            "\t   datas = redis.call(\"SMEMBERS\",KEYS[1])\n" +
//            "\t  redis.call(\"DEL\",KEYS[1])\n" +
            "\treturn datas\n" +
            "   else\n" +
            "\t return datas\n" +
            "end\n" +
            "\t";

    public static String SCRIPT2 = "\n" +
            "\t if redis.call(\"EXISTS\",KEYS[1]) > 0 then\n" +
            "\t return 1\n" +
            "   else\n" +
            "\t return 0\n" +
            "   end";

    @Test
    public void test12() {
        DefaultRedisScript<Long> getRedisScript = new DefaultRedisScript<>(SCRIPT2, Long.class);
        List<String> keys = Lists.newArrayList("setSchemaTableIds1");
        Long execute = (Long) redisTemplate.execute(getRedisScript, keys);
        if (execute != null) {
            System.out.println(execute);
            System.out.println(execute.getClass());
        }
    }


    //测试lua获取set集合
    @Test
    public void test11() {
        DefaultRedisScript<List> getRedisScript = new DefaultRedisScript<>(SCRIPT1, List.class);
        List<String> keys = Lists.newArrayList("setSchemaTableIds");
        List<String> execute = (List<String>) redisTemplate.execute(getRedisScript, keys);
        if (execute != null) {
            System.out.println(execute);
            System.out.println(execute.getClass());
            System.out.println(execute.size());
            List<String> partitionSets = (List<String>) execute;
            if (partitionSets != null && !partitionSets.isEmpty()) {
                for (String s : partitionSets) {
                    if (s == null) {
                        System.out.println("这个元素是Null");
                    } else {
                        System.out.println(s + " -- " + s.getClass());
                    }
                }
            }
        }
    }


    @Test
    public void test4() {
        String setKeyName = "mysetmzp";
        ArrayList<String> list = new ArrayList<>();
        list.add("setk1");
        boolean result = redisUtil.setAppendSadd(setKeyName, list);
        System.out.println(result);
    }

    @Test
    public void test5() {
        String setKeyName = "mysetmzp";
        Set<Object> members = redisUtil.members(setKeyName);
        for (Object o : members) {
            System.out.println(o);
        }
    }

    //集合添加
    @Test
    public void test6() {
        String setKeyName = "setSchemaTableIds";
        //long sadd = redisUtil.sadd(setKeyName, "hahahaha", "heiheihei", "hehehe","table:1");
        long sadd = redisUtil.sadd(setKeyName, "t5018test.test_:1");
        System.out.println("添加了：" + sadd);
        //获取
        Set<Object> members = redisUtil.members(setKeyName);
        for (Object o : members) {
            System.out.println(o);
        }
    }

    //集合元素转移
    @Test
    public void test7() {
        String setKeyName = "mysetmzp";
        String setKeyName1 = "mysetmzp1";
        boolean isMove = redisTemplate.opsForSet().move(setKeyName, "3", setKeyName1);
        if (isMove) {
            Set<Object> members = redisUtil.members(setKeyName);
            System.out.print("通过move(K key, V value, K destKey)方法转移变量的元素值到目的变量后的剩余元素:" + members);
            members = redisTemplate.opsForSet().members(setKeyName1);
            System.out.println(",目的变量中的元素值:" + members);
        }
    }


    public static String SCRIPT =
            "local present = redis.call(\"GET\", KEYS[1]);\n" +
                    "if  present == false  then \n" +
                    "\t redis.call(\"set\",KEYS[1], ARGV[1])\n" +
                    "\t return 0 \n" +
                    "\t end" +
                    "\t local sum = redis.call(\"GET\", KEYS[1]) + ARGV[1];\n" +
                    "\t if sum >=   ARGV[2]  then\n" +
                    "\t redis.call(\"set\",KEYS[1],0)\n" +
                    "     return 1\n" +
                    "   else\n" +
                    "     redis.call(\"INCRBY\",KEYS[1],ARGV[1])\n" +
                    "\t return 0\n" +
                    "   end";


    @Test
    public void test3() {
        DefaultRedisScript<Long> getRedisScript = new DefaultRedisScript<>(SCRIPT, Long.class);
        String redisKet = "dynamic_car.car__statistical1";
        List<String> keys = Lists.newArrayList(redisKet);
        //List<Long> args = new ArrayList<>();
        //keys.add(redisKet);
        //System.out.println(redisTemplate.opsForValue().get(redisKet));

        Object execResult = (Object) redisTemplate.execute(getRedisScript, keys, 3, 10);

        if (execResult != null) {
            System.out.println(execResult + " -- " + execResult.getClass());
        } else {
            System.out.println("End ... ");
        }
    }


    @Test
    public void test1() {
        List<String> list = new ArrayList<>();
        list.add("1");
        //stringRedisTemplate.opsForList().rightPush("mylist", list);
    }


    @Test
    public void test2() {
        String key = "countTest";
//        int i = redisUtil.incAndGet(key, 20);
        long i = redisUtil.inc(key, 20);

        System.out.println(i);
    }

}