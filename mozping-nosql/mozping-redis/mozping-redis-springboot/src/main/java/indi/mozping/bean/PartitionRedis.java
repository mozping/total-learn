package indi.mozping.bean;

import lombok.Data;

import java.util.Date;

/**
 * @author leo-yang
 * @date 2019/10/28 11:43
 */
@Data
public class PartitionRedis {
    //@JsonProperty(value = "id")
    private Integer id;
    //@JsonProperty(value = "node_ip_id")
    private Integer nodeIpId;
    //@JsonProperty(value = "schema_name")
    private String schemaName;
    //@JsonProperty(value = "prefix_table_name")
    private String prefixTableName;
    //@JsonProperty(value = "prefix_table_index")
    private Integer prefixTableIndex;
    //@JsonProperty(value = "start_time")
    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    //@JsonProperty(value = "end_time")
    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
}
