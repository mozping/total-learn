package indi.mozping;

import indi.mozping.controller.TestController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

@SpringBootApplication
public class MozpingRedisSpringbootApplication {


    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                            MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        //schema.table_info
        container.addMessageListener(listenerAdapter, new PatternTopic(TestController.CHANNEL));//配置要订阅的订阅项
        return container;
    }

    public static void main(String[] args) {
        SpringApplication.run(MozpingRedisSpringbootApplication.class, args);
    }

}
