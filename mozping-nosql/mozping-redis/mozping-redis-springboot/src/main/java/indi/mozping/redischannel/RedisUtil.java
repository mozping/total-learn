package indi.mozping.redischannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author by mozping
 * @Classname RedisUtil
 * @Description Redis工具类
 * @Date 2019/10/16 11:13
 */
@Component
public class RedisUtil {

    public static final Logger LOG = LoggerFactory.getLogger(RedisUtil.class);
    public static final String ERROR_MSG = "Redis 操作异常!";

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 读取缓存
     *
     * @param key
     * @return
     */
    public String get(final String key) {
        ValueOperations<String, String> operations = stringRedisTemplate.opsForValue();
        return operations.get(key);
    }

    /**
     * 写入缓存
     *
     * @param key        key
     * @param value      value
     * @param expireTime 过期时间，-1表示不过期
     * @return
     */
    public boolean set(final String key, String value, Long expireTime) {
        try {
            ValueOperations<String, String> operations = stringRedisTemplate.opsForValue();
            if (expireTime < 0) {
                operations.set(key, value);
            } else {
                operations.set(key, value, expireTime, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            LOG.error(ERROR_MSG, e);
            return false;
        }
    }

    /**
     * 发送广播
     *
     * @param channel 频道
     * @param msg     消息
     */
    public boolean notify(String channel, Object msg) {
        try {
            if (msg == null) {
                stringRedisTemplate.convertAndSend(channel, channel);
            }
            stringRedisTemplate.convertAndSend(channel, msg);
            //redisTemplate.convertAndSend(channel, msg);
        } catch (Exception e) {
            LOG.error(ERROR_MSG, e);
            return false;
        }
        return true;
    }


    /**
     * 删除缓存
     *
     * @param key key
     * @return
     */
    public boolean delete(final String key) {
        return stringRedisTemplate.delete(key);
    }


    public int incAndGet(final String key, int num) {
        stringRedisTemplate.opsForValue().increment(key, num);
        String s = stringRedisTemplate.opsForValue().get(key);
        return Integer.valueOf(s);
    }

    public long inc(final String key, int num) {
        return stringRedisTemplate.opsForValue().increment(key, num);
    }


    public void setInc() {
        SetOperations<String, String> set = redisTemplate.opsForSet();
        set.add("set1", "22");
        set.add("set1", "33");
        set.add("set1", "44");
        Set<String> resultSet = redisTemplate.opsForSet().members("set1");
        System.out.println("resultSet:" + resultSet);
    }


    public boolean setAppendSadd(String key, List<String> values) {
        boolean b = (Boolean) redisTemplate.execute((RedisCallback<Boolean>) connection -> {
            StringRedisSerializer keySerializer = (StringRedisSerializer) redisTemplate.getKeySerializer();
            GenericJackson2JsonRedisSerializer valueSerializer = (GenericJackson2JsonRedisSerializer) redisTemplate.getValueSerializer();
            Object obj = connection.execute("SADD", keySerializer.serialize(key),
                    valueSerializer.serialize(values));
            return obj != null;
        });
        return b;
    }


    public Set<Object> members(String setKey) {
        Set<Object> members = redisTemplate.opsForSet().members(setKey);

        return members;
    }

    public long sadd(String setKey, Object... values) {
        Long num = redisTemplate.opsForSet().add(setKey, values);
        return num;
    }

    public long sremove(String setKey, Object... values) {
        Long num = redisTemplate.opsForSet().remove(setKey, values);
        return num;
    }


}
