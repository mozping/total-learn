package indi.mozping;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.bean.PartitionRedis;
import indi.mozping.controller.TestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname MyListener
 * @Description TODO
 * @Date 2019/11/14 12:31
 */
@Component
public class RedisSubscriber extends MessageListenerAdapter {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public void onMessage(Message message, byte[] bytes) {
        System.out.println("------input------");
        String msg = redisTemplate.getStringSerializer().deserialize(message.getBody());
        String topic = redisTemplate.getStringSerializer().deserialize(message.getChannel());
        System.out.println("message: " + msg);
        System.out.println("topic: " + topic);
        if (TestController.CHANNEL.equalsIgnoreCase(topic)) {
            System.out.println("listen 收到消息...");
            PartitionRedis partitionRedis = JSONObject.parseObject(msg, PartitionRedis.class);
            System.out.println(partitionRedis.getId());
            System.out.println(partitionRedis.toString());
        }
    }
}