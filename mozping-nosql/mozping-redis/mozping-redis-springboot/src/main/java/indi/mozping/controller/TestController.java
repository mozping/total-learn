package indi.mozping.controller;

import indi.mozping.redischannel.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mozping
 * @Classname TestController
 * @Description TODO
 * @Date 2019/11/14 12:39
 */
@RestController
public class TestController {

    public static final String CHANNEL = "testChannel";

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    RedisTemplate redisTemplate;

    @GetMapping
    public String test() {
        System.out.println("开始...");
        return "ok";
    }

}