package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MozpingJunitApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingJunitApplication.class, args);
    }

}
