package indi.mozping.controller;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * @author by mozping
 * @Classname ComplexController
 * @Description TODO
 * @Date 2020/9/15 16:05
 */
@RestController
public class ComplexController {

    private static final Logger log = LoggerFactory.getLogger(ComplexController.class);

    @RequestMapping(value = "/complex", method = RequestMethod.POST)
    @ResponseBody
    public String complex(@RequestBody JSONObject param) {
        log.info("process complex request ... {}", param);
        return param.toString();
    }
}