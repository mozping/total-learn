package indi.mozping.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mozping
 * @Classname ComplexController
 * @Description TODO
 * @Date 2020/9/15 16:05
 */
@RestController
public class AssertController {

    private static final Logger log = LoggerFactory.getLogger(AssertController.class);

    @RequestMapping(value = "/assert", method = RequestMethod.POST)
    @ResponseBody
    public int assertMethod(int a, int b) {
        int result = a + b;
        log.info("process assert request ... {}", result);
        return result;
    }
}