package indi.mozping.util;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author moore.mo
 * @description indi.mozping.util TEST1
 * @date 2020/11/13 10:27 上午
 **/
public class TEST1 {

    public static void main(String[] args) {
        C1 c1 = new C1();
        c1.setAa(100);
        C2 c2 = new C2();
        BeanUtils.copyProperties(c1,c2);
        System.out.println(c2);

    }

    @Data
    static  class C1{
        Integer aa;
    }

    @Data
    static  class C2{
        Long aa;
    }
}

