package indi.mozping.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2020/9/15 17:49
 */
public class Test {

    private static final String ENCODING = "UTF-8";

    private static String percentEncode(String value) throws UnsupportedEncodingException {
//        return value != null ? URLEncoder.encode(value, ENCODING).replace("+", "%20").replace("*", "%2A").replace("%7E", "~") : null;

        if (value == null) {
            return null;
        } else {
            return URLEncoder
                    .encode(value, ENCODING)
                    .replace("+", "%20")
                    .replace("*", "%2A")
                    .replace("%7E", "~");
        }
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String ss = "++**VNSNVLSDKfjovsdvsd15155--__..~~  \"\"";
        String encode = URLEncoder.encode(ss, ENCODING);
        System.out.println("编码前：" + ss);
        System.out.println("编码后：" + encode);
    }


}