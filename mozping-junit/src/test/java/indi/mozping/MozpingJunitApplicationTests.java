package indi.mozping;

import com.alibaba.fastjson.JSONObject;
import indi.mozping.controller.AssertController;
import indi.mozping.controller.ComplexController;
import indi.mozping.controller.HelloController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MozpingJunitApplicationTests {

    private static final Logger log = LoggerFactory.getLogger(MozpingJunitApplicationTests.class);

    @Autowired
    private HelloController helloController;

    @Autowired
    private ComplexController complexController;

    @Autowired
    private AssertController assertController;

    @Test
    public void assertControllerTest() {
        int a = new Random().nextInt(100);
        int b = new Random().nextInt(100);
        int beforeResult = a + b;
        log.info("a={} , b={}", a, b);
        int result = assertController.assertMethod(a, b);
        Assert.assertEquals(result, beforeResult);
        //Assert.assertNotNull(o);
    }

    @Test
    public void complexControllerTest() {
        JSONObject object = new JSONObject();
        object.put("name", "mozping");
        object.put("age", 1);
        String result = complexController.complex(object);
        log.info("Result is :{}", result);
    }

    @Test
    public void helloControllerTest() {
        helloController.hello();
    }

    @Test
    public void contextLoads() {
    }

}
