package indi.mozping.consts;


/**
 * 自定义异常
 *
 * @author lx362
 */
public class MsgException extends RuntimeException {
    private String message;
    private int errorCode;

    public MsgException(String msg) {
        super(msg);
        this.message = msg;
    }

    public MsgException(String message, int errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }

    public MsgException(int errorCode) {
        this.message = "123";
        this.errorCode = errorCode;
    }

    public MsgException(String message, Exception cause) {
        super(message, cause);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

}