package indi.mozping.consts;

/**
 * 请求返回码
 *
 * @author lx362
 */
public class CodeConst {
    private CodeConst() {
    }

    /**
     * 请求成功
     */
    public static final int OK = 10000000;
    /**
     * 请求失败
     */
    public static final int FAIL = 15000001;

    /**
     * 文件为空
     */
    public static final int FILE_IS_NULL = 15010001;
    /**
     * 上传文件失败
     */
    public static final int FILE_UP_FAIL = 15010002;
    /**
     * 删除文件失败
     */
    public static final int FILE_DEL_FAIL = 15010003;
    /**
     * 文件地址为空
     */
    public static final int FILE_URL_IS_NULL = 15010004;
    /**
     * 文件下载失败
     */
    public static final int FILE_DOWNLOAD_FAIL = 15010005;
    /**
     * 文件不存在
     */
    public static final int FILE_NOT_EXIST = 15010006;
    /**
     * 大文件上传失败
     */
    public static final int BIGFILE_UP_FAIL = 15010007;
    /**
     * 文件上传中
     */
    public static final int FILE_IS_UPING = 15010008;
    /**
     * 获取用户信息出错
     */
    public static final int GET_USER_MSG_FAIL = 15010009;
    /**
     * 获取文件锁失败，文件或正在上传中
     */
    public static final int GET_FILE_LOCK_FAIL = 15010010;
    /**
     * 查询文件批量删除进度失败，ID过期或错误
     */
    public static final int CHECK_FILE_BATCH_DELETE_FAIL = 15010011;
    /**
     * 文件批量删除出错
     */
    public static final int FILE_BATCH_DELETE_FAIL = 15010012;


}
