package indi.mozping.util;

import com.github.tobato.fastdfs.domain.MataData;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.DefaultFastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;

/**
 * FastDFS工具类
 *
 * @author lx362
 */
@Component
public class FastClientWrapper {

    private final Logger logger = LoggerFactory.getLogger("FastClientWrapper");

    /**
     * Tracker节点的相关信息可以从这里获取
     */
    @Autowired
    private DefaultFastFileStorageClient defaultFastFileStorageClient;


    /**
     * 上传文件
     *
     * @param file 文件对象
     * @return 文件全路径 例如：group1/M00/00/00/wKhjZFsbgEGALzeKAFm1CNSDijc772.jpg
     */
    public String uploadFile(MultipartFile file, Set<MataData> metaDataSet) throws IOException {
        StorePath storePath = defaultFastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), metaDataSet);
        return storePath.getFullPath();
    }

}
