package indi.mozping.service;

import indi.mozping.dto.FileDto;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author lx362
 * @version V1.0
 * @date 2018 06-08 16:28.
 */

public interface FileService {


    /**
     * 上传文件（文件对象）
     *
     * @param file     文件
     * @param isRecord 是否存记录
     * @param token    登录用户token
     * @return 上传文件返回结果对象
     */
    FileDto uploadByFile(MultipartFile file, boolean isRecord, String token);

}
