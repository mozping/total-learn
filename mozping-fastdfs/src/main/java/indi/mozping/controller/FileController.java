package indi.mozping.controller;

import com.alibaba.fastjson.JSON;
import indi.mozping.consts.CodeConst;
import indi.mozping.consts.MsgException;
import indi.mozping.dto.BaseResponse;
import indi.mozping.dto.FileDto;
import indi.mozping.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;


/**
 * 上传文件接口
 */
@RestController
@RequestMapping("")
@EnableAsync
public class FileController {

    private final Logger logger = LoggerFactory.getLogger("FileController");

    private static final String UPLOAD_ERROR = "系统错误，上传文件异常";

    @Autowired
    private FileService fileService;


    /**
     * 上传文件（文件对象）
     *
     * @param file 文件
     * @return /data/fastdfs/storage/data/00/00
     */
    @PostMapping(value = "/fileUploadMulti")
    @ResponseBody
    public BaseResponse fileUploadMulti(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        logger.info("【服务商请求】上传文件（文件对象）接口入参：" + file.getOriginalFilename());

        try {
            FileDto dto = fileService.uploadByFile(file, false, request.getHeader("token"));

            logger.info("【服务商返回】上传文件（文件对象）接口： " + JSON.toJSONString(dto));
            return new BaseResponse(dto, CodeConst.OK, "12345");
        } catch (MsgException e) {
            return new BaseResponse(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            return new BaseResponse(null, CodeConst.FAIL, UPLOAD_ERROR, e.getMessage());
        }
    }


}
