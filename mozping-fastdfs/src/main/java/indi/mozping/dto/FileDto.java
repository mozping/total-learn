package indi.mozping.dto;

/**
 * 上传文件返回结果对象
 *
 * @author liuxin
 */
public class FileDto {

    private String fileUrl;
    private String fileId;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
}
