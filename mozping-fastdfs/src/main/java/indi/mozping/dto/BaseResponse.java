package indi.mozping.dto;

import java.io.Serializable;

/**
 * {
 * "respCode": "状态码",
 * "respMessage": "消息提示",
 * "respRemark": "解释性说明"
 * "data" : null
 * }
 *
 * @author lx362
 */
public class BaseResponse implements Serializable {

    private static final long serialVersionUID = 2405172041950251807L;

    private Object data;
    private Integer respCode;
    private String respMessage;
    private String respRemark;

    public BaseResponse(Object data) {
        this.data = data;
    }

    public BaseResponse(Object data, Integer respCode) {
        this.data = data;
        this.respCode = respCode;
    }

    public BaseResponse(Object data, Integer respCode, String respMessage) {
        this.data = data;
        this.respCode = respCode;
        this.respMessage = respMessage;
    }

    public BaseResponse(Object data, Integer respCode, String respMessage, String respRemark) {
        this.data = data;
        this.respCode = respCode;
        this.respMessage = respMessage;
        this.respRemark = respRemark;
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getRespCode() {
        return respCode;
    }

    public void setRespCode(Integer respCode) {
        this.respCode = respCode;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespRemark() {
        return respRemark;
    }

    public void setRespRemark(String respRemark) {
        this.respRemark = respRemark;
    }
}
