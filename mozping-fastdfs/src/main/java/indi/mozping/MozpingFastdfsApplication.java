package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MozpingFastdfsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingFastdfsApplication.class, args);
    }

}
