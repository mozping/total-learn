package indi.mozping.serializ;

import indi.mozping.bean.TestSerializable;
import indi.mozping.bean.TestSerializableFack;
import indi.mozping.bean.TestSerializableStatic;

import java.io.*;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2020/8/10 13:05
 */
public class Test {

    public static void main(String[] args) throws Exception {
        //testSerilize();
        //testSerilizeFack();


        //testSerilizeWrite();
        //testSerilizeRead();


        testSerilizeStatic();
    }

    static void testSerilizeFack() throws IOException, ClassNotFoundException {
        //序列化
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("TestSerializableFack.obj"));
        oos.writeObject("测试序列化");
        oos.writeObject(618);
        oos.writeObject("随意写入");
        TestSerializableFack test = new TestSerializableFack(1, "zhangsan", "123456");
        oos.writeObject(test);

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("TestSerializableFack.obj"));
        System.out.println((String) ois.readObject());
        System.out.println((Integer) ois.readObject());
        System.out.println((String) ois.readObject());
        System.out.println((TestSerializableFack) ois.readObject());
    }


    static void testSerilize() throws IOException, ClassNotFoundException {
        //序列化
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("TestSerializable.obj"));
        oos.writeObject("测试序列化");
        oos.writeObject(618);
        oos.writeObject("随意写入");
        TestSerializable test = new TestSerializable(1, "zhangsan", "123456");
        oos.writeObject(test);

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("TestSerializable.obj"));
        System.out.println((String) ois.readObject());
        System.out.println((Integer) ois.readObject());
        System.out.println((String) ois.readObject());
        System.out.println((TestSerializable) ois.readObject());
    }

    static void testSerilizeWrite() throws IOException, ClassNotFoundException {
        //序列化
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("TestSerializable.obj"));
        oos.writeObject("测试序列化");
        oos.writeObject(618);
        oos.writeObject("随意写入");
        TestSerializable test = new TestSerializable(1, "zhangsan", "123456");
        oos.writeObject(test);
    }

    static void testSerilizeRead() throws IOException, ClassNotFoundException {
        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("TestSerializable.obj"));
        System.out.println((String) ois.readObject());
        System.out.println((Integer) ois.readObject());
        System.out.println((String) ois.readObject());
        System.out.println((TestSerializable) ois.readObject());
    }


    static void testSerilizeStatic() throws IOException, ClassNotFoundException {
        //序列化
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("TestSerializableStatic.obj"));
        TestSerializableStatic test = new TestSerializableStatic(1, "zhangsan");
        test.setFlag(1);
        oos.writeObject(test);
        test.setFlag(10);

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("TestSerializableStatic.obj"));
        TestSerializableStatic testSerializableStatic = (TestSerializableStatic) ois.readObject();
        System.out.println(testSerializableStatic.toString());
        System.out.println(testSerializableStatic.getFlag());
    }
}
