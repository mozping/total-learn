package indi.mozping.bean;

/**
 * @author by mozping
 * @Classname TestSerializable
 * @Description TODO
 * @Date 2020/8/10 12:59
 */
public class TestSerializableSub extends TestSerializableParent {

    private static final long serialVersionUID = 5887391604554532906L;

    private transient String nameSub;
    private transient String passwordSub;

    public TestSerializableSub(String nameSub, String passwordSub) {
        super(1, "name", "password");
        this.nameSub = nameSub;
        this.passwordSub = passwordSub;
    }

    @Override
    public String toString() {
        return "TestSerializableSub{" +
                "nameSub='" + nameSub + '\'' +
                ", passwordSub='" + passwordSub + '\'' +
                '}';
    }
}
