package indi.mozping.bean;

import java.io.Serializable;

/**
 * @author by mozping
 * @Classname TestSerializable
 * @Description TODO
 * @Date 2020/8/10 12:59
 */
public class TestSerializableStatic implements Serializable {

    private static final long serialVersionUID = 5887391604554532907L;

    private int id;

    private String name;
    private static int flag;


    public TestSerializableStatic(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "TestSerializableStatic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public   int getFlag() {
        return flag;
    }

    public   void setFlag(int flag) {
        TestSerializableStatic.flag = flag;
    }
}
