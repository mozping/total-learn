package indi.mozping.bean;

import java.io.Serializable;

/**
 * @author by mozping
 * @Classname TestSerializable
 * @Description TODO
 * @Date 2020/8/10 12:59
 */
public class TestSerializableParent implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;

    private String name;
    private transient String password;

    public TestSerializableParent(int id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    @Override
    public String toString() {
        return "TestSerializableParent{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
