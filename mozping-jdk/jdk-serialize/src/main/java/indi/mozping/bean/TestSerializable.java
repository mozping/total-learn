package indi.mozping.bean;

import java.io.Serializable;

/**
 * @author by mozping
 * @Classname TestSerializable
 * @Description TODO
 * @Date 2020/8/10 12:59
 */
public class TestSerializable implements Serializable {

    private static final long serialVersionUID = 5887391604554532907L;

    private int id;

    private transient String name;
    private transient String password;


    public TestSerializable(int id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    @Override
    public String toString() {
        return "TestSerializable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
