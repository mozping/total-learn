package indi.mozping.bean;

/**
 * @author by mozping
 * @Classname TestSerializable
 * @Description TODO
 * @Date 2020/8/10 12:59
 */
public class TestSerializableFack {

    private int id;

    private transient String name;
    private transient String password;

    public TestSerializableFack(int id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    @Override
    public String toString() {
        return "TestSerializable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
