package maptest;


import java.util.concurrent.CountDownLatch;

/**
 * @author by mozping
 * @Classname HashMapTest
 * @Description TODO
 * @Date 2019/12/17 19:56
 */
public class HashMapTest {

    public static final CountDownLatch latch = new CountDownLatch(5);

    public static void main(String[] args) throws InterruptedException {
        HashMapThread thread1 = new HashMapThread("HashMap-TestThread-1");
        HashMapThread thread2 = new HashMapThread("HashMap-TestThread-2");
        HashMapThread thread3 = new HashMapThread("HashMap-TestThread-3");
        HashMapThread thread4 = new HashMapThread("HashMap-TestThread-4");
        HashMapThread thread5 = new HashMapThread("HashMap-TestThread-5");
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        latch.await();
        System.out.println("size: " + HashMapThread.map.size());
        for (Integer i : HashMapThread.map.keySet()) {
            if (!HashMapThread.map.get(i).equals(i)) {
                System.out.println(i + "  》》 " + HashMapThread.map.get(i));
            }
        }
    }
}


