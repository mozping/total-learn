package maptest;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author by mozping
 * @Classname HashMapThread
 * @Description TODO
 * @Date 2019/12/17 19:57
 */
public class HashMapThread extends Thread {

    private static AtomicInteger count = new AtomicInteger();
    public static Map<Integer, Integer> map = new HashMap<>();

    public HashMapThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        while (count.get() < 1000000) {
            map.put(count.get(), count.get());
            count.incrementAndGet();
        }
        HashMapTest.latch.countDown();
    }
}
