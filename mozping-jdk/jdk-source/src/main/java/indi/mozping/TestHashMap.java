package indi.mozping;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author by mozping
 * @Classname TestHashMap
 * @Description TODO
 * @Date 2019/8/9 15:19
 */
public class TestHashMap {

//    public static void main(String[] args) {
//        HashMap<String,String> map = new HashMap<String, String>();
//        map.put(null, "123");
//        map.put(null, "456");
//        System.out.println("----");
//        String s = map.get(null);
//        System.out.println(s);
//
//        Map m = Collections.synchronizedMap(map);
//
//
//        ArrayList<String> arrayList = new ArrayList<String>();
//        arrayList.add("123");
//        String ss = arrayList.get(0);
//        System.out.println(ss);
//    }

    //切换volatile和原子类型
    //static AtomicInteger auto = new AtomicInteger(0);
    static volatile int auto = 0;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            new Thread(new Modify()).start();
        }
        Thread.sleep(5000);
        System.out.println("修改后的值是:" + auto);
    }

    static class Modify implements Runnable {
        public void run() {
            for (int i = 0; i < 1000; i++) {
                //切换volatile和原子类型
                //auto.getAndIncrement();
                auto++;
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}