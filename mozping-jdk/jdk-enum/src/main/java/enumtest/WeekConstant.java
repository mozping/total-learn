package enumtest;

/**
 * @author by mozping
 * @Classname WeekConstant
 * @Description WeekConstant
 * @Date 2019/12/18 10:18
 */
public class WeekConstant {

    public static final int MONDAY = 1;
    public static final int TUESDAY = 2;
    public static final int WEDNESDAY = 3;
    public static final int THURSDAY = 4;
    public static final int FRIDAY = 5;
    public static final int SATURDAY = 6;
    public static final int SUNDAY = 7;
}