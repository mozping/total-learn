package enumtest;

/**
 * @author by mozping
 * @Classname EnumTest
 * @Description TODO
 * @Date 2019/12/18 10:27
 */
public class EnumTest {

    public static void main(String[] args) {
        WeekEnum[] values = WeekEnum.MON.values();
        for (WeekEnum w : values) {
            System.out.print(w + " ");
        }
        System.out.println();
        WeekEnum[] values1 = WeekEnum.values();
        for (WeekEnum w : values1) {
            System.out.print(w + " ");
        }
    }
}