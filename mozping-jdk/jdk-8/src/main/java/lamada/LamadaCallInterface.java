package lamada;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author by mozping
 * @Classname LamadaCallInterface
 * @Description TODO
 * @Date 2019/12/18 11:09
 */
public class LamadaCallInterface {

    //
    public static void main(String[] args) {
        //代替匿名内部类
        //1.lamada调用线程
        new Thread(() -> {
            System.out.println("lamada 调用线程");
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("new Runnable() 调用线程");
            }
        }).start();

        //1.lamada 实现接口
        People people = new People();
        people.say(() -> {
            System.out.println("lamada 实现接口");
        });


        //集合进行迭代
        List<String> languages = Arrays.asList("java", "scala", "python");
        //before java8
        for (String each : languages) {
            System.out.println(each);
        }
        //after java8
        languages.forEach(x -> System.out.println(x));
        languages.forEach(System.out::println);

        //lambda表达式实现map（map函数的作用是将一个对象变换为另外一个）
        List<Double> cost = Arrays.asList(10.0, 20.0, 30.0);
        cost.stream().map(x -> x + x * 0.5).forEach(x -> System.out.println(x));

        //实现map与reduce(map的作用是将一个对象变为另外一个，reduce实现的则是将所有值合并为一个)
        List<Double> cost1 = Arrays.asList(10.0, 20.0, 30.0);
        double allCost = cost1.stream().map(x -> x + x * 0.5).reduce((sum, x) -> sum + x).get();
        System.out.println(allCost);

        //.filter操作
        List<Double> cost2 = Arrays.asList(10.0, 20.0, 30.0, 40.0);
        List<Double> filteredCost = cost2.stream().filter(x -> x > 25.0).collect(Collectors.toList());
        filteredCost.forEach(x -> System.out.println(x));
    }

    static class People {
        void say(Hello hello) {
            hello.hello();
        }
    }

    interface Hello {
        void hello();
    }
}