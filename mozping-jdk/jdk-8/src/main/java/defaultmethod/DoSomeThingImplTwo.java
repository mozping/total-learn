package defaultmethod;


/**
 * @author by mozping
 * @Classname DoSomeThingImpl
 * @Description TODO
 * @Date 2019/12/18 11:01
 */
public class DoSomeThingImplTwo implements DoSomeThing {


    @Override
    public void doOne() {
        System.out.println("DoSomeThingImplTwo 实现 DoSomeThing 接口的 doOne 方法...");
    }

}