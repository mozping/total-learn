package defaultmethod;

/**
 * @author by mozping
 * @Classname DoSomeThing
 * @Description TODO
 * @Date 2019/12/18 10:58
 */
public interface DoSomeThing {

    void doOne();

    default void doTwo() {
        System.out.println("DoSomeThing 接口 doTwo 方法的 default 实现...");
    }

    default void doThree() {
        System.out.println("DoSomeThing 接口 doThree 方法的 default 实现...");
    }
}
