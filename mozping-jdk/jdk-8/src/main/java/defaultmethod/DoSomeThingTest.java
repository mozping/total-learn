package defaultmethod;

/**
 * @author by mozping
 * @Classname DoSomeThingTest
 * @Description TODO
 * @Date 2019/12/18 11:03
 */
public class DoSomeThingTest {

    public static void main(String[] args) {

        DoSomeThing doSomeThing = new DoSomeThingImpl();

        doSomeThing.doOne();
        doSomeThing.doTwo();
        doSomeThing.doThree();

        DoSomeThing doSomeThingImplTwo = new DoSomeThingImplTwo();
        doSomeThingImplTwo.doOne();
        doSomeThingImplTwo.doTwo();
        doSomeThingImplTwo.doThree();
    }
}