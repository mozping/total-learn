# mozping-concurrent
## 一、原子操作CAS    
### 1.原理
+ CAS在cpu的指令级别来保证原子操作，包括new，old，address三个值，old是期望值，new是新值，
address是地址值，如果期望值和地址处的值一样，则修改，反之则自旋直到成功
+ CAS的问题：ABA问题，引入版本号解决
+ CAS只能操作一个变量，可以封装多个需要操作的变量到一个对象中


### 2.创建和停止java线程
#### 2.1 创建
+  
#### 2.2 停止
+ 线程执行完毕自然停止
 
#### 2.3 比较下面三者的区别：
 
        
 

#### 2.4 InterruptedException异常：
 
 
### 3.线程的生命周期
#### 3.1 示图
![图片alt](pic/thread.jpg)
#### 3.1 周期转换过程的方法
+ yield:  



### 4.synchronized关键字
+ synchronized是对象锁，锁住的是实例对象，不通的对象锁之间可以并行
 


### 4.volatile关键字
+ 更新后直接刷回主内存
 

### 5.ThreadLocal线程本地变量
+ 示例：com.intellif.mozping.threadlocal.ThreadLocalTest

 













 
