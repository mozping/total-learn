# mozping-concurrent
## 一、     
### 1. 主要类说明
+ TaskResultType 响应类型，成功/失败/异常
+ TaskResult 响应结果的封装对象
+ PendingJobPool 框架核心类，业务调用类
+ MyTask 自定义任务
+ JobInfo 任务封装后的对象
+ ITaskProcesser 任务接口
+ AppTest 测试类
+ CheckJobProcesser 检查任务进度

### 2. 类详解
#### 2.1 
![Image text](pic/pendingjobpool.jpg)
