# LOCK  (lockp包)
## 一.重入锁ReentrantLock
```aidl
可重入锁是一种递归无阻塞的同步机制。它可以等同于synchronized的使用，但ReentrantLock 提供了比 synchronized 更强大、灵活的锁机制，可以
减少死锁发生的概率；
```
![Image text](../pic/concurrent/ReentrantLock.jpg)
#### 1.1 主要类介绍
###### ReentrantLock:默认为非公平锁(效率更高)，
#### 1.1 Sync 抽象类
>* Sync是 ReentrantLock 的静态内部类，实现 AbstractQueuedSynchronizer ，同步器抽象类。它
使用 AQS 的 state 字段，来表示当前锁的持有数量，从而实现可重入的特性。

#### 1.2 NonfairSync非公平锁实现类
#### 1.3 FairSync公平锁实现类
#### 1.4 Lock 接口 
>* 锁的抽象类
#### 1.5 ReentrantLock可重入锁，实现Lock接口
>* ReentrantLock类的方法，主要是对Sync类的调用
#### 1.6 ReentrantLock 与 synchronized 的区别
```aidl
1.与 synchronized 相比，ReentrantLock提供了更多，更加全面的功能，具备更强的扩展性。例如：时
间锁等候，可中断锁等候，锁投票。
2.ReentrantLock 还提供了条件 Condition ，对线程的等待、唤醒操作更加详细和灵活，所以在多个条件变量和高度竞争锁的地方，
ReentrantLock 更加适合（以后会阐述Condition）。
3.ReentrantLock 提供了可轮询的锁请求。它会尝试着去获取锁，如果成功则继续，否则可以等到下次运行时处理，而 synchronized 则一旦进入锁请求要么成功要么阻塞，
所以相比 synchronized 而言，ReentrantLock会不容易产生死锁些。
4.ReentrantLock 支持更加灵活的同步代码块，但是使用 synchronized 时，只能在同一个 synchronized 块结构中获取和释放。注意，ReentrantLock 的锁释放一定
要在 finally 中处理，否则可能会产生严重的后果。
5.ReentrantLock 支持中断处理，且性能较 synchronized 会好些。

```

 
 
 



## 二.读写锁ReentrantReadWriteLock
#### 2.1 主要类介绍
###### ReadWriteLock读写锁接口，提供读写两种锁的获取接口   
###### ReentrantReadWriteLock可重入读写锁实现类，内部维护读写锁，读锁共享，写锁独占,读写锁都是以内部类的形式维护在ReentrantReadWriteLock
类中的，还提供了很多相关的方法，比如判断是否为公平锁，获取锁的方法等等
####
#### 