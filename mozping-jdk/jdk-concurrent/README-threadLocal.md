# ThreadLocal - (threadlocalp包)
## 一. ThreadLocal
#### 1.1 引入
```aidl
ThreadLocal提供了线程局部 (thread-local) 变量,每个线程都有自己的局部变量，常用与private static字段，将值
与线程绑定。
```
#### 1.2 示例
###### 1.2.1 场景
```aidl
一个静态类变量，两个线程均对它做访问，线程1每次增加2，线程2每次增加4，通过代码演示这个过程中，
两个线程的值不会相互影响
```
###### 1.2.2 代码
```aidl
package com.intellif.mozping.threadlocalp;

/**
 * ThreadLocal用法
 *
 * @author mozping
 */
public class MyThreadLocal {
    private static final ThreadLocal<Object> value = new ThreadLocal<Object>() {
        /**
         * ThreadLocal没有被当前线程赋值时或当前线程刚调用remove方法后调用get方法，返回此方法值
         */
        @Override
        protected Object initialValue() {
            System.out.println("调用get方法时，当前线程共享变量没有设置，调用initialValue获取默认值！");
            return 0;
        }
    };

    public static void main(String[] args) {
        new Thread(new MyIntegerTask("1")).start();
        new Thread(new MyIntegerTask("2")).start();
    }

    public static class MyIntegerTask implements Runnable {
        
        private String name;
        MyIntegerTask(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                // ThreadLocal.get方法获取线程变量
                if (null == MyThreadLocal.value.get()) {
                    // ThreadLocal.et方法设置线程变量
                    MyThreadLocal.value.set(0);
                    System.out.println("线程" + name + ": 0");
                } else {
                    int num = (Integer) MyThreadLocal.threadLocal.get();
                    MyThreadLocal.threadLocal.set(num + 2 * Integer.parseInt(name));
                    System.out.println("线程" + name + ": " + MyThreadLocal.threadLocal.get());
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}


输出(每次的顺序会有不同)：
    调用get方法时，当前线程共享变量没有设置，调用initialValue获取默认值！
    线程1: 2
    线程1: 4
    调用get方法时，当前线程共享变量没有设置，调用initialValue获取默认值！
    线程2: 4
    线程1: 6
    线程2: 8
    线程2: 12
    线程1: 8
    线程1: 10
    线程2: 16
    线程2: 20
    
PS：
    看到线程1可以正常的每次增加2，因此输出2，4，6，8，10，线程2每次增加4，一次是4，8，12，16，20
```

#### 1.3 原理初探
```aidl
1.首先了解ThreadLocal这个类的总体设计，是如何做到变量线程隔离的
```
###### 1.3.1 ThreadLocal.ThreadLocalMap
>* ThreadLocal类包含一个内部类ThreadLocalMap，而Thread类包含一个ThreadLocal.ThreadLocalMap类型
的成员变量，看名字不用想应该是一个Map类型的东东，换言之，每个Thread对象，他的内部持有这样一个Map。
>* 我们通过threadLocal.get()方法来切入，看获取threadlocal变量和公共提到的这个Map是何种关系？
```aidl
public T get() {
        //1.获取当前线程
        Thread t = Thread.currentThread();
        //2.获取线程内部的Map，没错，就是刚刚提到的ThreadLocal.ThreadLocalMap类型的Map
        ThreadLocalMap map = getMap(t);
        if (map != null) {
            //2.1 获取map的Entry，Entry是一个包含k-v对的数据结构，key就是value这个变
            //量自身，value就是真正保存的值
            ThreadLocalMap.Entry e = map.getEntry(this);
            if (e != null) {
                @SuppressWarnings("unchecked")
                T result = (T)e.value;
                return result;
            }
        }
        //3.如果map尚未初始化，就执行setInitialValue方法，该方法后面做介绍
        return setInitialValue();
    }
    
    //2.1获取线程对象内部持有的ThreadLocal.ThreadLocalMap类型的Map
    ThreadLocalMap getMap(Thread t) {
            return t.threadLocals;
    }
    
PS：好的，到这一步我们大概知道ThreadLical是如何保证线程之间互不干扰的了，通过一个简图表示如下
```

###### 1.3.2 ThreadLocal.getEntry 和 ThreadLocal.getEntryAfterMiss
>* 这两个方法是获取Entry的关键，代码不长，我们看看：
```aidl
    private Entry getEntry(ThreadLocal<?> key) {
            //1.首先计算hash下标
            int i = key.threadLocalHashCode & (table.length - 1);
            //2.在下标位置获取对应的Entry
            Entry e = table[i];
            //3.如果获取到的Entry不为null，且key和ThreadLocal变量一致，说明一次命中那么就返回Entry对应的
            //value(Entry就是以ThreadLocal为k，值为value存储的键值对)
            if (e != null && e.get() == key)
                return e;
            else
                //4.如果获取的Entry为null或者key不对，那么就在没有命中之后重新获取
                return getEntryAfterMiss(key, i, e);
        }
        
        private Entry getEntryAfterMiss(ThreadLocal<?> key, int i, Entry e) {
                    Entry[] tab = table;
                    int len = tab.length;
                    //1.循环获取
                    while (e != null) {
                        //1.1.计算key
                        ThreadLocal<?> k = e.get();
                        if (k == key)
                        //1.2.如果key一致，和getEntry方法返回是一个情况，直接返回Entry
                            return e;
                        if (k == null)
                        //1.3如果key为null，说明这里有废弃的元素需要清理掉，会调用expungeStaleEntry方法清理掉可移除的位置
                            expungeStaleEntry(i);
                        else
                        //1.4如果key不一致也不是null，说明这里保存着其他的变量，那就继续往后面找，nextIndex方法会获取后面一个位置的下标值
                            i = nextIndex(i, len);
                        e = tab[i];
                    }
                    //2.如果Entry为null，说明getEntry方法中通过hash找到的位置里面没有元素，那么直接返回
                    return null;
                }
```

###### 1.3.3 ThreadLocal.expungeStaleEntry 
>* 清理过时变量，会引起所有元素的重新分布，清理当前的脏entry后继续往后清理直到Entry为null(脏entry是Entry的key为null，但是Entry不为null)
```aidl
   private int expungeStaleEntry(int staleSlot) {
            
            Entry[] tab = table;
            int len = tab.length;

            //1.staleSlot是key为null的Entry对应在table的下标位置，首先把他干掉，这里主要是干掉value，因为key本身就已经是null了
            tab[staleSlot].value = null;
            tab[staleSlot] = null;
            //2.size减一，size代表tables的元素个数
            size--;

            // Rehash until we encounter null
            Entry e;
            int i;
            //3.这个循环条件是从key为null的位置的后一个位置开始，一直向后遍历，直到遇到下一个Entry为null的位置(Entry不为null就一直往后走)
            for (i = nextIndex(staleSlot, len); (e = tab[i]) != null; i = nextIndex(i, len)) {
                //3.1首先拿到后面第一个位置的Entry的key
                ThreadLocal<?> k = e.get();
                //3.2如果这个位置key是null，那就把Entry的k，v域都置null，size减一(清理脏entry)
                if (k == null) {
                    e.value = null;
                    tab[i] = null;
                    size--;
                } else {
                    //3.3如果这个位置的key不是null，那么就应该保留这个Entry，因为table的总大小是可能变化的，所以要重新计算这个Entry的
                    //位置，重新找一个合适的地方存放
                    int h = k.threadLocalHashCode & (len - 1);
                    //3.4这里不相等，说明元素先后存放的位置发生了改变，如果位置没有变化就不用做处理了
                    if (h != i) {
                        //3.5先把原来的位置重置为null
                        tab[i] = null;
                        // Unlike Knuth 6.4 Algorithm R, we must scan until
                        // null because multiple entries could have been stale.
                        //3.6如果新的存放的问题已经有元素了，就一直往后面找，直到直到一个空位值
                        while (tab[h] != null)
                            h = nextIndex(h, len);
                        //3.7把元素放入新的位置
                        tab[h] = e;
                    }
                }
            }
            return i;
        }
```

###### 1.3.4 rehash扩容
>* ThreadLocal变量可以由很多，因此内部存储机制支持扩容，主要是rehash和resize方法
```aidl
          /**
           * 首先会扫描entire table并且移除脏Entry,如果容量还不够就会将table的大小翻倍
           */
          private void rehash() {
               //1.首先清理过期的脏Entry，原本进行rehash的阈值threshold是len 的2/3，但是清理了过期的脏数据之后，有可能会腾出一些空间
               //，size会减小，所以后面第二步判断是否需要真正的resize扩容的时候，就用了一个更低的比例，3/4的threshold,换言之如果expungeStaleEntries
               //之后，能够将size降到threshold的3/4，那么就不要进行后面的resize了，有助于性能。
              expungeStaleEntries();

              //2.如果当前元素个数已经大于阈值的3/4，那么就扩容，阈值是长度的2/3，因此元素占用总长度len的一半时，开始扩容
              //use lower threshold for doubling to avoid hysteresis
              if (size >= threshold - threshold / 4)
                  resize();
          }
  
          /**
           * 将tables的容量扩大2倍
           */
          private void resize() {
              Entry[] oldTab = table;
              int oldLen = oldTab.length;
              //1.新的长度扩大2倍
              int newLen = oldLen * 2;
              Entry[] newTab = new Entry[newLen];
              int count = 0;
  
              //2.遍历旧的table
              for (int j = 0; j < oldLen; ++j) {
                  Entry e = oldTab[j];
                   //2.1对不为null的Entry进行处理
                  if (e != null) {
                      ThreadLocal<?> k = e.get();
                      if (k == null) {
                          //2.2如果Entry的key为null，那么僵value也置为null，帮助GC垃圾回收
                          e.value = null; // Help the GC
                      } else {
                          //2.3计算Entry在新的table的下标，然后插入，如果新的位置已经有元素占用，则往后找，直到找到可用的位置
                          int h = k.threadLocalHashCode & (newLen - 1);
                          while (newTab[h] != null)
                              h = nextIndex(h, newLen);
                          newTab[h] = e;
                          count++;
                      }
                  }
              }
              //3.设置新的阈值， 元素个数，table 
              setThreshold(newLen);
              size = count;
              table = newTab;
          }
PS:
    rehash和resize方法逻辑比较简单清晰
```

###### 1.3.5 另一个清理脏Entry的方法ThreadLocal.cleanSomeSlots
>* ThreadLocal.cleanSomeSlots也是清理脏Entry的方法
```aidl
     * @param i a position known NOT to hold a stale entry. The scan starts at the element after i.
    i是Entry为null的位置,扫描清理从i后面的第一个位置开始
   private boolean cleanSomeSlots(int i, int n) {
               boolean removed = false;
               Entry[] tab = table;
               int len = tab.length;
               do {
                   i = nextIndex(i, len);
                   Entry e = tab[i];
                   //1.如果遇到了脏Entry，那么就会将n重置为当前的长度，然后再多扫描log2(len)次，如果没有遇到脏Entry，那么总共只扫描log2(n)次
                   //可以在时间和效率上做个折中
                   if (e != null && e.get() == null) {
                       n = len;
                       removed = true;
                       i = expungeStaleEntry(i);
                   }
               } while ( (n >>>= 1) != 0);
               //2.返回值如果为false说明没有遇到脏Entry，如果为true说明处理过脏Entry
               return removed;
           }
```
![Image text](../pic/concurrent/cleanSomeSlots.jpg)

###### 1.3.6 set/remove方法
>* 前面讲了get方法，以及处理脏Entry的方法，这里看下set值的方法和移除值的remove方法
```aidl
    //1.对外可用的public的set方法
    public void set(T value) {
        Thread t = Thread.currentThread();
        ThreadLocalMap map = getMap(t);
        if (map != null)
            //2.map存在，直接设置，调用private的set方法
            map.set(this, value);
        else
            //3.map不存在，创建
            createMap(t, value);
    }
    
    
   private void set(ThreadLocal<?> key, Object value) {
               //1.这里没有想get方法一样使用快速路径，因为用set方法需要设置新的Entry去替换旧的Entry，快速路径可能会更容易失败   
               Entry[] tab = table;
               int len = tab.length;
               int i = key.threadLocalHashCode & (len-1);
               //2.从i位置开始向后遍历，目的是找到一个为null的Entry 
               for (Entry e = tab[i]; e != null; e = tab[i = nextIndex(i, len)]) {
                   
                   ThreadLocal<?> k = e.get();
                   //2.1 key存在，直接设置值
                   if (k == key) {
                       e.value = value;
                       return;
                   }
                    //2.2key为null，但是这里的e!=null，说明之前对应的ThreadLocal变量已经被回收了，这是一个脏Entry，直接替换这个脏的Entry
                   if (k == null) {
                        //2.3用新的元素代替旧的元素
                       replaceStaleEntry(key, value, i);
                       return;
                   }
               }
                //3.如果第2步，没有脏Entry，也没有找到对应的key，那么就只好自己new一个保存起来了，
               tab[i] = new Entry(key, value);
               int sz = ++size;
               //4.cleanSomeSlots 清除陈旧的Entry（key == null）
               // 如果没有清理陈旧的 Entry 并且数组中的元素大于了阈值，则进行rehash
               if (!cleanSomeSlots(i, sz) && sz >= threshold)
                   rehash();
           }
   
   
        private void remove(ThreadLocal<?> key) {
                Entry[] tab = table;
                int len = tab.length;
                int i = key.threadLocalHashCode & (len-1);
                //1.从i处往后遍历，直到找到key符合传入对象的Entry，
                for (Entry e = tab[i]; e != null; e = tab[i = nextIndex(i, len)]) {
                    if (e.get() == key) {
                        //2.清除对应的Entry
                        e.clear();
                        //3.从i位置处清理脏Entry，清理i到下一个null Entry之间所有的脏Entry
                        expungeStaleEntry(i);
                        return;
                    }
                }
            }
```
###### 1.3.4 ThreadLocal.replaceStaleEntry 元素替换
>* 参考博客：https://www.jianshu.com/p/dde92ec37bd1
```aidl
将staleSlot位置替换为key-value对
 private void replaceStaleEntry(ThreadLocal<?> key, Object value,  int staleSlot) {
             Entry[] tab = table;
             int len = tab.length;
             Entry e;
 
             // Back up to check for prior stale entry in current run.
             // We clean out whole runs at a time to avoid continual
             // incremental rehashing due to garbage collector freeing
             // up refs in bunches (i.e., whenever the collector runs).
             int slotToExpunge = staleSlot;
             //1.向前遍历
             for (int i = prevIndex(staleSlot, len); (e = tab[i]) != null; i = prevIndex(i, len))
                 if (e.get() == null)
                     //1.1如果向前找到了key为null的位置，则标记此位置
                     slotToExpunge = i;
 
             // Find either the key or trailing null slot of run, whichever
             // occurs first
             //2.向后遍历
             for (int i = nextIndex(staleSlot, len); (e = tab[i]) != null; i = nextIndex(i, len)) {
                 ThreadLocal<?> k = e.get();
 
                 // If we find key, then we need to swap it
                 // with the stale entry to maintain hash table order.
                 // The newly stale slot, or any other stale slot
                 // encountered above it, can then be sent to expungeStaleEntry
                 // to remove or rehash all of the other entries in run.
                 if (k == key) {
                     e.value = value;
 
                     tab[i] = tab[staleSlot];
                     tab[staleSlot] = e;
 
                     // Start expunge at preceding stale entry if it exists
                     if (slotToExpunge == staleSlot)
                         slotToExpunge = i;
                     cleanSomeSlots(expungeStaleEntry(slotToExpunge), len);
                     return;
                 }
 
                 // If we didn't find stale entry on backward scan, the
                 // first stale entry seen while scanning for key is the
                 // first still present in the run.
                 if (k == null && slotToExpunge == staleSlot)
                     slotToExpunge = i;
             }
 
             // If key not found, put new entry in stale slot
             tab[staleSlot].value = null;
             tab[staleSlot] = new Entry(key, value);
 
             // If there are any other stale entries in run, expunge them
             if (slotToExpunge != staleSlot)
                 cleanSomeSlots(expungeStaleEntry(slotToExpunge), len);
         }

```
>1. 看了代码我们明白了，原来生成的代理类是Proxy的一个子类，并且实现类目标接口，因此他也重写了目标接口的方法，因此代理对象调用目标方法也就顺理成章了。
>2. 除了接口的方法之外，还默认包含三个方法，hashCode，toString，equals三个方法。
>3. 代理类调用目标接口的方法时，代码中执行saySomeThing方法，实际上调用的是handler对象的invoke方法，这也就解释了为什么我们在文章最开始的问题1。
>4. 为什么代理对象调用saySomeThing方法时，会调用handler的invoke方法呢，代理对象和handler的对象关系是什么？很简单，代理对象持有handler对象，回头去看
    Proxy.newProxyInstance方法的第三个参数不就是handler对象么？
>5. Java是单继承，代理类已经继承了Proxy，因此不能再继承其他的类了，因此jdk代理只能代理接口，不能代理类。
#### 1.4 温故知新
```aidl
前提：
    1.目标接口(包含目标方法)
    2.目标类(实现目标接口，)
    3.handler类(实现InvocationHandler接口，实现自己的代理逻辑)
    4.代理类(Proxy.newProxyInstance获取，持有handler对象)
粗看:
    代理类调用目标方法 --> (代理内内部实现了目标接口，并且持有handler对象) --> 代理类内部调用handler对象的invoke方法
    (表面看起来就是调用了我们在handler里面写的逻辑) 
细看:
    代理类的生成过程，这是难点，里面的缓存机制等等。        

```

 
 
 
#### 1.4 常见问题 
 
















