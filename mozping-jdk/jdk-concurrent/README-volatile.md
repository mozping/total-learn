# volatile (volatilep包)
## 一.作用和使用场景
>*  可以保证线程可见性且提供了一定的有序性，但是无法保证原子性,采用“内存屏障”来实现的。即保证可见性和原子性，禁止指令重排；

## 二.为什么volatile不是线程安全
假定线程1需要对变量cal做自增操作，那么从内存模型来看需要对变量read->load->use->assgin->store->write
这里read是从主存读取，load是将读取的值加载进线程的私有内存，然后再use使用，assign再分配值，store是保存
修改后的值，write是从私有内存写回主存

## volatile可见性详解
###### 实现原理
>* 修改volatile变量时会强制将修改后的值刷新的主内存中
>* 修改volatile变量后会导致其他线程工作内存中对应的变量值失效。再读取时需要重新从主内存中读取。
这两点保证了volatile变量在多线程间的可见性，第一点将assgin->store->write做成了一个原子操作
## volatile有序性详解


## 四.使用volatile的条件归纳
>* 对变量的写操作不依赖于当前值。
>* 该变量没有包含在具有其他变量的不变式中。




## 附:参考资料:
#### 关于volatile不具备原子性的解释：
###### [https://www.jianshu.com/p/f74044782927][1]
###### https://blog.csdn.net/shenmegui_32/article/details/70153821][2]
###### [https://www.cnblogs.com/rainwang/p/4398488.html][3]

