# AQS  (aqsp包)
## 一.作用和使用场景
>* AbstractQueuedSynchronizer类 
>* 管理程序的同步状态
#### 同步状态
```
state > 0 时，表示已经获取了锁。
state = 0 时，表示释放了锁。
```
#### 同步队列
```$xslt
当前线程获取同步状态失败（锁），AQS则会将线程以及等待状态等信息构造成一个节点（Node）加入同步队列，同时阻塞当前线程;
同步状态释放时，则会把节点中的线程唤醒，使其再次尝试获取同步状态
```
#### 主要内置方法
```$xslt
getState() setState(),compareAndSetState,
```

###### 方法分类
```$xslt
独占式获取与释放同步状态
共享式获取与释放同步状态
查询同步队列中的等待线程情况
```

#### 核心内部类Node
```
static final class Node {
       
        // 共享
        static final Node SHARED = new Node();       
         // 独占
        static final Node EXCLUSIVE = null;

        /**
         * 因为超时或者中断，节点会被设置为取消状态，被取消的节点时不会参与到竞争中的，他会一直保持取消状态不会转变为其他状态
         */
        static final int CANCELLED =  1;
        /**
         * 后继节点的线程处于等待状态，而当前节点的线程如果释放了同步状态或者被取消，将会通知后继节点，使后继节点的线程得以运行
        */
        static final int SIGNAL    = -1;
        /**
         * 节点在等待队列中，节点线程等待在Condition上，当其他线程对Condition调用了signal()后，该节点将会从等待队列中转移到同步队列中，加入到同步状态的获取中
        */
        static final int CONDITION = -2;
        /**
         * 表示下一次共享式同步状态获取，将会无条件地传播下去
         */
        static final int PROPAGATE = -3;

        /** 等待状态 */
        volatile int waitStatus;
        /** 前驱节点，当节点添加到同步队列时被设置（尾部添加） */
        volatile Node prev;
        /** 后继节点 */
        volatile Node next;
        /** 等待队列中的后续节点。如果当前节点是共享的，那么字段将是一个 SHARED 常量，也就是说节点类型（独占和共享）和等待队列中的后续节点共用同一个字段 */
        Node nextWaiter;
        /** 获取同步状态的线程 */
        volatile Thread thread;

        final boolean isShared() {
            return nextWaiter == SHARED;
        }

        final Node predecessor() throws NullPointerException {
            Node p = prev;
            if (p == null)
                throw new NullPointerException();
            else
                return p;
        }

        Node() {    // Used to establish initial head or SHARED marker
        }

        Node(Thread thread, Node mode) {     // Used by addWaiter
            this.nextWaiter = mode;
            this.thread = thread;
        }

        Node(Thread thread, int waitStatus) { // Used by Condition
            this.waitStatus = waitStatus;
            this.thread = thread;
        }
    }

```
>* waitStatus :等待状态，用来控制线程的阻塞和唤醒，并且可以避免不必要的调用,目前有 4 种：CANCELLED SIGNAL CONDITION PROPAGATE,
实际上，有第 5 种，INITAL ，值为 0 ，初始状态。它不仅仅指的是 Node 自己的线程的等待状态，也可以是下一个节点的线程的等待状态。



#### 核心方法解析
###### AbstractQueuedSynchronizer#addWaiter方法
```$xslt
 private Node addWaiter(Node mode) {
        //1.新建节点
        Node node = new Node(Thread.currentThread(), mode);
        // Try the fast path of enq; backup to full enq on failure
        //记录下原来的尾节点
        Node pred = tail;
        if (pred != null) {
            //先将当前节点node的前驱指向当前tail
            node.prev = pred;
            //CAS替换
            if (compareAndSetTail(pred, node)) {//CAS尝试将tail设置为node
                //如果CAS尝试成功，就说明"设置当前节点node的前驱"与"CAS设置tail"之间没有别的线程设置tail成功
                //只需要将"之前的tail"的后继节点指向node即可
                pred.next = node;
                return node;
            }
        }
        enq(node);//否则，通过死循环来保证节点的正确添加，早enq方法通过死循环保证知道正确的添加了节点为止
        return node;
    }
    往双向队列中添加一个节点，需要考虑多线程的情况，Node是一个数据结构，代表当前线程和他的排队信息
    优质博客：https://blog.csdn.net/sunxianghuang/article/details/52287968
```


## 二.AQS同步状态的获取与释放
#### 独占式
###### 1.1 独占式同步状态获取
```$xslt
     */
    public final void acquire(int arg) {
        if (!tryAcquire(arg) &&  //1
            acquireQueued(addWaiter(Node.EXCLUSIVE), arg))  //2
            selfInterrupt();  //3
    }
```
    tryAcquire尝试获取同步状态，获取成功则返回true，直接方法结束，获取失败，则进入后面的逻辑，首先addWaiter加入到同步队列，Node.EXCLUSIVE表示
    为独占模式，然后acquireQueued自旋直到获得同步状态成功
```$xslt
final boolean acquireQueued(final Node node, int arg) {
        boolean failed = true;
        try {
            boolean interrupted = false;
            for (;;) {
                final Node p = node.predecessor();
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return interrupted;
                }
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }
```
acquireQueued方法内部进行自旋，保证一定要获取同步状态成功，当条件满足时，节点会观察到然后退出自旋的过程
acquire方法获取独占锁的流程如下：
![Image text](../pic/concurrent/aqs.jpg)
```$xslt
acquireQueued方法流程解读
final boolean acquireQueued(final Node node, int arg) {
        // 记录是否获取同步状态成功，true表示失败，false表示成功
        boolean failed = true;
        try {
            // 记录过程中，是否发生线程中断
            boolean interrupted = false;
            //死循环自旋
            for (;;) {
                // 当前线程的前驱节点
                final Node p = node.predecessor();
                // 当前线程的前驱节点是头结点，且同步状态成功(即当前节点就是第一个节点)
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return interrupted;
                }
                // 获取失败，线程等待--具体后面介绍
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            // 获取同步状态发生异常，取消获取。
            if (failed)
                cancelAcquire(node);
        }
    }
    这里的shouldParkAfterFailedAcquire方法里面会继续尝试获取同步状态，里面会处理三种情况：
    private static boolean shouldParkAfterFailedAcquire(Node pred, Node node) {
            int ws = pred.waitStatus;
            if (ws == Node.SIGNAL)
                return true;
            if (ws > 0) {
                do {
                    node.prev = pred = pred.prev;
                } while (pred.waitStatus > 0);
                pred.next = node;
            } else {
                compareAndSetWaitStatus(pred, ws, Node.SIGNAL);
            }
            return false;
        }
        ws是前一个节点的等待状态：
        如果等待状态为Node.SIGNAL：说明前一个线程是阻塞的需要等待，则返回true表示当前线程可以被安全的阻塞
        如果等待状态为NODE.CANCELLED：说明前一个已经被中断或者超时了，那么需要不断往前找，循环回溯直到找到正常的线程节点
        如果等待状态为 0 或者 Node.PROPAGATE:那么对状态进行设置，使得下一次循环可以满足第一种条件，
        如果没有返回true，那么会不断的重试，因为在acquireQueued方法中通过死循环控制的
```
#### cancelAcquire取消获取状态
```$xslt
private void cancelAcquire(Node node) {
        if (node == null)
            return;
        //将节点的等待线程置空
        node.thread = null;

        Node pred = node.prev;
        //获取最前面的pred.waitStatus > 0的节点，同shouldParkAfterFailedAcquire方法ws>0的情况
        while (pred.waitStatus > 0)
            node.prev = pred = pred.prev;

        Node predNext = pred.next;

        node.waitStatus = Node.CANCELLED;

        //下面的逻辑理解为将前面的pred节点的后继节点
        if (node == tail && compareAndSetTail(node, pred)) {
            compareAndSetNext(pred, predNext, null);
        } else {
            int ws;
            if (pred != head &&
                ((ws = pred.waitStatus) == Node.SIGNAL ||
                 (ws <= 0 && compareAndSetWaitStatus(pred, ws, Node.SIGNAL))) &&
                pred.thread != null) {
                Node next = node.next;
                if (next != null && next.waitStatus <= 0)
                    compareAndSetNext(pred, predNext, next);
            } else {
                unparkSuccessor(node);
            }
            node.next = node; // help GC
        }
    }
```
###### 
###### 1.2 独占式获取响应中断
```$xslt
public final void acquireInterruptibly(int arg) throws InterruptedException {
        if (Thread.interrupted())
            throw new InterruptedException();
        if (!tryAcquire(arg))
            doAcquireInterruptibly(arg);
    }
```
acquireInterruptibly方法对中断响应，也就是说如果当前线程被中断了，会立刻响应中断，并抛出 InterruptedException 异常，
如果线程没有被中断，就会尝试调用tryAcquire来获取同步状态，tryAcquire默认抛出异常，需要子类来实现
```
private void doAcquireInterruptibly(int arg)
        throws InterruptedException {
        final Node node = addWaiter(Node.EXCLUSIVE);
        boolean failed = true;
        try {
            for (;;) {
                final Node p = node.predecessor();
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return;
                }
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    throw new InterruptedException();
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }
```
doAcquireInterruptibly方法会自旋，直到获取同步状态成功

###### 1.3 独占式超时获取
>* AQS 除了提供上面两个方法外，还提供了一个增强版的方法 #tryAcquireNanos(int arg, long nanos) 。该方法为 #acquireInterruptibly(int arg) 方法的进一步增强，它除了响应中断外，还有超时控制。即如果当前线程没有在指定时间内获取同步状态，则会返回 false ，否则返回 true 。
###### 1.4 独占式同步状态释放
>* 当线程获取同步状态后，执行完相应逻辑后，就需要释放同步状态。AQS 提供了#release(int arg)方法，释放同步状态。代码如下：
```aidl
public final boolean release(int arg) {
        if (tryRelease(arg)) {
            Node h = head;
            if (h != null && h.waitStatus != 0)
                unparkSuccessor(h);
            return true;
        }
        return false;
    }
```
tryRelease方法需要子类自己实现
###### 1.5 总结
```aidl
在 AQS 中维护着一个 FIFO 的同步队列。
当线程获取同步状态失败后，则会加入到这个 CLH 同步队列的对尾，并一直保持着自旋
在 CLH 同步队列中的线程在自旋时，会判断其前驱节点是否为首节点，如果为首节点则不断尝试获取同步状态，获取成功则退出CLH同步队列。
当线程执行完逻辑后，会释放同步状态，释放后会唤醒其后继节点
```

#### 2共享式
###### 共享式与独占式的最主要区别在于，同一时刻：独占式只能有一个线程获取同步状态。共享式可以有多个线程获取同步状态。这里的读写所就是对应的共享和独占。
###### 2.1共享式同步状态获取
```aidl
public final void acquireShared(int arg) {
        if (tryAcquireShared(arg) < 0)
            doAcquireShared(arg);
    }
    
    private void doAcquireShared(int arg) {
            final Node node = addWaiter(Node.SHARED);
            boolean failed = true;
            try {
                boolean interrupted = false;
                for (;;) {
                    final Node p = node.predecessor();
                    if (p == head) {
                        int r = tryAcquireShared(arg);
                        if (r >= 0) {
                        //设置头节点并唤醒后面的节点
                            setHeadAndPropagate(node, r);
                            p.next = null; // help GC
                            if (interrupted)
                                selfInterrupt();
                            failed = false;
                            return;
                        }
                    }
                    if (shouldParkAfterFailedAcquire(p, node) &&
                        parkAndCheckInterrupt())
                        interrupted = true;
                }
            } finally {
                if (failed)
                    cancelAcquire(node);
            }
        }
        
        private void setHeadAndPropagate(Node node, int propagate) {
                Node h = head; // Record old head for check below
                setHead(node);
               
                if (propagate > 0 || h == null || h.waitStatus < 0 ||
                    (h = head) == null || h.waitStatus < 0) {
                    Node s = node.next;
                    //如果是共享式节点唤醒
                    if (s == null || s.isShared())
                        doReleaseShared();
                }
            }
```
###### 2.2共享式获取响应中断
###### 2.3共享式超时获取
###### 2.4共享式同步状态释放

## 三.阻塞和唤醒线程
#### 3.1
>* 在线程获取同步状态时，如果获取失败，则加入CLH 同步队列，通过自旋的方式不断获取同步状态，在自旋的过程中，需要判断当前线程是否需要阻塞，其主
要方法在acquireQueued
```aidl
final boolean acquireQueued(final Node node, int arg) {
        boolean failed = true;
        try {
            boolean interrupted = false;
            for (;;) {
                final Node p = node.predecessor();
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return interrupted;
                }
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }
    
    //唤醒后继节点方法
    private void unparkSuccessor(Node node) {
           
            int ws = node.waitStatus;
            if (ws < 0)
                compareAndSetWaitStatus(node, ws, 0);
    
            Node s = node.next;
            if (s == null || s.waitStatus > 0) {
                s = null;
                for (Node t = tail; t != null && t != node; t = t.prev)
                    if (t.waitStatus <= 0)
                        s = t;
            }
            if (s != null)
                LockSupport.unpark(s.thread);
        }
```


#### 3.2

#### 3.3

#### 3.4
















