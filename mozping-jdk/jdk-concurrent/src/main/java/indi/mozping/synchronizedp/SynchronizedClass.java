package indi.mozping.synchronizedp;

import indi.mozping.tools.SleepTools;

/**
 * @author by mozping
 * @Classname SynchronizedClass
 * @Description ；使用类锁
 * @Date 2019/05/28 16:26
 */
public class SynchronizedClass {

    void test1() {
        synchronized (SynchronizedClass.class) {
            System.out.println("SynchronizedClass类锁开始....");
            System.out.println("-----SynchronizedClass-----");
            SleepTools.second(5);
            System.out.println("SynchronizedClass类锁结束....");
        }
    }
}
