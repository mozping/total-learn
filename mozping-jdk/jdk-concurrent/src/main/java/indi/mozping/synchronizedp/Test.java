package indi.mozping.synchronizedp;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/5/30 20:01
 */
public class Test {

    public synchronized void test1() {
        System.out.println("test1----");
    }

    public static synchronized void test2() {
        System.out.println("test2----");
    }

    public void test3() {
        synchronized (Test.class) {
            System.out.println("test3----");
        }
    }
}