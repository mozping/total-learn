package indi.mozping.synchronizedp;

import indi.mozping.tools.SleepTools;

/**
 * @author by mozping
 * @Classname SynchronizedCodeBlock
 * @Description 同步代码块，可以使用类锁，也可以使用对象锁，相当于自定义锁
 * @Date 2019/05/28 16:26
 */
public class SynchronizedCodeBlock {

    void test1() {
        synchronized (this) {
            System.out.println("SynchronizedCodeBlock synchronized 同步代码块.... ");
            System.out.println("-----SynchronizedCodeBlock-----");
            SleepTools.second(5);
            System.out.println("SynchronizedCodeBlock synchronized 同步代码块.... ");
        }

    }

    void test2() {
        synchronized (new Object()) {
            System.out.println("SynchronizedCodeBlock synchronized 同步代码块.... ");
            System.out.println("-----SynchronizedCodeBlock-----");
            SleepTools.second(5);
            System.out.println("SynchronizedCodeBlock synchronized 同步代码块.... ");
        }

    }

    void test3() {
        synchronized (SynchronizedCodeBlock.class) {
            System.out.println("SynchronizedCodeBlock synchronized 同步代码块.... ");
            System.out.println("-----SynchronizedCodeBlock-----");
            SleepTools.second(5);
            System.out.println("SynchronizedCodeBlock synchronized 同步代码块.... ");
        }

    }
}
