package indi.mozping.synchronizedp;

import indi.mozping.tools.SleepTools;

/**
 * @author by mozping
 * @Classname SynchronizedMethod
 * @Description ��Ա������ʹ��synchronized���Σ�ʹ�õ��Ƕ�����
 * @Date 2019/05/28 16:26
 */
public class SynchronizedMethod {

    synchronized void test1() {
        System.out.println("SynchronizedMethod��Ա��������ʼ....test1������ʼ");
        System.out.println("-----SynchronizedMethod-----");
        SleepTools.second(5);
        System.out.println("SynchronizedMethod��Ա����������....test1��������");
    }

    synchronized void test3() {
        System.out.println("SynchronizedMethod��Ա��������ʼ....test3������ʼ");
        System.out.println("-----SynchronizedMethod-----");
        SleepTools.second(5);
        System.out.println("SynchronizedMethod��Ա����������....test3��������");
    }


    static synchronized void test2() {
        System.out.println("SynchronizedMethod��̬��������ʼ....test2������ʼ");
        System.out.println("-----SynchronizedMethod-----");
        SleepTools.second(5);
        System.out.println("SynchronizedMethod��̬����������....test2��������");
    }
}
