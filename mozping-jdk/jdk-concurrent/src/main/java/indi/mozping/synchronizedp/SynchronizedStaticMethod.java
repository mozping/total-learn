package indi.mozping.synchronizedp;

import indi.mozping.tools.SleepTools;

/**
 * @author by mozping
 * @Classname SynchronizedStaticMethod
 * @Description 静态方法，使用synchronized修饰，使用的是类锁
 * @Date 2019/05/28 16:26
 */
public class SynchronizedStaticMethod {

    static synchronized void test1() {
        System.out.println("SynchronizedStaticMethod 静态方法锁开始....");
        System.out.println("-----SynchronizedStaticMethod-----");
        SleepTools.second(5);
        System.out.println("SynchronizedStaticMethod 静态方法锁结束....");
    }
}
