package indi.mozping.synchronizedp;

/**
 * @author by mozping
 * @Classname SynchronizedTest
 * @Description TODO
 * @Date 2019/5/28 19:06
 */
public class SynchronizedTest {

    public static void main(String[] args) {

        //test1();  //1.��������
        //test2();  //2.���Ծ�̬������
        test3();  //3.���Գ�Ա���������Աȳ�Ա�������;�̬������
        //test4();  //4.���Գ�Ա�����������ԵĶ���ʹ�õ����ǲ�һ����
        //test5();    //5.����ͬ������飬ʹ��this��ǰ������������ͬ����Ա��������
        //6.����ͬ������飬ÿ��ʹ��new Object()�����������൱��û������
        // �����߳�ͬʱ���У�������⣬ÿ�ε�������һ������ͬ��û����
        //test6();
        //7.����ͬ������飬ÿ��ʹ��������������൱����һ���������߳���Ҫͬ��
        //test7();
    }

    static void test7() {
        SynchronizedCodeBlock synchronizedCodeBlock = new SynchronizedCodeBlock();
        for (int i = 0; i < 3; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronizedCodeBlock.test3();
                }
            }).start();
        }

    }

    static void test6() {
        SynchronizedCodeBlock synchronizedCodeBlock = new SynchronizedCodeBlock();
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronizedCodeBlock.test2();
                }
            }).start();
        }
    }


    static void test5() {
        SynchronizedCodeBlock synchronizedCodeBlock = new SynchronizedCodeBlock();
        for (int i = 0; i < 2; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronizedCodeBlock.test1();
                }
            }).start();
        }

    }

    static void test4() {
        SynchronizedMethod synchronizedMethod = new SynchronizedMethod();
        SynchronizedMethod synchronizedMethod1 = new SynchronizedMethod();
        //1.�������̵߳��õ��ǳ�Ա�����������ǲ���Ҫ�Ŷӵģ���Ϊ����һ��������һ����
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronizedMethod.test1();
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronizedMethod1.test1();
            }
        }).start();


    }

    static void test3() {
        SynchronizedMethod synchronizedMethod = new SynchronizedMethod();
        //1.�������̵߳��õ��ǳ�Ա��������������Ҫ�Ŷӵ�,��������2���߳������һ�����󣬷ֱ��
        // ��test1��test3Ҳ����Ҫ�Ŷӵģ���Ա������ͬһ��������Ҫ��һ�����󣬷�����һ��Ҳ�����
        for (int i = 0; i < 2; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronizedMethod.test1();
                }
            }).start();
        }
        //2.�������̵߳��õ��Ǿ�̬����������Ҳ����Ҫ�Ŷӵ�
        for (int i = 0; i < 2; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    SynchronizedMethod.test2();
                }
            }).start();
        }

        //3.���ǳ�Ա�����;�̬����֮���ǲ���Ҫ�Ŷӵģ��������ţ���Ϊ
        // ǰ��ʹ�õ��Ƕ�����������ʹ�õ�������
    }


    static void test2() {
        //1.���Ծ�̬�������������̵߳���ͬһ����̬������Ҫͬ���Ŷӣ���Ϊ��һ����
        // ���󣬲��������������Class����
        for (int i = 0; i < 3; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    SynchronizedStaticMethod.test1();
                }
            }).start();
        }
    }

    static void test1() {
        //1.��������
        SynchronizedClass s1 = new SynchronizedClass();
        SynchronizedClass s2 = new SynchronizedClass();
        //1.2 ������ͬ�������test1������Ҫ�Ŷӣ���Ϊʹ�õ���ͬһ������
        s1.test1();
        s2.test1();
    }
}