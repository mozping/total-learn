package indi.mozping.concurrentutil.exchangep;

import indi.mozping.tools.SleepTools;

import java.util.ArrayList;
import java.util.concurrent.Exchanger;

/**
 * @author by mozping
 * @Classname ExchangeTest
 * @Description Exchange的使用
 * @Date 2019/1/2 18:50
 */
public class ExchangeTest {

    private static final Exchanger<ArrayList<Integer>> exchange = new Exchanger<>();

    public static void main(final String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<Integer> arr1 = new ArrayList<>();
                ArrayList<Integer> arr11 = new ArrayList<>();
                arr1.add(1);
                arr1.add(2);
                arr1.add(3);
                try {
                    arr11 = exchange.exchange(arr1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SleepTools.randomSecond(5);
                System.out.println("Thread1原集合:" + arr1.toString());
                System.out.println("Thread1新集合:" + arr11.toString());
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<Integer> arr2 = new ArrayList<>();
                ArrayList<Integer> arr22 = new ArrayList<>();
                arr2.add(11);
                arr2.add(22);
                arr2.add(33);
                try {
                    arr22 = exchange.exchange(arr2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SleepTools.randomSecond(5);
                System.out.println("Thread2原集合:" + arr2.toString());
                System.out.println("Thread2新集合:" + arr22.toString());

            }
        }).start();

    }
}
