package indi.mozping.concurrentutil.completionServicep;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * 类说明：任务类
 */
public class WorkTask implements Callable<Integer> {
    private String name;

    public WorkTask(String name) {
        this.name = name;
    }

    @Override
    public Integer call() {
        int time = new Random().nextInt(1000);
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return time;
    }
}
