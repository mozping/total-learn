package indi.mozping.concurrentutil.cyclicbarrierp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CyclicBarrier;

/**
 * @author by mozping
 * @Classname Solver
 * @Description 仿照CyclicBarrier源码中的demo写的
 * @Date 2019/5/24 14:46
 */
public class Solver {

    //定义flag表示任务是否完成
    static private volatile boolean flag = false;

    /**
     * 工作线程，可以循环处理数组，第N次处理数组会将数组的前N个元素求和，并将结果放在数组的最后的位置
     */
    static class Worker implements Runnable {
        int row;
        float[][] data;
        CyclicBarrier cyclicBarrier;
        //记录循环的次数
        int index = 0;

        public Worker(int row, float[][] data, CyclicBarrier cyclicBarrier) {
            this.row = row;
            this.data = data;
            this.cyclicBarrier = cyclicBarrier;
        }

        @Override
        public void run() {
            while (!done()) {
                processRow(row);
                index++;//处理完之后，下标加1
                try {
                    cyclicBarrier.await();
                } catch (Exception e) {
                    return;
                }
            }
        }

        private void processRow(int row) {
            //将小标处的元素累加到结果
            data[row][10000] += data[row][index];
            System.out.println("线程：" + Thread.currentThread().getName() + "处理第" + row +
                    "行第" + index + "列...");
        }

        private boolean done() {
            return flag;
        }
    }

    /**
     * 合并线程；如果累加的结果之和大于阈值，则任务结束，反之，继续向后累加
     */
    static class MergeRows implements Runnable {

        final float[][] data;
        //阈值
        final int threshold;

        public MergeRows(float[][] data, int threshold) {
            this.data = data;
            this.threshold = threshold;
        }

        @Override
        public void run() {
            float result = 0;
            System.out.println("MergeRows 开始Merge..." + Thread.currentThread().getName());
            //累加结果
            for (int i = 0; i < 10; i++) {
                result += data[i][10000];
            }
            try {
                Thread.sleep(3000);
                System.out.println("MergeRows 完毕耗费3000ms ," + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //确定是否中断任务
            if (result > threshold) {
                System.out.println("MergeRows 成功 ，结果是: " + result);
                flag = true;
            } else {
                System.out.println("MergeRows 失败 ，结果是: " + result);
                flag = false;
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        float[][] data = new float[10][10001];
        Random random = new Random();
        //1.初始化数组
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10000; j++) {
                data[i][j] = random.nextInt(100);
            }
        }
        System.out.println("初始化data完毕...");
        //2.初始化栅栏
        CyclicBarrier cyclicBarrier = new CyclicBarrier(10, new MergeRows(data, 2000));

        List<Thread> threads = new ArrayList<>(10);
        //3.启动线程
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new Worker(i, data, cyclicBarrier));
            threads.add(thread);
            thread.start();
        }

        //4.主线程等待，在工作线程之和再结束
        for (Thread thread : threads) {
            thread.join();
        }
    }
}
