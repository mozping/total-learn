package indi.mozping.concurrentutil.forkjiondemo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author by mozping
 * @Classname ForkJoinTest
 * @Description TODO
 * @Date 2019/2/28 14:50
 */
public class ForkJoinTest {

    //public static final String PATH = "D:\\mavenrepository\\repository";
    public static final String PATH = "X:\\";
    public static final String SUFFIX = ".jar";
    public static File ff = new File("./result.txt");
    static int total = 0;

    public static void main(String[] args) {
        long begin = System.currentTimeMillis();
        File file = new File(PATH);
        int fileCount = getFileCount(file);
        System.out.println("FileCount: " + fileCount);
        System.out.println("耗时：" + (System.currentTimeMillis() - begin));
    }


    public static int getFileCount(File path) {
        File file = path;

//        System.out.println("Param:" + path);
//        System.out.println("Boolean:" + file.isDirectory());
//        System.out.println("FileName:" + file.getName());
        if (file.isDirectory() && file.canRead()) {
            File[] fs = null;
            try {
                fs = file.listFiles();
                for (File f : fs) {
                    if (f.canRead()) {
                        getFileCount(f);
                    }
                }
            } catch (Exception e) {

            }

        } else {
            total++;
//            if (file.getName().endsWith(SUFFIX)) {
//                writeToFile(file.getName(), ff);
//                total++;
//            }
        }
        return total;
    }


    public static void writeToFile(String str, File file) {
        FileWriter fw = null;
        try {
            //如果文件存在，则追加内容；如果文件不存在，则创建文件
            File f = file;
            fw = new FileWriter(f, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter pw = new PrintWriter(fw);
        pw.println(str);
        pw.flush();
        try {
            fw.flush();
            pw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
