package indi.mozping;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * @author by mozping
 * @Classname OnlyMain
 * @Description 只包含main方法的java线程
 * @Date 2018/12/28 12:59
 */
public class OnlyMain {

    /**
     * 只包含main方法的java进程也包含多个线程，
     */
    public static void main(String[] args) {
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
        for (ThreadInfo ti : threadInfos) {
            System.out.println("[" + ti.getThreadId() + "]" + ti.getThreadName());
        }

    }


}
