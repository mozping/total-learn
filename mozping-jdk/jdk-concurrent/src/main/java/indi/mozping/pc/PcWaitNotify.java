package indi.mozping.pc;

import indi.mozping.tools.SleepTools;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * @author by mozping
 * @Classname Pc ������������ʵ�֣�ʹ��wait-notifyͬ�����ƣ�
 * ȱ���ǵ��ж�������������ߵ�ʱ��ʹ��notify�����ھ�ȷ֪ͨ�����߻��������ߣ�Ҫ��notifyAll��
 * ���ﻹ��Ҫ����synchronized�ؼ�����ͬ������
 * ����д��Ҳ�ǱȽϼ򵥵ģ��߼������
 * @Description TODO
 * @Date 2019/3/5 20:33
 */
public class PcWaitNotify {

    static int MAX = 10;
    static Queue<Object> queue = new ArrayDeque<>(MAX);

    public static void main(String[] args) {
        Producer p = new Producer();
        Consumer c1 = new Consumer();
        Consumer c2 = new Consumer();
        new Thread(p).start();
        new Thread(p).start();
        new Thread(c1).start();
        new Thread(c2).start();
    }


    static class Producer implements Runnable {

        @Override
        public void run() {
            while (true) {
                synchronized (queue) {
                    if (queue.size() >= MAX) {
                        queue.notifyAll();
                    } else {
                        queue.add(new Object());
                        System.out.println("������������ 1 ����������:" + queue.size() + " ����Ʒ...");
                        SleepTools.randomMs(500);
                    }
                }
            }
        }
    }

    static class Consumer implements Runnable {

        @Override
        public void run() {
            while (true) {
                synchronized (queue) {
                    if (queue.size() <= 0) {
                        queue.notifyAll();
                    } else {
                        queue.remove();
                        System.out.println("������������ 1 ����������:" + queue.size() + " ����Ʒ...");
                        SleepTools.randomMs(500);
                    }
                }
            }
        }
    }
}
