package indi.mozping.pc;

import indi.mozping.tools.SleepTools;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * @author by mozping
 * @Classname Pc ������������ʵ�֣�ʹ��Synchronizedͬ��
 * �������ڼ򵥣����Ƿ�������ͬ���ģ���˶����������̰߳�ȫ��û��Ҫ�����е����Ѻ��������ᴮ��ִ��
 * @Description TODO
 * @Date 2019/3/5 20:33
 */
public class PcSync {

    static int MAX = 10;
    static Queue<Object> queue = new ArrayDeque<>(MAX);

    public static void main(String[] args) {

        Producer p = new Producer();
        Consumer c1 = new Consumer();
        Consumer c2 = new Consumer();
        new Thread(p).start();
        new Thread(p).start();
        new Thread(c1).start();
        new Thread(c2).start();
    }


    static class Producer implements Runnable {

        @Override
        public void run() {
            while (true) {
                synchronized (PcSync.class) {
                    if (queue.size() < MAX) {
                        queue.add(new Object());
                        System.out.println("����һ��������������Ʒ" + queue.size() + "��");
                        SleepTools.randomMs(400);
                    } else {
                        System.out.println("���ˣ�������������...");
                    }
                }
            }
        }
    }

    static class Consumer implements Runnable {

        @Override
        public void run() {
            while (true) {
                synchronized (PcSync.class) {
                    if (queue.size() > 0) {
                        queue.remove();
                        System.out.println("����һ�������Ѻ�����Ʒ" + queue.size() + "��");
                        SleepTools.randomMs(400);
                    } else {
                        System.out.println("���ˣ�������������...");
                    }
                }
            }
        }
    }
}
