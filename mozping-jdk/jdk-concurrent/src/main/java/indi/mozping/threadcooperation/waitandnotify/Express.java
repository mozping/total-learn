package indi.mozping.threadcooperation.waitandnotify;

/**
 * @author by mozping
 * @Classname Express
 * @Description 快递实体类
 * @Date 2018/12/28 19:02
 */
public class Express {

    private static String site = "ChangSha";
    private int km = 100;

    public synchronized void waitSite() {
        while ("ChangSha".equalsIgnoreCase(site)) {
            try {
                wait();
                System.out.println("Thread " + Thread.currentThread().getName() + " is be notified...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("The site is " + site + ",will change the db ...");
    }

    public synchronized void waitKm() {
        while (this.km == 100) {
            try {
                wait();
                System.out.println("Thread " + Thread.currentThread().getName() + " is be notified......");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("The site is " + km + ",will call the user  ...");
    }

    public synchronized void notityKm() {
        km = 101;
        notifyAll();
    }

    public synchronized void notitySite() {
        site = "BeiJing";
        notifyAll();
    }

}
