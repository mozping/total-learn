package indi.mozping.threadcooperation.waitandnotify;

/**
 * @author by mozping
 * @Classname WaitAndNotify
 * @Description 线程间的协作--等待和通知机制
 * wait/notify/notifyAll ： 对象的方法
 * <p>
 * 等待通知标准范式：
 * 等待方：
 * 1.获取对象的锁
 * 2.循环判断是否满足，不满足调用wait
 * 3.处理业务
 * 通知方：
 * 1.获取对象的锁
 * 2.
 * 3.
 * @Date 2018/12/28 18:52
 */
public class WaitAndNotify {

    public static void main(String[] args) {
        Express express = new Express();
        Thread t11 = new CheckSite(express);
        Thread t12 = new CheckSite(express);
        Thread t13 = new CheckSite(express);
        Thread t21 = new ModifySite(express);
        Thread t22 = new ModifySite(express);
        Thread t23 = new ModifySite(express);

        //三个线程起来之后，检测地点是否发生改变
        t11.start();
        t12.start();
        t13.start();

        //三个线程起来之后会修改地点，对应会通知到前面的进程，这里是要notify可能会有问题，那样可能唤醒的是同类，随机唤醒一个，底层来说也不是完全随机，
        // 会有一个对应的等待队列。尽量是要notifyAll方法
        t21.start();
        t22.start();
        t23.start();
    }

    private static class CheckSite extends Thread {
        private Express express;

        CheckSite(Express express) {
            this.express = express;
        }

        @Override
        public void run() {
            express.waitSite();
        }
    }

    private static class ModifySite extends Thread {
        private Express express;

        ModifySite(Express express) {
            this.express = express;
        }

        @Override
        public void run() {
            express.notitySite();
        }
    }
}
