package indi.mozping.waitandnotify;

/**
 * @author by mozping
 * @Classname ShopTest
 * @Description TODO
 * @Date 2019/4/25 20:30
 */
public class ShopTest {
    static class MyThread extends Thread {

        Shop shop;
        long mills;

        public MyThread(Shop shop, long mills) {
            this.shop = shop;
            this.mills = mills;
        }

        @Override
        public void run() {
            shop.waitToy(mills);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //1.初始化玩具店，最初没有玩具
        Shop shop = new Shop("nothing");

        //2.等待玩具的线程
        new MyThread(shop, 2000).start();
        new MyThread(shop, 500).start();
        new MyThread(shop, 100).start();

        Thread.sleep(1000);

        //3.玩具到货
        shop.sendToy("babiwawa");

    }
}
