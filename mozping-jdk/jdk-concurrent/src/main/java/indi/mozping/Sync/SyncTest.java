package indi.mozping.Sync;

/**
 * @author by mozping
 * @Classname SyncTest
 * @Description TODO
 * @Date 2018/12/28 16:26
 */
public class SyncTest {

    private static final String LOCK = "lock";

    public static void main(String[] args) {

    }


    private class MyThread extends Thread {
        @Override
        public void run() {
            synchronized (LOCK) {
                System.out.println("");
            }
        }
    }
}
