package indi.mozping.createthread;

import java.util.concurrent.*;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2018/12/28 13:09
 */
public class Test {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {

        Thread t1 = new ThreadImpl();
        t1.start();
        t1.interrupt();//中断线程，将标志位置为true
        //t1.isInterrupted();//判断当前线程是否处于中断状态 -- private native boolean isInterrupted(false);
        //Thread.interrupted();//判断当前线程是否处于中断状态,修改标志位为false  private native boolean isInterrupted(true);

        Runnable r1 = new RunnableImpl();
        Thread t2 = new Thread(r1);
        t2.start();


        Callable c1 = new CallableImpl();
        FutureTask<String> futureTask = new FutureTask<>(c1);
        Thread t3 = new Thread(futureTask);
        t3.start();
        //get方法是阻塞的，可以添加阻塞的超时时间
        System.out.println("获取futureTask的返回值：" + futureTask.get(51, TimeUnit.MILLISECONDS));

        /*ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
        for (ThreadInfo ti : threadInfos) {
            System.out.println("[" + ti.getThreadId() + "]" + ti.getThreadName());
        }*/
    }
}
