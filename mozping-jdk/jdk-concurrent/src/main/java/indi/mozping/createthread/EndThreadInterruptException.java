package indi.mozping.createthread;

/**
 * @author by mozping
 * @Classname EndThread
 * @Description TODO
 * @Date 2018/12/28 14:09
 */
public class EndThreadInterruptException {

    public static void main(String[] args) throws InterruptedException {

        //线程1抛出InterruptedException异常之后，将中断标志位置为false，所以线程1会继续执行
        Thread t1 = new Thread(new MyRunnableInterruptedException(), "Thread-1");
        t1.start();

        //线程2抛出InterruptedException异常之后，捕获异常并且显示调用自身的interupt方法，所以线程2会中断
        Thread t2 = new Thread(new MyRunnableInterrupteSelf(), "Thread-2");
        t2.start();

        //线程3抛出InterruptedException异常之后，将中断标志位置为false，所以线程3会继续执行
        //线程3和线程一是样的，只是是继承了Thread类，因此在判断自身的中断状态时可以直接调用isInterrupted方法
        Thread t3 = new Thread(new MyThreadInterruptedException(), "Thread-3");
        t3.start();


        Thread.sleep(4000);
        t1.interrupt();
        t2.interrupt();
        t3.interrupt();


    }


    private static class MyRunnableInterruptedException implements Runnable {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    //sleep抛出InterruptedException后不主动调用interupt方法，线程并不会停止，因为抛出该异常后，线程的中断标志位会置为false
                    e.printStackTrace();
                    System.out.println(Thread.currentThread().getName() + "sleep抛出InterruptedException异常后，中断标志位：" + Thread.currentThread().isInterrupted());
                }
                System.out.println("线程" + Thread.currentThread().getName() + "执行...");
            }
        }
    }

    private static class MyRunnableInterrupteSelf implements Runnable {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    //sleep抛出InterruptedException后不主动调用interupt方法，线程并不会停止，因为抛出该异常后，线程的中断标志位会置为false
                    e.printStackTrace();
                    System.out.println(Thread.currentThread().getName() + "sleep抛出InterruptedException异常后，中断标志位：" + Thread.currentThread().isInterrupted());
                    //自己再次中断自己
                    Thread.currentThread().interrupt();
                    System.out.println(Thread.currentThread().getName() + "调用interrupt后，中断标志位：" + Thread.currentThread().isInterrupted());
                }
                System.out.println("线程" + Thread.currentThread().getName() + "执行...");
            }
        }
    }

    private static class MyThreadInterruptedException extends Thread {
        @Override
        public void run() {
            while (!this.isInterrupted()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    //sleep抛出InterruptedException后不主动调用interupt方法，线程并不会停止，因为抛出该异常后，线程的中断标志位会置为false
                    e.printStackTrace();
                    System.out.println(Thread.currentThread().getName() + "sleep抛出InterruptedException异常后，中断标志位：" + Thread.currentThread().isInterrupted());
                }
                System.out.println("线程" + Thread.currentThread().getName() + "执行...");
            }
        }
    }


}
