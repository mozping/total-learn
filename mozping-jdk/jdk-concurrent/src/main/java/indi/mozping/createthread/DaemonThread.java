package indi.mozping.createthread;

/**
 * @author by mozping
 * @Classname DaemonThread
 * @Description TODO
 * @Date 2018/12/28 15:19
 */
public class DaemonThread {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new MyThread();
        t1.setDaemon(true);
        t1.start();
        Thread.sleep(5 * 1000);
    }


    private static class MyThread extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                    Thread.sleep(1000);
                    System.out.println("线程" + Thread.currentThread().getName() + "执行...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                System.out.println("守护线程的finally代码块....");
            }
        }
    }
}


