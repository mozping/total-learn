package indi.mozping.createthread;

import java.util.concurrent.Callable;

/**
 * @author by mozping
 * @Classname CallableImpl
 * @Description TODO
 * @Date 2018/12/28 13:08
 */
public class CallableImpl implements Callable<String> {
    @Override
    public String call() throws Exception {
        Thread.sleep(50);
        System.out.println("实现Callable接口实现多线程...");
        return "callable implement";
    }
}
