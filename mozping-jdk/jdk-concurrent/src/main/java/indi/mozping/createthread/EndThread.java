package indi.mozping.createthread;

/**
 * @author by mozping
 * @Classname EndThread
 * @Description TODO
 * @Date 2018/12/28 14:09
 */
public class EndThread {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new MyThread();
        t1.start();

        Thread t2 = new Thread(new MyRunnable());
        t2.start();

        Thread.sleep(0, 100);
        t1.interrupt();
        t2.interrupt();

        t2.setDaemon(true);
    }


    private static class MyThread extends Thread {
        @Override
        public void run() {
            while (!isInterrupted()) {
                System.out.println("线程" + Thread.currentThread().getName() + "执行...");
            }
        }
    }

    private static class MyRunnable implements Runnable {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("线程" + Thread.currentThread().getName() + "执行...");
            }
        }
    }


}
