package indi.mozping.aqs.lockwithaqs;

import java.util.Date;
import java.util.concurrent.locks.Lock;

/**
 * @author by mozping
 * @Classname MyLockTest
 * @Description 我们定义了10个线程，所有线程共用一把锁，预期线程会同步依次执行打印。
 * @Date 2019/4/29 17:04
 */
public class MyLockTest {

    private static Lock lock = new MyLock();

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new MyThread(lock, "Thread-" + i).start();
        }
    }

    static class MyThread extends Thread {

        Lock lock;

        public MyThread(Lock lock, String name) {
            super(name);
            this.lock = lock;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                System.out.println("Thread " + Thread.currentThread().getName() + " do something thing..." + new Date());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
