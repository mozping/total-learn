package indi.mozping.alternatelyprint.locksupport;

import java.util.concurrent.locks.LockSupport;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/10/17 15:06
 */
public class Test {

    private static char[] charsArr1 = "12345".toCharArray();
    private static char[] charsArr2 = "ABCDE".toCharArray();
    static Thread t1 = null, t2 = null;

    public static void main(String[] args) {

        t1 = new Thread(() -> {
            for (int i = 0; i < charsArr1.length; ) {
                System.out.print(charsArr1[i++]);
                LockSupport.unpark(t2);
                LockSupport.park();
            }
        });

        t2 = new Thread(() -> {
            for (int i = 0; i < charsArr2.length; ) {
                LockSupport.park();
                System.out.print(charsArr2[i++]);
                LockSupport.unpark(t1);
            }
        });

        t1.start();
        t2.start();
    }
}