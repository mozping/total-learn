package indi.mozping.alternatelyprint.waitnotify;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/10/17 15:06
 */
public class Test {

    private static char[] charsArr1 = "12345".toCharArray();
    private static char[] charsArr2 = "ABCDE".toCharArray();
    private static Object lock = new Object();

    public static void main(String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    for (int i = 0; i < charsArr1.length; i++) {
                        System.out.print(charsArr1[i]);
                        try {
                            lock.notify();
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    //保证另一个线程退出
                    lock.notify();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    for (int i = 0; i < charsArr2.length; i++) {
                        System.out.print(charsArr2[i]);
                        try {
                            lock.notify();
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    //保证另一个线程退出
                    lock.notify();
                }
            }
        }).start();
    }
}