package indi.mozping.alternatelyprint.atom;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/10/17 15:06
 */
public class Test {

    private static AtomicInteger integer = new AtomicInteger(0);
    private static char[] charsArr1 = "12345".toCharArray();
    private static char[] charsArr2 = "ABCDE".toCharArray();

    public static void main(String[] args) {

        new Thread(() -> {
            for (int i = 0; i < charsArr1.length; ) {
                if (integer.get() % 2 == 0) {
                    System.out.print(charsArr1[i++]);
                    integer.getAndIncrement();
                }
            }
        }).start();

        new Thread(() -> {
            for (int i = 0; i < charsArr2.length; ) {
                if (integer.get() % 2 == 1) {
                    System.out.print(charsArr2[i++]);
                    integer.getAndIncrement();
                }
            }
        }).start();
    }
}