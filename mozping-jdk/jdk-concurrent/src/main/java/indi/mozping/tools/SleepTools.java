package indi.mozping.tools;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author Mark老师   享学课堂 https://enjoy.ke.qq.com
 * <p>
 * 更多课程咨询 安生老师 QQ：669100976  VIP课程咨询 依娜老师  QQ：2470523467
 * <p>
 * 类说明：线程休眠辅助工具类
 */
public class SleepTools {

    private static Random random = new Random();

    /**
     * 按秒休眠
     *
     * @param seconds 秒数
     */
    public static final void second(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
        }
    }

    /**
     * 按秒休眠，休眠随机事件
     *
     * @param seconds 时间上限
     */
    public static final void randomSecond(int seconds) {
        try {
            int s = (int) (Math.random() * seconds);
            TimeUnit.SECONDS.sleep(s);
        } catch (InterruptedException e) {
        }
    }

    /**
     * 按毫秒数休眠
     *
     * @param ms 毫秒数
     */
    public static final void ms(int ms) {
        try {
            TimeUnit.MILLISECONDS.sleep(ms);
        } catch (InterruptedException e) {
        }
    }

    /**
     * 按毫秒数休眠，休眠随机事件
     *
     * @param ms 毫秒数时间上限
     */
    public static final void randomMs(int ms) {
        try {
            TimeUnit.MILLISECONDS.sleep(random.nextInt(ms));
        } catch (InterruptedException e) {
        }
    }
}
