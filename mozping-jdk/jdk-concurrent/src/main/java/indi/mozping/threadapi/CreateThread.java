package indi.mozping.threadapi;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author by mozping
 * @Classname CreateThread
 * @Description TODO
 * @Date 2018/12/6 17:47
 */
public class CreateThread {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyThread myThread = new MyThread();
        myThread.start();
        Thread t1 = new Thread(new MyRunnable());
        t1.start();
        //将Callable包装成FutureTask，再交给Thread类执行线程任务
        FutureTask<String> futureTask = new FutureTask<>(new MyCallable());
        new Thread(futureTask).start();
        //futureTask.get()可以获取线程的返回值，并且是阻塞方法，会一直等待结果
        System.out.println(futureTask.get());


    }

}

class MyThread extends Thread {

    @Override
    public void run() {
        System.out.println("Create thread by extends Thread ... ");
    }
}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("Create thread by implements Runnable interface  ... ");
    }
}

class MyCallable implements Callable<String> {

    @Override
    public String call() throws Exception {
        System.out.println("Create thread by implements Callable interface  ... ");
        Thread.sleep(5 * 1000);
        return "Callable finish ... ";
    }
}