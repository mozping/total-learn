package indi.mozping.threadapi;

/**
 * @author by mozping
 * @Classname StopThread
 * @Description TODO
 * @Date 2018/12/6 20:27
 */
public class StopThread {
    public static void main(String[] args) throws InterruptedException {
        //1.首先跑起来一个线程t1
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                while (!Thread.currentThread().isInterrupted()) {
                    /*try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Thread TestThread is running ...");
                }
                System.out.println("Thread TestThread is interrupted  ...");
            }
        }, "TestThread");
        t1.start();
        //2.让线程1先跑起来
        Thread.sleep(1000);
        //3.然后可以去判断t1线程的状态
        if (t1.isInterrupted()) {
            System.out.println("线程t1是中断状态...");
        } else {
            System.out.println("线程t1是运行状态...");
        }
        //4.可以终止t1线程,interrupt会给该线程发出中断信号，但是不会立即中断该线程，线程可做完其必要的收尾工作，如资源释放
        t1.interrupt();
//        if (t1.isInterrupted()) {
//            System.out.println("线程t1是中断状态...");
//        } else {
//            System.out.println("线程t1是运行状态...");
//        }
    }
}
