package indi.mozping.threadlocal;

/**
 * @author by mozping
 * @Classname ThreadLocalTest
 * @Description TODO
 * @Date 2018/12/28 17:09
 */
public class ThreadLocalTest2 {


    private static class MyThread extends Thread {
        private ThreadLocal<byte[]> val = new ThreadLocal<>();
        private ThreadLocal<byte[]> val1 = new ThreadLocal<>();
        private ThreadLocal<String> val2 = new ThreadLocal<>();
        private ThreadLocal<String> val3 = new ThreadLocal<>();

        @Override
        public void run() {
            //����500MB�ڴ�
            val.set(new byte[500 * 1024 * 1024]);
            val1.set(new byte[500 * 1024 * 1024]);
            try {
                Thread.sleep(3 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            val1 = null; //�ϵ�link1  --- code line A
            try {
                Thread.sleep(2 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            val2.set("123"); //����set������һ�ζ�keyΪnull��Entry������  --- code line A
            val3.set("123"); //����set������һ�ζ�keyΪnull��Entry������  --- code line B

            System.out.println("Thread" + Thread.currentThread().getName() + " finished");
            try {
                Thread.sleep(100 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new MyThread();
        t1.start();
        Thread.sleep(2 * 1000);
        //������3���������յ�״̬
        for (int i = 0; i < 5; i++) {
            Thread.sleep(2 * 1000);
            System.gc();
        }
        Thread.sleep(1000 * 1000);
    }

}
