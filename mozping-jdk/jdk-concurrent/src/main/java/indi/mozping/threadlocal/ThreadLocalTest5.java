package indi.mozping.threadlocal;

/**
 * @author by mozping
 * @Classname ThreadLocalTest
 * @Description TODO
 * @Date 2018/12/28 17:09
 */
public class ThreadLocalTest5 {


    public static void main(String[] args) throws InterruptedException {
        ThreadLocal<byte[]> val = new ThreadLocal<>();
        ThreadLocal<byte[]> val1 = new ThreadLocal<>();
        ThreadLocal<byte[]> val2 = new ThreadLocal<>();
        ThreadLocal<byte[]> val3 = new ThreadLocal<>();
        ThreadLocal<byte[]> val4 = new ThreadLocal<>();
        System.out.println("-begin-");

        int i1 = -387276957;
        int i2 = 1640531527 + i1;
        int i3 = 1640531527 + i2;
        int i4 = 1640531527 + i3;
        int i5 = 1640531527 + i4;

        System.out.println("int 1 : " + i1);
        System.out.println("int 2 : " + i2);
        System.out.println("int 3 : " + i3);
        System.out.println("int 4 : " + i4);
        System.out.println("int 5 : " + i5);

        double v1 = Math.sqrt(5) - 1;
        System.out.println(v1);

        double v = (1024 * 1024 * 1024 * 2D);
        System.out.println((int) (v * v1));
        System.out.println((int) ((Math.sqrt(5) - 1) * (1024 * 1024 * 1024 * 2)));


        Thread.sleep(1000 * 1000);
    }
}
