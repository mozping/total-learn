package indi.mozping.threadlocal;

/**
 * @author by mozping
 * @Classname ThreadLocalTest
 * @Description TODO
 * @Date 2018/12/28 17:09
 */
public class ThreadLocalTest1 {

    private static ThreadLocal<byte[]> val = new ThreadLocal<>();

    private static class MyThread extends Thread {
        @Override
        public void run() {
            //����500MB�ڴ�
            val.set(new byte[500 * 1024 * 1024]);
            try {
                Thread.sleep(3 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            val.remove();//�Ƴ� --- code line A
            //val = null; //�ϵ�link1  --- code line B
            System.out.println("Thread" + Thread.currentThread().getName() + " finished-2");
            //�����̲߳��˳����Ա�Ч��
            try {
                Thread.sleep(100 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new MyThread();
        t1.start();
        Thread.sleep(2 * 1000);
        //������3���������յ�״̬
        for (int i = 0; i < 5; i++) {
            System.gc();
            Thread.sleep(2 * 1000);
        }
        Thread.sleep(1000 * 1000);
    }
}
