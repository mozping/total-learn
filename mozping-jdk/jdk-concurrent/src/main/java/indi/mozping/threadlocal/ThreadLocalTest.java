package indi.mozping.threadlocal;

/**
 * @author by mozping
 * @Classname ThreadLocalTest
 * @Description TODO
 * @Date 2018/12/28 17:09
 */
public class ThreadLocalTest {

    private static ThreadLocal<Integer> val = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            //��ʼ��ֵ
            return 100;
        }
    };


    private static class MyThread extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                Integer current = val.get();
                Integer ret = current + 1;
                val.set(ret);
            }
            System.out.println(Thread.currentThread().getName() + ":" + val.get());
            val.remove();
            System.out.println("�Ƴ���" + Thread.currentThread().getName() + ":" + val.get());
        }
    }

    public static void main(String[] args) {
        Thread t1 = new MyThread();
        Thread t2 = new MyThread();
        Thread t3 = new MyThread();

        t1.start();
        t2.start();
        t3.start();
    }
}
