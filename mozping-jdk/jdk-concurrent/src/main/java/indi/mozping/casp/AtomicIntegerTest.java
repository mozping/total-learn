package indi.mozping.casp;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author intellif
 */
public class AtomicIntegerTest {

    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();
        System.out.println(atomicInteger.addAndGet(2));
    }
}
