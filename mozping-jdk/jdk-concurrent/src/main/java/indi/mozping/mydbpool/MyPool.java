package indi.mozping.mydbpool;

import java.sql.Connection;

/**
 * @author by mozping
 * @Classname MyPool
 * @Description :定义的连接池接口，接口定义了获取连接和释放连接的方法
 * @Date 2019/2/26 17:20
 */
public interface MyPool {

    Connection getConnection(long overtime) throws InterruptedException;

    void releaseConn(Connection connection) throws InterruptedException;
}
