package indi.mozping.container;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author by mozping
 * @Classname MapTest
 * @Description TODO
 * @Date 2019/4/30 11:25
 */
public class MapTest {
    public static void main(String[] args) {
        ConcurrentHashMap<String, String> map = new ConcurrentHashMap(17);
        String key = "key1";

        System.out.println(map.put(key, "123"));
        System.out.println(map.get(key));
        System.out.println(map.put(key, "123456"));
        System.out.println(map.get(key));
    }
}
