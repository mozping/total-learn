package indi.mozping.lockp.lockandcondition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author by mozping
 * @Classname ConditionTest
 * @Description TODO
 * @Date 2020/3/12 10:55
 */
public class ConditionTest {

    public static final Lock lock = new ReentrantLock();
    public static final Condition condition = lock.newCondition();
    public static int count = 0;

    static class MyThread1 implements Runnable {
        @Override
        public void run() {

            try {
                lock.lock();
                if (count % 2 == 0) {
                    count++;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    }
                } else {
                    condition.await();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}