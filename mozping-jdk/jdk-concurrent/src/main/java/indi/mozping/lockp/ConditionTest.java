package indi.mozping.lockp;

import indi.mozping.tools.SleepTools;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author by mozping
 * @Classname ConditionTest
 * @Description TODO
 * @Date 2019/8/12 14:33
 */
public class ConditionTest {

    private static ReentrantLock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();


    public static void main(String[] args) {

        Thread thread1 = new Thread(new Thread1());
        thread1.start();

        SleepTools.second(5);

        Thread thread2 = new Thread(new Thread2());
        thread2.start();
    }

    static class Thread1 implements Runnable {

        @Override
        public void run() {
            lock.lock();
            try {
                condition.await();
                //lock.wait();
                System.out.println("Thread[" + Thread.currentThread().getName() + "] do something...");
            } catch (InterruptedException e) {
                e.printStackTrace();

            } finally {
                lock.unlock();
            }
        }
    }

    static class Thread2 implements Runnable {

        @Override
        public void run() {
            lock.lock();
            System.out.println("�߳�2�����...");
            try {
                condition.signalAll();
            } finally {
                lock.unlock();
            }
        }
    }
}