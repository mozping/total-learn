package indi.mozping.lockp;

import java.util.ArrayDeque;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author by mozping
 * @Classname ConditionTest
 * @Description TODO
 * @Date 2019/8/12 14:33
 */
public class ConditionProducerConsumer {

    private static ReentrantLock lock = new ReentrantLock();
    private static Condition consumer = lock.newCondition();
    private static Condition producer = lock.newCondition();
    private static ArrayDeque arrayDeque = new ArrayDeque(20);

    public static void main(String[] args) {

    }

    class Consumer implements Runnable {

        @Override
        public void run() {
            while (true) {

            }
//            try {
//                condition1.await();
//
//                System.out.println("Thread[" + Thread.currentThread().getName() + "] do something...");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//
//            } finally {
//                lock.unlock();
//            }
        }
    }

    class Producer implements Runnable {

        @Override
        public void run() {
            lock.lock();
            try {

            } finally {
                lock.unlock();
            }
        }
    }
}