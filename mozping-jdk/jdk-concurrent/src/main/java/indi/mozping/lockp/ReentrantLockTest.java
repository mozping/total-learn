package indi.mozping.lockp;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author intellif
 */
public class ReentrantLockTest {

    private static ReentrantLock reentrantLock = new ReentrantLock(true);
    private static ReentrantLock reentrantLockUnFair = new ReentrantLock();

    private static ReentrantReadWriteLock.ReadLock readLock = new ReentrantReadWriteLock().readLock();
    private static ReentrantReadWriteLock.WriteLock writeLock = new ReentrantReadWriteLock().writeLock();

    public static void main(String[] args) {

        //System.out.println("公平锁测试.....");
        //new ReentrantLockTest().test();

        //System.out.println("非公平锁测试.....");
        //new ReentrantLockTest().test1();

        //System.out.println("读锁测试.....");
        //new ReentrantLockTest().test2();

        //System.out.println("写锁测试.....");
        //new ReentrantLockTest().test3();
    }

    void test() {
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    reentrantLock.lock();
                    try {
                        System.out.println(Thread.currentThread().getName() + "begin...");
                        Thread.sleep((int) (Math.random() * 1000 * 5));
                        System.out.println(Thread.currentThread().getName() + "end...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    reentrantLock.unlock();
                }
            }).start();
        }
    }

    void test1() {
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    reentrantLockUnFair.lock();
                    try {
                        System.out.println(Thread.currentThread().getName() + "begin...");
                        Thread.sleep((int) (Math.random() * 1000 * 5));
                        System.out.println(Thread.currentThread().getName() + "end...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    reentrantLockUnFair.unlock();
                }
            }).start();
        }
    }

    void test2() {
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    readLock.lock();
                    try {
                        System.out.println(Thread.currentThread().getName() + "begin...");
                        Thread.sleep((int) (Math.random() * 1000 * 5));
                        System.out.println(Thread.currentThread().getName() + "end...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    readLock.unlock();
                }
            }).start();
        }
    }

    void test3() {
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    writeLock.lock();
                    try {
                        System.out.println(Thread.currentThread().getName() + "begin...");
                        Thread.sleep((int) (Math.random() * 1000 * 5));
                        System.out.println(Thread.currentThread().getName() + "end...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    writeLock.unlock();
                }
            }).start();
        }
    }
}
