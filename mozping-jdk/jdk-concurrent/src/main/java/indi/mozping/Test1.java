package indi.mozping;

import indi.mozping.tools.SleepTools;

import java.util.concurrent.*;

/**
 * @author by mozping
 * @Classname Test1
 * @Description TODO
 * @Date 2019/10/19 13:37
 */
public class Test1 {

    private static final int COUNT_BITS = Integer.SIZE - 3; //29
    private static final int CAPACITY = (1 << COUNT_BITS) - 1;

    // runState is stored in the high-order bits
    private static final int RUNNING = -1 << COUNT_BITS;
    private static final int SHUTDOWN = 0 << COUNT_BITS;
    private static final int STOP = 1 << COUNT_BITS;
    private static final int TIDYING = 2 << COUNT_BITS;
    private static final int TERMINATED = 3 << COUNT_BITS;

    public static void main(String[] args) {

        System.out.println(Integer.SIZE - 3);
        System.out.println("RUNNING" + ": " + Integer.toBinaryString(RUNNING));
        System.out.println("SHUTDOWN" + ": " + Integer.toBinaryString(SHUTDOWN));
        System.out.println("STOP" + ": " + Integer.toBinaryString(STOP));
        System.out.println("TIDYING" + ": " + Integer.toBinaryString(TIDYING));
        System.out.println("TERMINATED" + ": " + Integer.toBinaryString(TERMINATED));
        System.out.println("CAPACITY" + ": " + Integer.toBinaryString(CAPACITY));

        ExecutorService executorService = new ThreadPoolExecutor(2, 4,
                0L, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(10));
        for (int i = 0; i < 100; i++) {
            executorService.submit(new MyTask(i + ""));
        }
    }

    static class MyTask implements Callable<String> {

        String id;

        public MyTask(String id) {
            this.id = id;
        }

        @Override
        public String call() throws Exception {
            long begin = System.currentTimeMillis();
            SleepTools.randomMs(5000);
            System.out.println(id + " -- End");
            return id + " -- " + (System.currentTimeMillis() - begin);
        }
    }
}