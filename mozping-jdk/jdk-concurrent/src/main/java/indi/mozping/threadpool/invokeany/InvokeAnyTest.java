package indi.mozping.threadpool.invokeany;

import indi.mozping.tools.SleepTools;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author by mozping
 * @Classname InvokeAllTest
 * @Description TODO
 * @Date 2019/10/19 10:08
 */
public class InvokeAnyTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<Callable<String>> list = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            list.add(new MyTask(i + ""));
        }
        String result = executorService.invokeAny(list);
        System.out.println(result);
        executorService.shutdown();
    }

    static class MyTask implements Callable<String> {

        String id;

        public MyTask(String id) {
            this.id = id;
        }

        @Override
        public String call() throws Exception {
            long begin = System.currentTimeMillis();
            SleepTools.randomMs(5000);
            return id + " -- " + (System.currentTimeMillis() - begin);
        }
    }

}