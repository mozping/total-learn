package indi.mozping.threadpool;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author by mozping
 * @Classname MyThreadPoolTest
 * @Description TODO
 * @Date 2019/4/30 16:37
 */
public class MyThreadPoolTest {

    static final int THREAD_NUM = 100;
    static AtomicInteger integer = new AtomicInteger(0);
    static CountDownLatch countDownLatch = new CountDownLatch(THREAD_NUM);

    public static void main(String[] args) throws InterruptedException {

        MyThreadPool pool = new MyThreadPool(5);

        for (int i = 0; i < THREAD_NUM; i++) {
            Runnable task = new MyTask();
            pool.submit(task);
        }

        Thread.sleep(5 * 1000);

        pool.shutDown();
        System.out.println("Main主线程退出...");
    }


    static class MyTask implements Runnable {

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + "打印..." + integer.incrementAndGet());
        }
    }
}
