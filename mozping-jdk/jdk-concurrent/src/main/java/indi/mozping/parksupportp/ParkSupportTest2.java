package indi.mozping.parksupportp;

import indi.mozping.tools.SleepTools;

import java.util.Date;
import java.util.concurrent.locks.LockSupport;

/**
 * @author intellif
 * ������ʾ����ʾ�ȵ���unpark��Σ��߳��ٶ�ε���park����һ���̲߳������������Ǻ����̻߳�������
 * ˵��unpark�ṩ������ǲ��ܵ��ӵģ���ε���Ҳֻ���ṩһ����ɡ�
 */
public class ParkSupportTest2 {
    /**
     * ���̵߳���unpark��Σ�Thread-1 ����park��Σ���һ�ο��Ի�ȡ����ɲ��������������������˵����ɲ��ܵ��ӣ�
     * �����û�б�ʹ�õ�������£�unpark���ֻ���ṩһ�����
     */
    public static void main(String[] args) {
        Thread thread1 = new Thread(new MyRunnable(), "Thread-1");
        thread1.start();
        LockSupport.unpark(thread1);
        LockSupport.unpark(thread1);
        LockSupport.unpark(thread1);
        System.out.println("Main execute...");
    }

    static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("Thread [" + Thread.currentThread().getName() + "] start..." + new Date());
            SleepTools.second(1);
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 1 park end..." + new Date());
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 2 park end..." + new Date());
        }
    }
}
