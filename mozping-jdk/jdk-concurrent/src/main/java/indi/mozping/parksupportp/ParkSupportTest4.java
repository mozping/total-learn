package indi.mozping.parksupportp;

import indi.mozping.tools.SleepTools;

import java.util.Date;
import java.util.concurrent.locks.LockSupport;

/**
 * @author intellif
 * ������ʾ�����߳�ÿ���3���ṩһ����ɣ����߳����ÿ�����ȡ��һ�������������ִ��
 */
public class ParkSupportTest4 {
    /**
     * ������ʾ�����߳�ÿ���3���ṩһ����ɣ����߳����ÿ�����ȡ��һ�������������ִ��
     */
    public static void main(String[] args) {
        Thread thread1 = new Thread(new MyRunnable(), "Thread-1");
        thread1.start();
        System.out.println("Main execute...");
        for (int i = 1; i <= 3; i++) {
            LockSupport.unpark(thread1);
            SleepTools.second(3);
        }
    }

    static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("Thread [" + Thread.currentThread().getName() + "] start..." + new Date());
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 1 park end..." + new Date());
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 2 park end..." + new Date());
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 3 park end..." + new Date());
        }
    }
}
