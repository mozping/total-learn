package indi.mozping.parksupportp;

import indi.mozping.tools.SleepTools;

import java.util.Date;
import java.util.concurrent.locks.LockSupport;

/**
 * @author intellif
 * ������ʾ����ʾ�ȵ���park���̱߳�������Ȼ�����unpark���߳̿��Լ���ִ�С�
 */
public class ParkSupportTest {
    /**
     * ���߳�3�����ṩ��ɣ����Thread-1�������룬������֮���ټ���ִ��
     */
    public static void main(String[] args) {
        Thread thread1 = new Thread(new MyRunnable(), "Thread-1");
        thread1.start();
        System.out.println("Main execute...");
        SleepTools.second(3);
        LockSupport.unpark(thread1);
    }

    static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("Thread [" + Thread.currentThread().getName() + "] start..." + new Date());
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 1 park end..." + new Date());
        }
    }
}
