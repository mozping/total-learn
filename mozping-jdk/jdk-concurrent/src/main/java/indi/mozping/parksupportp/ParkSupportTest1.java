package indi.mozping.parksupportp;

import indi.mozping.tools.SleepTools;

import java.util.Date;
import java.util.concurrent.locks.LockSupport;

/**
 * @author intellif
 * ������ʾ����ʾ�ȵ���unpark�ṩ��ɣ��ٵ���park���̲߳���������˵������ǿ���Ԥ���ṩ�ģ�����߳�֮��
 * ��await��notify�����кܴ�Ĳ�ͬ
 */
public class ParkSupportTest1 {
    /**
     * ���߳����ṩ��ɣ����Thread-1 Sleep��1�룬Ȼ�����������֮���ټ���ִ��,˵��parkû���������ͼ���ִ����
     */
    public static void main(String[] args) {
        Thread thread1 = new Thread(new MyRunnable(), "Thread-1");
        thread1.start();
        LockSupport.unpark(thread1);
        System.out.println("Main execute...");
    }

    static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("Thread [" + Thread.currentThread().getName() + "] start..." + new Date());
            SleepTools.second(1);
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 1 park end..." + new Date());
        }
    }
}
