package indi.mozping.parksupportp;

import indi.mozping.tools.SleepTools;

import java.util.Date;
import java.util.concurrent.locks.LockSupport;

/**
 * @author intellif
 * ������ʾ���߳���park������Ȼ��unpark���У�Ȼ���߳��ٴ�park���̻߳�������˵��park������������ȡ֮��
 * ���Կ������룬park���õ���ɣ��������û���ṩ�µ���ɣ��ٴ�park����������
 */
public class ParkSupportTest3 {
    /**
     * ���̵߳���unparkһ�Σ��ṩһ����ɣ�Thread-1 ����park��Σ���һ�ο��Ի�ȡ����ɲ��������������������
     * ˵����ȡ���֮�������һ���Ե�
     */
    public static void main(String[] args) {
        Thread thread1 = new Thread(new MyRunnable(), "Thread-1");
        thread1.start();
        LockSupport.unpark(thread1);
        System.out.println("Main execute...");
    }

    static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("Thread [" + Thread.currentThread().getName() + "] start..." + new Date());
            SleepTools.second(1);
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 1 park end..." + new Date());
            LockSupport.park();
            System.out.println("Thread [" + Thread.currentThread().getName() + "] 2 park end..." + new Date());
        }
    }
}
