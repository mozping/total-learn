package indi.mozping.atmoandcas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author by mozping
 * @Classname AtomoUnsafe
 * @Description TODO
 * @Date 2019/5/28 14:11
 */
public class AtomoUnsafe {


    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(10);
        boolean b = atomicInteger.compareAndSet(10, 15);
        System.out.println("结束:..." + atomicInteger + " , 结果是: " + b);

        boolean bb = atomicInteger.compareAndSet(11, 15);
        System.out.println("结束:..." + atomicInteger + " , 结果是: " + bb);
    }
}