package indi.mozping.atmoandcas;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @author by mozping
 * @Classname AtomicArray
 * @Description TODO
 * @Date 2019/4/28 15:46
 */
public class AtomicArray {

    static int[] arr = new int[]{1, 2};

    static AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(arr);

    public static void main(String[] args) {
        atomicIntegerArray.getAndSet(0, 4);
        System.out.println(atomicIntegerArray.get(0));
        System.out.println(arr[0]);
    }

}
