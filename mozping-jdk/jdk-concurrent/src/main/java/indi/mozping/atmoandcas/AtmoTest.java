package indi.mozping.atmoandcas;

import indi.mozping.tools.SleepTools;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author by mozping
 * @Classname AtmoTest
 * @Description TODO
 * @Date 2019/4/28 11:21
 */
public class AtmoTest {

    //切换volatile和原子类型
    static AtomicInteger auto = new AtomicInteger(0);
    //static volatile int  auto = 0;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            new Thread(new Modify()).start();
        }
        SleepTools.second(5);
        System.out.println("修改后的值是:" + auto);
    }

    static class Modify implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 1000; i++) {
                //切换volatile和原子类型
                auto.getAndIncrement();
                //auto++;
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
