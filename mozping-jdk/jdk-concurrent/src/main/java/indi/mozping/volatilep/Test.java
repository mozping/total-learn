package indi.mozping.volatilep;

/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/5/30 20:01
 */
public class Test {

    public volatile int val;

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }
}