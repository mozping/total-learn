package indi.mozping.volatilep;

/**
 * @author by mozping
 * @Classname VolatileTest
 * @Description TODO
 * <p>
 * -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly
 * @Date 2018/12/28 16:48
 */
public class VolatileTest1 {

    public static boolean flag = false;
    public static int config = 0;

    public static void main(String[] args) {

        //线程B使用配置
        (new Thread(() -> {
            while (!flag) {
                System.out.println(flag);
//                try {
//                    //Thread.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
            System.out.println("线程B可以读取配置做任务了..." + config);
        })).start();

        //线程A初始化配置
        (new Thread(() -> {
            //读取解析配置
            System.out.println("线程A读取并解析配置...");

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            config = 1;
            flag = true;
        })).start();


    }
}
