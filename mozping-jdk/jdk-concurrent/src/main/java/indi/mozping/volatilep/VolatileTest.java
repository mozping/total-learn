package indi.mozping.volatilep;

/**
 * @author by mozping
 * @Classname VolatileTest
 * @Description TODO
 * <p>
 * -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly
 * @Date 2018/12/28 16:48
 */
public class VolatileTest {

    public volatile Integer inc = 0;

    public void increase() {
        inc++;
    }

    public static void main(String[] args) {
        final VolatileTest test = new VolatileTest();
        for (int i = 0; i < 10; i++) {
            new Thread() {
                public void run() {
                    for (int j = 0; j < 10; j++) {
                        test.increase();
                    }
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                ;
            }.start();
        }

        try {
            //保证前面的线程都执行完
            Thread.sleep(3 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        test.increase();
        System.out.println();
    }
}
