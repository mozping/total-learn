package indi.mozping.volatilep;

/**
 * @author by mozping
 * @Classname VolatileExample
 * @Description TODO
 * @Date 2019/6/21 12:29
 */
public class VolatileExample
        extends Thread {
    private static boolean flag = false;

    @Override
    public void run() {
        while (!flag) {
            //System.out.println("1");
        }
        ;
    }

    public static void main(String[] args) throws Exception {
        new VolatileExample().start();
        Thread.sleep(100);
        flag = true;
    }
}