//package indi.mozping.fockjoin;
//
//import cn.huolala.driver.subsidy.common.constant.Constants;
//import cn.huolala.driver.subsidy.entity.SubsidyStrategyEntity;
//import cn.lalaframework.tools.collection.CollUtil;
//import com.google.common.collect.Lists;
//
//import java.util.List;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.ForkJoinPool;
//import java.util.concurrent.Future;
//import java.util.concurrent.RecursiveTask;
//
///**
// * @author moore.mo
// * @description 使用ForkJoin线程池过滤策略状态
// * @date 2020/12/1 4:23 下午
// **/
//public class MatchStatusForkJoinTask extends RecursiveTask<List<SubsidyStrategyEntity>> {
//
//    private static final long serialVersionUID = 1L;
//    public static final int DEFAULT_THRESHOLD = 1000;
//
//    private List<SubsidyStrategyEntity> strategyEntityList;
//    private long threshold;
//    private int status;
//    private int fromIndex;
//    private int toIndex;
//
//    public MatchStatusForkJoinTask(List<SubsidyStrategyEntity> strategyEntityList, long threshold, int status, int fromIndex, int toIndex) {
//        this.strategyEntityList = strategyEntityList;
//        this.threshold = threshold;
//        this.status = status;
//        this.fromIndex = fromIndex;
//        this.toIndex = toIndex;
//    }
//
//    /**
//     * compute执行业务逻辑，必要时将任务进行拆分计算
//     */
//    @Override
//    protected List<SubsidyStrategyEntity> compute() {
//        List<SubsidyStrategyEntity> result = Lists.newArrayList();
//        if (CollUtil.isEmpty(strategyEntityList)) {
//            return result;
//        }
//        //判断是否是拆分完毕
//        boolean canCompute = (toIndex - fromIndex) <= threshold;
//        if (canCompute) {
//            result =  getStrategyMatchStatus(strategyEntityList, status, fromIndex, toIndex);
//        } else {
//            int middle = (fromIndex + toIndex) / 2;
//            cn.huolala.driver.subsidy.service.convert.MatchStatusForkJoinTask task1 = new cn.huolala.driver.subsidy.service.convert.MatchStatusForkJoinTask(strategyEntityList, 1000L, status, fromIndex, middle);
//            cn.huolala.driver.subsidy.service.convert.MatchStatusForkJoinTask task2 = new cn.huolala.driver.subsidy.service.convert.MatchStatusForkJoinTask(strategyEntityList, 1000L, status, middle, toIndex);
//
//            task1.fork();
//            task2.fork();
//
//            List<SubsidyStrategyEntity> result1 = task1.join();
//            List<SubsidyStrategyEntity> result2 = task2.join();
//            result.addAll(result1);
//            result.addAll(result2);
//        }
//        return result;
//    }
//
//    public static List<SubsidyStrategyEntity> getStrategyMatchStatusUseForkJoin(List<SubsidyStrategyEntity> entityList, int matchStatus) throws ExecutionException, InterruptedException {
//        ForkJoinPool pool = new ForkJoinPool();
//        cn.huolala.driver.subsidy.service.convert.MatchStatusForkJoinTask task = new cn.huolala.driver.subsidy.service.convert.MatchStatusForkJoinTask(entityList, cn.huolala.driver.subsidy.service.convert.MatchStatusForkJoinTask.DEFAULT_THRESHOLD, matchStatus, 0, entityList.size());
//        Future<List<SubsidyStrategyEntity>> future = pool.submit(task);
//        List<SubsidyStrategyEntity> entitiesMatchStatus = future.get();
//        return entitiesMatchStatus;
//    }
//
//    public static List<SubsidyStrategyEntity> getStrategyMatchStatus(List<SubsidyStrategyEntity> subsidyStrategyEntities, int matchStatus, int fromIndex, int toIndex) {
//        List<SubsidyStrategyEntity> entitiesMathStatus = Lists.newArrayList();
//        if (matchStatus < Constants.STRATEGY_STATUS_WAIT || matchStatus > Constants.STRATEGY_STATUS_END) {
//            return subsidyStrategyEntities;
//        }
//        for (int index = fromIndex; (index >= 0) && (index < subsidyStrategyEntities.size()) && (index < toIndex); index++) {
//            SubsidyStrategyEntity entity = subsidyStrategyEntities.get(index);
//            int entityStatus = StrategyConvert.checkStrategyStatus(entity);
//            if (entityStatus == matchStatus) {
//                entitiesMathStatus.add(entity);
//            }
//        }
//        return entitiesMathStatus;
//    }
//}
//
