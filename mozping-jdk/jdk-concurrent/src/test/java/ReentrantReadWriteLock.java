/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;

import java.util.concurrent.TimeUnit;
import java.util.Collection;

/**
 * An implementation of {@link ReadWriteLock} supporting similar
 * semantics to {@link ReentrantLock}.
 * <p>This class has the following properties:
 *
 * <ul>
 * <li><b>Acquisition order</b>
 *
 * <p>This class does not impose a reader or writer preference
 * ordering for lock access.  However, it does support an optional
 * <em>fairness</em> policy.
 *
 * <dl>
 * <dt><b><i>Non-fair mode (default)</i></b>
 * <dd>When constructed as non-fair (the default), the order of entry
 * to the read and write lock is unspecified, subject to reentrancy
 * constraints.  A nonfair lock that is continuously contended may
 * indefinitely postpone one or more reader or writer threads, but
 * will normally have higher throughput than a fair lock.
 *
 * <dt><b><i>Fair mode</i></b>
 * <dd>When constructed as fair, threads contend for entry using an
 * approximately arrival-order policy. When the currently held lock
 * is released, either the longest-waiting single writer thread will
 * be assigned the write lock, or if there is a group of reader threads
 * waiting longer than all waiting writer threads, that group will be
 * assigned the read lock.
 *
 * <p>A thread that tries to acquire a fair read lock (non-reentrantly)
 * will block if either the write lock is held, or there is a waiting
 * writer thread. The thread will not acquire the read lock until
 * after the oldest currently waiting writer thread has acquired and
 * released the write lock. Of course, if a waiting writer abandons
 * its wait, leaving one or more reader threads as the longest waiters
 * in the queue with the write lock free, then those readers will be
 * assigned the read lock.
 *
 * <p>A thread that tries to acquire a fair write lock (non-reentrantly)
 * will block unless both the read lock and write lock are free (which
 * implies there are no waiting threads).  (Note that the non-blocking
 * {@link java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock#tryLock()} and {@link java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock#tryLock()} methods
 * do not honor this fair setting and will immediately acquire the lock
 * if it is possible, regardless of waiting threads.)
 * <p>
 * </dl>
 *
 * <li><b>Reentrancy</b>
 *
 * <p>This lock allows both readers and writers to reacquire read or
 * write locks in the style of a {@link ReentrantLock}. Non-reentrant
 * readers are not allowed until all write locks held by the writing
 * thread have been released.
 *
 * <p>Additionally, a writer can acquire the read lock, but not
 * vice-versa.  Among other applications, reentrancy can be useful
 * when write locks are held during calls or callbacks to methods that
 * perform reads under read locks.  If a reader tries to acquire the
 * write lock it will never succeed.
 *
 * <li><b>Lock downgrading</b>
 * <p>Reentrancy also allows downgrading from the write lock to a read lock,
 * by acquiring the write lock, then the read lock and then releasing the
 * write lock. However, upgrading from a read lock to the write lock is
 * <b>not</b> possible.
 *
 * <li><b>Interruption of lock acquisition</b>
 * <p>The read lock and write lock both support interruption during lock
 * acquisition.
 *
 * <li><b>{@link Condition} support</b>
 * <p>The write lock provides a {@link Condition} implementation that
 * behaves in the same way, with respect to the write lock, as the
 * {@link Condition} implementation provided by
 * {@link ReentrantLock#newCondition} does for {@link ReentrantLock}.
 * This {@link Condition} can, of course, only be used with the write lock.
 *
 * <p>The read lock does not support a {@link Condition} and
 * {@code readLock().newCondition()} throws
 * {@code UnsupportedOperationException}.
 *
 * <li><b>Instrumentation</b>
 * <p>This class supports methods to determine whether locks
 * are held or contended. These methods are designed for monitoring
 * system state, not for synchronization control.
 * </ul>
 *
 * <p>Serialization of this class behaves in the same way as built-in
 * locks: a deserialized lock is in the unlocked state, regardless of
 * its state when serialized.
 *
 * <p><b>Sample usages</b>. Here is a code sketch showing how to perform
 * lock downgrading after updating a cache (exception handling is
 * particularly tricky when handling multiple locks in a non-nested
 * fashion):
 *
 * <pre> {@code
 * class CachedData {
 *   Object data;
 *   volatile boolean cacheValid;
 *   final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
 *
 *   void processCachedData() {
 *     rwl.readLock().lock();
 *     if (!cacheValid) {
 *       // Must release read lock before acquiring write lock
 *       rwl.readLock().unlock();
 *       rwl.writeLock().lock();
 *       try {
 *         // Recheck state because another thread might have
 *         // acquired write lock and changed state before we did.
 *         if (!cacheValid) {
 *           data = ...
 *           cacheValid = true;
 *         }
 *         // Downgrade by acquiring read lock before releasing write lock
 *         rwl.readLock().lock();
 *       } finally {
 *         rwl.writeLock().unlock(); // Unlock write, still hold read
 *       }
 *     }
 *
 *     try {
 *       use(data);
 *     } finally {
 *       rwl.readLock().unlock();
 *     }
 *   }
 * }}</pre>
 * <p>
 * ReentrantReadWriteLocks can be used to improve concurrency in some
 * uses of some kinds of Collections. This is typically worthwhile
 * only when the collections are expected to be large, accessed by
 * more reader threads than writer threads, and entail operations with
 * overhead that outweighs synchronization overhead. For example, here
 * is a class using a TreeMap that is expected to be large and
 * concurrently accessed.
 *
 * <pre> {@code
 * class RWDictionary {
 *   private final Map<String, Data> m = new TreeMap<String, Data>();
 *   private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
 *   private final Lock r = rwl.readLock();
 *   private final Lock w = rwl.writeLock();
 *
 *   public Data get(String key) {
 *     r.lock();
 *     try { return m.get(key); }
 *     finally { r.unlock(); }
 *   }
 *   public String[] allKeys() {
 *     r.lock();
 *     try { return m.keySet().toArray(); }
 *     finally { r.unlock(); }
 *   }
 *   public Data put(String key, Data value) {
 *     w.lock();
 *     try { return m.put(key, value); }
 *     finally { w.unlock(); }
 *   }
 *   public void clear() {
 *     w.lock();
 *     try { m.clear(); }
 *     finally { w.unlock(); }
 *   }
 * }}</pre>
 *
 * <h3>Implementation Notes</h3>
 *
 * <p>This lock supports a maximum of 65535 recursive write locks
 * and 65535 read locks. Attempts to exceed these limits result in
 * {@link Error} throws from locking methods.
 *
 * @author Doug Lea
 * @since 1.5
 */
public class ReentrantReadWriteLock
        implements ReadWriteLock, java.io.Serializable {
    private static final long serialVersionUID = -6992448646407690164L;
    /**
     * Inner class providing readlock
     */
    private final java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock readerLock;
    /**
     * Inner class providing writelock
     */
    private final java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock writerLock;
    /**
     * Performs all synchronization mechanics
     */
    final java.util.concurrent.locks.ReentrantReadWriteLock.Sync sync;

    /**
     * Creates a new {@code ReentrantReadWriteLock} with
     * default (nonfair) ordering properties.
     */
    public ReentrantReadWriteLock() {
        this(false);
    }

    /**
     * Creates a new {@code ReentrantReadWriteLock} with
     * the given fairness policy.
     *
     * @param fair {@code true} if this lock should use a fair ordering policy
     */
    public ReentrantReadWriteLock(boolean fair) {
        sync = fair ? new java.util.concurrent.locks.ReentrantReadWriteLock.FairSync() : new java.util.concurrent.locks.ReentrantReadWriteLock.NonfairSync();
        readerLock = new java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock(this);
        writerLock = new java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock(this);
    }

    public java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock writeLock() {
        return writerLock;
    }

    public java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock readLock() {
        return readerLock;
    }

    /**
     * Synchronization implementation for ReentrantReadWriteLock.
     * Subclassed into fair and nonfair versions.
     */
    abstract static class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 6317671515068378041L;

        /*
         * Read vs write count extraction constants and functions.
         * Lock state is logically divided into two unsigned shorts:
         * The lower one representing the exclusive (writer) lock hold count,
         * and the upper the shared (reader) hold count.
         */
        //切分位数
        static final int SHARED_SHIFT = 16;
        //
        static final int SHARED_UNIT = (1 << SHARED_SHIFT);
        static final int MAX_COUNT = (1 << SHARED_SHIFT) - 1;
        //表示0XFFFF，与运算时保留低16位，用来获取写状态
        static final int EXCLUSIVE_MASK = (1 << SHARED_SHIFT) - 1;

        /**
         * Returns the number of shared holds represented in count
         */
        //获取读状态，右移16位获取到高位，高16位表示读
        static int sharedCount(int c) {
            return c >>> SHARED_SHIFT;
        }

        /**
         * Returns the number of exclusive holds represented in count
         */
        //获取写状态，低16位表示写
        static int exclusiveCount(int c) {
            return c & EXCLUSIVE_MASK;
        }

        /**
         * A counter for per-thread read hold counts.
         * Maintained as a ThreadLocal; cached in cachedHoldCounter
         * 每一个线程的读锁持有的计数器，使用ThreadLocal来保存，线程之间隔离
         * 并且会缓存在cachedHoldCounter变量里面，
         * 这个类比较简单，就是一个线程id和一个int计数变量
         */
        static final class HoldCounter {
            int count = 0;
            // Use id, not reference, to avoid garbage retention
            final long tid = getThreadId(Thread.currentThread());
        }

        /**
         * ThreadLocal subclass. Easiest to explicitly define for sake
         * of deserialization mechanics.
         * 保存线程读锁持有数量的ThreadLocal变量，这里没有直接使用ThreadLocal，是继
         * 承ThreadLocalHoldCounter，
         */
        static final class ThreadLocalHoldCounter
                extends ThreadLocal<HoldCounter> {
            public HoldCounter initialValue() {
                return new HoldCounter();
            }
        }

        /**
         * The number of reentrant read locks held by current thread.
         * Initialized only in constructor and readObject.
         * Removed whenever a thread's read hold count drops to 0.
         * ThreadLocalHoldCounter类实例，用来保存每个线程锁持有的读锁数量，
         * 只会在构造方法和读锁中初始化，当线程持有的读锁减为0就会移除
         */
        private transient ThreadLocalHoldCounter readHolds;

        /**
         * The hold count of the last thread to successfully acquire
         * readLock. This saves ThreadLocal lookup in the common case
         * where the next thread to release is the last one to
         * acquire. This is non-volatile since it is just used
         * as a heuristic, and would be great for threads to cache.
         *
         * <p>Can outlive the Thread for which it is caching the read
         * hold count, but avoids garbage retention by not retaining a
         * reference to the Thread.
         *
         * <p>Accessed via a benign data race; relies on the memory
         * model's final field and out-of-thin-air guarantees.
         * 最后一个获得读锁的线程的HoldCounter的缓存
         */
        private transient HoldCounter cachedHoldCounter;

        /**
         * firstReader is the first thread to have acquired the read lock.
         * firstReaderHoldCount is firstReader's hold count.
         *
         * <p>More precisely, firstReader is the unique thread that last
         * changed the shared count from 0 to 1, and has not released the
         * read lock since then; null if there is no such thread.
         *
         * <p>Cannot cause garbage retention unless the thread terminated
         * without relinquishing its read locks, since tryReleaseShared
         * sets it to null.
         *
         * <p>Accessed via a benign data race; relies on the memory
         * model's out-of-thin-air guarantees for references.
         *
         * <p>This allows tracking of read holds for uncontended read
         * locks to be very cheap.
         * 第一个获取读锁的线程
         * 第一个获取读锁的线程的HoldCount对象
         */
        private transient Thread firstReader = null;
        private transient int firstReaderHoldCount;

        Sync() {
            //在构造方法中初始化readHolds
            readHolds = new ThreadLocalHoldCounter();
            setState(getState()); // ensures visibility of readHolds
        }

        /*
         * Acquires and releases use the same code for fair and
         * nonfair locks, but differ in whether/how they allow barging
         * when queues are non-empty.
         */

        /**
         * Returns true if the current thread, when trying to acquire
         * the read lock, and otherwise eligible to do so, should block
         * because of policy for overtaking other waiting threads.
         * 尝试获取读锁的时候，是否应该阻塞，由子类实现
         */
        abstract boolean readerShouldBlock();

        /**
         * Returns true if the current thread, when trying to acquire
         * the write lock, and otherwise eligible to do so, should block
         * because of policy for overtaking other waiting threads.
         * 尝试获取写锁的时候，是否应该阻塞，由子类实现
         */
        abstract boolean writerShouldBlock();

        /**
         * Note that tryRelease and tryAcquire can be called by
         * Conditions. So it is possible that their arguments contain
         * both read and write holds that are all released during a
         * condition wait and re-established in tryAcquire.
         * 独占式释放，对应写锁的释放锁
         */
        protected final boolean tryRelease(int releases) {
            //1.如果不是当前线程持有锁，抛出异常
            if (!isHeldExclusively())
                throw new IllegalMonitorStateException();
            //2.因为是独占式释放，因此是线程安全的，直接计算释放后的state变量值
            int nextc = getState() - releases;
            boolean free = exclusiveCount(nextc) == 0;
            //3.如果释放之后是0，说明已经解锁了，那就设置占有线程为null
            if (free)
                setExclusiveOwnerThread(null);
            //4.反之说明还没解锁，修改state即可
            setState(nextc);
            return free;
        }

        //独占模式获取共享状态
        protected final boolean tryAcquire(int acquires) {
            /**
             * Walkthrough:
             * 1. If read count nonzero or write count nonzero
             *    and owner is a different thread, fail.
             * 2. If count would saturate, fail. (This can only
             *    happen if count is already nonzero.)
             * 3. Otherwise, this thread is eligible for lock if
             *    it is either a reentrant acquire or
             *    queue policy allows it. If so, update state
             *    and set owner.
             *
             * 独占式获取，对应写模式下获取锁
             *1.如果读锁或者写锁非零，并且锁不是被当前线程持有，那么获取失败(因为写写互斥，读写也是互斥)
             *2.如果计数饱和，失败
             *3.不满足1,2，并且是可重入锁，排队策略允许的话，线程就会尝试获取锁，获取成功会
             * 将自身设置为锁的拥有者线程
             */
            Thread current = Thread.currentThread();
            int c = getState();
            //1.获取写锁状态，低16位表示写
            int w = exclusiveCount(c);
            //2.写锁状态不为0，
            if (c != 0) {
                // (Note: if c != 0 and w == 0 then shared count != 0)
                //逻辑1：若写锁状态w为0，但c!=0,说明此时高位读模式不为0，读写互斥，这里获取写锁应该失败返回false，上面的源码注释提示的比较清楚
                //逻辑2：当前线程不是拥有者线程，此时c!=0,说明有其他线程要么在读要么在写，因此也要返回false
                if (w == 0 || current != getExclusiveOwnerThread())
                    return false;
                //3.到这里说明w不是0代表写锁被持有，并且自身是拥有者线程，很明显自己就是写锁的持有者，此时应该是写锁的重入，
                //那么就需要判断，重入次数是否到最大值了，超过的话抛异常
                if (w + exclusiveCount(acquires) > MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                // Reentrant acquire
                //4.没有超过就是锁重入，这里源码有关键的注释提示"Reentrant acquire"
                setState(c + acquires);
                return true;
            }
            //6.如果c为0，说明读锁写锁都是0(c是0，那么高低位肯定都是0)，这时候根据writerShouldBlock判断当前是否需要阻塞，
            // 非公平模式返回false是不阻塞的，公平模式则在有前驱的情况下要阻塞，
            //逻辑1：writerShouldBlock为false，说明是非公平模式，那就尝试CAS设置state，设置失败就返回false，设置成功
            // 就继续后面的逻辑
            //逻辑2：如果writerShouldBlock为true，说明是公平模式下并且有前驱节点，因此需要阻塞排队，返回false
            if (writerShouldBlock() || !compareAndSetState(c, c + acquires))
                return false;
            //7.如果是前面的逻辑1，那就设置拥有者线程为当前线程，并返回true
            setExclusiveOwnerThread(current);
            return true;
        }

        /**
         * 共享模式释放共享状态，对应读模式下释放锁
         */
        protected final boolean tryReleaseShared(int unused) {
            Thread current = Thread.currentThread();
            //1.如果当前线程是第一个读线程
            if (firstReader == current) {
                // assert firstReaderHoldCount > 0;
                //第一个读线程，并且之前持有的读锁次数是1，现在释放了，那就将firstReader设为null
                if (firstReaderHoldCount == 1)
                    firstReader = null;
                else
                    //第一个读线程，并且之前持有的读锁次数不是1，那就减一
                    // (不是1肯定大于1，不会小与等于0，因此释放之前肯定获取过)
                    firstReaderHoldCount--;
            } else {
                //2.如果当前线程不是第一个获取读锁的线程，那就获取最后一个线
                //程的HoldCounter(最后一个线程的HoldCounter缓存在cachedHoldCounter)
                HoldCounter rh = cachedHoldCounter;
                //2.1 如果为null，或者最后一个线程不是自己
                if (rh == null || rh.tid != getThreadId(current))
                    //那就将自身的持有锁次数更新到rh
                    rh = readHolds.get();
                //逻辑1：如果最后一次那个线程就是自己(也就是自己连续2次获取读锁)，那么就先记下下自己持有锁的次数
                //逻辑2：如果最后一次的线程不是自己，那么就几下自己持有的读锁的次数
                //不管逻辑1还是逻辑2，最终都是记下自己的持有锁次数，只不过如果自己是连续获取锁，处理逻辑不一样
                int count = rh.count;

                if (count <= 1) {
                    readHolds.remove();
                    if (count <= 0)
                        throw unmatchedUnlockException();
                }
                //之前在上面记下了自己持有锁的次数，如果次数大于1，现在在这里就释放一次，如果是1或者0，就移除自身持有锁的
                // 记录，如果是0或者负数，还会抛出一个异常
                --rh.count;
            }
            //使用CAS更新同步状态
            for (; ; ) {
                int c = getState();
                //这里将state设置为一个state-65536的数字是模式目的呢？
                //因为读锁是再高16位，低16位是写锁，低16为值是65535，首先确认一点，此时的低位全部都是0，因为在释放读锁
                //而读锁和写锁是互斥的，不能同时大于0
                // 情况1：如果state-65536=0，说明state为65536，高位为1低位全部是0，此时只有一个读锁，因此释放之后就没有锁了，
                // 后续的读锁线程和写锁线程可以继续竞争锁，这也是下面nextc等于0的情况
                //情况2:如果nextc>0,说明高位大于1，即有多把读锁，因此释放之后，依然还有其他读锁占有，加入c是65538，那么
                // nextc是2，到此我们知道nextc就是剩余的读锁数量。
                //情况3：nextc<0,这种情况不存在，因为至少有一把读锁，高位至少为1，高位为1时，c就是65536
                //到这我们明白了这里其实就是重新设置读锁的数量(需要理解读写锁高低位的设计，否则有点不明白)，netxc是释放后
                //读锁的数量，如果释放后为0，返回true，反之返回false，返回值的含义就是当前锁是否还被持有。(这对读线程没有影响，
                // 但是写线程很关心这个，如果被持有，读线程可以继续进来写线程却不可以，如果没有被持有，读锁线程就竞争)
                int nextc = c - SHARED_UNIT;
                if (compareAndSetState(c, nextc))
                    // Releasing the read lock has no effect on readers,
                    // but it may allow waiting writers to proceed if
                    // both read and write locks are now free.
                    //释放读锁对读线程没有影响，但是如果读锁和写锁都释放了的话，
                    //可以让等待写锁的线程继续执行
                    return nextc == 0;
            }
        }

        private IllegalMonitorStateException unmatchedUnlockException() {
            return new IllegalMonitorStateException(
                    "attempt to unlock read lock, not locked by current thread");
        }

        //共享模式获取共享状态
        protected final int tryAcquireShared(int unused) {
            /*
             * Walkthrough:
             * 1. If write lock held by another thread, fail.
             * 2. Otherwise, this thread is eligible for
             *    lock wrt state, so ask if it should block
             *    because of queue policy. If not, try
             *    to grant by CASing state and updating count.
             *    Note that step does not check for reentrant
             *    acquires, which is postponed to full version
             *    to avoid having to check hold count in
             *    the more typical non-reentrant case.
             * 3. If step 2 fails either because thread
             *    apparently not eligible or CAS fails or count
             *    saturated, chain to version with full retry loop.
             *  共享式获取，对应读模式下获取锁
             *1.如果写锁被持有，则获取失败
             *2.不满足1，那么线程尝试获取读锁，根据排队模式，确定是否需要阻塞，
             * 如果不需要这是，使用CAS尝试获取state变量并在获取成功后更新，但是
             * 这个步骤没有检查重入获取读锁，这被推迟到了完整版本，避免检查holdCount，
             * 考虑到更多场景是非重入的情况
             *3.如果步骤2失败了，不管是因为锁统计饱和，或者是线程不能进行去获取锁(比如被写锁阻塞)
             * 或者CAS失败，跳转到full retry loop（这里不好解释，看代码就是进入
             * fullTryAcquireShared方法里面自旋）
             */
            Thread current = Thread.currentThread();
            int c = getState();
            //1.写锁被持有，并且不是被自身持有，说明写锁被其他线程持有，返回-1
            if (exclusiveCount(c) != 0 && getExclusiveOwnerThread() != current)
                return -1;
            //能够跳过步骤1的逻辑，说明要么写锁被持有但是是被自己持有，要么写锁没有被持有，
            //2.获取读锁值，
            int r = sharedCount(c);
            //3.如果不要被阻塞(公平模式下没有前驱)，并且读写未达到上限65535，并且设置state成功，就进入里面的逻辑
            //反之就要调用fullTryAcquireShared方法
            //修改高16位的状态，所以要加上2^16
            if (!readerShouldBlock() && r < MAX_COUNT && compareAndSetState(c, c + SHARED_UNIT)) {
                //3.1 如果读锁为0，说明自己是第一个，设置firstReader和firstReaderHoldCount
                if (r == 0) {
                    firstReader = current;
                    firstReaderHoldCount = 1;
                    //3.2 如果自己是第一个读线程，但是r!=0,那就firstReaderHoldCount加1
                } else if (firstReader == current) {
                    firstReaderHoldCount++;
                    //3.3如果r!=0,并且自己不是第一个线程，那么自己就考虑之前获取锁的最后一个线程是不是自己，
                } else {
                    HoldCounter rh = cachedHoldCounter;
                    //如果自己不是之前获取锁的最后一个线程，那么将自己的持有锁的数量保存到cachedHoldCounter
                    if (rh == null || rh.tid != getThreadId(current))
                        cachedHoldCounter = rh = readHolds.get();
                        //到这里，说明之前获取锁的最后一个线程就是自己，并且rh.count还是0
                        //那就将cachedHoldCounter保存到readHolds，也就是将自己的持有锁的数量保存到cachedHoldCounter
                        //不明白这里怎么会有为0的情况呢？之前最后获取锁的线程是自己，那么自己持有锁，count至少为1，如何为0？
                    else if (rh.count == 0)
                        readHolds.set(rh);
                    //如果不满足前面2个逻辑，说明之前获取锁的最后一个线程就是自己，并且rh不是null，并且rh.count也不是0，
                    //那么就直接count自增表示持有的锁加1
                    rh.count++;
                }
                return 1;
            }
            //4.第三点的条件没有满足，进入fullTryAcquireShared方法，在方法里面其实就是不断自旋
            //直到
            return fullTryAcquireShared(current);
        }

        /**
         * Full version of acquire for reads, that handles CAS misses
         * and reentrant reads not dealt with in tryAcquireShared.
         */
        final int fullTryAcquireShared(Thread current) {
            /*
             * This code is in part redundant with that in
             * tryAcquireShared but is simpler overall by not
             * complicating tryAcquireShared with interactions between
             * retries and lazily reading hold counts.
             * 如果是锁降级的话，步骤1的判断会失败，回玩后面继续走，然后readerShouldBlock()会失败，因为一个已经获取
             * 写锁的线程降级，那么他是没有前驱的，因此会直接走到第2和3的逻辑，如果读锁没有超限，就直接CAS设置state了
             */
            HoldCounter rh = null;
            for (; ; ) {
                int c = getState();
                //1.如果写锁不为0，并且被其他线程持有，返回-1
                if (exclusiveCount(c) != 0) {
                    if (getExclusiveOwnerThread() != current)
                        return -1;
                    // else we hold the exclusive lock; blocking here
                    // would cause deadlock.
                } else if (readerShouldBlock()) {
                    // Make sure we're not acquiring read lock reentrantly
                    if (firstReader == current) {
                        // assert firstReaderHoldCount > 0;
                    } else {
                        if (rh == null) {
                            rh = cachedHoldCounter;
                            if (rh == null || rh.tid != getThreadId(current)) {
                                rh = readHolds.get();
                                if (rh.count == 0)// 计数为 0 ，说明没得到读锁，清空线程变量
                                    readHolds.remove();
                            }
                        }
                        if (rh.count == 0)// 说明没得到读锁
                            return -1;
                    }
                }
                //2.读锁超出最大范围抛出异常
                if (sharedCount(c) == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                //3.CAS设置读锁成功
                if (compareAndSetState(c, c + SHARED_UNIT)) {
                    //c=0，说明是第1次获取“读取锁”，则更新firstReader和firstReaderHoldCount
                    if (sharedCount(c) == 0) {
                        firstReader = current;
                        firstReaderHoldCount = 1;
                        //如果想要获取锁的线程(current)是第1个获取锁(firstReader)的线程,则将firstReaderHoldCount+1
                    } else if (firstReader == current) {
                        firstReaderHoldCount++;
                        //里面需要处理之前获取锁的最后一个线程是不是自己的逻辑，和tryAcquireShared里面的逻辑很相似
                    } else {
                        if (rh == null)
                            rh = cachedHoldCounter;
                        if (rh == null || rh.tid != getThreadId(current))
                            rh = readHolds.get();
                        else if (rh.count == 0)
                            readHolds.set(rh);
                        //更新线程的获取“读取锁”的共享计数
                        rh.count++;
                        cachedHoldCounter = rh; // cache for release
                    }
                    return 1;
                }
            }
        }

        /**
         * Performs tryLock for write, enabling barging in both modes.
         * This is identical in effect to tryAcquire except for lack
         * of calls to writerShouldBlock.
         * 尝试获取写锁，获取成功返回true，反之返回false，不会阻塞,无视公平模式下的排队原则
         */
        final boolean tryWriteLock() {
            Thread current = Thread.currentThread();
            int c = getState();
            if (c != 0) {
                //1.获取写锁数量
                int w = exclusiveCount(c);
                //2.1如果w=0，但是c不为0，说名读锁被占用，返回false
                //2.2如果w不为0，那么此时有可能高位读锁为0，那么此时再判断自己是不是拥有者线程，如果不是就返回false，
                if (w == 0 || current != getExclusiveOwnerThread())
                    return false;
                //3.写锁达到上限，抛出异常
                if (w == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
            }
            //4.到这里说明c是0，或者是c为0，但是不是前面那三种逻辑，那么此时就CAS尝试修改state变量，
            //修改失败返回false，不阻塞
            if (!compareAndSetState(c, c + 1))
                return false;
            //5.修改成功，设置自身为拥有者线程
            setExclusiveOwnerThread(current);
            return true;
        }

        /**
         * Performs tryLock for read, enabling barging in both modes.
         * This is identical in effect to tryAcquireShared except for
         * lack of calls to readerShouldBlock.
         * 尝试获取读锁。获取成功返回true，反之返回false，不会阻塞，无视公平模式下的排队原则
         */
        final boolean tryReadLock() {
            Thread current = Thread.currentThread();
            for (; ; ) {
                int c = getState();
                //1.写锁被占有，并且不是自己占用，返回fasle
                // (如果是自己占有写锁，再获取读锁，是要成功的，这就是锁降级)
                if (exclusiveCount(c) != 0 && getExclusiveOwnerThread() != current)
                    return false;
                //2.获取读锁数量
                int r = sharedCount(c);
                //3.读锁超限，抛异常
                if (r == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                //4.读锁没有超限，CAS设置state
                if (compareAndSetState(c, c + SHARED_UNIT)) {
                    //4.1读锁状态为0，说明自己是第一个获取读锁的线程，设置相关变量
                    if (r == 0) {
                        firstReader = current;
                        firstReaderHoldCount = 1;
                        //4.2自己是记录的第一个获取读锁的线程，同样修改firstReaderHoldCount自增1
                    } else if (firstReader == current) {
                        firstReaderHoldCount++;
                        //4.3else逻辑说明读锁状态不为0，已经被其他线程占有，且第一个线程并不是自己
                    } else {
                        //下面需要处理最近最后一次获取锁的线程不是当前线程和是当前线程的2种情况
                        HoldCounter rh = cachedHoldCounter;
                        if (rh == null || rh.tid != getThreadId(current))
                            cachedHoldCounter = rh = readHolds.get();
                        else if (rh.count == 0)
                            readHolds.set(rh);
                        rh.count++;
                    }
                    return true;
                }
            }
        }

        //判断当前线程是否为锁的独占拥有线程
        protected final boolean isHeldExclusively() {
            // While we must in general read state before owner,
            // we don't need to do so to check if current thread is owner
            return getExclusiveOwnerThread() == Thread.currentThread();
        }

        // Methods relayed to outer class
        //获取Condition对象
        final ConditionObject newCondition() {
            return new ConditionObject();
        }

        //写锁为0时，返回null，反之返回锁的独占线程
        final Thread getOwner() {
            // Must read state before owner to ensure memory consistency
            return ((exclusiveCount(getState()) == 0) ?
                    null :
                    getExclusiveOwnerThread());
        }

        //获取读锁的加锁次数
        final int getReadLockCount() {
            return sharedCount(getState());
        }

        //判断写锁是否被持有，写锁被持有时，写锁状态位不为0
        final boolean isWriteLocked() {
            return exclusiveCount(getState()) != 0;
        }

        //判断当前线程写锁的持有次数，其实也是重入次数(因为写锁是独占的，写锁状态大于0只能是重入，不可能被多个线程共享)
        //为0表示写锁没有被当前线程持有，(如果当前线程没有持有锁，那么isHeldExclusively()是false，直接返回0)
        final int getWriteHoldCount() {
            return isHeldExclusively() ? exclusiveCount(getState()) : 0;
        }

        //获取当前线程持有读锁的个数
        final int getReadHoldCount() {
            //1.读锁加锁次数为0，那么直接返回0，最简单的情况
            if (getReadLockCount() == 0)
                return 0;
            //2.当前线程是第一个获取读锁的线程，返回firstReaderHoldCount即可
            Thread current = Thread.currentThread();
            if (firstReader == current)
                return firstReaderHoldCount;

            //3.如果当前线程是最后一个线程，那么返回最后一个线程持有读锁的个数，
            // (最后一个获取读锁的线程的持有锁信息保存在cachedHoldCounter变量)
            HoldCounter rh = cachedHoldCounter;
            if (rh != null && rh.tid == getThreadId(current))
                return rh.count;
            //4.如果前面几种情况都不是，那么就从readHolds中获取当前线程持有读锁的次数(利用了ThreadLocal机制)
            int count = readHolds.get().count;
            if (count == 0) readHolds.remove();
            return count;
        }

        /**
         * Reconstitutes the instance from a stream (that is, deserializes it).
         */
        private void readObject(java.io.ObjectInputStream s)
                throws java.io.IOException, ClassNotFoundException {
            s.defaultReadObject();
            readHolds = new java.util.concurrent.locks.ReentrantReadWriteLock.Sync.ThreadLocalHoldCounter();
            setState(0); // reset to unlocked state
        }

        final int getCount() {
            return getState();
        }
    }

    /**
     * Nonfair version of Sync
     */
    static final class NonfairSync extends java.util.concurrent.locks.ReentrantReadWriteLock.Sync {
        private static final long serialVersionUID = -8159625535654395037L;

        final boolean writerShouldBlock() {
            return false; // writers can always barge
        }

        final boolean readerShouldBlock() {
            /* As a heuristic to avoid indefinite writer starvation,
             * block if the thread that momentarily appears to be head
             * of queue, if one exists, is a waiting writer.  This is
             * only a probabilistic effect since a new reader will not
             * block if there is a waiting writer behind other enabled
             * readers that have not yet drained from the queue.
             */
            return apparentlyFirstQueuedIsExclusive();
        }
    }

    /**
     * Fair version of Sync
     */
    static final class FairSync extends java.util.concurrent.locks.ReentrantReadWriteLock.Sync {
        private static final long serialVersionUID = -2274990926593161451L;

        final boolean writerShouldBlock() {
            return hasQueuedPredecessors();
        }

        final boolean readerShouldBlock() {
            return hasQueuedPredecessors();
        }
    }

    /**
     * The lock returned by method {@link java.util.concurrent.locks.ReentrantReadWriteLock#readLock}.
     */
    public static class ReadLock implements Lock, java.io.Serializable {
        private static final long serialVersionUID = -5992448646407690164L;
        private final java.util.concurrent.locks.ReentrantReadWriteLock.Sync sync;

        /**
         * Constructor for use by subclasses
         *
         * @param lock the outer lock object
         * @throws NullPointerException if the lock is null
         *                              构造方法，传入Sync对象
         */
        protected ReadLock(java.util.concurrent.locks.ReentrantReadWriteLock lock) {
            sync = lock.sync;
        }

        /**
         * Acquires the read lock.
         *
         * <p>Acquires the read lock if the write lock is not held by
         * another thread and returns immediately.
         *
         * <p>If the write lock is held by another thread then
         * the current thread becomes disabled for thread scheduling
         * purposes and lies dormant until the read lock has been acquired.
         * 加锁方法，以共享方式获取同步状态变量state，因此读锁是共享的
         */
        public void lock() {
            sync.acquireShared(1);
        }

        /**
         * Acquires the read lock unless the current thread is
         * {@linkplain Thread#interrupt interrupted}.
         *
         * <p>Acquires the read lock if the write lock is not held
         * by another thread and returns immediately.
         *
         * <p>If the write lock is held by another thread then the
         * current thread becomes disabled for thread scheduling
         * purposes and lies dormant until one of two things happens:
         *
         * <ul>
         *
         * <li>The read lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread.
         *
         * </ul>
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method; or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the read lock,
         *
         * </ul>
         * <p>
         * then {@link InterruptedException} is thrown and the current
         * thread's interrupted status is cleared.
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock.
         *
         * @throws InterruptedException if the current thread is interrupted
         */
        public void lockInterruptibly() throws InterruptedException {
            sync.acquireSharedInterruptibly(1);
        }

        /**
         * Acquires the read lock only if the write lock is not held by
         * another thread at the time of invocation.
         *
         * <p>Acquires the read lock if the write lock is not held by
         * another thread and returns immediately with the value
         * {@code true}. Even when this lock has been set to use a
         * fair ordering policy, a call to {@code tryLock()}
         * <em>will</em> immediately acquire the read lock if it is
         * available, whether or not other threads are currently
         * waiting for the read lock.  This &quot;barging&quot; behavior
         * can be useful in certain circumstances, even though it
         * breaks fairness. If you want to honor the fairness setting
         * for this lock, then use {@link #tryLock(long, TimeUnit)
         * tryLock(0, TimeUnit.SECONDS) } which is almost equivalent
         * (it also detects interruption).
         *
         * <p>If the write lock is held by another thread then
         * this method will return immediately with the value
         * {@code false}.
         *
         * @return {@code true} if the read lock was acquired
         * <p>
         * 这里注意，尝试获取锁，即使是公平锁也会抢占，忽略排队的线程，
         * 目的是快速尝试是否能够获取锁，
         */
        public boolean tryLock() {
            return sync.tryReadLock();
        }

        /**
         * Acquires the read lock if the write lock is not held by
         * another thread within the given waiting time and the
         * current thread has not been {@linkplain Thread#interrupt
         * interrupted}.
         *
         * <p>Acquires the read lock if the write lock is not held by
         * another thread and returns immediately with the value
         * {@code true}. If this lock has been set to use a fair
         * ordering policy then an available lock <em>will not</em> be
         * acquired if any other threads are waiting for the
         * lock. This is in contrast to the {@link #tryLock()}
         * method. If you want a timed {@code tryLock} that does
         * permit barging on a fair lock then combine the timed and
         * un-timed forms together:
         *
         * <pre> {@code
         * if (lock.tryLock() ||
         *     lock.tryLock(timeout, unit)) {
         *   ...
         * }}</pre>
         *
         * <p>If the write lock is held by another thread then the
         * current thread becomes disabled for thread scheduling
         * purposes and lies dormant until one of three things happens:
         *
         * <ul>
         *
         * <li>The read lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread; or
         *
         * <li>The specified waiting time elapses.
         *
         * </ul>
         *
         * <p>If the read lock is acquired then the value {@code true} is
         * returned.
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method; or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the read lock,
         *
         * </ul> then {@link InterruptedException} is thrown and the
         * current thread's interrupted status is cleared.
         *
         * <p>If the specified waiting time elapses then the value
         * {@code false} is returned.  If the time is less than or
         * equal to zero, the method will not wait at all.
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock, and over reporting the elapse of the waiting time.
         *
         * @param timeout the time to wait for the read lock
         * @param unit    the time unit of the timeout argument
         * @return {@code true} if the read lock was acquired
         * @throws InterruptedException if the current thread is interrupted
         * @throws NullPointerException if the time unit is null
         *                              支持超时的tryLock
         */
        public boolean tryLock(long timeout, TimeUnit unit)
                throws InterruptedException {
            return sync.tryAcquireSharedNanos(1, unit.toNanos(timeout));
        }

        /**
         * Attempts to release this lock.
         *
         * <p>If the number of readers is now zero then the lock
         * is made available for write lock attempts.
         * 解锁，释放同步状态state
         */
        public void unlock() {
            sync.releaseShared(1);
        }

        /**
         * Throws {@code UnsupportedOperationException} because
         * {@code ReadLocks} do not support conditions.
         *
         * @throws UnsupportedOperationException always
         *                                       读锁不支持Condition，Condition是用来做线程的等待和唤醒的，对读锁而言，
         *                                       一把读锁多个线程是共享的，不存在相互阻塞唤醒的说法
         */
        public Condition newCondition() {
            throw new UnsupportedOperationException();
        }

        /**
         * Returns a string identifying this lock, as well as its lock state.
         * The state, in brackets, includes the String {@code "Read locks ="}
         * followed by the number of held read locks.
         *
         * @return a string identifying this lock, as well as its lock state
         */
        public String toString() {
            int r = sync.getReadLockCount();
            return super.toString() +
                    "[Read locks = " + r + "]";
        }
    }

    /**
     * The lock returned by method {@link java.util.concurrent.locks.ReentrantReadWriteLock#writeLock}.
     */
    public static class WriteLock implements Lock, java.io.Serializable {
        private static final long serialVersionUID = -4992448646407690164L;
        private final java.util.concurrent.locks.ReentrantReadWriteLock.Sync sync;

        /**
         * Constructor for use by subclasses
         *
         * @param lock the outer lock object
         * @throws NullPointerException if the lock is null
         *                              构造方法
         */
        protected WriteLock(java.util.concurrent.locks.ReentrantReadWriteLock lock) {
            sync = lock.sync;
        }

        /**
         * Acquires the write lock.
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately, setting the write lock hold count to
         * one.
         *
         * <p>If the current thread already holds the write lock then the
         * hold count is incremented by one and the method returns
         * immediately.
         *
         * <p>If the lock is held by another thread then the current
         * thread becomes disabled for thread scheduling purposes and
         * lies dormant until the write lock has been acquired, at which
         * time the write lock hold count is set to one.
         * 加锁，独占式获取同步状态state
         */
        public void lock() {
            sync.acquire(1);
        }

        /**
         * Acquires the write lock unless the current thread is
         * {@linkplain Thread#interrupt interrupted}.
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately, setting the write lock hold count to
         * one.
         *
         * <p>If the current thread already holds this lock then the
         * hold count is incremented by one and the method returns
         * immediately.
         *
         * <p>If the lock is held by another thread then the current
         * thread becomes disabled for thread scheduling purposes and
         * lies dormant until one of two things happens:
         *
         * <ul>
         *
         * <li>The write lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread.
         *
         * </ul>
         *
         * <p>If the write lock is acquired by the current thread then the
         * lock hold count is set to one.
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method;
         * or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the write lock,
         *
         * </ul>
         * <p>
         * then {@link InterruptedException} is thrown and the current
         * thread's interrupted status is cleared.
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock.
         *
         * @throws InterruptedException if the current thread is interrupted
         *                              响应字段的加锁方式
         */
        public void lockInterruptibly() throws InterruptedException {
            sync.acquireInterruptibly(1);
        }

        /**
         * Acquires the write lock only if it is not held by another thread
         * at the time of invocation.
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately with the value {@code true},
         * setting the write lock hold count to one. Even when this lock has
         * been set to use a fair ordering policy, a call to
         * {@code tryLock()} <em>will</em> immediately acquire the
         * lock if it is available, whether or not other threads are
         * currently waiting for the write lock.  This &quot;barging&quot;
         * behavior can be useful in certain circumstances, even
         * though it breaks fairness. If you want to honor the
         * fairness setting for this lock, then use {@link
         * #tryLock(long, TimeUnit) tryLock(0, TimeUnit.SECONDS) }
         * which is almost equivalent (it also detects interruption).
         *
         * <p>If the current thread already holds this lock then the
         * hold count is incremented by one and the method returns
         * {@code true}.
         *
         * <p>If the lock is held by another thread then this method
         * will return immediately with the value {@code false}.
         *
         * @return {@code true} if the lock was free and was acquired
         * by the current thread, or the write lock was already held
         * by the current thread; and {@code false} otherwise.
         * 尝试获取锁，
         */
        public boolean tryLock() {
            return sync.tryWriteLock();
        }

        /**
         * Acquires the write lock if it is not held by another thread
         * within the given waiting time and the current thread has
         * not been {@linkplain Thread#interrupt interrupted}.
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately with the value {@code true},
         * setting the write lock hold count to one. If this lock has been
         * set to use a fair ordering policy then an available lock
         * <em>will not</em> be acquired if any other threads are
         * waiting for the write lock. This is in contrast to the {@link
         * #tryLock()} method. If you want a timed {@code tryLock}
         * that does permit barging on a fair lock then combine the
         * timed and un-timed forms together:
         *
         * <pre> {@code
         * if (lock.tryLock() ||
         *     lock.tryLock(timeout, unit)) {
         *   ...
         * }}</pre>
         *
         * <p>If the current thread already holds this lock then the
         * hold count is incremented by one and the method returns
         * {@code true}.
         *
         * <p>If the lock is held by another thread then the current
         * thread becomes disabled for thread scheduling purposes and
         * lies dormant until one of three things happens:
         *
         * <ul>
         *
         * <li>The write lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread; or
         *
         * <li>The specified waiting time elapses
         *
         * </ul>
         *
         * <p>If the write lock is acquired then the value {@code true} is
         * returned and the write lock hold count is set to one.
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method;
         * or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the write lock,
         *
         * </ul>
         * <p>
         * then {@link InterruptedException} is thrown and the current
         * thread's interrupted status is cleared.
         *
         * <p>If the specified waiting time elapses then the value
         * {@code false} is returned.  If the time is less than or
         * equal to zero, the method will not wait at all.
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock, and over reporting the elapse of the waiting time.
         *
         * @param timeout the time to wait for the write lock
         * @param unit    the time unit of the timeout argument
         * @return {@code true} if the lock was free and was acquired
         * by the current thread, or the write lock was already held by the
         * current thread; and {@code false} if the waiting time
         * elapsed before the lock could be acquired.
         * @throws InterruptedException if the current thread is interrupted
         * @throws NullPointerException if the time unit is null
         *                              支持超时的尝试获取锁
         */
        public boolean tryLock(long timeout, TimeUnit unit)
                throws InterruptedException {
            return sync.tryAcquireNanos(1, unit.toNanos(timeout));
        }

        /**
         * Attempts to release this lock.
         *
         * <p>If the current thread is the holder of this lock then
         * the hold count is decremented. If the hold count is now
         * zero then the lock is released.  If the current thread is
         * not the holder of this lock then {@link
         * IllegalMonitorStateException} is thrown.
         *
         * @throws IllegalMonitorStateException if the current thread does not
         *                                      hold this lock
         *                                      解锁
         */
        public void unlock() {
            sync.release(1);
        }

        /**
         * Returns a {@link Condition} instance for use with this
         * {@link Lock} instance.
         * <p>The returned {@link Condition} instance supports the same
         * usages as do the {@link Object} monitor methods ({@link
         * Object#wait() wait}, {@link Object#notify notify}, and {@link
         * Object#notifyAll notifyAll}) when used with the built-in
         * monitor lock.
         *
         * <ul>
         *
         * <li>If this write lock is not held when any {@link
         * Condition} method is called then an {@link
         * IllegalMonitorStateException} is thrown.  (Read locks are
         * held independently of write locks, so are not checked or
         * affected. However it is essentially always an error to
         * invoke a condition waiting method when the current thread
         * has also acquired read locks, since other threads that
         * could unblock it will not be able to acquire the write
         * lock.)
         *
         * <li>When the condition {@linkplain Condition#await() waiting}
         * methods are called the write lock is released and, before
         * they return, the write lock is reacquired and the lock hold
         * count restored to what it was when the method was called.
         *
         * <li>If a thread is {@linkplain Thread#interrupt interrupted} while
         * waiting then the wait will terminate, an {@link
         * InterruptedException} will be thrown, and the thread's
         * interrupted status will be cleared.
         *
         * <li> Waiting threads are signalled in FIFO order.
         *
         * <li>The ordering of lock reacquisition for threads returning
         * from waiting methods is the same as for threads initially
         * acquiring the lock, which is in the default case not specified,
         * but for <em>fair</em> locks favors those threads that have been
         * waiting the longest.
         *
         * </ul>
         *
         * @return the Condition object
         * 返回Condition对象
         */
        public Condition newCondition() {
            return sync.newCondition();
        }

        /**
         * Returns a string identifying this lock, as well as its lock
         * state.  The state, in brackets includes either the String
         * {@code "Unlocked"} or the String {@code "Locked by"}
         * followed by the {@linkplain Thread#getName name} of the owning thread.
         *
         * @return a string identifying this lock, as well as its lock state
         */
        public String toString() {
            Thread o = sync.getOwner();
            return super.toString() + ((o == null) ?
                    "[Unlocked]" :
                    "[Locked by thread " + o.getName() + "]");
        }

        /**
         * Queries if this write lock is held by the current thread.
         * Identical in effect to {@link
         * java.util.concurrent.locks.ReentrantReadWriteLock#isWriteLockedByCurrentThread}.
         *
         * @return {@code true} if the current thread holds this lock and
         * {@code false} otherwise
         * @since 1.6
         * 判断锁是否被当前线程持有
         */
        public boolean isHeldByCurrentThread() {
            return sync.isHeldExclusively();
        }

        /**
         * Queries the number of holds on this write lock by the current
         * thread.  A thread has a hold on a lock for each lock action
         * that is not matched by an unlock action.  Identical in effect
         * to {@link java.util.concurrent.locks.ReentrantReadWriteLock#getWriteHoldCount}.
         *
         * @return the number of holds on this lock by the current thread,
         * or zero if this lock is not held by the current thread
         * @since 1.6
         * 判断当前线程重入写锁的次数
         */
        public int getHoldCount() {
            return sync.getWriteHoldCount();
        }
    }

    // Instrumentation and status

    /**
     * Returns {@code true} if this lock has fairness set true.
     *
     * @return {@code true} if this lock has fairness set true
     * 判断公平模式
     */
    public final boolean isFair() {
        return sync instanceof java.util.concurrent.locks.ReentrantReadWriteLock.FairSync;
    }

    /**
     * Returns the thread that currently owns the write lock, or
     * {@code null} if not owned. When this method is called by a
     * thread that is not the owner, the return value reflects a
     * best-effort approximation of current lock status. For example,
     * the owner may be momentarily {@code null} even if there are
     * threads trying to acquire the lock but have not yet done so.
     * This method is designed to facilitate construction of
     * subclasses that provide more extensive lock monitoring
     * facilities.
     *
     * @return the owner, or {@code null} if not owned
     * 获取锁的拥有线程
     */
    protected Thread getOwner() {
        return sync.getOwner();
    }

    /**
     * Queries the number of read locks held for this lock. This
     * method is designed for use in monitoring system state, not for
     * synchronization control.
     *
     * @return the number of read locks held
     * 返回读锁的持有个数，用于系统状态检测，不是用于线程同步
     */
    public int getReadLockCount() {
        return sync.getReadLockCount();
    }

    /**
     * Queries if the write lock is held by any thread. This method is
     * designed for use in monitoring system state, not for
     * synchronization control.
     *
     * @return {@code true} if any thread holds the write lock and
     * {@code false} otherwise
     * 判断写锁是否被其他线程持有
     */
    public boolean isWriteLocked() {
        return sync.isWriteLocked();
    }

    /**
     * Queries if the write lock is held by the current thread.
     *
     * @return {@code true} if the current thread holds the write lock and
     * {@code false} otherwise
     * 判断写锁是否被当前线程持有
     */
    public boolean isWriteLockedByCurrentThread() {
        return sync.isHeldExclusively();
    }

    /**
     * Queries the number of reentrant write holds on this lock by the
     * current thread.  A writer thread has a hold on a lock for
     * each lock action that is not matched by an unlock action.
     *
     * @return the number of holds on the write lock by the current thread,
     * or zero if the write lock is not held by the current thread
     * 判断当前线程重入写锁的次数
     */
    public int getWriteHoldCount() {
        return sync.getWriteHoldCount();
    }

    /**
     * Queries the number of reentrant read holds on this lock by the
     * current thread.  A reader thread has a hold on a lock for
     * each lock action that is not matched by an unlock action.
     *
     * @return the number of holds on the read lock by the current thread,
     * or zero if the read lock is not held by the current thread
     * @since 1.6
     * 判断读锁被持有的数量
     */
    public int getReadHoldCount() {
        return sync.getReadHoldCount();
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire the write lock.  Because the actual set of threads may
     * change dynamically while constructing this result, the returned
     * collection is only a best-effort estimate.  The elements of the
     * returned collection are in no particular order.  This method is
     * designed to facilitate construction of subclasses that provide
     * more extensive lock monitoring facilities.
     *
     * @return the collection of threads
     * 获取等待写锁的线程的集合，只是一个估计值
     */
    protected Collection<Thread> getQueuedWriterThreads() {
        return sync.getExclusiveQueuedThreads();
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire the read lock.  Because the actual set of threads may
     * change dynamically while constructing this result, the returned
     * collection is only a best-effort estimate.  The elements of the
     * returned collection are in no particular order.  This method is
     * designed to facilitate construction of subclasses that provide
     * more extensive lock monitoring facilities.
     *
     * @return the collection of threads
     * 获取等待读锁的线程的集合，只是一个估计值
     */
    protected Collection<Thread> getQueuedReaderThreads() {
        return sync.getSharedQueuedThreads();
    }

    /**
     * Queries whether any threads are waiting to acquire the read or
     * write lock. Note that because cancellations may occur at any
     * time, a {@code true} return does not guarantee that any other
     * thread will ever acquire a lock.  This method is designed
     * primarily for use in monitoring of the system state.
     *
     * @return {@code true} if there may be other threads waiting to
     * acquire the lock
     * 判断是否有线程等待锁(读锁或者写锁)
     */
    public final boolean hasQueuedThreads() {
        return sync.hasQueuedThreads();
    }

    /**
     * Queries whether the given thread is waiting to acquire either
     * the read or write lock. Note that because cancellations may
     * occur at any time, a {@code true} return does not guarantee
     * that this thread will ever acquire a lock.  This method is
     * designed primarily for use in monitoring of the system state.
     *
     * @param thread the thread
     * @return {@code true} if the given thread is queued waiting for this lock
     * @throws NullPointerException if the thread is null
     *                              判断某线程是否在等待锁(读锁或者写锁)
     */
    public final boolean hasQueuedThread(Thread thread) {
        return sync.isQueued(thread);
    }

    /**
     * Returns an estimate of the number of threads waiting to acquire
     * either the read or write lock.  The value is only an estimate
     * because the number of threads may change dynamically while this
     * method traverses internal data structures.  This method is
     * designed for use in monitoring of the system state, not for
     * synchronization control.
     *
     * @return the estimated number of threads waiting for this lock
     * 获取等待锁线程的数量估计值
     */
    public final int getQueueLength() {
        return sync.getQueueLength();
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire either the read or write lock.  Because the actual set
     * of threads may change dynamically while constructing this
     * result, the returned collection is only a best-effort estimate.
     * The elements of the returned collection are in no particular
     * order.  This method is designed to facilitate construction of
     * subclasses that provide more extensive monitoring facilities.
     *
     * @return the collection of threads
     */
    protected Collection<Thread> getQueuedThreads() {
        return sync.getQueuedThreads();
    }

    /**
     * Queries whether any threads are waiting on the given condition
     * associated with the write lock. Note that because timeouts and
     * interrupts may occur at any time, a {@code true} return does
     * not guarantee that a future {@code signal} will awaken any
     * threads.  This method is designed primarily for use in
     * monitoring of the system state.
     *
     * @param condition the condition
     * @return {@code true} if there are any waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException     if the given condition is
     *                                      not associated with this lock
     * @throws NullPointerException         if the condition is null
     *                                      判断是否有线程等待在Condition上
     */
    public boolean hasWaiters(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.hasWaiters((AbstractQueuedSynchronizer.ConditionObject) condition);
    }

    /**
     * Returns an estimate of the number of threads waiting on the
     * given condition associated with the write lock. Note that because
     * timeouts and interrupts may occur at any time, the estimate
     * serves only as an upper bound on the actual number of waiters.
     * This method is designed for use in monitoring of the system
     * state, not for synchronization control.
     *
     * @param condition the condition
     * @return the estimated number of waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException     if the given condition is
     *                                      not associated with this lock
     * @throws NullPointerException         if the condition is null
     */
    public int getWaitQueueLength(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitQueueLength((AbstractQueuedSynchronizer.ConditionObject) condition);
    }

    /**
     * Returns a collection containing those threads that may be
     * waiting on the given condition associated with the write lock.
     * Because the actual set of threads may change dynamically while
     * constructing this result, the returned collection is only a
     * best-effort estimate. The elements of the returned collection
     * are in no particular order.  This method is designed to
     * facilitate construction of subclasses that provide more
     * extensive condition monitoring facilities.
     *
     * @param condition the condition
     * @return the collection of threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException     if the given condition is
     *                                      not associated with this lock
     * @throws NullPointerException         if the condition is null
     */
    protected Collection<Thread> getWaitingThreads(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitingThreads((AbstractQueuedSynchronizer.ConditionObject) condition);
    }

    /**
     * Returns a string identifying this lock, as well as its lock state.
     * The state, in brackets, includes the String {@code "Write locks ="}
     * followed by the number of reentrantly held write locks, and the
     * String {@code "Read locks ="} followed by the number of held
     * read locks.
     *
     * @return a string identifying this lock, as well as its lock state
     */
    public String toString() {
        int c = sync.getCount();
        int w = java.util.concurrent.locks.ReentrantReadWriteLock.Sync.exclusiveCount(c);
        int r = java.util.concurrent.locks.ReentrantReadWriteLock.Sync.sharedCount(c);

        return super.toString() +
                "[Write locks = " + w + ", Read locks = " + r + "]";
    }

    /**
     * Returns the thread id for the given thread.  We must access
     * this directly rather than via method Thread.getId() because
     * getId() is not final, and has been known to be overridden in
     * ways that do not preserve unique mappings.
     */
    static final long getThreadId(Thread thread) {
        return UNSAFE.getLongVolatile(thread, TID_OFFSET);
    }

    // Unsafe mechanics
    private static final sun.misc.Unsafe UNSAFE;
    private static final long TID_OFFSET;

    static {
        try {
            UNSAFE = sun.misc.Unsafe.getUnsafe();
            Class<?> tk = Thread.class;
            TID_OFFSET = UNSAFE.objectFieldOffset
                    (tk.getDeclaredField("tid"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

}
