# CAS (casp包)
## 一.概念
>* Compare And Swap（比较并交换），是 J.U.C 的基石，是乐观锁的一种实现方式，乐观锁
适用场景是读多写少的场景，修改较少，如果修改频繁，那么自旋将耗费很多性能，按照阿里云
的开发规范：
```
并发修改同一记录时，避免更新丢失，需要加锁。要么在应用层加锁，要么在缓存加
锁，要么在数据库层使用乐观锁，使用 version 作为更新依据。
说明：如果每次访问冲突概率小于 20%，推荐使用乐观锁，否则使用悲观锁。乐观锁的重试次
数不得小于 3 次
```
![Image text](../pic/concurrent/cas.jpg)

## 二.作用和使用场景
###### 以AtomicInteger类举例，里面的getAndAdd方法，作用是获取值并且加上某个值，比如:
```
        AtomicInteger atomicInteger = new AtomicInteger();
        System.out.println(atomicInteger.addAndGet(2));
```
###### 上面2行代码，返回的就是2(初始值是0),分析addAndGet方法如下:
 ```
 public final int addAndGet(int delta) {
           return unsafe.getAndAddInt(this, valueOffset, delta) + delta;
       }
 ```
###### 这里的unsafe是一个类，它提供了硬件级别的原子操作,跟进getAndAddInt方法：
```
 public final int getAndAddInt(Object var1, long var2, int var4) {
            int var5;
            do {
                var5 = this.getIntVolatile(var1, var2); //1
            } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4)); //2
    
            return var5;
        }

    这里的核心是：!this.compareAndSwapInt(var1, var2, var5, var5 + var4)这个方法
    这是本地方法看不到代码，这里是一直做死循环，知道这个方法成功才会退出，这里得参数含义是
    var1:当前对象，var2，偏移地址，var4，新增值，var5，期望值，即当前值，
    假定第一步获取的值为3，如果有其他线程修改了这个地址偏移处的值为4，那么第2步这里就会失败，
    因为第2步在尝试修改的时候，发现这里的实际值是4，与本线程所期望的3不一致，就会再次执行一次
    获取和修改的操作，假如没有其他线程修改，再次执行步骤1就会获取到4，修改也会成功。
    这里的核心思想就是先获取值(1)，在修改(2)，原本这两步不是原子的，这样通过一个期望值的对比，
    只要过程中其他线程修改了值，就会感知到，因此将2个步骤变成了原子的了，而步骤2本就是原子的，
    这里的原子操作是在硬件的CPU指令层面实现的，这样就保证了AtomicInteger类在多线程环境下的安
    全；
 ```  
###### 这里看出来了为什么说CAS是作为一个乐观锁的实现，因为他会一直重试，因此适用于冲突概率较低的
场景(20%)，它也有他的缺点
```
    PS：缺点
    1.循环时间太长(循环时间其实并不可控，极端情况下占用CPU做无用功)
    2.只能保证一个共享变量原子操作(只能对一个变量做原子保证)
    3.ABA 问题(CAS无法解决ABA问题)

```
## 二.ABA问题
###### 问题描述
```$xslt
在上面的代码步骤2中，如果其他线程修改了var5为另外的值，然后再把var5修改回来，那么在
本线程是感知不到的，这就是ABA问题，就是变量被修改了，然后有改回来了，虽然值一样但是我们认为这
也是被修改了。
```
###### ABA问题解决方法(AtomicStampedReference类)
```$xslt
    PS:解决思路:每次修改变量，递增一个版本戳stamp，核心方法如下：
    public boolean compareAndSet(V   expectedReference,
                                     V   newReference,
                                     int expectedStamp,
                                     int newStamp) {
            Pair<V> current = pair;
            return
                expectedReference == current.reference &&   //1
                expectedStamp == current.stamp &&           //2
                ((newReference == current.reference &&      //3
                  newStamp == current.stamp) ||             //4
                 casPair(current, Pair.of(newReference, newStamp)));  //5
    }
    参数释义：
    expectedReference：预期引用
    newReference：更新后的引用
    newReference：预期标志
    newReference：更新后的标志
    借助一个内部类Pair来实现，其实对比的就是引用和标志这两个属性，
    代码解析：
    行1和行2的含义是：传进来的期望对象(即期望引用)应该和内部的current对象保持一致，换言之要
    进行修改，首先比如是同一个对象，并且是同一个版本号，否则不考虑直接返回false，这2者是前提
    行3和行4是一个条件，这个条件以意味着传入的对象和版本号和current一样，换言之根本就没有更改，
    这种情况直接返回true
    行5就是修改current为新的值，新值是通过Pair.of方法创建的，这里通过底层的方法来修改current；
    因此这里34的条件和5之间是逻辑或的关系，前者满足就直接返回，不满足再尝试修改current。
    小结：通过AtomicStampedReference类来解决ABA的问题，大概思路是AtomicStampedReference类
    内部可以维护一个对象和该对象的版本号，每次修改都会使得版本号自增1，修改时会比对版本号，版本号
    不一致时则修改失败。
```


## 三.ABA问题示例代码
