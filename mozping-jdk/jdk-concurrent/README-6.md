# mozping-concurrent
## 一、并发容器    
### 1.线程不安全容器
+ HashMap-线程不安全，put操作可能引起死循环，让Entry链表产生环形数据结构，使用Entry.next的时候没有尾部，进入死循环
+ HashTable-方法全部用synchronized同步，效率低  
### 2. ConcurrentHashMap 
+ 实现上，jdk1.7和1.8有不同
#### 2.1 ConcurrentHashMap-1.7
+ 每个segment里面有一个table，一个table的每个位置后面挂着一个Entry的链表，每个segment都是可重入锁(ReentrantLock)的实现类，  
+ 根据初始大小和并发度来确定segment的数量
+ 为了快速定位key在哪一个segment，根据hash值计算，
+ 存储节点的数据结构是Entry

#### 2.2 ConcurrentHashMap-1.8
+ 取消了segment，直接用table数组保存数据，锁粒度更小
+ 采用table数组+红黑树/链表的形式实现，和HashTable设计类似，转红黑树的阈值是>=8，转为链表的阈值是<=6
+ 存储节点的数据结构是Node类，继承自Entry
+ 初始化：给成员变量赋初始值，put操作的时候才会

 

#### 2.3 get方法(1.7)
+ 定位segment，key的hashcode再散列值的高位，
+ 定位table，key的hashcode再散列，取模，
+ 依次扫描链表，要么找到元素，要么返回null
+ get方法不需要加锁，因为Entry的字段是volatile，别的线程修改，本线程可以立刻感知到
+ segment不扩容，只会扩容table数组
+ size的时候进行两次不加锁的统计，如果统计一致就直接返回结果，如果不一致，就会重新加锁再次统计，因此尽量少使用size操作
+ put方法会覆盖，会直接返回已存在的旧值，把新的值存进去，覆盖使用replace方法

