# mozping-concurrent
## 一、线程基础    
### 1.并发和并行
+ 并发是单位时间内可以处理的事件数目
+ 并行是同一时刻，可以同时运行的线程数量
+ 如果有10个cpu核心，同时可以运行10个线程，那么并行度就是10，每一个任务处理是5ms，那么
  每秒并发就是10*200=2000     
+ 查看java程序中的线程：OnlyMain.java

### 2.创建和停止java线程
#### 2.1 创建
+ 继承Thread类: 没有返回值（单继承）
+ 实现Runnable: 没有返回值（多接口实现）
+ 实现Callable: 可以返回结果
#### 2.2 停止
+ 线程执行完毕自然停止
+ 抛出异常
+ stop方法(不会释放资源)
+ interrupt:线程之间是协作式的，该方法并不会立即停止该线程，类似于告知该线程，尽快停止，线程何时
停止，何时停止，则由线程本身决定(线程本身有足够的灵活度来进行停止线程前的处理)，将线程的中断标志位值
true
+ 示例：com.intellif.mozping.createthread.EndThread
#### 2.3 比较下面三者的区别：

t1.interrupt(); 

t1.isInterrupted();  

Thread.interrupted();   
        
```
interrupt();
    --中断线程，将标志位置为true
isInterrupted();
    --判断当前线程是否处于中断状态,调用的是private native boolean isInterrupted(false);
Thread.interrupted();
    --判断当前线程是否处于中断状态,修改标志位为false  private native boolean isInterrupted(true);

    /**
     * Tests if some Thread has been interrupted.  The interrupted state
     * is reset or not based on the value of ClearInterrupted that is
     * passed.
     * 由传入的boolean值决定是否会修改字段标志位，isInterrupted不会修改，静态interrupted方法会修改
     */
    private native boolean isInterrupted(boolean ClearInterrupted);
    
    方法2是Thread类提供的方法，如果线程是继承Thread类，可以直接调用方法2，如果是实现的Runnable接口，
    那么该类的内部只能Thread.currentThread().isInterrupted(),先获取当前线程对象，再调用方法，
    方法3没有关系，因为是静态方法，方法内部就是通过Thread.currentThread()先获取的当前线程对象
    方法1通常是通过线程对象调用，线程对象都是Thread类对象，都可以调用，只有在Runnable接口的现象类
    的内部无法直接调用interrupted
```

#### 2.4 InterruptedException异常：
如果方法会抛出该异常，那么会将中断标志位置为false，如此一来，外部调用的中断信号不会生效，该线程并不会停止，
对应的处理方法是，线程内部捕获到该异常后，在catch到异常后主动调用interupt方法，将自身中断标志位置为true
会抛出该异常的方法有：sleep，join
+ 示例：com.intellif.mozping.createthread.EndThreadInterruptException
InterruptedException异常：常用的阻塞方法都会抛出该异常，此时可以感知到外部中断该线程(外部调用该线程的interrupt
方法，线程内部会抛出异常，并将标志位置为false)，如果采用自定义标志作为线程循环处理的标记，那么仅仅判断标记是不够的
因为循环内部可能阻塞，阻塞处就无法及时感知到外部标志的变化，这里的判断条件还需要加上该线程的标志位，这样一来只要外
部发出了中断该线程的信号，循环条件就可以感知到，内部的阻塞方法会抛出InterruptedException异常，

 
### 3.线程的生命周期
#### 3.1 示图
![图片alt](pic/thread.jpg)
#### 3.1 周期转换过程的方法
+ yield: 使当前线程从执行状态（运行状态）变为可执行态（就绪状态）。cpu会从众多的可执行态里选择，也就是说，当前
也就是刚刚的那个线程还是有可能会被再次执行到的，并不是说一定会执行其他线程而该线程在下一次中不会执行到了。用了
yield方法后，该线程就会把CPU时间让掉，让其他或者自己的线程执行（也就是谁先抢到谁执行）



### 4.synchronized关键字
+ synchronized是对象锁，锁住的是实例对象，不通的对象锁之间可以并行
+ 类锁，锁的对象是当前类的class对象，class对象在JVM中只有一个，因此该类的所有静态方法会被同步
+ 从上所述，类锁和实例锁锁的方法之间可以相互交替执行，他们的锁对象是不同的，可以并行


### 4.volatile关键字
+ 更新后直接刷回主内存
+ 读取时直接去主内存加载
+ 只能保证可见性不能保证原子性
+ volatile最适用的场景：一个线程写，多个线程读
+ 示例：com.intellif.mozping.volatilep.VolatileTest


### 5.ThreadLocal线程本地变量
+ 示例：com.intellif.mozping.threadlocal.ThreadLocalTest


## 二、线程间协作(threadcooperation)  
### 1.线程间等待和通知机制
#### 1.1 示例  
+ 示例：com.intellif.mozping.threadcooperation.waitandnotify.WaitAndNotify
+ 示例：com.intellif.mozping.threadcooperation.waitandnotify.Express
#### 1.2  等待超时模式模拟连接池
```
超时模式范式：
如果等待市场为T，当前时间为now，那么now+T以后超时
long overtime = now+T
long remain = T
while(result不满足 && remian>0){
    wait(remian); //等待，最多等待remian的时间
    remain = overtime - now;
}
   return result;
```
+ 示例：com.intellif.mozping.threadcooperation.pool.MyConnection
#### 1.3 join方法(线程排队)
+ join方法保证一个线程在另一个线程之后执行，可能因为前面的方法阻塞过久导致后面的方法得不到执行

```aidl
yeild：不释放锁 ，让出cpu时间片，但是让出之后有可能再次获得锁，sleep则不会，这是最大的区别
sleep：不释放锁，让出cpu时间片，同步块中尽量不要sleep，否则占锁不释放
wait：调用时，必须占用锁，调用之后，锁会立刻释放，方法返回后会重新占用锁，这个过程通常在同步代码块中执行，这里是JVM保证的，但是需要了解该过程上面方法对锁的影响，也就是wait的前后都是持锁的状态，只有wait的过程中才是不持有锁的
notify：调用时 ，必须占用锁，调用之后不会释放锁，需要同步代码块执行完毕之后才释放锁，通常该方法写在同步代码块的最后
小结：
yeild和sleep调用时不需要持有锁，wait和notify需要
如果持有锁，yeild和sleep调用时不释放锁，wait会释放但是返回后会重新获得，notify需要在同步代码块执行完毕之后才释放锁
notifyAll类似于notify
```



## 三、并发编程工具类(concurrentutil)
### 1.fork-join思想(fork-join子包)
+ 分而治之；N规模的问题可以直接解决，那么对于大规模的问题就可以降低器规模分别解决，
要求子问题之间相互独立，子问题与母问题有相似的形式
动态规划中子问题和母问题是有联系的，这点是和分而治之的不同
+ 工作密取：对于问题规模相同的子问题，一个先完成一个后完成，先完成的线程协助处理未完成
的任务，成为工作密取
#### 1.1 fork-join实战
ForkJoinTask接口
ForkJoinTask的子类：RecursiveTask/RecursiveAction都是抽象类，需要自行实现
RecursiveTask有返回值
RecursiveAction无返回值

#### 1.2 fork-join的标准范式
ForkJoinPool pool = new ForkJoinPool(); //创建pool
MyTask task = new MyTask();//创建自己的任务MyTask是继承自RecursiveTask或者RecursiveAction
pool.invoke(innerFind);//同步调用
innerFind.join();//获取结果
代码在fokejoin包下

#### 1.3 CountDownLatch
+ 一个线程等待其他线程执行完后再执行，加强版join
+ 示例：concurrentutil的CountDownLatchp子包



#### 1.4 CyclicBarrier
+ 让一组线程执行到某一个屏障点之后，这一组线程才可以继续执行


#### 1.5 CountDownLatch和CyclicBarrier的区别
CountDownLatch取决于外部，需要外部的线程执行完毕，自身才能继续执行。方行条件通常>=线程数，开发人员自身决定扣减
CyclicBarrier：取决于自身，自身的多个线程到达指定屏障点，然后才可以继续执行，放行条件通常=线程数
CountDownLatch和CyclicBarrier的源码：依赖于一些同步器和AQS等底层原理

cyclicBarrier.await()方法会返回一个整数，这个这个数的计算方式是getParties() - 1
如果有3个线程，那么第一个到达栅栏执行await方法的线程返回的是2，最后一个返回的是0，
相当于依次减少。如果没有指定barrierAction线程，可以通过这个值来在前面的线程里面选择一个合适的线程来执行后面的操作，在CyclicBarrier里面的注释有说明

#### 1.6 Semaphore
+ 控制同时访问某个特定资源的线程数量，如流量控制，数据库访问线程数控制等



#### 1.7 Exchange
+ 两个线程之间交换数据















## 常用小方法
```
获取线程名称：Thread.currentThread().getName();
线程优先级：t2.setPriority(); 部分操作系统会忽略优先级，优先级高不保证更多的执行机会，值为1-10，
守护线程：t2.setDaemon(true);没有非守护线程执行时，守护线程都会退出，做一些资源回收，资源调度之类。
守护线程的finally语句不能保证执行

```
+ 示例：com.intellif.mozping.createthread.DaemonThread
