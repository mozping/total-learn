package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

@SpringBootApplication
@RestController
public class MozpingPackageRunApplication {

    @GetMapping
    public String test() {
        return (new Date()) + "-->" + UUID.randomUUID().toString();
    }

    public static void main(String[] args) {
        SpringApplication.run(MozpingPackageRunApplication.class, args);
    }

}
