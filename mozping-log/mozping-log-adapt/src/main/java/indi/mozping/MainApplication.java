package indi.mozping;

import indi.mozping.logging.Log;
import indi.mozping.logging.IntellifLogFactory;

/**
 * @author by mozping
 * @Classname MainApplication
 * @Description TODO
 * @Date 2019/3/18 19:42
 */
public class MainApplication {

    private static final Log LOG = IntellifLogFactory.getLog(MainApplication.class);

    public static void main(String[] args) {
        LOG.trace("trace 测试日志...");
        LOG.debug("debug 测试日志...");
        //LOG.info("info 测试日志...");
        LOG.warn("warn 测试日志...");
        LOG.error("error 测试日志...");
    }

}
