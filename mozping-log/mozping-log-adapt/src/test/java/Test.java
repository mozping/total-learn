/**
 * @author by mozping
 * @Classname Test
 * @Description TODO
 * @Date 2019/3/19 10:47
 */
public class Test {

    /**
     * kafka-simple-consumer-shell.sh --topic __consumer_offsets --partition 100 --broker-list localhost:9092 --formatter "kafka.coordinator.group.GroupMetadataManager\$OffsetsMessageFormatter"
     */

    public static void main(String[] args) {
        String groupId1 = "ifaas-collection";
        String groupId2 = "ifaas-data";
        int numPartitions = 50;
        int result1 = Math.abs(groupId1.hashCode()) % numPartitions;
        int result2 = Math.abs(groupId2.hashCode()) % numPartitions;
        System.out.println(result1);
        System.out.println(result2);
    }
}
