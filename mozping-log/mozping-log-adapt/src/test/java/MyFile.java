import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author by mozping
 * @Classname MyFile
 * @Description TODO
 * @Date 2019/3/19 11:16
 */
public class MyFile {

    public static String sourceFilePath = ".//src.txt";
    public static String desFilePath = ".//out.txt";
    public static String targetStr = "alalyze-progress";
    public static String BEGIN = "CommitTime";
    public static String END = "ExpirationTime";

    public static void main(String[] args) throws IOException {
        FileWriter fw = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFilePath)));
            fw = new FileWriter(desFilePath, true);
            StringBuffer sb = new StringBuffer();
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.contains(targetStr)) {
                    //包含目标topic的才需要处理
                    //System.out.println("读取到:" + line);
                    sb.append(line, 0, line.indexOf(BEGIN) + 10);
                    String beginTime = line.substring(line.indexOf(BEGIN) + 10, line.indexOf(END) - 1);
                    sb.append(" " + getDateStrByUnixTime(Long.valueOf(beginTime.trim())));
                    sb.append("," + END + " ");
                    String endTime = line.substring(line.indexOf(END) + 14, line.length() - 1);
                    sb.append(getDateStrByUnixTime(Long.valueOf(endTime.trim())));
                    sb.append("]");
                    fw.write(sb.toString() + "\r\n");
                    //System.out.println(sb.toString() + "\r\n");
                    sb.setLength(0);
                }
            }
            fw.close();
        } catch (IOException e1) {
            e1.printStackTrace();
            System.out.println("写入失败");
        } finally {
            fw.close();
            br.close();
        }
    }

    public static String getDateStrByUnixTime(long unixTime) {
        Date dateTmp = new Date(unixTime);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(dateTmp);
    }
}
