package indi.mozping.logs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @author by mozping
 * @Classname LogTest
 * @Description TODO
 * @Date 2019/2/13 17:06
 */
public class LogTest {

    private static final Logger LOG = LoggerFactory.getLogger(LogTest.class.getName());

    public static void main(String[] args) {
        System.out.println(LOG.getClass());
        LOG.debug("test:" + new Date());
        LOG.info("test:" + new Date());
        LOG.warn("test:" + new Date());
        LOG.error("test:" + new Date());

    }
}
