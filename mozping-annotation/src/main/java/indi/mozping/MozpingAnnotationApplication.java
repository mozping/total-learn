package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MozpingAnnotationApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingAnnotationApplication.class, args);
    }

}
