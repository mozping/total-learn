package indi.mozping.dao;

import org.springframework.stereotype.Service;

/**
 * @author by intellif
 * @Classname com.intellif.mozping.dao.AccessDB
 * @Description TODO
 * @Date 2018/12/4 19:42
 */
@Service
public interface AccessDB {


    /**
     * @Description: 访问数据库的方法
     * @date 2018/12/4 19:43
     */
    String operator();

}
