package indi.mozping.controller;


import indi.mozping.annotations.EnableCountTimeIntellif;
import indi.mozping.annotations.EnableLogIntellif;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by intellif
 * @Classname HelloController
 * @Description TODO
 * @Date 2018/12/4 20:01
 */
@RestController
@RequestMapping("/test")
public class HelloController {

    /*@Autowired
    AccessDB accessDB;

    @GetMapping("/accessdb")
    public String getAccessDB() {
        return accessDB.operator();
    }
    */

    @EnableCountTimeIntellif
    @GetMapping("/a")
    public String testA() {
        testB("123", 1);
        return "hello a!";
    }

    @EnableLogIntellif
    @GetMapping("/b")
    public String testB(String str, int a) {
        System.out.println("testB的参数是：" + str + "---" + a);
        return "hahaha";
    }

}
