package indi.mozping.annotations;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname EnableCountTimeAspect
 * @Description TODO
 * @Date 2018/12/27 14:13
 */
@Component
@Aspect
public class EnableCountTimeAspect {


    @Around("@annotation(ect)")
    public Object doAround(ProceedingJoinPoint pjp, EnableCountTimeIntellif ect) throws Throwable {
        long begin = System.currentTimeMillis();
        Object obj = pjp.proceed();
        String methodName = pjp.getSignature().getName();
        String className = pjp.getSignature().getDeclaringTypeName();
        System.out.println(className + "." + methodName + " 方法消耗时间：" + (System.currentTimeMillis() - begin) + " ms");
        return obj;
    }
}
