package indi.mozping.annotations;

import org.apache.commons.lang.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * @author by mozping
 * @Classname EnableCountTimeIntellif
 * @Description 记录日志的注解
 * @Date 2018/12/27 14:02
 */
@Component
@Aspect
public class EnableLogAspect {

    @Around("@annotation(eli)")
    public Object doAround(ProceedingJoinPoint pjp, EnableLogIntellif eli) throws Throwable {


        //1.这里获取到所有的参数值的数组
        Object[] methodArgs = pjp.getArgs();
        for (Object o : methodArgs) {
            System.out.println(o);
        }

        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        //2.最关键的一步:通过这获取到方法的所有参数名称的字符串数组
        String[] parameterNames = methodSignature.getParameterNames();

        try {
            //3.通过你需要获取的参数名称的下标获取到对应的值
            int index = ArrayUtils.indexOf(parameterNames, "type");
            String tableType;
            if (index != -1) {
                tableType = (String) methodArgs[index];
                //记录日志
                System.out.println(" 连接的表是:" + tableType);
            }
        } catch (Exception ex) {
            System.out.println("切面获取表名称异常，跳过日志记录");
        }
        Object obj = pjp.proceed();

        return obj;
    }

}
