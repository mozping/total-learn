package indi.mozping.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author by mozping
 * @Classname EnableCountTimeIntellif
 * @Description 添加缓存的注解
 * @Date 2018/12/27 14:02
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableCacheIntellif {


}
