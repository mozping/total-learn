package indi.mozping.entity;

import com.arangodb.entity.DocumentField;
import lombok.Data;

import java.io.Serializable;

/**
 * 点
 * @author percyl
 */
@Data
public class PersonDetail implements Serializable {

    @DocumentField(DocumentField.Type.ID)
    private String id;

    @DocumentField(DocumentField.Type.KEY)
    private String key;

    @DocumentField(DocumentField.Type.REV)
    private String revision;

    private String pid;
    private String createTime;
    private String data;

    public PersonDetail(String key, String pid, String createTime, String data) {
        this.key = key;
        this.pid = pid;
        this.createTime = createTime;
        this.data = data;
    }

}