package indi.mozping.entity;

import com.arangodb.entity.DocumentField;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 边
 *
 * @author percyl
 */
@Data
public class EdgeDetail implements Serializable {

    @DocumentField(DocumentField.Type.ID)
    private String id;

    @DocumentField(DocumentField.Type.KEY)
    private String key;

    @DocumentField(DocumentField.Type.REV)
    private String revision;

    @DocumentField(DocumentField.Type.FROM)
    private String from;

    @DocumentField(DocumentField.Type.TO)
    private String to;

    private String dt;
    private String fromId;
    private String targetId;
    private String label;
    private String direction;
    private String createTime;
    private String modifyTime;
    private Map<String, Object> props;

    public EdgeDetail(String key, String from, String to, String fromId, String targetId, String label, String direction, String createTime) {
        this.key = key;
        this.from = from;
        this.to = to;
        this.fromId = fromId;
        this.targetId = targetId;
        this.label = label;
        this.direction = direction;
        this.createTime = createTime;
    }

}
