package indi.mozping.entity;

import com.arangodb.entity.DocumentField;
import lombok.Data;

import java.io.Serializable;

/**
 * 点
 * @author percyl
 */
@Data
public class VertexDetail implements Serializable {

    @DocumentField(DocumentField.Type.ID)
    private String id;

    @DocumentField(DocumentField.Type.KEY)
    private String key;

    @DocumentField(DocumentField.Type.REV)
    private String revision;

    private String aid;
    private String createTime;
    private String data;

    public VertexDetail(String key, String aid,   String createTime, String data) {
        this.key = key;
        this.aid = aid;
        this.createTime = createTime;
        this.data = data;
    }

}