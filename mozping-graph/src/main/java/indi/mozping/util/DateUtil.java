package indi.mozping.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.MissingResourceException;

/**
 * @author by mozping
 * @Classname DateUtil
 * @Description TODO
 * @Date 2020/5/30 10:26
 */
public class DateUtil {


    public static String convertDateToString(Date aDate) {

        if (aDate == null) {
            aDate = Calendar.getInstance(Locale.getDefault()).getTime();
        }
        return getDateTime(getDatePattern(), aDate);
    }

    public static String getDateTime(String aMask, Date aDate) {

        SimpleDateFormat df;
        String returnValue = "";
        if (aDate != null) {
            df = new SimpleDateFormat(aMask);
            returnValue = df.format(aDate);
        }
        return (returnValue);
    }

    public static String getDatePattern() {
        String defaultDatePattern;
        try {
            defaultDatePattern = "yyyy-MM-dd HH:mm:ss";
        } catch (MissingResourceException mse) {
            throw mse;
        }

        return defaultDatePattern;
    }
}