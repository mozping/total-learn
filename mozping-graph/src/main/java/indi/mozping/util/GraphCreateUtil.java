package indi.mozping.util;

import com.arangodb.ArangoDatabase;
import com.arangodb.entity.CollectionEntity;
import com.arangodb.entity.CollectionType;
import com.arangodb.entity.EdgeDefinition;
import com.arangodb.entity.GraphEntity;
import com.arangodb.model.CollectionCreateOptions;
import indi.mozping.ds.ArangoDbUtil;
import indi.mozping.entity.PersonDetail;

import java.util.*;

/**
 * @author by mozping
 * @Classname GraphCreateUtil
 * @Description 创建图工具类
 * @Date 2020/5/30 17:24
 */
public class GraphCreateUtil {

    /**
     * @Description: 创建图
     * @date 2020/5/30 17:25
     */
    public static void createGraph(String vertexTableName, String edgeTableName, String graphName) {
        ArangoDatabase arangoDatabase = ArangoDbUtil.getInstance();

        EdgeDefinition edgeDefinition = new EdgeDefinition()
                .collection(edgeTableName)
                .from(vertexTableName)
                .to(vertexTableName);

        List<String> existGraphs = new ArrayList<String>();
        Collection<GraphEntity> graphs = arangoDatabase.getGraphs();
        for (GraphEntity graph : graphs) {
            existGraphs.add(graph.getName());
        }
        if (existGraphs.contains(graphName)) {
            System.out.println("Graph 已经存在..!");
            return;
        }
        System.out.println("创建Graph...");
        arangoDatabase.createGraph(graphName, Collections.singletonList(edgeDefinition));
    }


    /**
     * @Description: 创建顶点，Person代表顶点
     * @date 2020/5/30 17:25
     */
    public static void createVertexPerson(String vertexTableName, int vertexNum) {
        ArangoDatabase arangoDatabase = ArangoDbUtil.getInstance();

        String vertexTable = vertexTableName;
        int vNum = vertexNum;

        List<String> existTableName = new ArrayList<String>();
        Collection<CollectionEntity> collections = arangoDatabase.getCollections();
        for (CollectionEntity collectionEntity : collections) {
            existTableName.add(collectionEntity.getName());
        }
        if (!existTableName.contains(vertexTable)) {
            arangoDatabase.createCollection(vertexTable);
        }

        for (int i = 1; i <= vNum; i++) {
            String now = DateUtil.convertDateToString(new Date());
            PersonDetail personDetail = new PersonDetail(Integer.toString(i), Integer.toString(i), now, "person:" + i);
            arangoDatabase.collection(vertexTable).insertDocument(personDetail);
        }
        System.out.println("End...");
    }

    /**
     * @Description: 创建边，关系代表边
     * @date 2020/5/30 17:27
     */
    public static void createEdgeRelation(String vertexTableName, String edgeTableName, int edgeNum, int vertexNum) {
        ArangoDatabase arangoDatabase = ArangoDbUtil.getInstance();

        String edgeTable = edgeTableName;
        int e_num = edgeNum;
        int v_num = vertexNum;
        List<String> e_List = new ArrayList<String>();
        Random random = new Random();


        for (int i = 1; i <= e_num; i++) {
            int from = random.nextInt(v_num) + 1;
            int to = random.nextInt(v_num) + 1;
            String edge = from + "->" + to;
            String reverse_edge = from + "->" + to;
            if (from != to && !e_List.contains(edge) && !e_List.contains(reverse_edge)) {
                e_List.add(edge);
            }
        }
        for (String ss : e_List) {
            System.out.println("Edge: " + ss + "  ====== from: " + ss.split("->")[0] + ", to: " + ss.split("-")[1]);
        }

        List<String> existTableName = new ArrayList<String>();
        Collection<CollectionEntity> collections = arangoDatabase.getCollections();
        for (CollectionEntity collectionEntity : collections) {
            existTableName.add(collectionEntity.getName());
        }
        if (!existTableName.contains(edgeTable)) {
            CollectionCreateOptions collectionCreateOptions = new CollectionCreateOptions();
            collectionCreateOptions.type(CollectionType.EDGES);
            arangoDatabase.createCollection(edgeTable, collectionCreateOptions);
        }

        for (String e : e_List) {
            StringBuilder aql = new StringBuilder();
            String from = e.split("->")[0];
            String to = e.split("->")[1];
            aql.append("INSERT {_key:").append("'" + from).append("_").append(to + "'").append(",")
                    .append("_from: ").append("'" + vertexTableName + "/" + from + "'").append(",")
                    .append("from: ").append("'" + from + "'").append(",")
                    .append("_to: ").append("'" + vertexTableName + "/" + to + "'").append(",")
                    .append("to: ").append("'" + to + "'").append(",")
                    .append("weight: ").append(new Random().nextFloat()).append("}")
                    .append(" INTO ")
                    .append(edgeTable);

            System.out.println(aql.toString());
            arangoDatabase.query(aql.toString(), null, null, null);
        }
    }
}