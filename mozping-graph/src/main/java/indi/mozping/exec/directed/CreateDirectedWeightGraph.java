package indi.mozping.exec.directed;

import indi.mozping.util.GraphCreateUtil;

/**
 * @author by mozping
 * @Classname GraphTest
 * @Description 创建有向有权图
 * @Date 2020/5/29 15:39
 */
public class CreateDirectedWeightGraph {

    //Config
    public static final String VERTEX_TABLE_NAME = "person";
    public static final int VERTEX_NUM = 100;

    public static final String EDGE_TABLE_NAME = "relation";
    public static final int EDGE_NUM = 5 * VERTEX_NUM;

    public static final String GRAPH_NAME = "relationGraph";

    public static void main(String[] args) {

        //创建顶点
        GraphCreateUtil.createVertexPerson(VERTEX_TABLE_NAME, VERTEX_NUM);
        //创建边
        GraphCreateUtil.createEdgeRelation(VERTEX_TABLE_NAME, EDGE_TABLE_NAME, EDGE_NUM, VERTEX_NUM);
        //利用边和顶点创建图
        GraphCreateUtil.createGraph(VERTEX_TABLE_NAME, EDGE_TABLE_NAME, GRAPH_NAME);
    }
}