package indi.mozping.exec;

import com.arangodb.ArangoGraph;
import indi.mozping.entity.EdgeDetail;
import indi.mozping.entity.VertexDetail;
import indi.mozping.ds.ArangoDbUtil;

import java.util.Date;

/**
 * @author by mozping
 * @Classname GraphTest
 * @Description TODO
 * @Date 2020/5/29 15:39
 */
public class GraphTest {

    public static void main(String[] args) {
        String graph = "myGraph1";
        ArangoGraph arangoGraph = ArangoDbUtil.getInstance().graph(graph);
        String vertexCollection = "coll_1";
        String edgeCollection = "coll_2";

        String aid1 = "5";
        //顶点1
        VertexDetail vertex1 = new VertexDetail(aid1, aid1, new Date().toString(), "node: " + aid1);
        arangoGraph.vertexCollection(vertexCollection).insertVertex(vertex1);

        String aid2 = "6";
        //顶点2
        VertexDetail vertex2 = new VertexDetail(aid2, aid2, new Date().toString(), "node: " + aid1);
        arangoGraph.vertexCollection(vertexCollection).insertVertex(vertex2);

        //边
        String edgeKey = buildEdgeKey(aid1, aid2, "有关系");
        EdgeDetail edge = new EdgeDetail(edgeKey, buildEdgeFromTo(aid1, vertexCollection), buildEdgeFromTo(aid2, vertexCollection), aid1, aid2, "标签", "Friend", new Date().toString());
        arangoGraph.edgeCollection(edgeCollection).insertEdge(edge);
    }


    private static String buildEdgeKey(String aid1, String aid2, String label) {
//        return aid1 + "_" + aid2 + "_" + label;
        return aid1 + "_" + aid2;
    }

    private static String buildEdgeFromTo(String aid, String vertex) {
        return vertex + "/" + aid;
    }
}