package indi.mozping.ds;

import com.arangodb.ArangoDB;
import com.arangodb.ArangoDatabase;

/**
 * ArangoDB单例类
 */
public class ArangoDbUtil {

    private static ArangoDB arangoDB = null;
    private static ArangoDatabase db = null;

    /**
     * 获得实例
     *
     * @return 实例对象
     */
    public static ArangoDatabase getInstance() {
        if (arangoDB == null) {
            synchronized (ArangoDbUtil.class) {
                if (arangoDB == null) {
                    arangoDB = new ArangoDB.Builder()
                            .maxConnections(10)
                            .host(ArangoDbConfig.HOST, ArangoDbConfig.PORT)
                            .user(ArangoDbConfig.USER)
                            .password(ArangoDbConfig.PWD)
                            .build();
                    db = arangoDB.db(ArangoDbConfig.DB_NAME);
                }
            }
        }
        return db;
    }

    /**
     * 销毁方法
     */
    public static void destroy() {
        if (arangoDB != null) {
            arangoDB.shutdown();
        }
    }
}
