package indi.mozping.ds;


import lombok.Data;


/**
 * redis 配置
 *
 * @author percyl
 */
@Data
public class ArangoDbConfig {

    public static final String HOST = "192.168.11.72";
    //    public static final String HOST = "127.0.0.1";
    public static final int PORT = 8529;
    public static final String USER = "root";
    public static final String PWD = "654321";
    public static final String DB_NAME = "m_test";

}


