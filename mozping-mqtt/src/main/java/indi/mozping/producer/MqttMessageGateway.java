package indi.mozping.kafka.producer;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * 消息发送接口，不需要实现，spring会通过代理的方式实现
 *
 * @author mozping
 * @version 1.0
 * @date 2018/9/13 16:30
 * @since JDK1.8
 */
@Component
@MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
public interface MqttMessageGateway {

    /**
     * mqtt发送消息方法
     *
     * @param message
     * @return void
     * @throws
     * @see
     */
    void sendMessage(Message<?> message);
}
