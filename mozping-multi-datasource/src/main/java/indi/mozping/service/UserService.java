package indi.mozping.service;

import indi.mozping.dao.one.UserDaoOne;
import indi.mozping.dao.two.UserDaoTwo;
import indi.mozping.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserDaoOne userDaoOne;

    @Autowired
    UserDaoTwo userDaoTwo;

    /**
     * 访问数据源一
     */
    public List<User> findUserOneList() {
        return userDaoOne.findAll();
    }

    /**
     * 访问数据源二
     */
    public List<User> findUserTwoList() {
        return userDaoTwo.findAll();
    }
}
