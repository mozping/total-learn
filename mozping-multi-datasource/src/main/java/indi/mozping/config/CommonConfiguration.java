package indi.mozping.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author by mozping
 * @Classname CommonConfiguration
 * @Description 数据源公共配置
 * @Date 2019/11/7 16:19
 */
@Configuration
public class CommonConfiguration {

    @Value("${spring.datasource.driver-class-name}")
    String driverClassName;

    @Value("${spring.datasource.druid.initialSize}")
    int initialSize;

    @Value("${spring.datasource.druid.minIdle}")
    int minIdle;

    @Value("${spring.datasource.druid.maxActive}")
    int maxActive;

    @Value("${spring.datasource.druid.maxWait}")
    int maxWait;

    @Value("${spring.datasource.druid.timeBetweenEvictionRunsMillis}")
    int timeBetweenEvictionRunsMillis;

    @Value("${spring.datasource.druid.minEvictableIdleTimeMillis}")
    int minEvictableIdleTimeMillis;

    @Value("${spring.datasource.druid.validationQuery}")
    String validationQuery;

    @Value("${spring.datasource.druid.testWhileIdle}")
    boolean testWhileIdle;

    @Value("${spring.datasource.druid.testOnBorrow}")
    boolean testOnBorrow;

    @Value("${spring.datasource.druid.testOnReturn}")
    boolean testOnReturn;

}

