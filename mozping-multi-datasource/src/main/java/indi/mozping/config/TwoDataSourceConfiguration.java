package indi.mozping.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @author by mozping
 * @Classname TwoDataSourceConfiguration
 * @Description 数据源二的配置
 * @Date 2019/11/7 16:21
 */
@Configuration
@MapperScan(basePackages = "indi.mozping.dao.two", sqlSessionTemplateRef = "sqlSessionTemplateTwo")
public class TwoDataSourceConfiguration extends CommonConfiguration {
    public final static org.slf4j.Logger logger = LoggerFactory.getLogger(TwoDataSourceConfiguration.class);

    @Value("${spring.datasource.two-url}")
    private String url;

    @Value("${spring.datasource.two-username}")
    private String username;

    @Value("${spring.datasource.two-password}")
    private String password;


    @Bean(name = "dataSourceTwo")
    public DataSource dataSourceTwo() {
        logger.info("初始化数据库 sqlSessionFactoryTwo 连接池");
        DruidDataSource datasource = new DruidDataSource();
        datasource.setUrl(url);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setDriverClassName(driverClassName);

        //configuration  
        datasource.setInitialSize(initialSize);
        datasource.setMinIdle(minIdle);
        datasource.setMaxActive(maxActive);
        datasource.setMaxWait(maxWait);
        datasource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        datasource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        datasource.setValidationQuery(validationQuery);
        datasource.setTestWhileIdle(testWhileIdle);
        datasource.setTestOnBorrow(testOnReturn);
        datasource.setTestOnReturn(testOnReturn);
        logger.info("初始化数据库连接池 sqlSessionFactoryTwo 完成");
        return datasource;
    }

    @Bean(name = "transactionManagerTwo")
    public DataSourceTransactionManager transactionManagerTwo(@Qualifier("dataSourceTwo") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    @Bean(name = "sqlSessionFactoryTwo")
    public SqlSessionFactory sqlSessionFactoryTwo(@Qualifier("dataSourceTwo") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
        bean.setTypeAliasesPackage("indi.mozping.entity");
        return bean.getObject();
    }

    @Bean(name = "sqlSessionTemplateTwo")
    public SqlSessionTemplate sqlSessionTemplateTwo(@Qualifier("sqlSessionFactoryTwo") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
