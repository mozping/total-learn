package indi.mozping.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @author by mozping
 * @Classname OneDataSourceConfiguration
 * @Description 数据源一的配置
 * @Date 2019/11/7 16:20
 */
@Configuration
@MapperScan(basePackages = "indi.mozping.dao.one", sqlSessionTemplateRef = "sqlSessionTemplateOne")
public class OneDataSourceConfiguration extends CommonConfiguration {

    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(OneDataSourceConfiguration.class);

    @Value("${spring.datasource.one-url}")
    private String url;

    @Value("${spring.datasource.one-username}")
    private String username;

    @Value("${spring.datasource.one-password}")
    private String password;

    @Bean(name = "dataSourceOne")
    @Primary
    public DataSource dataSourceOne() {
        logger.info("初始化数据库 dataSourceOne 连接池 ");
        DruidDataSource datasource = new DruidDataSource();
        datasource.setUrl(url);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setDriverClassName(driverClassName);

        //configuration  
        datasource.setInitialSize(initialSize);
        datasource.setMinIdle(minIdle);
        datasource.setMaxActive(maxActive);
        datasource.setMaxWait(maxWait);
        datasource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        datasource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        datasource.setValidationQuery(validationQuery);
        datasource.setTestWhileIdle(testWhileIdle);
        datasource.setTestOnBorrow(testOnReturn);
        datasource.setTestOnReturn(testOnReturn);
        logger.info("初始化数据库连接池 dataSourceOne 完成 ");
        return datasource;
    }

    @Bean(name = "transactionManagerOne")
    @Primary
    public DataSourceTransactionManager transactionManagerOne(@Qualifier("dataSourceOne") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "sqlSessionFactoryOne")
    @Primary
    public SqlSessionFactory sqlSessionFactoryOne(@Qualifier("dataSourceOne") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
        bean.setTypeAliasesPackage("indi.mozping.entity");
        return bean.getObject();
    }

    @Bean(name = "sqlSessionTemplateOne")
    @Primary
    public SqlSessionTemplate sqlSessionTemplateOne(@Qualifier("sqlSessionFactoryOne") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
