package indi.mozping.dao.one;

import indi.mozping.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDaoOne {

    List<User> findAll();
}
