package indi.mozping.dao.two;

import indi.mozping.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDaoTwo {
    List<User> findAll();
}
