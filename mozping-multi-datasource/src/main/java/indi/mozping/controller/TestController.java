package indi.mozping.controller;

import indi.mozping.entity.User;
import indi.mozping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@RestController
public class TestController {

    @Autowired
    private UserService userService;

    /**
     * 随机访问两个数据源
     */
    @RequestMapping("/query")
    public List<User> queryRandom() {
        if ((new Random().nextBoolean())) {
            return userService.findUserOneList();
        }
        return userService.findUserTwoList();
    }

    /**
     * 访问数据源一
     */
    @GetMapping("/queryOne")
    public List<User> queryOne() {
        return userService.findUserOneList();
    }

    /**
     * 访问数据源二
     */
    @GetMapping("/queryTwo")
    public List<User> queryTwo() {
        return userService.findUserTwoList();
    }
}
