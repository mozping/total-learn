package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author intellif
 */
@SpringBootApplication
public class MozpingMultiDatasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingMultiDatasourceApplication.class, args);
    }

}
