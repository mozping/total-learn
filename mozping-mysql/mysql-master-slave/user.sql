/*
Navicat MySQL Data Transfer

Source Server         : 192.168.12.168
Source Server Version : 50722
Source Host           : 192.168.12.168:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2019-12-23 11:02:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `job` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '周杰伦', '华语歌手');
INSERT INTO `user` VALUES ('2', '黄晓明', '演员');
INSERT INTO `user` VALUES ('3', '姚明', '运动员');
INSERT INTO `user` VALUES ('4', '郭敬明', '作家');
INSERT INTO `user` VALUES ('5', '撒贝宁', '主持人');
INSERT INTO `user` VALUES ('6', '马云', '企业家');
INSERT INTO `user` VALUES ('7', '马士兵', '程序员');
INSERT INTO `user` VALUES ('8', '习大大', '主席');
INSERT INTO `user` VALUES ('9', '李宗盛', '创作人');
