package indi.mozping.service;

import indi.mozping.dao.UserDao;
import indi.mozping.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {

    @Autowired
    UserDao userDao;

    /**
     * 新增
     */
    public Integer addUser(User user) {
        return userDao.insert(user);
    }

    /**
     * 删除
     */
    public Integer deleteById(Integer id) {
        return userDao.deleteById(id);
    }

    /**
     * 修改
     */
    public Integer updateById(User user) {
        return userDao.updateById(user);
    }

    /**
     * 查询
     */
    public List<User> findAll() {
        return userDao.findAll();
    }

    public User findById(Integer id) {

        return userDao.findById(id);
    }

    //走主库
    public List<User> realTimeFindAll() {
        return userDao.findAll();
    }
}
