package indi.mozping.controller;

import indi.mozping.entity.User;
import indi.mozping.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    private UserService userService;

    @PostMapping("/addUser")
    public Integer addUser(@RequestBody User user) {
        System.out.println("开始执行 addUser ...");
        Integer integer = userService.addUser(user);
        System.out.println("执行 addUser 完毕 ...");
        return integer;
    }

    @GetMapping("/deleteById")
    public Integer deleteById(@Param("id") Integer id) {
        System.out.println("开始执行 deleteById ...");
        Integer integer = userService.deleteById(id);
        System.out.println("执行 deleteById 完毕 ...");
        return integer;
    }

    @PostMapping("/updateById")
    public Integer updateById(@RequestBody User user) {
        System.out.println("开始执行 updateById ...");
        Integer integer = userService.updateById(user);
        System.out.println("执行 updateById 完毕 ...");
        return integer;
    }

    @GetMapping("/queryAll")
    public List<User> queryAll() {
        System.out.println("开始执行 queryAll ...");
        List<User> all = userService.findAll();
        System.out.println("执行 queryAll 完毕 ...");
        return all;
    }


    @GetMapping("/queryById")
    public User queryById(@Param("id") Integer id) {
        System.out.println("开始执行 queryById ...");
        User user = userService.findById(id);
        System.out.println("执行 queryById 完毕 ...");
        return user;
    }


    @GetMapping("/queryRealTime")
    public List<User> queryRealTime() {
        System.out.println("开始执行 queryRealTime ...");
        List<User> all = userService.realTimeFindAll();
        System.out.println("执行 queryRealTime 完毕 ...");
        return all;
    }
}
