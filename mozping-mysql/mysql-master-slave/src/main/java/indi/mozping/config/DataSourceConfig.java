package indi.mozping.config;

import indi.mozping.dbutils.MyRoutingDataSource;
import indi.mozping.vo.DBTypeEnum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


/**
 * @author by mozping
 * @Classname DataSourceConfig
 * @Description 数据源的相关配置
 * @Date 2019/11/12 11:13
 */
@Configuration
public class DataSourceConfig {
    /**
     * 主库的数据源
     */
    @Bean("masterDS")
    @ConfigurationProperties("spring.datasource.master")
    public DataSource masterDataSource() {
        return DataSourceBuilder.create().build();
    }

    /**
     * 从库1的数据源
     */
    @Bean("slaveOneDS")
    @ConfigurationProperties("spring.datasource.slave-1")
    public DataSource slave1DataSource() {
        return DataSourceBuilder.create().build();
    }

    /**
     * 自定义数据源，内部持有了主库和从库的数据源，
     * 按照Key-Value的形式来切换数据源
     * 比如内部通过 (DBTypeEnum.MASTER, masterDataSource); 的形式将数据源保存到Map
     * 后续通过  DBContextHolder.set(DBTypeEnum.MASTER); 来设置需要切换到主库
     * MyRoutingDataSource通过determineCurrentLookupKey方法读取到对应的返回值来确定需要切换到主库
     */
    @Bean
    public DataSource myRoutingDataSource(
            @Qualifier("masterDS") DataSource masterDataSource,
            @Qualifier("slaveOneDS") DataSource slaveDataSource) {
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DBTypeEnum.MASTER, masterDataSource);
        targetDataSources.put(DBTypeEnum.SLAVE, slaveDataSource);
        MyRoutingDataSource myRoutingDataSource = new MyRoutingDataSource();
        myRoutingDataSource.setTargetDataSources(targetDataSources);
        /*当执行的方法没有被Aop拦截时，缺省使用的数据源*/
        myRoutingDataSource.setDefaultTargetDataSource(masterDataSource);
        return myRoutingDataSource;
    }


}
