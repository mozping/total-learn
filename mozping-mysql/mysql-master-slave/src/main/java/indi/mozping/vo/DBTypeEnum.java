package indi.mozping.vo;


/**
 * @author by mozping
 * @Classname DBTypeEnum
 * @Description 主从库的枚举, 可以有更多的SLAVE
 * @Date 2019/11/12 11:14
 */
public enum DBTypeEnum {
    //主库
    MASTER,
    //从库
    SLAVE
}
