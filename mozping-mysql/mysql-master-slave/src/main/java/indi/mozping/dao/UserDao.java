package indi.mozping.dao;

import indi.mozping.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface UserDao {

    /**
     * 新增
     */
    Integer insert(User user);

    /**
     * 删除
     */
    Integer deleteById(Integer id);

    /**
     * 修改
     */
    Integer updateById(User user);


    /**
     * 查询
     */
    List<User> findAll();

    User findById(Integer id);
}
