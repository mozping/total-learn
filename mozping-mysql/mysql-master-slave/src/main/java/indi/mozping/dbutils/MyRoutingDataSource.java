package indi.mozping.dbutils;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author by mozping
 * @Classname MyRoutingDataSource
 * @Description 自定义的数据源，继承自AbstractRoutingDataSource，重写了数据源的路由方法，通过
 * determineCurrentLookupKey方法的返回值来决定切换到那个数据源
 * 这种切换规则可以自定义，在初始化 MyRoutingDataSource 的时候，将选择的key和数据源保存到Map中
 * @Date 2019/11/12 11:14
 */
public class MyRoutingDataSource extends AbstractRoutingDataSource {
    /**
     * 自定义选择规则
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DBContextHolder.get();
    }
}
