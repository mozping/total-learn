package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Properties;

@RestController
@SpringBootApplication
public class SpringbootHelloworldApplication {

    @RequestMapping("/hello")
    public String hello() {

        Properties properties = new Properties();
        return "hello .." + new Date();

    }


    public static void main(String[] args) {
        SpringApplication.run(SpringbootHelloworldApplication.class, args);
    }

}
