package indi.mozping;

/**
 * @author by mozping
 * @Classname GenderEnum
 * @Description TODO
 * @Date 2019/11/13 9:37
 */
public enum GenderEnum implements Hello {
    MAN("男") {
        @Override
        public void say() {
            System.out.println("男人好难！");
        }

    },

    WOMAN("女") {
        @Override
        public void say() {
            System.out.println("女人好难！");
        }

    };

    String desc;

    GenderEnum(String desc) {
        this.desc = desc;
    }
}