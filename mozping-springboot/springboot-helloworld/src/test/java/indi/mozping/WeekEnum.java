package indi.mozping;


public enum WeekEnum {
    MON("星期一"),
    TUE("星期二"),
    WEN("星期三"),
    THR("星期四"),
    FRI("星期五"),
    SAT("星期六"),
    SYN("星期日");

    String desc;

    WeekEnum(String desc) {
        this.desc = desc;
    }


}
