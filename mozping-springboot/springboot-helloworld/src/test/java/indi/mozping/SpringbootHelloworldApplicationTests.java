package indi.mozping;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringbootHelloworldApplicationTests {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void test() {
        ResponseEntity<String> response = template.getForEntity("http://localhost:8080/",
                String.class);
        System.out.println("输出结果：" + response.getBody());
        Assert.assertEquals(response.getBody(), "Hello world! Springboot ... mozping -- 100");
    }

}
