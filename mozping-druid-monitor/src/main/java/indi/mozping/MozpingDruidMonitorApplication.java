package indi.mozping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MozpingDruidMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MozpingDruidMonitorApplication.class, args);
    }

}

