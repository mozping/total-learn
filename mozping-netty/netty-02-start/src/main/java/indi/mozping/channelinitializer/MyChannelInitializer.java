package indi.mozping.channelinitializer;

import indi.mozping.echo.server.EchoServerHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author by mozping
 * @Classname MyChannelInitializer
 * @Description TODO
 * @Date 2019/12/10 12:55
 */
public class MyChannelInitializer extends ChannelInitializer {

    @Override
    protected void initChannel(Channel ch) throws Exception {
        System.out.println("MyChannelInitializer begin ... ");
        ch.pipeline().addLast("logging", new LoggingHandler(LogLevel.INFO));
        ch.pipeline().addLast(new EchoServerHandler());
    }
}