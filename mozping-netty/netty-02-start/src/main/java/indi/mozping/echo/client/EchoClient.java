package indi.mozping.echo.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.net.InetSocketAddress;

/**
 * @author by mozping
 * @Classname EchoClient
 * @Description Echo客户端，需要做的是：
 * 1.连接服务器
 * 2.发送信息
 * 3.发送的每个信息，等待和接收从服务器返回的同样的信息
 * 4.关闭连接
 * 这里面的写法和服务端很相似
 * @Date 2019/9/11 19:25
 */
public class EchoClient {

    private static final String HOST = "127.0.0.1";
    private static final int PORT = 12345;

    private static void start() throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            //1.创建 Bootstrap
            Bootstrap b = new Bootstrap();
            //2.指定 EventLoopGroup 来处理客户端事件。由于我们使用 NIO 传输，所以用到了 NioEventLoopGroup 的实现
            b.group(group)
                    //3.使用的 channel 类型是一个用于 NIO 传输
                    .channel(NioSocketChannel.class)
                    //4.设置服务器的 InetSocketAddress
                    .remoteAddress(new InetSocketAddress(HOST, PORT))
                    //5.当建立一个连接和一个新的通道时，创建添加到 EchoClientHandler 实例 到 channel pipeline
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch)
                                throws Exception {
                            ch.pipeline().addLast("logging", new LoggingHandler(LogLevel.INFO));
                            ch.pipeline().addLast(new EchoClientHandler());
                        }
                    });

            //6.连接到远程;等待连接完成
            ChannelFuture f = b.connect().sync();
            //7.阻塞直到 Channel 关闭
            f.channel().closeFuture().sync();
        } finally {
            //8.调用 shutdownGracefully() 来关闭线程池和释放所有资源
            group.shutdownGracefully().sync();
        }
    }

    public static void main(String[] args) throws Exception {
        EchoClient.start();
    }
}