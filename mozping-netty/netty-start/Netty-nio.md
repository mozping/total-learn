## 一、几种线程模型
### 1.1 BIO
+ 阻塞IO
#### 缺点：
+ 并发数低，一个线程对应一个连接，连接数受限制
+ 线程切换，开销大
+ 可能需要进行线程间同步 
### 1.2 NIO
+ 非阻塞IO
### 优点：
+ 并发高，一个线程可以处理很多个连接

### 1.3 Reactor
+ Reactor是一种线程模型
#### 特点：
+ 
+


### 参考：
+ https://www.infoq.cn/article/netty-threading-model?utm_source=infoq&utm_medium=popular_links_homepage
+ https://blog.csdn.net/memery_last/article/details/83757711