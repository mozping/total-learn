package indi.mozping.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

/**
 * @author by mozping
 * @Classname EchoServerHandler
 * @Description TODO
 * @Date 2019/3/27 20:54
 */
public class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {


    //客户单读取到数据后做什么
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) throws Exception {
        //简单打印
        System.out.println("Client Accept:" + byteBuf.toString(CharsetUtil.UTF_8));
    }

    //客户端被通知活跃之后，做事情
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(Unpooled.copiedBuffer("Hello Netty! ", CharsetUtil.UTF_8));
    }


    //异常后的处理
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
