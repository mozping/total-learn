/**
 * @author by mozping
 * @Classname TestMain
 * @Description TODO
 * @Date 2019/3/28 12:15
 */
public class TestMain {

    private static String EMPTY_STR = " ";

    public static String getTraceIdFromSpan(String spanString) {
        String original = spanString;
        String[] split = original.split(EMPTY_STR);
        String traceId = split[1];
        return traceId.substring(0, traceId.length() - 1);

    }

    public static void main(String[] args) {
        String str = "[Trace: 6a523ec75a1be7b4, Span: 6a523ec75a1be7b4, Parent: null, exportable:true]";
        System.out.println(getTraceIdFromSpan(str));
    }
}
