package indi.mozping.nio.server;

/**
 * @author by mozping
 * @Classname ServerMain
 * @Description 服务端启动程序
 * @Date 2019/9/11 17:11
 */
public class ServerMain {

    public static void main(String[] args) {
        int port = 12345;
        NioThread nioThread = new NioThread(port);
        new Thread(nioThread).start();
    }
}
