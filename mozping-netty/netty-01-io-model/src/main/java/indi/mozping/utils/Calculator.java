package indi.mozping.utils;

/**
 * @author by mozping
 * @Classname Calculator
 * @Description TODO
 * @Date 2019/4/23 15:44
 */

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public final class Calculator {
    private final static ScriptEngine jse = new ScriptEngineManager().getEngineByName("JavaScript");

    public static Object cal(String expression) throws ScriptException {
        return jse.eval(expression);
    }
}
