package indi.mozping.aio.server;

/**
 * @author by mozping
 * @Classname Server
 * @Description TODO
 * @Date 2019/4/23 15:42
 */

import indi.mozping.utils.Utils;

/**
 * AIO服务端
 *
 * @author yangtao__anxpp.com
 * @version 1.0
 */
public class Server {
    private static int DEFAULT_PORT = Utils.PORT;
    private static AsyncServerHandler serverHandle;
    public volatile static long clientCount = 0;

    public static void start() {
        start(DEFAULT_PORT);
    }

    public static synchronized void start(int port) {
        if (serverHandle != null)
            return;
        serverHandle = new AsyncServerHandler(port);
        new Thread(serverHandle, "Server").start();
    }

    public static void main(String[] args) {
        Server.start();
    }
}

