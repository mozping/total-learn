package indi.mozping.aio.client;

import indi.mozping.utils.Utils;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author by mozping
 * @Classname AioClient
 * @Description TODO
 * @Date 2019/4/23 10:10
 */
public class AioClient {

    private static AioClientHandler clientHandler;
    private static String IP = "127.0.0.1";
    private static int PORT = Utils.PORT;


    public static void start() {
        if (clientHandler != null) {
            return;
        }
        clientHandler = new AioClientHandler(IP, PORT);
        //创建负责网络通讯的线程
        new Thread(clientHandler, "Client").start();
    }

    public static boolean sendMsg(String msg) {
        if ("q".equals(msg)) {
            return false;
        }
        clientHandler.sendMessage(msg + System.getProperty("line.separator"));
        return true;
    }

    public static void main(String[] args) throws IOException {
        AioClient.start();
        System.out.println("请输入消息:");
        Scanner scanner = new Scanner(System.in);
        while (AioClient.sendMsg(scanner.nextLine())) ;
    }
}
