package server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by mozping
 * @Classname NettyServerInHandlerOne
 * @Description TODO
 * @Date 2019/4/2 19:03
 */
public class NettyServerOutHandlerOne extends ChannelOutboundHandlerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(NettyServerOutHandlerOne.class);

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        LOG.info("NettyServerOutHandlerOne write...");
        ByteBuf result = (ByteBuf) msg;
        String resp = result.toString(CharsetUtil.UTF_8) + "NettyServerOutHandlerOne ";
        ByteBuf encoded = ctx.alloc().buffer(4 * resp.length());
        encoded.writeBytes(resp.getBytes());
        ctx.write(encoded);
        ctx.flush();

    }
}
