package server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @author by mozping
 * @Classname NettyServer
 * @Description TODO
 * @Date 2019/4/2 19:02
 */
public class NettyServer {

    private final int port;

    public NettyServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) {
        NettyServer nettyServer = new NettyServer(9999);
        System.out.println("启动http server...");
        nettyServer.start();
    }

    private void start() {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup(10);
        EventLoopGroup workGroup = new NioEventLoopGroup();
        try {
            serverBootstrap.group(eventLoopGroup, workGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            //这里的顺序有关系，之前顺序错了导致功能不正确 in1 -> in2 -> out1 -> out2 不正确
                            ch.pipeline().addLast(new NettyServerOutHandlerTwo());
                            ch.pipeline().addLast(new NettyServerOutHandlerOne());
                            ch.pipeline().addLast(new NettyServerInHandlerOne());
                            ch.pipeline().addLast(new NettyServerInHandlerTwo());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            ChannelFuture sync = serverBootstrap.bind(port).sync();
            sync.channel().closeFuture().sync(); //监听关闭时间，一旦channel关闭，就会拿到一个ChannelFuture对象，然后调用sync关闭

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //服务端关闭后，关闭线程组
            workGroup.shutdownGracefully();
            eventLoopGroup.shutdownGracefully();
        }

    }
}
