package server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by mozping
 * @Classname NettyServerOutHandlerTwo
 * @Description TODO
 * @Date 2019/4/2 19:07
 */
public class NettyServerOutHandlerTwo extends ChannelOutboundHandlerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(NettyServerOutHandlerTwo.class);

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        LOG.info("NettyServerOutHandlerTwo write...");
        ByteBuf result = (ByteBuf) msg;
        String resp = result.toString(CharsetUtil.UTF_8) + "NettyServerOutHandlerTwo ";
        ByteBuf encoded = ctx.alloc().buffer(4 * resp.length());
        encoded.writeBytes(resp.getBytes());
        ctx.write(encoded);
        ctx.flush();
    }

}
