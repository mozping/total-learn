package server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by mozping
 * @Classname NettyServerInHandlerTwo
 * @Description TODO
 * @Date 2019/4/2 19:03
 */
public class NettyServerInHandlerTwo extends ChannelInboundHandlerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(NettyServerInHandlerTwo.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        LOG.info("Channel InboundHandlerTwo channelRead 执行...");
        ByteBuf result = (ByteBuf) msg;
        String msgStr = result.toString(CharsetUtil.UTF_8);
        LOG.info("Client said:{}", msgStr);
        ByteBuf newByteBuf = Unpooled.copiedBuffer(msgStr + "NettyServerInHandlerTwo ", CharsetUtil.UTF_8);
        ctx.write(newByteBuf);
        //ctx.fireChannelRead(newByteBuf);
        //ctx.flush();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        LOG.info("Channel InboundHandlerTwo channelReadComplete 执行...");
        ctx.flush();
    }

}
