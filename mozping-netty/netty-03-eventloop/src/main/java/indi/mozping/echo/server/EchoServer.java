package indi.mozping.echo.server;

import indi.mozping.channelinitializer.MyChannelInitializer;
import indi.mozping.factory.BossThreadFactory;
import indi.mozping.factory.WorkerThreadFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;
import java.util.concurrent.ThreadFactory;

/**
 * @author by mozping
 * @Classname EchoServer
 * @Description EchoServer是Netty服务端的引导服务器
 * <p>
 * 监听和接收进来的连接请求
 * 配置Channel来通知一个关于入站消息的EchoServerHandler 实例
 * <p>
 * <p>
 * 服务器的主代码组件是：
 * 1.EchoServerHandler 实现了的业务逻辑，2.通过引导了服务器启动
 * <p>
 * 引导服务器需要执行的步骤是：
 * 1.创建 ServerBootstrap 实例来引导服务器并随后绑定
 * 2.创建并分配一个 NioEventLoopGroup 实例来处理事件的处理，如接受新的连接和读/写数据。
 * 3.指定通道类型是NIO
 * 4.指定本地 InetSocketAddress 给服务器绑定
 * 5.通过 EchoServerHandler实例给每一个新的Channel初始化
 * 6.最后调用 ServerBootstrap.bind() 绑定服务器
 * @Date 2019/9/11 19:24
 */
public class EchoServer {

    private static final int PORT = 12345;

    public static void main(String[] args) throws Exception {
        EchoServer.start();
    }

    /**
     * Netty server端的三种Reactor模型实现：
     * 单线程模型：
     * EventLoopGroup single = new NioEventLoopGroup(1);
     * bootstrap.group(single);
     * <p>
     * 多线程模型：
     * NioEventLoopGroup boss = new NioEventLoopGroup(1);
     * NioEventLoopGroup worker = new NioEventLoopGroup();
     * bootstrap.group(boss, worker);
     * <p>
     * 主从线程模型:
     * NioEventLoopGroup boss = new NioEventLoopGroup(4);
     * NioEventLoopGroup worker = new NioEventLoopGroup();
     * bootstrap.group(boss, worker);
     * <p>
     * 参考：Netty只有多线程模型，主从模型也是多线程模型，只会有一个EventLoop 接受监听
     * https://blog.csdn.net/liuhuiteng/article/details/93921859
     */
    private static void start() throws Exception {
        //1.创建EventLoopGroup,
        ThreadFactory bossFactory = new BossThreadFactory();
        ThreadFactory workerFactory = new WorkerThreadFactory();
        NioEventLoopGroup boss = new NioEventLoopGroup(4, bossFactory);
        NioEventLoopGroup worker = new NioEventLoopGroup(4, workerFactory);
        try {
            //2.创建 ServerBootstrap
            ServerBootstrap bootstrap = new ServerBootstrap();
            //3.指定NioEventLoopGroup接受和处理新连接
            bootstrap.group(boss, worker)
                    //5.设置通道类型，指定使用NIO的传输Channel
                    .channel(NioServerSocketChannel.class)
                    //6.设置socket地址使用所选的端口，服务器将监听该地址端口
                    .localAddress(new InetSocketAddress(PORT))
                    //7.添加EchoServerHandler到Channel的ChannelPipeline
                    //这里的含义是：每当接受一个新的连接，一个子Channel将会被创建，ChannelInitializer
                    //会添加我们的EchoServerHandler实例到Channel的ChannelPipeline。如果有入站信息，这个处理器将被通知。
                    .childHandler(new MyChannelInitializer());
            //8.绑定的服务器;sync同步方式会阻塞当前线程
            ChannelFuture f = bootstrap.bind().sync();
            //9.关闭 channel,直到它被关闭,sync同步方式会阻塞当前线程
            f.channel().closeFuture().sync();
        } finally {
            //10.关闭 EventLoopGroup，释放所有资源。
            boss.shutdownGracefully().sync();
            worker.shutdownGracefully().sync();
        }
    }
}