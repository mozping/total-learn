package indi.mozping;


import indi.mozping.lock.Lock;
import indi.mozping.lock.ZookeeperDistributeListenLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by mozping
 * @Classname ZookeeperDistributeLockTest
 * @Description TODO
 * @Date 2019/4/19 14:07
 */
public class ZookeeperDistributeLockTest {

    private static final Logger LOG = LoggerFactory.getLogger(ZookeeperDistributeLockTest.class);
    private static int num = 0;


    static class Worker implements Runnable {

        //private Lock lock = new ZookeeperDistributeLock();
        private Lock lock = new ZookeeperDistributeListenLock();

        public void run() {
            try {
                lock.getLock();
                num++;
                Thread.sleep(20000);
                LOG.info("num是: " + num + "当前时间:" + System.currentTimeMillis());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unLock();
            }
        }
    }


    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new Thread(new Worker()).start();
        }

    }
}
