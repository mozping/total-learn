package indi.mozping.lock;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by mozping
 * @Classname ZookeeperAbstractLock
 * @Description TODO
 * @Date 2019/4/19 13:44
 */
public abstract class ZookeeperAbstractLock implements Lock {

    private static final Logger LOG = LoggerFactory.getLogger(ZookeeperAbstractLock.class);
    private static final String ZookeeperConnectString = "192.168.13.48:2181";

    //创建zk连接
    protected ZkClient zkClient = new ZkClient(ZookeeperConnectString);
    protected static final String PATH = "/lock";
    protected static final String PATH2 = "/lock2";

    public void getLock() {
        if (tryLock()) {
            LOG.info("获取zk锁资源...");
        } else {
            //等待
            waitLock();
            //重新获取锁资源
            getLock();
        }
    }

    //获取锁资源
    abstract boolean tryLock();

    //等待
    abstract void waitLock();

}
