package indi.mozping.lock;

/**
 * @author by mozping
 * @Classname Lock
 * @Description TODO
 * @Date 2019/4/19 13:41
 */
public interface Lock {

    public void getLock();

    public void unLock();

}
