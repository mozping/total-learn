package indi.mozping;

import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author by mozping
 * @Classname ZkClientHelloTest
 * @Description TODO
 * @Date 2019/4/20 17:06
 * zkClient：简化了原生客户端的使用
 * 1.实例化连接对象更加简单
 * 2.可以支持多级新增，不指定值时，值为null
 * 3.watch机制的简化
 */
public class ZkClientHelloTest {

    private static final Logger LOG = LoggerFactory.getLogger(ZkClientHelloTest.class);
    private static final String ZookeeperConnectString = "192.168.11.215:2181";

    @Test
    public void test1() throws InterruptedException {
        //1.创建zk连接
        ZkClient zkClient = new ZkClient(ZookeeperConnectString);
        LOG.info("连接zookeeper,:{}", zkClient);

        //2.多级新增节点

        String deepNode = "/aa/bb/cc";
        String parent = "/aa";

        zkClient.createPersistent(deepNode, true);
        List<String> children = zkClient.getChildren("/");
        for (String children1 : children) {
            LOG.info("节点名称是:{},  ", children1);
        }
        zkClient.deleteRecursive(parent);
    }


    //Watch机制
    @Test
    public void test2() throws InterruptedException {


        //1.创建zk连接
        ZkClient zkClient = new ZkClient(ZookeeperConnectString);
        LOG.info("连接zookeeper,:{}", zkClient);

        String parent = "/aa";
        //2.注册监听--数据变化时候的监听器
        zkClient.subscribeDataChanges("/aa", new IZkDataListener() {
            public void handleDataChange(String dataPath, Object data) throws Exception {
                LOG.info("数据修改了,路径是:{},新的值是:{}", dataPath, data);
            }

            public void handleDataDeleted(String dataPath) throws Exception {
                LOG.info("数据删除了...");
            }
        });
        //2.注册监听--子节点变化时候的监听器
        zkClient.subscribeChildChanges("/aa", new IZkChildListener() {
            public void handleChildChange(String parentPath, List<String> currentChilds) throws Exception {
                LOG.info("子节点修改了,路径是:{},新的子节点列表是:{}", parentPath, currentChilds);
            }
        });


        //3.多级新增节点
        String deepNode = "/aa/bb/cc";
        //创建子节点触发Watch
        zkClient.createPersistent(deepNode, true);
        List<String> children = zkClient.getChildren("/");
        LOG.info("节点名称是:{},  ", children);
        //写入节点数据触发Watch
        zkClient.writeData(parent, "123");
        //最后删除节点
        zkClient.deleteRecursive(parent);
    }


}
