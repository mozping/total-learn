package indi.mozping;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.zookeeper.CreateMode;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author by mozping
 * @Classname CuratorOperatorDemoAsyn
 * @Description Curator异步操作
 * @Date 2019/6/17 11:10
 */
public class CuratorOperatorDemoAsyn {

    static final CountDownLatch countDownLatch = new CountDownLatch(1);

    public static void main(String[] args) throws InterruptedException {
        CuratorFramework curatorFramework = CuratorClientUtils.getInstance();
        System.out.println("连接成功.........");

        //异步操作
        ExecutorService service = Executors.newFixedThreadPool(1);
        try {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL)
                    .inBackground(new BackgroundCallback() {
                        public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
                            System.out.println(Thread.currentThread().getName()
                                    + "->resultCode:" + curatorEvent.getResultCode()
                                    + "->"
                                    + curatorEvent.getType());
                            countDownLatch.countDown();
                        }
                    }, service).forPath("/test", "testVal".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        countDownLatch.await();
        service.shutdown();
    }
}