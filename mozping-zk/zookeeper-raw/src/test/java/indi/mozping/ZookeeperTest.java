package indi.mozping;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author by mozping
 * @Classname ZookeeperTest
 * @Description TODO
 * @Date 2019/4/20 14:48
 * <p>
 * 1.zk的原生客户端的依赖包是:
 * <dependency>
 * <groupId>org.apache.zookeeper</groupId>
 * <artifactId>zookeeper</artifactId>
 * <version>3.4.12</version>
 * </dependency>
 * 2.原生客户端的问题
 * 1.连接是异步的，需要监听配合countDownLatch，麻烦
 * 2.Watch机制需要重复注册，冗余不好用
 * 3.Session重连机制，
 * 4.开发复杂度比较高，代码量大
 * 5.不支持多级节点的新增和删除
 */

public class ZookeeperTest {

    private static final String CONNECT_STRING = "192.168.11.215";
    private static final int CONNECT_TIME_OUT = 5000;

    private static final Logger LOG = LoggerFactory.getLogger(ZookeeperTest.class);
    private static CountDownLatch countDownLatch = new CountDownLatch(1);


    /**
     * 1.创建节点
     * 2.获取根节点下面的全部节点
     */
    @Test
    public void test1() throws Exception {
        ZooKeeper zooKeeper = new ZooKeeper(CONNECT_STRING, CONNECT_TIME_OUT, new Watcher() {
            public void process(WatchedEvent event) {
                if (event.getState() == Event.KeeperState.SyncConnected) {
                    countDownLatch.countDown();
                }
            }
        });
        //在这里阻塞，直到连接成功之后，再放行
        countDownLatch.await();
        //1.增；创建节点，指定权限和节点模式
        zooKeeper.create("/testTemp",
                "testTemp".getBytes(),
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.PERSISTENT);
        //2.查；获取节点
        List<String> children = zooKeeper.getChildren("/", null);
        Stat stat = new Stat();
        for (String children1 : children) {
            String value = new String(zooKeeper.getData("/" + children1, false, stat));
            LOG.info("节点名称是:{}, 值是：{},版本是:{}", children1, value, stat.getVersion());
        }
        LOG.info("-----------------");
        Thread.sleep(3000);
        //3.改；修改节点
        zooKeeper.setData("/testTemp", "newTestTemp".getBytes(), -1);
        for (String children1 : children) {
            String value = new String(zooKeeper.getData("/" + children1, false, stat));
            LOG.info("节点名称是:{}, 值是：{},版本是:{}", children1, value, stat.getVersion());
        }
        //3.删；删除节点 -1表示不做版本控制，可以指定对某个版本的数据进行操作
        zooKeeper.delete("/testTemp", -1);

    }

    /**
     * Watch对象只能使用一次：
     * Watch机制，当数据变更时通知客户端
     */
    @Test
    public void test2() throws Exception {
        ZooKeeper zooKeeper = new ZooKeeper(CONNECT_STRING, CONNECT_TIME_OUT, new Watcher() {
            public void process(WatchedEvent event) {
                if (event.getState() == Event.KeeperState.SyncConnected) {
                    countDownLatch.countDown();
                    LOG.info("连接成功，state:{}", event.getState());
                }
                if (event.getType() == Event.EventType.NodeDataChanged) {
                    LOG.info("节点数据修改，carType:{}", event.getType());
                }
                if (event.getType() == Event.EventType.NodeCreated) {
                    LOG.info("节点新增，carType:{}", event.getType());
                }


            }
        });
        //在这里阻塞，直到连接成功之后，再放行
        countDownLatch.await();
        Stat stat = new Stat();
        //1.增；创建节点，指定权限和节点模式 --- 新增不会触发数据变化的NodeDataChanged事件
        zooKeeper.create("/testTemp",
                "testTemp".getBytes(),
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.PERSISTENT);

        //注意这里，在set值之前需要先get一次，将watch设置为true，否则即使在set里面修改了值，监听器也不会被触发
        //而且如果set一次，后面修改2次的话，也只有第一次修改会触发watch，需要2次都触发的话，在第一次修改完之后
        //需要再get一次
        //在zk的原生客户端里面，watch是一次性的，这里get方法里面的watch参数为true相当于使用的前面的watch，也
        //可以自己再创建一个新的watch，
        zooKeeper.getData("/testTemp", true, stat);//这里的关键是将watch为true设置进去
        //2.修改
        zooKeeper.setData("/testTemp", "newTestTemp".getBytes(), -1);
        zooKeeper.getData("/testTemp", true, stat);//这里的关键是将watch为true设置进去
        zooKeeper.setData("/testTemp", "newTestTemp1".getBytes(), -1);

        //3.查询
        LOG.info("-----------------");
        List<String> children = zooKeeper.getChildren("/", true);

        for (String children1 : children) {
            String value = new String(zooKeeper.getData("/" + children1, false, stat));
            LOG.info("节点名称是:{}, 值是：{},版本是:{}", children1, value, stat.getVersion());
        }
        LOG.info("-----------------");
    }
}
