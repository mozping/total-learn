package indi.mozping;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @author by mozping
 * @Classname HelloZookeeper
 * @Description 使用zookeeper原生客户端
 * @Date 2019/4/20 14:44
 */
public class HelloZookeeper {

    private static final String CONNECT_STRING = "192.168.13.53";
    private static final int CONNECT_TIME_OUT = 5000;
    private static CountDownLatch countDownLatch = new CountDownLatch(1);
    private static final Logger LOG = LoggerFactory.getLogger(HelloZookeeper.class);


    public static void main(String[] args) throws IOException, InterruptedException {

        ZooKeeper zooKeeper = new ZooKeeper(CONNECT_STRING, CONNECT_TIME_OUT, new Watcher() {
            public void process(WatchedEvent watchedEvent) {
                //如果当前的连接状态是连接成功的，那么方向主线程
                if (watchedEvent.getState() == Event.KeeperState.SyncConnected) {
                    countDownLatch.countDown();
                    LOG.info(watchedEvent.getState().toString());
                }
            }
        });
        //1.主线程在这里阻塞等待，因为可能到这里还没有连接成功
        countDownLatch.await();
        LOG.info(zooKeeper.getState().toString());
    }
}
