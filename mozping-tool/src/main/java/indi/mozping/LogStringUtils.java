package indi.mozping;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * @author moore.mo
 * @description indi.mozping LogStringUtils
 * @date 2021/1/28 7:43 下午
 **/
public class LogStringUtils {


    public static String log2JSONString() {
        String str = "{\\\"action\\\":\\\"push_order\\\",\\\"actionType\\\":1001,\\\"createTime\\\":1611824698565,\\\"from\\\":\\\"driver-subsidy\\\",\\\"data\\\":{\\\"orderId\\\":586576574,\\\"userId\\\":30465882,\\\"epId\\\":0,\\\"createdAt\\\":\\\"2021-01-28 17:04:45\\\",\\\"basePriceFen\\\":3600,\\\"abCreatedAt\\\":null,\\\"vehicleId\\\":1,\\\"vehicleName\\\":null,\\\"cityId\\\":1001,\\\"orderType\\\":1,\\\"orderSourceType\\\":\\\"1\\\",\\\"vehicleAttr\\\":0,\\\"businessType\\\":1,\\\"latLong\\\":\\\"23.13053613568|113.28289903965,23.09176690233|113.27215417415\\\",\\\"strategyId\\\":106212,\\\"orderDistance\\\":6773,\\\"hitOnePrice\\\":0,\\\"delaySecond\\\":60,\\\"subsidyStartedAt\\\":1611824758528,\\\"abtestGroupName\\\":null}}";
        String result = StringEscapeUtils.unescapeJava(str);
        return result;
    }
}

