package indi.mozping.http;

import com.xiaoleilu.hutool.http.HttpUtil;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;

/**
 * @author by mozping
 * @Classname HttpTest
 * @Description TODO
 * @Date 2020/9/22 9:41
 */
public class HttpTest {

    private static final Log log = LogFactory.get(HttpTest.class.getName());

    public static void main(String[] args) {

        String result = HttpUtil.get("https://www.baidu.com");

        log.info("result:{}", result.substring(0, 100));
    }
}