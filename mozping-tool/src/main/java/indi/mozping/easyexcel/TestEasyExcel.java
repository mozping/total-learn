package indi.mozping.easyexcel;

import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

/**
 * @author moore.mo
 * @description indi.mozping.easyexcel TestEasyExcel
 * @date 2021/3/2 11:03 上午
 **/
public class TestEasyExcel {

    public static void main(String[] args) throws Exception {
        List<ArticleWriteModel> records = Lists.newArrayList();
        ArticleWriteModel articleWriteModel = new ArticleWriteModel();
        articleWriteModel.setId(0L);
        articleWriteModel.setCode("1");
        articleWriteModel.setTitle("测试");
        articleWriteModel.setKeywords("测试key");
        articleWriteModel.setAuthor("moore");
        articleWriteModel.setPublishTime(""+new Date());

        records.add(articleWriteModel);
        ExportFile exportFile = new ExportFile();
        String pathName = "./a.xlsx";
        exportFile.writeExcel(pathName, records, ArticleWriteModel.class);
    }
}