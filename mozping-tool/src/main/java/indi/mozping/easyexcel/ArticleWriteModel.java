package indi.mozping.easyexcel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

/**
 * @author moore.mo
 * @description indi.mozping.easyexcel ArticleWriteModel
 * @date 2021/3/2 10:51 上午
 **/
@Data
public class ArticleWriteModel extends BaseRowModel {

    @ExcelProperty(value = "编号", index = 0)
    private Long id;

    @ExcelProperty(value = "编码", index = 1)
    private String code;

    @ExcelProperty(value = "标题", index = 2)
    private String title;

    @ExcelProperty(value = "关键字", index = 3)
    private String keywords;

    @ExcelProperty(value = "作者", index = 4)
    private String author;

    @ExcelProperty(value = "发布时间", index = 5)
    private String publishTime;

}


