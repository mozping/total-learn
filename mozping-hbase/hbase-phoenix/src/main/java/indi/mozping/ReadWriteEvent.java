package indi.mozping.mozping;


import java.sql.*;
import java.util.List;


/**
 * @author by mozping
 * @Classname ReadWriteDeputyFace
 * @Description TODO
 * @Date 2020/4/7 17:27
 */
public class ReadWriteEvent {

    private static Connection connection = null;
    private static PreparedStatement prepareStatement = null;
    private static final String sql = "upsert into event (aid,sys_code,thumbnail_id,thumbnail_url,image_id,\n" +
            "image_url,feature_info,algo_version,gender_info,age_info,hairstyle_info,hat_info,glasses_info,race_info,mask_info,skin_info,pose_info" +
            ",quality_info,target_rect,target_rect_float,land_mark_info,source_id,source_type" +
            ",site,feature_quality,time,create_time,save_time,score" +
            ",column1,column2,column3,target_thumbnail_rect,field1) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    static {
        try {
            Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
            connection = DriverManager.getConnection(Config.CONN_URL);
            prepareStatement = connection.prepareStatement(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {

        writeEvent();
    }


    private static void writeEvent() {

        try {
            prepareStatement.setString(1, "1");
            prepareStatement.setString(2, "1");
            prepareStatement.setString(3, "1");
            prepareStatement.setString(4, "1");
            prepareStatement.setString(5, "1");
            prepareStatement.setString(6, "1");

            prepareStatement.setBytes(7, "1".getBytes());

            prepareStatement.setString(8, "1");
            prepareStatement.setString(9, "1");
            prepareStatement.setString(10, "1");
            prepareStatement.setString(11, "1");
            prepareStatement.setString(12, "1");
            prepareStatement.setString(13, "1");
            prepareStatement.setString(14, "1");
            prepareStatement.setString(15, "1");
            prepareStatement.setString(16, "1");
            prepareStatement.setString(17, "1");

            prepareStatement.setFloat(18, 2.0F);

            prepareStatement.setString(19, "2");
            prepareStatement.setString(20, "2");
            prepareStatement.setString(21, "2");
            prepareStatement.setString(22, "2");
            prepareStatement.setString(23, "2");
            prepareStatement.setString(24, "2");

            prepareStatement.setFloat(25, 2.0F);

            prepareStatement.setDate(26, new Date(System.currentTimeMillis()));
            prepareStatement.setDate(27, new Date(System.currentTimeMillis()));
            prepareStatement.setDate(28, new Date(System.currentTimeMillis()));

            prepareStatement.setString(29, "2");
            prepareStatement.setString(30, "2");
            prepareStatement.setString(31, "2");
            prepareStatement.setString(32, "2");
            prepareStatement.setString(33, "2");
            prepareStatement.setString(34, "2");

            int i = prepareStatement.executeUpdate();
            System.out.println(i + " --> " + i);
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void writeEvent(Event event) throws SQLException {

        prepareStatement.setString(1, event.getAid());
        prepareStatement.setString(2, event.getSys_code());
        prepareStatement.setString(3, event.getThumbnail_id());
        prepareStatement.setString(4, event.getThumbnail_url());
        prepareStatement.setString(5, event.getImage_id());
        prepareStatement.setString(6, event.getImage_url());

        prepareStatement.setBytes(7, event.getFeature_info());

        prepareStatement.setString(8, event.getAlgo_version());
        prepareStatement.setString(9, event.getGender_info());
        prepareStatement.setString(10, event.getAge_info());
        prepareStatement.setString(11, event.getHairstyle_info());
        prepareStatement.setString(12, event.getHat_info());
        prepareStatement.setString(13, event.getGlasses_info());
        prepareStatement.setString(14, event.getRace_info());
        prepareStatement.setString(15, event.getMask_info());
        prepareStatement.setString(16, event.getSkin_info());
        prepareStatement.setString(17, event.getPose_info());

        prepareStatement.setFloat(18, event.getQuality_info());

        prepareStatement.setString(19, event.getTarget_rect());
        prepareStatement.setString(20, event.getTarget_rect_float());
        prepareStatement.setString(21, event.getLand_mark_info());
        prepareStatement.setString(22, event.getSource_id());
        prepareStatement.setString(23, event.getSource_type());
        prepareStatement.setString(24, event.getSite());

        prepareStatement.setFloat(25, event.getFeature_quality());

        prepareStatement.setDate(26, new Date(event.getTime().getTime()));
        prepareStatement.setDate(27, new Date(event.getCreate_time().getTime()));
        prepareStatement.setDate(28, new Date(event.getSave_time().getTime()));

        prepareStatement.setString(29, event.getScore());
        prepareStatement.setString(30, event.getColumn1());
        prepareStatement.setString(31, event.getColumn2());
        prepareStatement.setString(32, event.getColumn3());
        prepareStatement.setString(33, event.getTarget_thumbnail_rect());
        prepareStatement.setString(34, event.getField1());
        prepareStatement.executeUpdate();
        connection.commit();
    }

    private static void writeEvent(List<Event> eventList) throws SQLException {
        for (Event event : eventList) {
            prepareStatement.setString(1, event.getAid());
            prepareStatement.setString(2, event.getSys_code());
            prepareStatement.setString(3, event.getThumbnail_id());
            prepareStatement.setString(4, event.getThumbnail_url());
            prepareStatement.setString(5, event.getImage_id());
            prepareStatement.setString(6, event.getImage_url());

            prepareStatement.setBytes(7, event.getFeature_info());

            prepareStatement.setString(8, event.getAlgo_version());
            prepareStatement.setString(9, event.getGender_info());
            prepareStatement.setString(10, event.getAge_info());
            prepareStatement.setString(11, event.getHairstyle_info());
            prepareStatement.setString(12, event.getHat_info());
            prepareStatement.setString(13, event.getGlasses_info());
            prepareStatement.setString(14, event.getRace_info());
            prepareStatement.setString(15, event.getMask_info());
            prepareStatement.setString(16, event.getSkin_info());
            prepareStatement.setString(17, event.getPose_info());

            prepareStatement.setFloat(18, event.getQuality_info());

            prepareStatement.setString(19, event.getTarget_rect());
            prepareStatement.setString(20, event.getTarget_rect_float());
            prepareStatement.setString(21, event.getLand_mark_info());
            prepareStatement.setString(22, event.getSource_id());
            prepareStatement.setString(23, event.getSource_type());
            prepareStatement.setString(24, event.getSite());

            prepareStatement.setFloat(25, event.getFeature_quality());

            prepareStatement.setDate(26, new Date(event.getTime().getTime()));
            prepareStatement.setDate(27, new Date(event.getCreate_time().getTime()));
            prepareStatement.setDate(28, new Date(event.getSave_time().getTime()));

            prepareStatement.setString(29, event.getScore());
            prepareStatement.setString(30, event.getColumn1());
            prepareStatement.setString(31, event.getColumn2());
            prepareStatement.setString(32, event.getColumn3());
            prepareStatement.setString(33, event.getTarget_thumbnail_rect());
            prepareStatement.setString(34, event.getField1());
            prepareStatement.executeUpdate();
        }
        connection.commit();
    }
}
