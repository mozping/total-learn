package indi.mozping.mozping;

import lombok.Data;

import java.util.Date;

/**
 * @author by mozping
 * @Classname PersonFile
 * @Description ������Bean
 * @Date 2020/4/10 15:52
 */
@Data
public class PersonFile {

    private String archive_id;
    private String person_name;
    private int marriage_status;
    private String nation;
    private String height;
    private String current_residential_address;

    private Date birthday;
    private String age;
    private String avatar_url;
    private String thumbnail_url;

    public byte[] feature_info;

    private String image_url;
    private String image_id;
    private String algo_version;

    private String image_type;
    private String target_rect;

    private int gender;

    private String occupation;
    private String highest_degree;
    private String identity_no;
    private String phone_no;
    private String email;
    private String source_type;
    private String mark_info;
    private String sys_code;

    private String column1;
    private String column2;
    private String column3;


    private Date time;
    private Date sync_time;
    private Date create_time;
    private Date update_time;

    private int delete_flag;
    private int archive_type;
    private float confidence;

}