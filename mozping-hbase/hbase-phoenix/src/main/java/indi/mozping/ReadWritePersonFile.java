package indi.mozping.mozping;

import java.sql.*;
import java.util.List;

/**
 * @author by mozping
 * @Classname ReadWritePerson
 * @Description TODO
 * @Date 2020/4/9 17:22
 */
public class ReadWritePersonFile {


    private static Connection connection = null;
    private static PreparedStatement prepareStatement = null;
//    private static final String sql = "upsert into personfile (archive_id,person_name,marriage_status,nation,height," +
//            "current_residential_address,birthday,age,avatar_url,thumbnail_url,feature_info,image_url,image_id,algo_version,image_type,target_rect,gender" +
//            ",occupation,highest_degree,identity_no,phone_no,email,source_type" +
//            ",mark_info,sys_code,column1,column2,column3,time,sync_time,create_time,update_time,delete_flag,archive_type,confidence)" +
//            " values(?,?,?,?,?,?,?.?,?,?,?,?,?,?.?,?,?,?,?,?,?.?,?,?,?,?,?,?.?,?,?,?,?,?,?)";

    private static final String sql = "upsert into personfile values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    static {
        try {
            Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
            connection = DriverManager.getConnection(Config.CONN_URL);
            prepareStatement = connection.prepareStatement(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {

        //readDeputyFace();
        writePersonFile();
    }


    private static void writePersonFile() {

        try {
            prepareStatement.setString(1, "1");
            prepareStatement.setString(2, "1");
            prepareStatement.setInt(3, 1);
            prepareStatement.setString(4, "1");
            prepareStatement.setString(5, "1");
            prepareStatement.setString(6, "1");

            prepareStatement.setDate(7, new Date(System.currentTimeMillis()));

            prepareStatement.setString(8, "1");
            prepareStatement.setString(9, "1");
            prepareStatement.setString(10, "1");
            prepareStatement.setBytes(11, "1".getBytes());
            prepareStatement.setString(12, "1");
            prepareStatement.setString(13, "1");
            prepareStatement.setString(14, "1");
            prepareStatement.setString(15, "1");
            prepareStatement.setString(16, "1");

            prepareStatement.setInt(17, 1);

            prepareStatement.setString(18, "2");
            prepareStatement.setString(19, "2");
            prepareStatement.setString(20, "2");
            prepareStatement.setString(21, "2");
            prepareStatement.setString(22, "2");
            prepareStatement.setString(23, "2");
            prepareStatement.setString(24, "2");
            prepareStatement.setString(25, "2");
            prepareStatement.setString(26, "2");
            prepareStatement.setString(27, "2");
            prepareStatement.setString(28, "2");

            prepareStatement.setDate(29, new Date(System.currentTimeMillis()));
            prepareStatement.setDate(30, new Date(System.currentTimeMillis()));
            prepareStatement.setDate(31, new Date(System.currentTimeMillis()));
            prepareStatement.setDate(32, new Date(System.currentTimeMillis()));


            prepareStatement.setInt(33, 2);
            prepareStatement.setInt(34, 2);
            prepareStatement.setFloat(35, 2.0F);

            int i = prepareStatement.executeUpdate();
            System.out.println(i + " --> " + i);
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void writePersonFile(PersonFile personFile) throws SQLException {
        prepareStatement.setString(1, personFile.getArchive_id());
        prepareStatement.setString(2, personFile.getPerson_name());
        prepareStatement.setInt(3, personFile.getMarriage_status());
        prepareStatement.setString(4, personFile.getNation());
        prepareStatement.setString(5, personFile.getHeight());
        prepareStatement.setString(6, personFile.getCurrent_residential_address());

        prepareStatement.setDate(7, new Date(personFile.getBirthday().getTime()));

        prepareStatement.setString(8, personFile.getAge());
        prepareStatement.setString(9, personFile.getAvatar_url());
        prepareStatement.setString(10, personFile.getThumbnail_url());
        prepareStatement.setBytes(11, personFile.getFeature_info());
        prepareStatement.setString(12, personFile.getImage_url());
        prepareStatement.setString(13, personFile.getImage_id());
        prepareStatement.setString(14, personFile.getAlgo_version());
        prepareStatement.setString(15, personFile.getImage_type());
        prepareStatement.setString(16, personFile.getTarget_rect());

        prepareStatement.setInt(17, personFile.getGender());

        prepareStatement.setString(18, personFile.getOccupation());
        prepareStatement.setString(19, personFile.getHighest_degree());
        prepareStatement.setString(20, personFile.getIdentity_no());
        prepareStatement.setString(21, personFile.getPhone_no());
        prepareStatement.setString(22, personFile.getEmail());
        prepareStatement.setString(23, personFile.getSource_type());
        prepareStatement.setString(24, personFile.getMark_info());
        prepareStatement.setString(25, personFile.getSys_code());
        prepareStatement.setString(26, personFile.getColumn1());
        prepareStatement.setString(27, personFile.getColumn2());
        prepareStatement.setString(28, personFile.getColumn3());

        prepareStatement.setDate(29, new Date(personFile.getTime().getTime()));
        prepareStatement.setDate(30, new Date(personFile.getSync_time().getTime()));
        prepareStatement.setDate(31, new Date(personFile.getCreate_time().getTime()));
        prepareStatement.setDate(32, new Date(personFile.getUpdate_time().getTime()));


        prepareStatement.setInt(33, personFile.getDelete_flag());
        prepareStatement.setInt(34, personFile.getArchive_type());
        prepareStatement.setFloat(35, personFile.getConfidence());

        prepareStatement.executeUpdate();
        connection.commit();
    }

    private static void writeEvent(List<Event> eventList) throws SQLException {
        for (Event event : eventList) {
            prepareStatement.executeUpdate();
        }
        connection.commit();
    }

}