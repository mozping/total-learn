package indi.mozping.mozping;

import lombok.Data;

import java.util.Date;

/**
 * @author by mozping
 * @Classname Face
 * @Description �����Bean
 * @Date 2019/4/12 15:49
 */
@Data
public class DeputyFace {

    private long id;
    private String aid;
    public byte[] feature_val;


    private String image_id;
    private String image_url;
    private String thumbnail_id;
    private String thumbnail_url;

    private int present_flag;
    private Date create_time;
    private Date modify_time;

    private int status;
    private String source;
    private String algo_version;

    private String column1;
    private String column2;
    private String column3;

    private int pq_probe;
    public byte[] pq_code;

}
