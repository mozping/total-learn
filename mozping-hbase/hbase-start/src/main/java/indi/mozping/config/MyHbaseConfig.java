package indi.mozping.mozping.config;

/**
 * @author by mozping
 * @Classname MyHbaseConfig
 * @Description TODO
 * @Date 2020/4/3 13:56
 */
public class MyHbaseConfig {


    public static final String ZK_PORT = "2181";
    public static final String ZK_ADD = "192.168.11.72";
    public static final String HBASE_PATH = "/hbase";
    public static final String HBASE_MASTER_ADDR = "192.168.11.72:16010";


}