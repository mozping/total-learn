package indi.mozping.mozping.clusterhbase;

/**
 * @author by mozping
 * @Classname HbaseReadTest
 * @Description TODO
 * @Date 2020/4/2 15:14
 */

import indi.mozping.mozping.config.MyHbaseConfig;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class HbaseWriteTest {

    private static Configuration configuration;
    private static Connection connection;
    private static Admin admin;

    public static void main(String[] args) throws IOException {
        //创建表
        System.out.println("创建表...");
        createTable("t2", new String[]{"cf1", "cf2"});

        //插入记录
        System.out.println("新增记录...");
        insterRow("t2", "rw1", "cf1", "c1", "val1");
        insterRow("t2", "rw1", "cf2", "c2", "val2");
        insterRow("t2", "rw2", "cf1", "c1", "val3");
        insterRow("t2", "rw2", "cf2", "c2", "val4");
        insterRow("t2", "rw3", "cf1", "c1", "val5");
        insterRow("t2", "rw3", "cf2", "c2", "val6");

        System.out.println("扫描表...");
        scanData("t2", "rw1", "rw3");

        System.out.println("删除记录...");
        deleRow("t2", "rw1");

        System.out.println("删除表...");
        deleteTable("t2");
    }

    /**
     * @Description: 初始化链接
     * @date 2020/4/3 14:02
     */
    private static void init() {
        //设置本地 hadoop 的路径
        System.setProperty("hadoop.home.dir", "D:\\hadoop\\hadoop-2.7.4");
        configuration = HBaseConfiguration.create();

        //设置相关地址端口
        configuration.set("hbase.zookeeper.property.clientPort", MyHbaseConfig.ZK_PORT);
        configuration.set("hbase.zookeeper.quorum", MyHbaseConfig.ZK_ADD);
        configuration.set("zookeeper.znode.parent", MyHbaseConfig.HBASE_PATH);
        configuration.set("hbase.master", MyHbaseConfig.HBASE_MASTER_ADDR);

        try {
            connection = ConnectionFactory.createConnection(configuration);
            admin = connection.getAdmin();
        } catch (IOException e) {
            System.out.println("初始化异常...");
        }
        System.out.println("初始化完成...");
    }

    /**
     * @Description: 关闭连接
     * @date 2020/4/3 14:02
     */
    public static void close() {
        try {
            if (null != admin) {
                admin.close();
            }
            if (null != connection) {
                connection.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("关闭完成...");
    }

    // 建表
    public static void createTable(String tableNmae, String[] cols) throws IOException {

        init();
        TableName tableName = TableName.valueOf(tableNmae);

        if (admin.tableExists(tableName)) {
            System.out.println("talbe is exists!");
        } else {
            HTableDescriptor hTableDescriptor = new HTableDescriptor(tableName);
            for (String col : cols) {
                HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(col);
                hTableDescriptor.addFamily(hColumnDescriptor);
            }
            admin.createTable(hTableDescriptor);
        }
        System.out.println("创建完毕...");
        close();
    }

    // 删表
    public static void deleteTable(String tableName) throws IOException {
        init();
        TableName tn = TableName.valueOf(tableName);
        if (admin.tableExists(tn)) {
            admin.disableTable(tn);
            admin.deleteTable(tn);
        }
        close();
    }

    // 查看已有表
    public static void listTables() throws IOException {
        init();
        System.out.println("开始获取...");
        HTableDescriptor hTableDescriptors[] = admin.listTables();
        System.out.println("数目：" + hTableDescriptors.length);
        for (HTableDescriptor hTableDescriptor : hTableDescriptors) {
            System.out.println("表名: " + hTableDescriptor.getNameAsString());
        }
        close();
    }

    // 插入数据
    public static void insterRow(String tableName, String rowkey, String colFamily, String col, String val)
            throws IOException {
        init();
        Table table = connection.getTable(TableName.valueOf(tableName));
        Put put = new Put(Bytes.toBytes(rowkey));
        put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col), Bytes.toBytes(val));
        table.put(put);

        // 批量插入
        /*
         * List<Put> putList = new ArrayList<Put>(); puts.add(put);
         * table.put(putList);
         */
        table.close();
        close();
    }

    // 删除数据
    public static void deleRow(String tableName, String rowkey) throws IOException {
        init();
        Table table = connection.getTable(TableName.valueOf(tableName));
        Delete delete = new Delete(Bytes.toBytes(rowkey));
        // 删除指定列族
        // delete.addFamily(Bytes.toBytes(colFamily));
        // 删除指定列
        // delete.addColumn(Bytes.toBytes(colFamily),Bytes.toBytes(col));
        table.delete(delete);
        // 批量删除
        /*
         * List<Delete> deleteList = new ArrayList<Delete>();
         * deleteList.add(delete); table.delete(deleteList);
         */
        table.close();
        close();
    }

    // 根据rowkey查找数据
    public static void getData(String tableName, String rowkey, String colFamily, String col) throws IOException {
        init();
        Table table = connection.getTable(TableName.valueOf(tableName));
        Get get = new Get(Bytes.toBytes(rowkey));
        // 获取指定列族数据
        // get.addFamily(Bytes.toBytes(colFamily));
        // 获取指定列数据
        // get.addColumn(Bytes.toBytes(colFamily),Bytes.toBytes(col));
        Result result = table.get(get);

        showCell(result);
        table.close();
        close();
    }

    // 格式化输出
    public static void showCell(Result result) {
        Cell[] cells = result.rawCells();
        for (Cell cell : cells) {
            System.out.println("RowName:" + new String(CellUtil.cloneRow(cell)) + " ");
            System.out.println("Timetamp:" + cell.getTimestamp() + " ");
            System.out.println("column Family:" + new String(CellUtil.cloneFamily(cell)) + " ");
            System.out.println("row Name:" + new String(CellUtil.cloneQualifier(cell)) + " ");
            System.out.println("value:" + new String(CellUtil.cloneValue(cell)) + " ");
        }
    }

    // 批量查找数据
    public static void scanData(String tableName, String startRow, String stopRow) throws IOException {
        init();
        Table table = connection.getTable(TableName.valueOf(tableName));
        Scan scan = new Scan();
        // scan.setStartRow(Bytes.toBytes(startRow));
        // scan.setStopRow(Bytes.toBytes(stopRow));
        ResultScanner resultScanner = table.getScanner(scan);
        for (Result result : resultScanner) {
            showCell(result);
        }
        table.close();
        close();
    }
}