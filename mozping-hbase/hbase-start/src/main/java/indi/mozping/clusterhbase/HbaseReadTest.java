package indi.mozping.mozping.clusterhbase;

/**
 * @author by mozping
 * @Classname HbaseReadTest 读取hbase
 * @Description TODO
 * @Date 2020/4/2 15:14
 */

import indi.mozping.mozping.config.MyHbaseConfig;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

public class HbaseReadTest {

    private static Configuration configuration;
    private static Connection connection;
    private static Admin admin;

    public static void main(String[] args) throws IOException {
        listTables();
    }

    /**
     * @Description: 初始化链接
     * @date 2020/4/3 14:02
     */
    private static void init() {
        //设置本地 hadoop 的路径
        System.setProperty("hadoop.home.dir", "D:\\hadoop\\hadoop-2.7.4");
        configuration = HBaseConfiguration.create();

        //设置相关地址端口
        configuration.set("hbase.zookeeper.property.clientPort", MyHbaseConfig.ZK_PORT);
        configuration.set("hbase.zookeeper.quorum", MyHbaseConfig.ZK_ADD);
        configuration.set("zookeeper.znode.parent", MyHbaseConfig.HBASE_PATH);
        configuration.set("hbase.master", MyHbaseConfig.HBASE_MASTER_ADDR);

        try {
            connection = ConnectionFactory.createConnection(configuration);
            admin = connection.getAdmin();
        } catch (IOException e) {
            System.out.println("初始化异常...");
        }
        System.out.println("初始化完成...");
    }

    /**
     * @Description: 关闭连接
     * @date 2020/4/3 14:02
     */
    public static void close() {
        try {
            if (null != admin) {
                admin.close();
            }
            if (null != connection) {
                connection.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("关闭完成...");
    }

    /**
     * @Description: 查看已有表
     * @date 2020/4/3 14:02
     */
    public static void listTables() throws IOException {
        init();
        System.out.println("开始获取...");
        HTableDescriptor hTableDescriptors[] = admin.listTables();
        System.out.println("数目：" + hTableDescriptors.length);
        for (HTableDescriptor hTableDescriptor : hTableDescriptors) {
            System.out.println("表名: " + hTableDescriptor.getNameAsString());
        }
        close();
    }
}