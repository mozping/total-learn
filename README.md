# total-learn

## 一、介绍
自己学习时编写的代码和归档笔记

## 二、模块说明
1.mozping-jdk：关于jdk的学习代码示例，如枚举，泛型，注解，集合等
2.mozping-jvm：关于jvm的学习代码示例
3.mozping-mybatis：关于mybatis框架的学习示例代码
4.mozping-spring：关于spring框架的学习示例代码
5.mozping-springmvc：关于springmvc框架的学习示例代码
6.mozping-mq：关于mq学习示例代码
7.mozping-log：关于log日志框架学习示例代码
8.mozping-docker：关于docker学习示例代码
9.mozping-zk：关于zookeeper学习示例代码
9.mozping-nosql：关于nosql学习示例代码
10.mozping-scene：场景问题示例代码
11.mozping-maven：maven问题
12.mozping-pattern：设计模式



#### 使用说明

1. xxxx
2. xxxx
3. xxxx



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/) 