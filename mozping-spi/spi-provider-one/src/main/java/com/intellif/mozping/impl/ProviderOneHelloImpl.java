package com.intellif.mozping.impl;

import com.intellif.mozping.itf.SayHello;

/**
 * @author by mozping
 * @Classname ProviderOneHelloImpl
 * @Description TODO
 * @Date 2018/12/4 20:27
 */
public class ProviderOneHelloImpl implements SayHello {
    @Override
    public String hello() {
        String str = "Hello said by provider one ...";
        System.out.println("ProviderOneHelloImpl: " + str);
        return str;
    }
}
