package com.intellif.mozping.impl;

import com.intellif.mozping.itf.SayHello;

/**
 * @author by mozping
 * @Classname ProviderTwoHelloImpl
 * @Description TODO
 * @Date 2018/12/4 20:30
 */
public class ProviderTwoHelloImpl implements SayHello {
    @Override
    public String hello() {
        String str = "Hello said by provider two ...";
        System.out.println("ProviderTwoHelloImpl: " + str);
        return str;
    }
}
