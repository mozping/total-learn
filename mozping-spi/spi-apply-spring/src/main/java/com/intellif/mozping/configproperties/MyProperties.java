package com.intellif.mozping.configproperties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author by mozping
 * @Classname MyProperties
 * @Description 属性注入类，自动装配时需要读取该类的配置
 * @Date 2018/12/4 21:07
 */
@ConfigurationProperties(prefix = "my")
public class MyProperties {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
