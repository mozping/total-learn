package com.intellif.mozping.configbean;

/**
 * @author by mozping
 * @Classname Owner
 * @Description 注入到容器的bean
 * @Date 2018/12/4 21:10
 */
public class Owner {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Owner{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
