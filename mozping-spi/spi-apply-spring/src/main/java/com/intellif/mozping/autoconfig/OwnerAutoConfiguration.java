package com.intellif.mozping.autoconfig;

import com.intellif.mozping.configbean.Owner;
import com.intellif.mozping.configproperties.MyProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author by mozping
 * @Classname OwnerAutoConfiguration
 * @Description 自动装配类
 * ConditionalOnClass 有对应的bean类才进行自动装配
 * EnableConfigurationProperties 指定装配时读取配置属性的类
 * @Date 2018/12/4 21:13
 */
@Configuration
@ConditionalOnClass({Owner.class})
@EnableConfigurationProperties(MyProperties.class)
public class OwnerAutoConfiguration {

    @Resource
    private MyProperties myProperties;


    /**
     * @Description: 进行自动装配的方法，将新创建的对象作为bean返回到容器，在返回之前对其进行属性的赋值操作
     * 赋值的初始值来源于配置类的读取，配置类的属性来自于配置文件，这里还可以做一个开关配置，并指定默认值
     * @date 2018/12/4 21:17
     */
    @Bean
    @ConditionalOnMissingBean(Owner.class)
    @ConditionalOnProperty(name = "owner.enabled", matchIfMissing = true)
    public Owner ownerResolver() {
        Owner owner = new Owner();
        owner.setAge(myProperties.getAge());
        owner.setName(myProperties.getName());
        return owner;
    }
}
