package com.intellif.mozping;

import com.intellif.mozping.configbean.Owner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mozping
 * @Classname Application
 * @Description TODO
 * @Date 2018/12/4 21:20
 */
@SpringBootApplication
@RestController
public class Application {

    @Autowired
    Owner owner;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @GetMapping("/onwer")
    public String onwer() {
        System.out.println(owner.toString());
        return owner.toString();
    }
}
