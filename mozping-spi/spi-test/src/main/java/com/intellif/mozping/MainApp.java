package com.intellif.mozping;

import com.intellif.mozping.itf.SayHello;

import java.util.ServiceLoader;

/**
 * @author by mozping
 * @Classname com.intellif.mozping.MainApp
 * @Description TODO
 * @Date 2018/12/4 20:37
 */
public class MainApp {

    public static void main(String[] args) {
        ServiceLoader<SayHello> sayHello = ServiceLoader.load(SayHello.class);
        for (SayHello sh : sayHello) {
            System.out.println("MAIN: " + sh.hello());
        }
    }

}
