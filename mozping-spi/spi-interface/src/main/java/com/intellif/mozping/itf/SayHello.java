package com.intellif.mozping.itf;

/**
 * @author by intellif
 * @Classname SayHello
 * @Description TODO
 * @Date 2018/12/4 20:25
 */
public interface SayHello {

    String hello();
}
