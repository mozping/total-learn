package indi.mozping.test;


import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author moore.mo
 * @description cn.huolala.driver.subsidy.mytest Test
 * @date 2020/12/23 7:50 下午
 **/
public class Test {

    static String str = "{\n" +
            "    \"code\":200,\n" +
            "    \"message\":\"OK\",\n" +
            "    \"data\":{\n" +
            "        \"push_side_infos\":{\n" +
            "            \"app\":[\n" +
            "                {\n" +
            "                    \"time\":1608695887,\n" +
            "                    \"content\":1739272706\n" +
            "                },\n" +
            "                {\n" +
            "                    \"time\":1608705201,\n" +
            "                    \"content\":1739272706\n" +
            "                }\n" +
            "            ],\n" +
            "            \"push_user_id\":[\n" +
            "                {\n" +
            "                    \"time\":1608695887,\n" +
            "                    \"content\":\"iPhone10,1@18182\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"time\":1608705201,\n" +
            "                    \"content\":\"iPhone10,1@18182\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"env\":[\n" +
            "                {\n" +
            "                    \"time\":1608695887,\n" +
            "                    \"content\":\"test\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"time\":1608705201,\n" +
            "                    \"content\":\"test\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"sdk_version\":[\n" +
            "                {\n" +
            "                    \"time\":1608695887,\n" +
            "                    \"content\":\"1.20.0.813_stable_video_201217_154059_express_stable-0-g5d818a09a3_liveroom_release-new-0-gf36b794707_ve-201217_020131-release_\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"time\":1608705201,\n" +
            "                    \"content\":\"1.20.0.813_stable_video_201217_154059_express_stable-0-g5d818a09a3_liveroom_release-new-0-gf36b794707_ve-201217_020131-release_\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"os\":[\n" +
            "                {\n" +
            "                    \"time\":1608695887,\n" +
            "                    \"content\":\"iPhone:14.2:iPhone10.1:iOS\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"time\":1608705201,\n" +
            "                    \"content\":\"iPhone:14.2:iPhone10.1:iOS\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"region\":[\n" +
            "                {\n" +
            "                    \"time\":1608695887,\n" +
            "                    \"content\":\"中国 广东 深圳\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"time\":1608705201,\n" +
            "                    \"content\":\"中国 广东 深圳\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"network\":[\n" +
            "                {\n" +
            "                    \"time\":1608695887,\n" +
            "                    \"content\":\"WIFI/电信\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"time\":1608705201,\n" +
            "                    \"content\":\"WIFI/电信\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"start_time\":[\n" +
            "\n" +
            "            ]\n" +
            "        },\n" +
            "        \"pull_side_infos\":{\n" +
            "            \"app\":[\n" +
            "                {\n" +
            "                    \"time\":1608695939,\n" +
            "                    \"content\":1739272706\n" +
            "                }\n" +
            "            ],\n" +
            "            \"env\":[\n" +
            "                {\n" +
            "                    \"time\":1608695939,\n" +
            "                    \"content\":\"test\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"sdk_version\":[\n" +
            "                {\n" +
            "                    \"time\":1608695939,\n" +
            "                    \"content\":\"1.20.0.813_stable_video_201217_154059_express_stable-0-g5d818a09a3_liveroom_release-new-0-gf36b794707_ve-201217_020131-release_\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"os\":[\n" +
            "                {\n" +
            "                    \"time\":1608695939,\n" +
            "                    \"content\":\"iPhone:14.0:iPhone10.2:iOS\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"region\":[\n" +
            "                {\n" +
            "                    \"time\":1608695939,\n" +
            "                    \"content\":\"中国 广东 深圳\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"network\":[\n" +
            "                {\n" +
            "                    \"time\":1608695939,\n" +
            "                    \"content\":\"WIFI/电信\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"start_time\":[\n" +
            "\n" +
            "            ]\n" +
            "        },\n" +
            "        \"timeline\":[\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608695887,\n" +
            "                    \"end_time\":1608696502,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"开始推流\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608695888,\n" +
            "                    \"end_time\":1608696504,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"登录房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608695939,\n" +
            "                    \"end_time\":1608696331,\n" +
            "                    \"type\":1,\n" +
            "                    \"user_id\":\"iPhone10,2@41580\",\n" +
            "                    \"description\":\"开始拉流\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608695940,\n" +
            "                    \"end_time\":1608696332,\n" +
            "                    \"type\":1,\n" +
            "                    \"user_id\":\"iPhone10,2@41580\",\n" +
            "                    \"description\":\"登录房间\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"start_time\":1608695940,\n" +
            "                    \"end_time\":1608696332,\n" +
            "                    \"type\":1,\n" +
            "                    \"user_id\":\"iPhone10,2@41580\",\n" +
            "                    \"description\":\"开始拉流\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608696332,\n" +
            "                    \"end_time\":1608696332,\n" +
            "                    \"type\":1,\n" +
            "                    \"user_id\":\"iPhone10,2@41580\",\n" +
            "                    \"description\":\"退出房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608696504,\n" +
            "                    \"end_time\":1608696504,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"退出房间\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"start_time\":1608696504,\n" +
            "                    \"end_time\":1608696525,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"登录房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608696525,\n" +
            "                    \"end_time\":1608696525,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"退出房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608696530,\n" +
            "                    \"end_time\":1608705181,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"登录房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608696538,\n" +
            "                    \"end_time\":1608696826,\n" +
            "                    \"type\":1,\n" +
            "                    \"user_id\":\"iPhone10,2@41580\",\n" +
            "                    \"description\":\"登录房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608696826,\n" +
            "                    \"end_time\":1608696826,\n" +
            "                    \"type\":1,\n" +
            "                    \"user_id\":\"iPhone10,2@41580\",\n" +
            "                    \"description\":\"退出房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608705181,\n" +
            "                    \"end_time\":1608705181,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"退出房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608705201,\n" +
            "                    \"end_time\":1608705210,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"开始推流\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608705202,\n" +
            "                    \"end_time\":1608705211,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"登录房间\"\n" +
            "                }\n" +
            "            ],\n" +
            "            [\n" +
            "                {\n" +
            "                    \"start_time\":1608705211,\n" +
            "                    \"end_time\":1608705211,\n" +
            "                    \"type\":2,\n" +
            "                    \"user_id\":\"iPhone10,1@18182\",\n" +
            "                    \"description\":\"退出房间\"\n" +
            "                }\n" +
            "            ]\n" +
            "        ],\n" +
            "        \"timeline_times\":[\n" +
            "            1608695887,\n" +
            "            1608695888,\n" +
            "            1608695939,\n" +
            "            1608695940,\n" +
            "            1608696332,\n" +
            "            1608696504,\n" +
            "            1608696525,\n" +
            "            1608696530,\n" +
            "            1608696538,\n" +
            "            1608696826,\n" +
            "            1608705181,\n" +
            "            1608705201,\n" +
            "            1608705202,\n" +
            "            1608705211\n" +
            "        ],\n" +
            "        \"pull_1st_frm_span\":0,\n" +
            "        \"pull_1st_frm_span_avg\":0\n" +
            "    },\n" +
            "    \"RequestId\":\"c5d36b9f0a8ce8382d7dbb0bfa0c25d3\"\n" +
            "}";

    public static void main(String[] args) {

        //1.对象转换
        TestDataModel testDataModel = JSONObject.parseObject(str, TestDataModel.class);

        //2.处理数据
        MyData testData = testDataModel.getData();

        List<List<TimeLineEntity>> timeline = testData.getTimeline();
        //3.先处理时间，补点
        List<Long> timeline_times = testData.getTimeline_times();
        System.out.println("补之前：");
        for (int i = 0; i < timeline_times.size(); i++) {
            System.out.println(i + " : " + timeline_times.get(i));
        }
        //先计算有多少点
        long num = timeline_times.get(timeline_times.size() - 1) - timeline_times.get(0) + 1;
        long from = timeline_times.get(0);
        System.out.println("num:" + num);
        List<Long> list = Lists.newArrayList();
        for (int i = 0; i < num; i++) {
            list.add(i, from + i);
        }
        System.out.println("补之后：");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + " : " + list.get(i));
        }


        System.out.println(testDataModel);

    }
}

