package indi.mozping.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author moore.mo
 * @description cn.huolala.driver.subsidy MyData
 * @date 2020/12/23 7:44 下午
 **/
@Data
public class MyData {


    @JsonProperty("push_side_infos")
    Complex  push_side_infos;


    @JsonProperty("pull_side_infos")
    Complex  pull_side_infos;


    @JsonProperty("timeline")
    List<List<TimeLineEntity>> timeline;

    @JsonProperty("timeline_times")
    List<Long> timeline_times;


    int pull_1st_frm_span;

    int pull_1st_frm_span_avg;


}

