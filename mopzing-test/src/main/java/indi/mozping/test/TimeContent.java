package indi.mozping.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author moore.mo
 * @description cn.huolala.driver.subsidy.mytest Timecontent
 * @date 2020/12/23 8:04 下午
 **/
@Data
public class TimeContent {

    @JsonProperty("timeline")
    long time;

    @JsonProperty("content")
    String content;
}

