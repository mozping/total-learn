package indi.mozping.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author moore.mo
 * @description cn.huolala.driver.subsidy.mytest TimeLineEntity
 * @date 2020/12/23 7:46 下午
 **/
@Data
public class TimeLineEntity {

    @JsonProperty("start_time")
    long start_time;

    @JsonProperty("end_time")
    long end_time;

    @JsonProperty("type")
    int type;

    @JsonProperty("user_id")
    String user_id;

    @JsonProperty("description")
    String description;
}

