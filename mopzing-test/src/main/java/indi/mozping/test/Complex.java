package indi.mozping.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


/**
 * @author moore.mo
 * @description cn.huolala.driver.subsidy.mytest Complext
 * @date 2020/12/23 8:04 下午
 **/
@Data
public class Complex {


    @JsonProperty("app")
    List<TimeContent> app;

    @JsonProperty("push_user_id")
    List<TimeContent> push_user_id;

    @JsonProperty("env")
    List<TimeContent> env;

    @JsonProperty("sdk_version")
    List<TimeContent> sdk_version;

    @JsonProperty("os")
    List<TimeContent> os;

    @JsonProperty("region")
    List<TimeContent> region;

    @JsonProperty("network")
    List<TimeContent> network;

    @JsonProperty("start_time")
    List<TimeContent> start_time;

}

