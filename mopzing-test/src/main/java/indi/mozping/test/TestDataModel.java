package indi.mozping.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author moore.mo
 * @description cn.huolala.driver.subsidy TestDataModel
 * @date 2020/12/23 7:43 下午
 **/
@Data
public class TestDataModel {

    @JsonProperty("code")
    int code;

    @JsonProperty("message")
    String message;

    @JsonProperty("RequestId")
    String requestId;

    @JsonProperty("data")
    MyData data;
}

