package indi.mozping;

import indi.mozping.service.TestService;
import indi.mozping.service.TestServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

/**
 * @author by mozping
 * @Classname TestAutoConfig
 * @Description TODO
 * @Date 2019/8/8 15:16
 */
public class TestAutoConfig {

    @Bean
    @ConditionalOnMissingBean(TestService.class)
    public TestService demoService() {
        return new TestServiceImpl();
    }
}